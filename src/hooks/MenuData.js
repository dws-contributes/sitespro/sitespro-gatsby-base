import { useStaticQuery, graphql } from "gatsby"
import { useMemo } from "react"

export const useMenuData = () => {
  const { allMenuLinkContentMenuLinkContent } = useStaticQuery(
    graphql`
      query SiteMenuQuery {
        allMenuLinkContentMenuLinkContent(sort: { weight: ASC }) {
          edges {
            node {
              enabled
              title
              description
              expanded
              external
              langcode
              weight
              link {
                uri
                uri_alias
              }
              drupal_parent_menu_item
              bundle
              drupal_id
              menu_name
            }
          }
        }
      }
    `
  )
  return allMenuLinkContentMenuLinkContent.edges
}

export function CheckMenuChildren(drupalID) {
  const menuData = useMenuData()
  const match = useMemo(
    () =>
      menuData.find(({ node }) =>
        node.drupal_parent_menu_item?.includes(drupalID)
      ),
    [menuData, drupalID]
  )

  if (match && match.node) return true
  else return false
}

// Pass menu name (Ex. action) and return the number of enabled links in the menu
export function CountLinksInMenu(menuName) {
  const menuData = useMenuData()

  const count = menuData.reduce((acc, item) => {
    if (
      item.node &&
      item.node.menu_name === menuName &&
      item.node.enabled === true
    ) {
      return acc + 1
    }
    return acc
  }, 0)

  return count
}
