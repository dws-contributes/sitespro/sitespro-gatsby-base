import { useStaticQuery, graphql } from "gatsby"
import PropTypes from "prop-types"
import parseInternalUri from "../components/_utils/parseInternalUri"

const buildSiteData = (siteInfoNode, announcementNode, siteMetadata) => {
  const primaryCTA = siteInfoNode.field_site_primary_cta
    ? {
        uri:
          siteInfoNode.field_site_primary_cta.uri_alias ||
          siteInfoNode.field_site_primary_cta.uri,
        title: siteInfoNode.field_site_primary_cta.title,
      }
    : null

  const secondaryCTA = siteInfoNode.field_site_secondary_cta
    ? {
        uri:
          siteInfoNode.field_site_secondary_cta.uri_alias ||
          siteInfoNode.field_site_secondary_cta.uri,
        title: siteInfoNode.field_site_secondary_cta.title,
      }
    : null

  const announcement = announcementNode
    ? {
        body: announcementNode.body,
        priority: announcementNode.relationships?.field_priority?.name,
      }
    : null

  let listPages = {
    news: {
      url: siteInfoNode.field_site_news_page_url || "news",
      title: siteInfoNode.field_site_news_page_title || "News",
      activate: siteInfoNode.field_site_activate_news,
    },
    stories: {
      url: siteInfoNode.field_site_stories_page_url || "stories",
      title: siteInfoNode.field_site_stories_page_title || "Stories",
      activate: siteInfoNode.field_site_activate_stories,
    },
    blog: {
      url: siteInfoNode.field_site_blog_page_url || "blog",
      title: siteInfoNode.field_site_blog_page_title || "Blog",
      activate: siteInfoNode.field_site_activate_blog,
    },
    events: {
      url: siteInfoNode.field_site_events_page_url || "events",
      title: siteInfoNode.field_site_events_page_title || "Events",
      activate: siteInfoNode.field_site_activate_events,
    },
    "past-events": {
      url: siteInfoNode.field_site_past_events_page_url || "past-events",
      title: siteInfoNode.field_site_pastevents_page_title || "Past Events",
      activate: siteInfoNode.field_site_activate_past_events,
    },
    policies: {
      url: siteInfoNode.field_site_policies_page_url || "policies",
      title: siteInfoNode.field_site_policies_page_title || "Policies",
      activate: siteInfoNode.field_site_activate_policies,
    },
    projects: {
      url: siteInfoNode.field_site_projects_page_url || "projects",
      title: siteInfoNode.field_site_projects_page_title || "Projects",
      activate: siteInfoNode.field_site_activate_projects,
    },
    faculty: {
      url: siteInfoNode.field_site_faculty_page_url || "faculty",
      title: siteInfoNode.field_site_faculty_page_title || "Faculty",
      activate: siteInfoNode.field_site_activate_faculty,
    },
    resources: {
      url: siteInfoNode.site_resources_page_url || "resources",
      title: siteInfoNode.site_resources_page_title || "Resources",
      activate: siteInfoNode.field_site_activate_resources,
    },
    staff: {
      url: siteInfoNode.field_site_staff_page_url || "staff",
      title: siteInfoNode.field_site_staff_page_title || "Staff",
      activate: siteInfoNode.field_site_activate_staff,
    },
    faqs: {
      url: siteInfoNode.field_site_faqs_page_url || "faqs",
      title: siteInfoNode.field_site_faqs_page_title || "FAQs",
      activate: siteInfoNode.field_site_activate_faqs,
    },
    students: {
      url: siteInfoNode.field_site_students_page_url || "students",
      title: siteInfoNode.field_site_students_page_title || "Students",
      activate: siteInfoNode.field_site_activate_students,
    },
  }

  return {
    name: siteInfoNode.field_site_name,
    prefix: siteInfoNode.field_site_name_prefix,
    additionalInfo: siteInfoNode.field_site_additional_info,
    addressLines: siteInfoNode.field_site_address || [],
    phoneNumber: siteInfoNode.field_site_phone_number,
    email: siteInfoNode.field_site_email,
    facebookUri: parseInternalUri(siteInfoNode.field_site_facebook?.uri),
    instagramUri: parseInternalUri(siteInfoNode.field_site_instagram?.uri),
    linkedinUri: parseInternalUri(siteInfoNode.field_site_linkedin?.uri),
    twitterUri: parseInternalUri(siteInfoNode.field_site_twitter?.uri),
    youtubeUri: parseInternalUri(siteInfoNode.field_site_youtube?.uri),
    primaryCTA: primaryCTA,
    secondaryCTA: secondaryCTA,
    ctaDescription: siteInfoNode.field_site_cta_description,
    ctaHeading: siteInfoNode.field_site_cta_heading,
    announcement: announcement,
    site_url: siteMetadata.siteUrl,
    drupal_url: siteMetadata.drupalBaseUrl,
    cdn_url: siteMetadata.cdnUrl,
    listPages: listPages,
    activateBrandBar: siteInfoNode.field_site_activate_brand_bar,
  }
}

export const announcementShape = {
  body: PropTypes.string,
  priority: PropTypes.string,
}

export const siteDataShape = {
  name: PropTypes.string,
  prefix: PropTypes.string,
  additionalInfo: PropTypes.string,
  addressLines: PropTypes.arrayOf(PropTypes.string),
  phoneNumber: PropTypes.string,
  email: PropTypes.string,
  facebookUri: PropTypes.string,
  instagramUri: PropTypes.string,
  linkedinUri: PropTypes.string,
  twitterUri: PropTypes.string,
  youtubeUri: PropTypes.string,
  primaryCTA: PropTypes.exact({
    uri: PropTypes.string,
    title: PropTypes.string,
  }),
  secondaryCTA: PropTypes.exact({
    uri: PropTypes.string,
    title: PropTypes.string,
  }),
  ctaDescription: PropTypes.string,
  ctaHeading: PropTypes.string,
  announcement: PropTypes.exact(announcementShape),
  site_url: PropTypes.string,
  drupal_url: PropTypes.string,
  cdn_url: PropTypes.string,
  listPages: PropTypes.object,
  activateBrandBar: PropTypes.bool,
}

export const useSiteInfoData = () => {
  const { allBlockContentSiteInformation, allNodeAnnouncement, site } =
    useStaticQuery(
      graphql`
        query {
          allBlockContentSiteInformation(limit: 1, sort: { drupal_id: ASC }) {
            nodes {
              field_site_name
              field_site_name_prefix
              field_site_additional_info
              field_site_address
              field_site_phone_number
              field_site_email
              field_site_activate_brand_bar
              field_site_facebook {
                uri
              }
              field_site_instagram {
                uri
              }
              field_site_linkedin {
                uri
              }
              field_site_twitter {
                uri
              }
              field_site_youtube {
                uri
              }
              field_site_primary_cta {
                uri_alias
                uri
                title
              }
              field_site_secondary_cta {
                uri_alias
                uri
                title
              }
              field_site_cta_description
              field_site_cta_heading
              field_site_news_page_url
              field_site_blog_page_url
              field_site_events_page_url
              field_site_past_events_page_url
              field_site_projects_page_url
              field_site_stories_page_url
              field_site_faculty_page_url
              field_site_staff_page_url
              field_site_students_page_url
              field_site_news_page_title
              field_site_blog_page_title
              field_site_events_page_title
              field_site_pastevents_page_title
              field_site_projects_page_title
              field_site_stories_page_title
              field_site_faculty_page_title
              field_site_staff_page_title
              field_site_students_page_title
              field_site_activate_news
              field_site_activate_blog
              field_site_activate_events
              field_site_activate_past_events
              field_site_activate_projects
              field_site_activate_resources
              field_site_activate_stories
              field_site_activate_faculty
              field_site_activate_staff
              field_site_activate_faqs
              field_site_activate_students
              site_resources_page_url
              site_resources_page_title
            }
          }
          allNodeAnnouncement(
            limit: 1
            filter: { status: { eq: true } }
            sort: { changed: DESC }
          ) {
            nodes {
              body
              relationships {
                field_priority {
                  name
                }
              }
            }
          }
          site {
            siteMetadata {
              title
              description
              author
              siteUrl
              cdnUrl
              drupalBaseUrl
            }
          }
        }
      `
    )

  const siteInfoNode =
    allBlockContentSiteInformation.nodes.length > 0
      ? allBlockContentSiteInformation.nodes[0]
      : {}

  const announcementNode =
    allNodeAnnouncement.nodes.length > 0 ? allNodeAnnouncement.nodes[0] : null

  const siteMetadata = site.siteMetadata

  return buildSiteData(siteInfoNode, announcementNode, siteMetadata)
}
