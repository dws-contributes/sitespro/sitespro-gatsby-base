import React from "react"
import LayoutResource from "../components/Organisms/Layout/LayoutResource"
import Resource from "../components/Templates/Resource/Resource"
import { imageDetails } from "../components/Atoms/Image/BB8Image"
import { graphql } from "gatsby"

const buildResource = resourceNode => {
  const relationships = resourceNode.relationships || {}
  let categories = resourceNode?.relationships?.field_categories.map(
    category => category.name
  )
  return {
    title: resourceNode?.title,
    description: resourceNode?.body?.processed,
    summary: resourceNode?.body?.summary,
    thumbnailImageDetails: imageDetails(relationships?.field_featured_media),
    categories: categories,
    lastModified: resourceNode?.field_last_modified_date,
    owners: resourceNode?.field_owners,
  }
}

const ResourceTemplate = ({ data, pageContext }) => {
  const { fields = {} } = data
  const n = buildResource(fields)
  return (
    <LayoutResource
      title={n.title}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <Resource {...n} />
    </LayoutResource>
  )
}

export default ResourceTemplate
export { Head } from "components/Organisms/Layout/Head"

export const ResourceQuery = graphql`
  query ($ID: String!) {
    fields: nodeResource(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      body {
        summary
        processed
      }
      field_last_modified_date(formatString: "M/D/YY")
      field_alternate_link {
        uri
        title
      }
      field_owners
      relationships {
        field_categories {
          name
        }
        field_featured_media {
          ...ImageDetails
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href          
        }
      }
    }
  }
`
