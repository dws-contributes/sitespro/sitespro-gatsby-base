/**
 * Faqs landing page, displays Faq teasers with pagination
 */
import React, { useContext, useEffect } from "react"
import { useLocation } from "@reach/router"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import { Accordion, AccordionContext, Card } from "react-bootstrap"
import { useAccordionButton } from "react-bootstrap/AccordionButton"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import decode from "components/_utils/decode"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildFaqCard = federatedFaq => {
  return {
    id: federatedFaq.id,
    question: federatedFaq.title,
    answer: federatedFaq.overview,
    urlAnchor: federatedFaq.url?.replace(/\//g, "-").substring(1),
    url: federatedFaq.url,
  }
}

const FaqPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "FAQs"

  const perPage = 5000 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["FAQ"],
    },
    fixedSort: "ss_federated_title_sort_string asc",
  })

  const urlParams = new URLSearchParams(useLocation().search)
  const urlFAQParam = urlParams.get("f")

  useEffect(() => {
    const urlFAQAnchor = urlFAQParam?.replace(/\//g, "-").substring(1)
    const timer = setTimeout(() => {
      let scrollToFAQElement = document.getElementById(urlFAQAnchor)
      scrollToFAQElement &&
        scrollToFAQElement.scrollIntoView({
          behavior: "smooth",
          block: "start",
        })
    }, 1000)
    return () => clearTimeout(timer)
  })

  function CustomToggle({ children, eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext)

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    )

    const isCurrentEventKey = activeEventKey === eventKey

    return (
      <button
        type="button"
        className={`accordion-toggle ${isCurrentEventKey ? `current` : ``}`}
        aria-expanded={isCurrentEventKey ? true : false}
        tabIndex="0"
        onClick={decoratedOnClick}
      >
        {children}
      </button>
    )
  }

  return (
    <LayoutBase
      pageTitle={pageTitle}
      metaTags={{
        title: pageTitle,
        description: pageTitle,
      }}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            excludeFacetGroups={[]}
            nothingFoundMessage="There are currently no FAQs."
            render={results => {
              const faqCardsProperties = results.map(e => buildFaqCard(e))
              return (
                <div className="faq-card-wrapper">
                  {faqCardsProperties.map(
                    ({ id, question, answer, urlAnchor, url }) => (
                      <Accordion
                        key={`faq-${id}`}
                        defaultActiveKey={
                          url === urlFAQParam ? `faq-${id}` : ""
                        }
                        id={urlAnchor}
                      >
                        <Card>
                          <CustomToggle eventKey={`faq-${id}`}>
                            <span className="accordion-icon"></span>
                            <span className="mt-0 mb-0">
                              {decode(question)}
                            </span>
                          </CustomToggle>
                          <Accordion.Collapse eventKey={`faq-${id}`}>
                            <Card.Body>
                              <CKEditorContent content={decode(answer)} />
                            </Card.Body>
                          </Accordion.Collapse>
                        </Card>
                      </Accordion>
                    )
                  )}
                </div>
              )
            }}
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default FaqPage
export { Head } from "components/Organisms/Layout/Head"
