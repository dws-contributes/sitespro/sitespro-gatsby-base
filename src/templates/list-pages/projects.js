/**
 * Stories landing page, displays Project teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import ProjectTeaser from "components/Templates/Project/ProjectTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildProjectTeaser = federatedProject => {
  return {
    id: federatedProject.id,
    title: federatedProject.title,
    timeframe: federatedProject.subtitle,
    link: federatedProject.url,
    image: federatedProject.image,
    summary: federatedProject.overview,
    imageAlt: federatedProject.image_alt,
  }
}

const ProjectPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Projects"

  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Project"],
    },
    fixedSort: "ss_federated_title_sort_string asc",
  })

  return (
    <LayoutBase
      pageTitle={pageTitle}
      metaTags={{
        title: pageTitle,
        description: pageTitle,
      }}
      showStatusInfo={false}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results =>
              results.map(buildProjectTeaser).map(r => (
                <li>
                  <ProjectTeaser key={r.id} {...r} />
                </li>
              ))
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default ProjectPage
export { Head } from "components/Organisms/Layout/Head"
