/**
 * Events landing page, displays Event teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import EventCard from "components/Molecules/EventCard/EventCard"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildEventTeaser = federatedEvent => {
  const statusTerm = federatedEvent.terms?.filter(t =>
    t.startsWith("Event Status")
  )[0]
  const status = statusTerm ? statusTerm.split(">")[1] : null
  return {
    id: federatedEvent.id,
    title: federatedEvent.title_override || federatedEvent.title,
    date: federatedEvent.date,
    link: federatedEvent.url,
    locationUrl: federatedEvent.location_url,
    locationText: federatedEvent.location,
    status: status,
  }
}

const EventPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Events"

  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    facetRanges: [
      {
        field: "ds_federated_date",
        start: "NOW/MONTH",
        end: "NOW/MONTH+10YEAR",
        gap: "+1MONTH",
      },
    ],
    fixedFieldFilters: {
      ss_federated_type: ["Event"],
    },
    fixedRangeFilters: {
      ds_federated_date: [{ from: "NOW/DAY", to: "*" }],
    },
    fixedSort: "ds_federated_date asc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            excludeFacetGroups={["Event Status"]}
            nothingFoundMessage="There are currently no scheduled events. Please check back soon!"
            render={results =>
              results.map(buildEventTeaser).map(r => (
                <li>
                  <EventCard key={r.id} modifiers={["teaser"]} {...r} />
                </li>
              ))
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default EventPage
export { Head } from "components/Organisms/Layout/Head"
