/**
 * Staff landing page, displays Staff teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import ProfileTeaser from "components/Templates/Profile/ProfileTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container, Row } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildStaffTeaser = staffNode => {
  let body = staffNode.overview ? staffNode.overview : ""

  return {
    id: staffNode.id,
    link: staffNode.url,
    title: staffNode.title,
    subtitle: staffNode.subtitle,
    image: staffNode.image,
    imageAlt: null,
    body: body,
  }
}

const StaffPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Staff"

  const perPage = 21 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Profile"],
      "-ss_federated_source": ["1"],
    },
    fixedSort: "ss_federated_title_sort_string asc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results => {
              return (
                <Row>
                  {results.map(buildStaffTeaser).map(r => (
                    <li className="col-lg-4 col-md-6 col-sm-12">
                      <ProfileTeaser displayBio={true} key={r.id} {...r} />
                    </li>
                  ))}
                </Row>
              )
            }}
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default StaffPage
export { Head } from "components/Organisms/Layout/Head"
