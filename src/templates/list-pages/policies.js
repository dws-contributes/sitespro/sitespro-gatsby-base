/**
 * Stories landing page, displays Policy teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import PolicyTeaser from "components/Templates/Policy/PolicyTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildPolicyTeaser = federatedPolicy => {
  return {
    id: federatedPolicy.id,
    title: federatedPolicy.title,
    link: federatedPolicy.url,
    summary: federatedPolicy.overview,
  }
}

const PolicyPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Policies"

  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Policy"],
    },
    fixedSort: "ss_federated_title_sort_string asc",
  })

  return (
    <LayoutBase
      pageTitle={pageTitle}
      metaTags={{
        title: pageTitle,
        description: pageTitle,
      }}
      showStatusInfo={false}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results =>
              results.map(buildPolicyTeaser).map(r => (
                <li key={r.id}>
                  <PolicyTeaser {...r} />
                </li>
              ))
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default PolicyPage
export { Head } from "components/Organisms/Layout/Head"
