/**
 * Stories landing page, displays Story teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import StoryTeaser from "components/Templates/Story/StoryTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildStoryTeaser = federatedStory => {
  return {
    id: federatedStory.id,
    title: federatedStory.title,
    date: federatedStory.date,
    link: federatedStory.url,
    image: federatedStory.image,
    summary: federatedStory.overview,
    imageAlt: federatedStory.image_alt,
    authors: federatedStory.author,
  }
}

const StoryPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Stories"

  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    facetRanges: [
      {
        field: "ds_federated_date",
        start: "NOW/MONTH-10YEAR",
        end: "NOW/MONTH+10YEAR",
        gap: "+1MONTH",
      },
    ],
    fixedFieldFilters: {
      ss_federated_type: ["Story"],
    },
    fixedSort: "ds_federated_date desc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results =>
              results
                .map(buildStoryTeaser)
                .map(r => <li><StoryTeaser key={r.id} {...r} /></li>)
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default StoryPage
export { Head } from "components/Organisms/Layout/Head"
