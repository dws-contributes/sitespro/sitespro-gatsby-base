/**
 * Student landing page, displays Student teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import ScholarsProfileTeaser from "components/Templates/ScholarsProfile/ScholarsProfileTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container, Row } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildStudentTeaser = studentNode => {
  let summary = studentNode.title_override ? studentNode.title_override : ""

  return {
    id: studentNode.id,
    link: studentNode.url,
    title: studentNode.title,
    subtitle: studentNode.subtitle,
    image: studentNode.image,
    imageAlt: null,
    summary: summary,
  }
}

const StudentPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Students"

  const perPage = 21 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Scholars@Duke Profile"],
      ss_federated_subtitle: ["Student"],
    },
    fixedSort: "ss_federated_title_sort_string asc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results => {
              return (
                <Row>
                  {results.map(buildStudentTeaser).map(r => (
                    <li className="col-lg-4 col-md-6 col-sm-12">
                      <ScholarsProfileTeaser
                        displayScholarsSummary={true}
                        key={r.id}
                        {...r}
                      />
                    </li>
                  ))}
                </Row>
              )
            }}
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default StudentPage
export { Head } from "components/Organisms/Layout/Head"
