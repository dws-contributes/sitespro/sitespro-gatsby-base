/**
 * Blog landing page, displays Blog teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import BlogTeaser from "components/Templates/BlogPost/BlogPostTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildBlogTeaser = blogNode => {
  return {
    id: blogNode.id,
    date: blogNode.date,
    link: blogNode.url,
    title: blogNode.title,
    summary: blogNode.overview,
    image: blogNode.image,
    imageAlt: blogNode.image_alt,
    authors: blogNode.author,
  }
}

const BlogPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Blog"
  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Blog Post"],
    },
    facetRanges: [
      {
        field: "ds_federated_date",
        start: "NOW/MONTH-10YEAR",
        end: "NOW/MONTH+10YEAR",
        gap: "+1MONTH",
      },
    ],
    fixedSort: "ds_federated_date desc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results =>
              results.map(buildBlogTeaser).map(r => (
                <li>
                  <BlogTeaser key={r.id} {...r} />
                </li>
              ))
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default BlogPage
export { Head } from "components/Organisms/Layout/Head"
