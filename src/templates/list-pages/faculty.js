/**
 * Faculty landing page, displays Faculty teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"

import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import ScholarsProfileTeaser from "components/Templates/ScholarsProfile/ScholarsProfileTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container, Row } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildFacultyTeaser = facultyNode => {
  let summary = facultyNode.title_override ? facultyNode.title_override : ""

  return {
    id: facultyNode.id,
    link: facultyNode.url,
    title: facultyNode.title,
    subtitle: facultyNode.subtitle,
    image: facultyNode.image,
    imageAlt: null,
    summary: summary,
  }
}

const FacultyPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Faculty"
  const perPage = 21 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Scholars@Duke Profile"],
      "-ss_federated_subtitle": ["Student"],
      "-sm_federated_terms": [
        "Scholars@Duke VivoTypes>http://vivoweb.org/ontology/core#NonFacultyAcademic",
      ],
    },
    fixedSort: "ss_federated_title_sort_string asc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            perPage={perPage}
            fixedType={true}
            fixedSort={true}
            solrQuery={solrQuery}
            render={results => {
              return (
                <Row>
                  {results.map(buildFacultyTeaser).map(r => (
                    <li className="profile-teaser col-lg-4 col-md-6">
                      <ScholarsProfileTeaser
                        displayScholarsSummary={true}
                        key={r.id}
                        {...r}
                      />
                    </li>
                  ))}
                </Row>
              )
            }}
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default FacultyPage
export { Head } from "components/Organisms/Layout/Head"
