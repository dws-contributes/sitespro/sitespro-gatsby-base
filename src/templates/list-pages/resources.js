/**
 * Resources landing page, displays Resources teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import ResourceTeaser from "components/Templates/Resource/ResourceTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildResourceTeaser = resourceNode => {
  let initialValue = ""
  const categories = resourceNode.terms?.reduce(
    (previousValue, currentValue) =>
      (previousValue ? previousValue + ", " : "") + currentValue.split(">")[1],
    initialValue
  )
  return {
    id: resourceNode.id,
    date: resourceNode.date,
    link: resourceNode.url,
    title: resourceNode.title,
    summary: resourceNode.overview,
    image: resourceNode.image,
    imageAlt: resourceNode.image_alt,
    categories: categories,
  }
}

const ResourcesPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "Resources"

  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["Resource"],
    },
    facetRanges: [
      {
        field: "ds_federated_date",
        start: "NOW/MONTH-10YEAR",
        end: "NOW/MONTH+10YEAR",
        gap: "+1MONTH",
      },
    ],
    fixedSort: "ss_federated_title_sort_string asc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results =>
              results.map(buildResourceTeaser).map(r => (
                <li>
                  <ResourceTeaser key={r.id} {...r} />
                </li>
              ))
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default ResourcesPage
export { Head } from "components/Organisms/Layout/Head"
