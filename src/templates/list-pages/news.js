/**
 * News landing page, displays News teasers with pagination
 */
import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import NewsTeaser from "components/Templates/News/NewsTeaser"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { Container } from "react-bootstrap"
import { SearchInterface, createSolrQuery } from "components/Organisms/Search"

const buildNewsTeaser = newsNode => {
  return {
    id: newsNode.id,
    date: newsNode.date,
    link: newsNode.url,
    title: newsNode.title,
    summary: newsNode.overview,
    source: newsNode.source,
    image: newsNode.image,
    imageAlt: newsNode.image_alt,
  }
}

const NewsPage = ({ location, navigate, pageContext }) => {
  const pageTitle = pageContext.title ? pageContext.title : "News"

  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["sm_federated_terms"],
    fixedFieldFilters: {
      ss_federated_type: ["News"],
    },
    facetRanges: [
      {
        field: "ds_federated_date",
        start: "NOW/MONTH-10YEAR",
        end: "NOW/MONTH+10YEAR",
        gap: "+1MONTH",
      },
    ],
    fixedSort: "ds_federated_date desc",
  })

  return (
    <LayoutBase pageTitle={pageTitle} isListPage={true} showStatusInfo={false}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <SearchInterface
            location={location}
            navigate={navigate}
            fixedType={true}
            fixedSort={true}
            perPage={perPage}
            solrQuery={solrQuery}
            render={results =>
              results
                .map(buildNewsTeaser)
                .map(r => <li key={r.id}><NewsTeaser {...r} /></li>)
            }
          />
        </Container>
      </main>
    </LayoutBase>
  )
}

export default NewsPage
export { Head } from "components/Organisms/Layout/Head"