import { CheckMenuChildren } from "hooks/MenuData"
import LayoutPage from "components/Organisms/Layout/LayoutPage"
import PropTypes from "prop-types"
import React from "react"
import { getMetaDetails } from "components/Atoms/Seo/Seo"
import { getParagraph } from "components/Molecules/Paragraphs/ParagraphsHelper.js"
/**
 *  Renders Page content type template
 */
import { graphql } from "gatsby"

const PageTemplate = ({ data, pageContext, location }) => {
  const hero = data.fields?.relationships?.hero
    ? getParagraph(data.fields.relationships.hero)
    : null
  const paragraphs = data.fields.relationships.paragraphs?.map(getParagraph)

  // check if this page is in menu and has menu children
  const pageHasMenuParent = Boolean(data.menuLink?.drupal_parent_menu_item)
  const pageHasMenuEnabled = Boolean(data.menuLink?.enabled)
  const pageHasMenuChildren = CheckMenuChildren(data.menuLink?.drupal_id)
  const pageHasSidemenu =
    pageHasMenuEnabled && (pageHasMenuParent || pageHasMenuChildren)

  return (
    <LayoutPage
      hasSidebar={pageHasSidemenu}
      location={location}
      title={data.fields.title}
      hero={hero}
      metatags={getMetaDetails(data.fields.metatag_normalized)}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      {paragraphs}
    </LayoutPage>
  )
}

PageTemplate.propTypes = {
  children: PropTypes.node,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default PageTemplate
export { Head } from "../components/Organisms/Layout/Head"

export const pageQuery = graphql`
  query ($ID: String!, $uri: String!) {
    fields: nodePage(id: { eq: $ID }) {
      id
      title
      field_shibboleth_access
      relationships {
        field_thumbnail_media {
          ...ImageDetails
        }
        hero: field_hero {
          type: __typename
          ...ParagraphCarousel
          ...ParagraphHeroImage
          ...ParagraphHeroImageText
          ...ParagraphHeroVideoText
          ...ParagraphHeroFullWidthVideo
        }
        paragraphs: field_content_blocks {
          type: __typename
          ...ParagraphAccordionSection
          ...ParagraphBlogList
          ...ParagraphButton
          ...ParagraphCallout
          ...ParagraphContent
          ...ParagraphContentReference
          ...ParagraphEventList
          ...ParagraphFaqsList
          ...ParagraphIconRow
          ...ParagraphIframe
          ...ParagraphImageCardRow
          ...ParagraphImageText
          ...ParagraphMedia
          ...ParagraphNewsList
          ...ParagraphPageList
          ...ParagraphPersonList
          ...ParagraphPolicyList
          ...ParagraphProjectList
          ...ParagraphPublicationList
          ...ParagraphPullQuote
          ...ParagraphResourceList
          ...ParagraphScholarsList
          ...ParagraphSlideshow
          ...ParagraphSocialMedia
          ...ParagraphSpacer
          ...ParagraphSpecialText
          ...ParagraphStoryList
          ...ParagraphTwoColumn
          ...ParagraphThreeColumn
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
    menuLink: menuLinkContentMenuLinkContent(
      link: { uri_alias: { eq: $uri } }
    ) {
      drupal_id
      menu_name
      drupal_parent_menu_item
      enabled
    }
  }
`
