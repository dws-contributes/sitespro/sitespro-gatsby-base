import LayoutProfile from "../components/Organisms/Layout/LayoutProfile"
import { MapRelatedItems } from "../components/Templates/Profile/ProfileMappedRelatedContent"
import React from "react"
import ScholarsProfile from "../components/Templates/ScholarsProfile/ScholarsProfile"
import { graphql } from "gatsby"
import { GetCustomParents } from "components/Molecules/Breadcrumbs/Breadcrumbs"

const ScholarsTemplate = ({ data, pageContext }) => {
  let { positionPreferredTitle, roles } = processRoles(
    data?.fields?.relationships?.duke_scholars_profile_positions
  )

  let relatedContent = MapRelatedItems(
    data?.fields?.relationships?.relatedContent
  )

  const customParents =
    data?.fields?.duke_scholars_profile_preferred_title === "Student"
      ? "students"
      : "faculty"

  return (
    <LayoutProfile
      title={data.fields.title}
      customParents={GetCustomParents(customParents)}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
      editPath="admin/content/duke-scholars-profile"
    >
      <ScholarsProfile
        roles={roles}
        preferred_title={
          data?.fields?.duke_scholars_profile_preferred_title ||
          positionPreferredTitle
        }
        thumbnail_photo={
          data?.fields?.duke_scholars_profile_thumbnail?.thumbnail
        }
        portrait_photo={data?.fields?.duke_scholars_profile_thumbnail?.portrait}
        alt_text={data?.fields?.duke_scholars_profile_thumbnail?.alt_text}
        additional_information={
          data?.fields?.duke_scholars_profile_additional_information?.processed
        }
        email_address={data?.fields?.duke_scholars_profile_email}
        phone_number={data?.fields?.duke_scholars_profile_phone}
        address_1={data?.fields?.duke_scholars_profile_address_1}
        address_2={data?.fields?.duke_scholars_profile_address_2}
        scholarsUri={data?.fields?.duke_scholars_uri}
        related_content={relatedContent}
      />
    </LayoutProfile>
  )
}

export default ScholarsTemplate
export { Head } from "components/Organisms/Layout/Head"

function processRoles(rolesData) {
  let roles = []
  let preferredTitle = ""

  rolesData.map(function (role) {
    if (role?.position_preferred) {
      preferredTitle = role.title
    } else {
      roles.push(role.title)
    }
    return true
  })

  return {
    roles: roles,
    preferredTitle: preferredTitle,
  }
}

export const scholarsProfileQuery = graphql`
  query ($ID: String!) {
    fields: dukeScholarsProfileDukeScholarsProfile(id: { eq: $ID }) {
      title
      path {
        alias
      }
      duke_scholars_uri
      duke_scholars_profile_name_last
      duke_scholars_profile_name_first
      duke_scholars_profile_preferred_title
      field_summary
      duke_scholars_profile_additional_information {
        processed
      }
      duke_scholars_profile_thumbnail {
        portrait
        alt_text
        thumbnail
      }
      duke_scholars_profile_email
      duke_scholars_profile_phone
      duke_scholars_profile_address_1
      duke_scholars_profile_address_2
      relationships {
        duke_scholars_profile_positions {
          title
          position_preferred
        }
        relatedContent: field_related_content {
          type: __typename
          ...NodeBlogPost
          ...NodeEvent
          ...NodeNews
          ...NodePage
          ...NodeStory
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
  }
`
