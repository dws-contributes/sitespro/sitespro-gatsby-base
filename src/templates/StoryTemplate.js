import Story, { storyDataNormalizer } from "../components/Templates/Story/Story"
import LayoutStory from "../components/Organisms/Layout/LayoutStory"
import React from "react"
import RelatedContent from "../components/Molecules/RelatedContent/RelatedContent"
import { imageDetails } from "../components/Atoms/Image/BB8Image"
import { graphql } from "gatsby"

const buildRelatedContent = relatedStories => {
  let relatedContent = []
  for (const node of relatedStories) {
    relatedContent.push({
      id: node.id,
      title: node.title,
      summary: node.field_summary,
      date: node.field_date,
      path: node.path?.alias,
      imageDetails: imageDetails(node.relationships.field_featured_media),
      feedImageUrl: null,
      feedImageAlt: null,
    })
  }
  return relatedContent
}

const StoryTemplate = ({ data, pageContext }) => {
  // Authors for the header of the Story (names linked to profile or URL/email)
  // const authors = data.story.relationships.authors.map(getParagraph)

  // Extended Authors info for the footer of the Story (names linked to profile or URL/email, profile photo, short bio (summary))
  // const authorsFull = data.story.relationships.field_author_reference.map((author, index) => {
  //   return author.type === "paragraph__existing_profile" ? (
  //     <Author node={author} key={index} />
  // })
  const { fields = {}, relatedStories = {} } = data
  const n = storyDataNormalizer(fields)
  const relatedContent = buildRelatedContent(relatedStories?.nodes)

  return (
    <LayoutStory
      title={n.title}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <Story {...n} />
      <div className="story-related-wrapper">
        {relatedStories?.nodes?.length > 0 && (
          <RelatedContent relatedContent={relatedContent} heading="Related" />
        )}
      </div>
    </LayoutStory>
  )
}

export default StoryTemplate
export { Head } from "components/Organisms/Layout/Head"

export const pageQuery = graphql`
  query ($ID: String!, $categories: [String]) {
    fields: nodeStory(id: { eq: $ID }) {
      id
      title
      subtitle: field_subtitle
      date: field_date(formatString: "MMMM D, YYYY")
      summary: field_summary
      relationships {
        field_author_reference {
          ...AuthorFullDetails
        }
        field_featured_media {
          ...ImageDetails
        }
        paragraphs: field_content_blocks {
          type: __typename
          ...ParagraphAccordionSection
          ...ParagraphButton
          ...ParagraphCallout
          ...ParagraphContent
          ...ParagraphIconRow
          ...ParagraphIframe
          ...ParagraphImageCardRow
          ...ParagraphImageText
          ...ParagraphMedia
          ...ParagraphPullQuote
          ...ParagraphSlideshow
          ...ParagraphSocialMedia
          ...ParagraphSpacer
          ...ParagraphSpecialText
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
    relatedStories: allNodeStory(
      filter: {
        relationships: {
          field_categories: { elemMatch: { name: { in: $categories } } }
        }
        id: { ne: $ID }
      }
      limit: 3
      sort: { field_date: DESC }
    ) {
      nodes {
        id
        title
        path {
          alias
        }
        field_summary
        field_date(formatString: "YYYY-MM-DDT08:mm:ssZ")
        relationships {
          field_featured_media {
            ...ImageDetails
          }
        }
      }
    }
  }
`
