import Event from "../components/Templates/Event/Event"
import LayoutEvent from "../components/Organisms/Layout/LayoutEvent"
import React from "react"
import { graphql } from "gatsby"
import { imageDetails } from "../components/Atoms/Image/BB8Image"

const buildEvent = eventNode => {
  const relationships = eventNode.relationships || {}

  return {
    title: eventNode.field_title_override || eventNode.title,
    imageDetails: imageDetails(relationships?.field_featured_media),
    startAt: eventNode.date?.start,
    endAt: eventNode.date?.end,
    locationText: eventNode.field_location_text,
    locationUrl: eventNode.field_location_link?.uri,
    presenter: eventNode.field_presenter_speaker,
    cost: eventNode.field_cost,
    summary: eventNode.body?.summary,
    body: eventNode.body?.processed,
    additionalLocationInfo:
      eventNode.field_additional_location_inform?.processed,
    additionalInfo: eventNode.field_additional_info?.processed,
    additionalLinks: eventNode.field_additional_links || [],
    contactName: eventNode.field_contact_name,
    contactEmail: eventNode.field_contact_email,
    contactPhone: eventNode.field_contact_phone,
    moreEventInfoUrl: eventNode.field_more_event_info?.uri,
    categories: relationships.field_categories?.map(c => c.name) || [],
    categories_site_specific:
      relationships.field_categories_site_specific?.map(c => c.name) || [],
    series: relationships.field_series?.map(r => r.name) || [],
    sponsor: relationships.field_sponsors?.name,
    cosponsors: relationships.field_co_sponsors?.map(cs => cs.name) || [],
    webcastUrl: eventNode.field_webcast_url?.uri,
    feedImageUrl: eventNode.field_feed_image_url?.uri,
    feedImageAltText: eventNode.field_feed_image_alt_text,
    status: relationships.field_status?.name,
  }
}

const EventTemplate = ({ data, pageContext }) => {
  const { fields = {} } = data
  const e = buildEvent(fields)

  // Determine if the event end date has past, so that we can add the correct breadcrumb (to past-events) in LayoutEvent
  const dateToday = new Date()
  const eventEndDate = new Date(e.endAt)

  return (
    <LayoutEvent
      title={e.title}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
      pastEvent={dateToday > eventEndDate}
    >
      <Event {...e} />
    </LayoutEvent>
  )
}

export default EventTemplate
export { Head } from "components/Organisms/Layout/Head"

export const eventQuery = graphql`
  query ($ID: String!) {
    fields: nodeEvent(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      field_title_override
      field_alternate_link {
        uri
      }
      date: field_event_date {
        start: value
        end: end_value
      }
      field_location_text
      field_location_link {
        uri
      }
      field_presenter_speaker
      field_cost
      body {
        summary
        processed
      }
      field_additional_links {
        uri
        title
      }
      field_additional_location_inform {
        processed
      }
      field_additional_info {
        processed
      }
      field_contact_name
      field_contact_email
      field_contact_phone
      field_more_event_info {
        uri
        title
      }
      field_webcast_url {
        uri
      }
      field_feed_image_url {
        uri
      }
      field_feed_image_alt_text
      relationships {
        field_featured_media {
          ...ImageDetails
        }
        field_categories {
          name
          id
          path {
            alias
          }
        }
        field_categories_site_specific {
          name
          id
          path {
            alias
          }
        }
        field_series {
          name
          id
          path {
            alias
          }
        }
        field_sponsors {
          name
          id
          path {
            alias
          }
        }
        field_co_sponsors {
          name
          id
          path {
            alias
          }
        }
        field_status {
          id
          name
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
  }
`
