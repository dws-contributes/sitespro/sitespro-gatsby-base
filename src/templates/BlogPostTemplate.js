import BlogPost from "../components/Templates/BlogPost/BlogPost"
import LayoutBlogPost from "../components/Organisms/Layout/LayoutBlogPost"
import React from "react"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"
import { graphql } from "gatsby"
import { imageDetails } from "../components/Atoms/Image/BB8Image"

const BlogPostTemplate = ({ data, pageContext }) => {
  let {
    title,
    description,
    summary,
    slideShow,
    date,
    imageDetails,
    categories,
    authors,
  } = blogPostDataNormalizer(data?.fields)

  return (
    <LayoutBlogPost
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <BlogPost
        title={title}
        description={description}
        summary={summary}
        date={date}
        imageDetails={imageDetails}
        categories={categories}
        authors={authors}
        slideShow={slideShow}
      />
    </LayoutBlogPost>
  )
}

export default BlogPostTemplate
export { Head } from "components/Organisms/Layout/Head"

const blogPostDataNormalizer = data => {
  let categories = data?.relationships?.field_categories.map(category => ({
    title: category?.name,
    path: category?.path?.alias,
  }))

  let authors = data?.relationships?.field_author_reference.map(author => ({
    title: author?.title,
    path: author?.path?.alias,
  }))

  return {
    title: data?.title,
    description: data?.body?.processed,
    summary: data?.body?.summary,
    date: data?.field_date,
    imageDetails: imageDetails(data?.relationships?.field_featured_media),
    categories: categories,
    slideShow: getParagraph(data?.relationships?.paragraphs),
    authors: authors,
  }
}

export const blogPostQuery = graphql`
  query ($ID: String!) {
    fields: nodeBlogPost(id: { eq: $ID }) {
      id
      title
      field_date(formatString: "MMMM D, YYYY")
      body {
        processed
        summary
      }
      path {
        alias
      }
      relationships {
        field_categories {
          id
          name
          path {
            alias
          }
        }
        field_author_reference {
          ...AuthorFullDetails
        }
        field_featured_media {
          ...ImageDetails
        }
        paragraphs: field_slideshow {
          type: __typename
          ...ParagraphSlideshow
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
  }
`
