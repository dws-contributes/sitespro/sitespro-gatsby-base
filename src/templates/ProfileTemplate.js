import LayoutProfile from "../components/Organisms/Layout/LayoutProfile"
import { MapRelatedItems } from "../components/Templates/Profile/ProfileMappedRelatedContent"
import Profile from "../components/Templates/Profile/Profile"
import React from "react"
import { graphql } from "gatsby"
import { imageDetails } from "../components/Atoms/Image/BB8Image"

const ProfileTemplate = ({ data, pageContext }) => {
  let profile = ProfileTemplateDataNormalizer(data?.fields)

  return (
    <LayoutProfile
      title={data.fields.title}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <Profile {...profile} />
    </LayoutProfile>
  )
}

export default ProfileTemplate
export { Head } from "components/Organisms/Layout/Head"

const byDateDescAndMatches = (a, b) => {
  const aDate = new Date(a.date)
  const bDate = new Date(b.date)
  if (bDate > aDate) return 1
  if (bDate < aDate) return -1
  return b.matches - a.matches
}

const ProfileTemplateDataNormalizer = data => {
  let blogs = MapRelatedItems(data?.relationships?.node__blog_post)
  let stories = MapRelatedItems(data?.relationships?.node__story)
  let news = MapRelatedItems(data?.relationships?.node__news)

  let authoredContent = [...(blogs || []), ...(stories || []), ...(news || [])]
  authoredContent.sort(byDateDescAndMatches)

  let relatedContent = MapRelatedItems(data?.relationships?.relatedContent)

  return {
    authoredContent: authoredContent,
    relatedContent: relatedContent,
    title: data?.title,
    roles: data?.field_role,
    bio: data?.field_bio?.processed,
    email_address: data?.field_email_address,
    phone_number: data?.field_phone_number,
    address_1: data?.field_address_line_1,
    address_2: data?.field_address_line_2,
    address_3: data?.field_address_line_3,
    website_url: data?.field_personal_website?.uri,
    website_title: data?.field_personal_website?.title,
    imageDetails: imageDetails(data?.relationships?.field_image),
  }
}

export const ProfileQuery = graphql`
  query ($ID: String!) {
    fields: nodeProfile(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      field_last_name
      field_address_line_1
      field_address_line_2
      field_address_line_3
      field_email_address
      field_phone_number
      field_role
      field_bio {
        processed
      }
      field_personal_website {
        uri
        title
      }
      relationships {
        field_image {
          ...ImageDetails
        }
        node__blog_post {
          title
          date: field_date
          path {
            alias
          }
          relationships {
            node_type {
              name
            }
          }
        }
        node__story {
          title
          date: field_date
          path {
            alias
          }
          relationships {
            node_type {
              name
            }
          }
        }
        node__news {
          title
          date: field_date(formatString: "YYYY-MM-DDT08:mm:ssZ")
          path {
            alias
          }
          relationships {
            node_type {
              name
            }
          }
        }
        relatedContent: field_related_content {
          type: __typename
          ...NodeBlogPost
          ...NodeEvent
          ...NodeNews
          ...NodePage
          ...NodeStory
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href          
        }
      }
    }
  }
`
