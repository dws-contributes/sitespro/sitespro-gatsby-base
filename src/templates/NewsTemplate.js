import LayoutNews from "../components/Organisms/Layout/LayoutNews"
import News from "../components/Templates/News/News"
import React from "react"
import RelatedContent from "../components/Molecules/RelatedContent/RelatedContent"
import { getParagraph } from "../components/Molecules/Paragraphs/ParagraphsHelper.js"
import { graphql } from "gatsby"
import { imageDetails } from "../components/Atoms/Image/BB8Image"

const buildNews = newsNode => {
  const relationships = newsNode.relationships || {}

  return {
    title: newsNode.field_title_override || newsNode.title,
    subtitle: newsNode.field_subtitle,
    imageDetails: imageDetails(relationships?.field_featured_media),
    date: newsNode.field_date,
    authors: relationships.field_author_reference?.map(a => {
      return { path: a.path["alias"], title: a.title }
    }),
    body: newsNode.body?.processed,
    summary: newsNode.body?.summary,
    feedImageUrl: newsNode.field_feed_image_url?.uri,
    feedImageAltText: newsNode.field_feed_image_alt_text,
    slideshow: getParagraph(relationships.field_slideshow),
  }
}

const buildRelatedContent = relatedNews => {
  let relatedContent = []
  for (const node of relatedNews) {
    relatedContent.push({
      id: node.id,
      title: node.title,
      summary: node.body?.summary,
      date: node.field_date,
      path: node.field_alternate_link?.uri
        ? node.field_alternate_link?.uri
        : node.path?.alias,
      imageDetails: imageDetails(node.relationships.field_featured_media),
      feedImageUrl: node.field_feed_image_url?.uri,
      feedImageAlt: node.field_feed_image_alt_text,
    })
  }
  return relatedContent
}

const NewsTemplate = ({ data, pageContext }) => {
  const { fields = {}, relatedNews = {}, relatedNewsSiteSpecific = {} } = data
  const n = buildNews(fields)
  // Related content by (imported) categories
  const relatedContent = buildRelatedContent(relatedNews?.nodes)
  // Related content by site specific categories
  const relatedContentSiteSpecificCats = buildRelatedContent(
    relatedNewsSiteSpecific?.nodes
  )

  // Combine related content, sort by date, and limit to 3
  const combinedRelatedContent = [
    ...relatedContent,
    ...relatedContentSiteSpecificCats,
  ]
    .sort((a, b) => new Date(b.date) - new Date(a.date)) // Sort by date DESC
    .slice(0, 3) // Limit to 3 items

  return (
    <LayoutNews
      title={n.title}
      metatags={n.metatags}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <News {...n} />
      {combinedRelatedContent.length > 0 && (
        <RelatedContent
          relatedContent={combinedRelatedContent}
          heading="Related"
        />
      )}
    </LayoutNews>
  )
}

export default NewsTemplate
export { Head } from "components/Organisms/Layout/Head"

export const newsQuery = graphql`
  query ($ID: String!, $categories: [String]) {
    fields: nodeNews(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      body {
        summary
        processed
      }
      field_title_override
      field_subtitle
      field_date(formatString: "YYYY-MM-DDT08:mm:ssZ")
      field_feed_image_url {
        uri
      }
      field_alternate_link {
        uri
      }
      field_feed_image_alt_text
      relationships {
        field_author_reference {
          ...AuthorFullDetails
        }
        field_categories {
          name
        }
        field_featured_media {
          ...ImageDetails
        }
        field_slideshow {
          type: __typename
          ...ParagraphSlideshow
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
    relatedNews: allNodeNews(
      filter: {
        relationships: {
          field_categories: { elemMatch: { name: { in: $categories } } }
        }
        id: { ne: $ID }
        status: { eq: true }
      }
      limit: 3
      sort: { field_date: DESC }
    ) {
      nodes {
        id
        title
        path {
          alias
        }
        body {
          summary
          processed
        }
        field_date(formatString: "YYYY-MM-DDT08:mm:ssZ")
        field_alternate_link {
          uri
        }
        field_feed_image_url {
          uri
        }
        field_feed_image_alt_text
        relationships {
          field_featured_media {
            ...ImageDetails
          }
        }
      }
    }
    relatedNewsSiteSpecific: allNodeNews(
      filter: {
        relationships: {
          field_categories_site_specific: {
            elemMatch: { name: { in: $categories } }
          }
        }
        id: { ne: $ID }
        status: { eq: true }
      }
      limit: 3
      sort: { field_date: DESC }
    ) {
      nodes {
        id
        title
        path {
          alias
        }
        body {
          summary
          processed
        }
        field_date(formatString: "YYYY-MM-DDT08:mm:ssZ")
        field_alternate_link {
          uri
        }
        field_feed_image_url {
          uri
        }
        field_feed_image_alt_text
        relationships {
          field_featured_media {
            ...ImageDetails
          }
        }
      }
    }
  }
`
