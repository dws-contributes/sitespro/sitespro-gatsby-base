import Project from "components/Templates/Project/Project"
import LayoutProject from "components/Organisms/Layout/LayoutProject"
import React from "react"
import { graphql } from "gatsby"
import { imageDetails } from "components/Atoms/Image/BB8Image"

const buildProject = ProjectNode => {
  const relationships = ProjectNode.relationships || {}

  return {
    title: ProjectNode.title,
    imageDetails: imageDetails(relationships?.field_featured_media),
    body: ProjectNode.body?.processed,
    additionalLinks: ProjectNode.field_additional_links || [],
    actionLink: ProjectNode.field_project_action_link,
    contact: ProjectNode.field_project_contact,
    members: ProjectNode.field_project_member?.processed,
    leaders: ProjectNode.field_project_leader?.processed,
    sponsors: ProjectNode.field_project_sponsor?.processed,
    relatedCourses: ProjectNode.field_project_related_courses,
    timeframeDetails: ProjectNode.field_timeframe_details,
    categories: relationships.field_categories?.map(c => c.name) || [],
    timeframeStart: relationships.field_project_start_timeframe?.name,
    timeframeEnd: relationships.field_project_end_timeframe?.name,
    status: relationships.field_project_status?.name,
  }
}

const ProjectTemplate = ({ data, pageContext }) => {
  const { fields = {}, relatedProjects = {} } = data
  const e = buildProject(fields)

  return (
    <LayoutProject
      title={e.title}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <Project {...e} relatedProjects={relatedProjects} />
    </LayoutProject>
  )
}

export default ProjectTemplate
export { Head } from "components/Organisms/Layout/Head"

export const ProjectQuery = graphql`
  query ($ID: String!, $categories: [String]) {
    fields: nodeProject(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      body {
        processed
      }
      field_additional_links {
        uri
        uri_alias
        title
      }
      field_project_action_link {
        uri_alias
        title
      }
      field_project_contact
      field_project_leader {
        processed
      }
      field_project_member {
        processed
      }
      field_project_sponsor {
        processed
      }
      field_project_related_courses
      field_timeframe_details
      relationships {
        field_featured_media {
          ...ImageDetails
        }
        field_categories {
          name
        }
        field_project_status {
          name
        }
        field_project_start_timeframe {
          name
        }
        field_project_end_timeframe {
          name
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
    relatedProjects: allNodeProject(
      filter: {
        relationships: {
          field_categories: { elemMatch: { name: { in: $categories } } }
        }
        id: { ne: $ID }
      }
      limit: 3
      sort: { title: ASC }
    ) {
      nodes {
        id
        title
        path {
          alias
        }
        body {
          summary
        }
        relationships {
          field_project_start_timeframe {
            name
          }
          field_project_end_timeframe {
            name
          }
          field_featured_media {
            ...ImageDetails
          }
        }
      }
    }
  }
`
