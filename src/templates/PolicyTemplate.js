import Policy from "components/Templates/Policy/Policy"
import LayoutPolicy from "components/Organisms/Layout/LayoutPolicy"
import React from "react"
import { graphql } from "gatsby"

const buildPolicy = PolicyNode => {
  const relationships = PolicyNode.relationships || {}
  return {
    title: PolicyNode.title,
    body: PolicyNode.body?.processed,
    additionalLinks: PolicyNode.field_additional_links || [],
    owners: PolicyNode.field_owners,
    date: PolicyNode.field_date,
    lastModified: PolicyNode.field_last_modified_date,
    lastReviewed: PolicyNode.field_last_reviewed_date,
    documents: relationships?.field_policy_related_documents,
    reviewFrequency: PolicyNode.field_policy_review_frequency,
    categories: relationships.field_categories?.map(c => c.name) || [],
    types: relationships.field_policy_type?.map(t => t.name) || [],
  }
}

const PolicyTemplate = ({ data, pageContext }) => {
  const { fields = {} } = data
  const e = buildPolicy(fields)

  return (
    <LayoutPolicy
      title={e.title}
      nid={pageContext.NID}
      publishedStatus={pageContext.publishedStatus}
    >
      <Policy {...e} />
    </LayoutPolicy>
  )
}

export default PolicyTemplate
export { Head } from "components/Organisms/Layout/Head"

export const PolicyQuery = graphql`
  query ($ID: String!) {
    fields: nodePolicy(id: { eq: $ID }) {
      title
      id
      path {
        alias
      }
      body {
        processed
      }
      field_alternate_link {
        uri_alias
        title
      }
      field_additional_links {
        uri_alias
        title
      }
      field_date(formatString: "MMMM D, YYYY")
      field_last_modified_date(formatString: "MMMM D, YYYY")
      field_last_reviewed_date(formatString: "MMMM D, YYYY")
      field_owners
      field_policy_review_frequency
      relationships {
        field_policy_related_documents {
          ... on media__document {
            type: __typename
            name
            relationships {
              field_document {
                uri {
                  url
                }
              }
            }
          }
        }
        field_categories {
          name
        }
        field_policy_type {
          name
        }
      }
      metatag_normalized {
        tag
        attributes {
          content
          property
          name
          rel
          href
        }
      }
    }
  }
`
