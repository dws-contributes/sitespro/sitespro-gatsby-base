import { graphql } from "gatsby"

export const ParagraphPersonList = graphql`
  fragment ParagraphPersonList on paragraph__person_list {
    id
    field_display_title
    field_profile_list_format
    field_profile_type
    relationships {
      field_profile_groups {
        name
      }
      field_categories {
        name
      }
      field_organizational_unit {
        name
      }
      field_profile_types {
        name
      }
    }
  }
`
