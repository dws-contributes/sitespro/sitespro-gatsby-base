import { graphql } from "gatsby"

export const ParagraphPolicyList = graphql`
  fragment ParagraphPolicyList on paragraph__policy_list {
    id
    field_display_title
    field_number_of_items
    field_link {
      title
      uri_alias
      uri
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
