import { graphql } from "gatsby"

export const NodeFAQ = graphql`
  fragment NodeFAQ on node__faq {
    id
    title
    field_answer
    field_question
  }
`
