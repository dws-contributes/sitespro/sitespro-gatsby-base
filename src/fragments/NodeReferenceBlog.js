import { graphql } from "gatsby"

export const NodeBlogPost = graphql`
  fragment NodeBlogPost on node__blog_post {
    id
    date: field_date(formatString: "MMMM DD, YYYY")
    title
    path {
      alias
    }
    relationships {
      node_type {
        name
      }
      field_author_reference {
        title
        field_last_name
        path {
          alias
        }
      }
      field_featured_media {
        ...ImageDetails
      }
    }
  }
`
