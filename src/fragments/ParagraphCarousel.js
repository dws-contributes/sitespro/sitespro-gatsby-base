import { graphql } from "gatsby"

export const ParagraphCarousel = graphql`
  fragment ParagraphCarousel on paragraph__carousel {
    id
    relationships {
      field_paragraph_items {
        field_heading
        field_tagline
        field_url {
          title
          uri_alias
        }
        relationships {
          field_image {
            ...ImageDetails
          }
        }
      }
    }
  }
`
