import { graphql } from "gatsby"

export const NodeNews = graphql`
  fragment NodeNews on node__news {
    id
    date: field_date(formatString: "MMMM DD, YYYY")
    field_news_source
    field_title_override
    title
    path {
      alias
    }
    field_alternate_link {
      uri
    }
    summary: body {
      processed
      summary
    }
    field_feed_image_url {
      uri
    }
    field_feed_image_alt_text
    relationships {
      node_type {
        name
      }
      field_author_reference {
        ...AuthorDetails
      }
      field_featured_media {
        ...ImageDetails
      }
    }
  }
`
