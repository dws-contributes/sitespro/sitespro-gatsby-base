import { graphql } from "gatsby"

export const NodePage = graphql`
  fragment NodePage on node__page {
    id
    title
    path {
      alias
    }
    field_summary
    relationships {
      node_type {
        name
      }
    }
  }
`
