import { graphql } from "gatsby"

export const ParagraphPullQuote = graphql`
  fragment ParagraphPullQuote on paragraph__pull_quote {
    id
    field_quote
    field_attribution
  }
`
