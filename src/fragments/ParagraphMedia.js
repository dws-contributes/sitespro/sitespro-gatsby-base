import { graphql } from "gatsby"

export const ParagraphMedia = graphql`
  fragment ParagraphMedia on paragraph__media {
    id
    field_display_title
    field_tagline
    field_media_caption
    relationships {
      field_media_item {
        type: __typename
        ...ImageDetails
        ... on media__video {
          field_media_oembed_video
        }
        ... on media__audio {
          field_media_soundcloud
          field_transcript_link {
            uri_alias
          }
        }
        ... on media__document {
          name
          relationships {
            field_document {
              uri {
                url
              }
            }
          }
        }
      }
    }
  }
`
