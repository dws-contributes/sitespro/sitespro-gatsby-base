import { graphql } from "gatsby"

export const ParagraphScholarsList = graphql`
  fragment ParagraphScholarsList on paragraph__scholars_profile_list {
    id
    field_display_title
    field_number_of_items
    relationships {
      field_category {
        name
      }
    }
  }
`
