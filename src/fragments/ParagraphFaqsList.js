import { graphql } from "gatsby"

export const ParagraphFaqsList = graphql`
  fragment ParagraphFaqsList on paragraph__faqs_list {
    id
    field_display_title
    field_number_of_items
    field_expanded_faq_list
    relationships {
      field_category {
        name
      }
    }
  }
`
