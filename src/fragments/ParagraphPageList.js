import { graphql } from "gatsby"

export const ParagraphPageList = graphql`
  fragment ParagraphPageList on paragraph__page_list {
    id
    field_display_title
    field_number_of_items
    field_list_format
    field_link {
      title
      uri_alias
      uri
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
