import { graphql } from "gatsby"

export const ParagraphThreeColumn = graphql`
  fragment ParagraphThreeColumn on paragraph__three_column {
    id
    field_display_title
    relationships {
      field_left_column {
        type: __typename
        ...ParagraphAccordionSection
        ...ParagraphBlogList
        ...ParagraphButton
        ...ParagraphCallout
        ...ParagraphContent
        ...ParagraphContentReference
        ...ParagraphEventList
        ...ParagraphFaqsList
        ...ParagraphIframe
        ...ParagraphImageText
        ...ParagraphMedia
        ...ParagraphNewsList
        ...ParagraphPageList
        ...ParagraphPolicyList
        ...ParagraphProjectList
        ...ParagraphPullQuote
        ...ParagraphResourceList
        ...ParagraphScholarsList
        ...ParagraphSocialMedia
        ...ParagraphSpacer
        ...ParagraphSpecialText
        ...ParagraphStoryList
      }
      field_middle_column {
        type: __typename
        ...ParagraphAccordionSection
        ...ParagraphBlogList
        ...ParagraphButton
        ...ParagraphCallout
        ...ParagraphContent
        ...ParagraphContentReference
        ...ParagraphEventList
        ...ParagraphFaqsList
        ...ParagraphIframe
        ...ParagraphImageText
        ...ParagraphMedia
        ...ParagraphNewsList
        ...ParagraphPageList
        ...ParagraphPolicyList
        ...ParagraphProjectList
        ...ParagraphPullQuote
        ...ParagraphResourceList
        ...ParagraphScholarsList
        ...ParagraphSocialMedia
        ...ParagraphSpacer
        ...ParagraphSpecialText
        ...ParagraphStoryList
      }
      field_right_column {
        type: __typename
        ...ParagraphAccordionSection
        ...ParagraphBlogList
        ...ParagraphButton
        ...ParagraphCallout
        ...ParagraphContent
        ...ParagraphContentReference
        ...ParagraphEventList
        ...ParagraphFaqsList
        ...ParagraphIframe
        ...ParagraphImageText
        ...ParagraphMedia
        ...ParagraphNewsList
        ...ParagraphPageList
        ...ParagraphPullQuote
        ...ParagraphPolicyList
        ...ParagraphProjectList
        ...ParagraphResourceList
        ...ParagraphScholarsList
        ...ParagraphSocialMedia
        ...ParagraphSpacer
        ...ParagraphSpecialText
        ...ParagraphStoryList
      }
    }
  }
`
