import { graphql } from "gatsby"

export const ParagraphPublicationList = graphql`
  fragment ParagraphPublicationList on paragraph__publication_list {
    id
    field_pubs_display_title
    field_pubs_items_per_page
    field_pubs_org_id
    field_pubs_primary_only
    field_pubs_timeframe
    field_pubs_id_type
  }
`
