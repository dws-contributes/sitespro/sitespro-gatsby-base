import { graphql } from "gatsby"

export const ParagraphResourceList = graphql`
  fragment ParagraphResourceList on paragraph__resource_list {
    id
    field_display_title
    field_number_of_items
    field_list_format
    field_link {
      title
      uri_alias
      uri
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
