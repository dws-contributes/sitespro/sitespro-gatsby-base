import { graphql } from "gatsby"

export const ParagraphImageText = graphql`
  fragment ParagraphImageText on paragraph__image_text {
    id
    field_heading
    field_image_alignment
    field_link {
      title
      uri_alias
      uri
    }
    field_textarea
    relationships {
      field_image {
        ...ImageDetails
      }
    }
  }
`
