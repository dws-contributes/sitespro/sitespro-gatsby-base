import { graphql } from "gatsby"

export const ParagraphHeroImage = graphql`
  fragment ParagraphHeroImage on paragraph__hero_image {
    id
    field_heading
    field_link {
      title
      uri_alias
      uri
    }
    relationships {
      field_image {
        ...ImageDetails
      }
    }
  }
`
