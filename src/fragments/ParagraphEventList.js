import { graphql } from "gatsby"

export const ParagraphEventList = graphql`
  fragment ParagraphEventList on paragraph__event_listing {
    id
    field_display_title
    field_number_of_items
    field_link {
      uri_alias
      uri
      title
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
