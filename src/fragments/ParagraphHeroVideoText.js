import { graphql } from "gatsby"

export const ParagraphHeroVideoText = graphql`
  fragment ParagraphHeroVideoText on paragraph__hero_video_text {
    id
    field_display_title
    field_heading
    field_link {
      title
      uri_alias
    }
    field_tagline
    relationships {
      field_media_item {
        field_media_oembed_video
      }
    }
  }
`
