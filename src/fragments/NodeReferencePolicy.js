import { graphql } from "gatsby"

export const NodePolicy = graphql`
  fragment NodePolicy on node__policy {
    id
    title
    summary: body {
      summary
    }
    path {
      alias
    }
    relationships {
      node_type {
        name
      }
      field_categories {
        name
      }
    }
  }
`
