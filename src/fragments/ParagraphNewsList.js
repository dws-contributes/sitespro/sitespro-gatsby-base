import { graphql } from "gatsby"

export const ParagraphNewsList = graphql`
  fragment ParagraphNewsList on paragraph__news_list {
    id
    field_display_title
    field_number_of_items
    field_list_format
    field_link {
      uri_alias
      uri
      title
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
