import { graphql } from "gatsby"

export const ParagraphHeroFullWidthVideo = graphql`
  fragment ParagraphHeroFullWidthVideo on paragraph__hero_full_width_video {
    id
    field_heading
    field_link {
      title
      uri_alias
      uri
    }
    relationships {
      field_media_item {
        field_media_oembed_video
      }
    }
  }
`
