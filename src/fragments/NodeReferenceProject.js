import { graphql } from "gatsby"

export const NodeProject = graphql`
  fragment NodeProject on node__project {
    id
    title
    summary: body {
      summary
    }
    path {
      alias
    }
    relationships {
      node_type {
        name
      }
      field_categories {
        name
      }
      field_project_start_timeframe {
        name
      }
      field_project_end_timeframe {
        name
      }      
      field_featured_media {
        ...ImageDetails
      }
    }
  }
`
