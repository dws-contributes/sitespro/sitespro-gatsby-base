import { graphql } from "gatsby"

export const NodeProfile = graphql`
  fragment NodeProfile on node__profile {
    id
    field_email_address
    field_phone_number
    field_role
    title
    field_bio {
      summary
      processed
    }
    path {
      alias
    }
    relationships {
      field_image {
        ...ImageDetails
      }
    }
  }
`
