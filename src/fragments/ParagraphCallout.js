import { graphql } from "gatsby"

export const ParagraphCallout = graphql`
  fragment ParagraphCallout on paragraph__callout {
    id
    field_display_title
    field_body
    field_link {
      uri_alias
      uri
      title
    }
    relationships {
      field_image {
        ...ImageDetails
      }
    }
  }
`
