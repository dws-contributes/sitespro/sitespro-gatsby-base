import { graphql } from "gatsby"

export const AuthorDetails = graphql`
  fragment AuthorDetails on node__profile {
    title
    last_name: field_last_name
    path {
      alias
    }
    bio: field_bio {
      summary
    }
  }
`

export const AuthorFullDetails = graphql`
  fragment AuthorFullDetails on node__profile {
    title
    last_name: field_last_name
    path {
      alias
    }
    bio: field_bio {
      summary
      processed
    }
    relationships {
      field_image {
        ...ImageDetails
      }
    }
  }
`
