import { graphql } from "gatsby"

export const ParagraphImageCardRow = graphql`
  fragment ParagraphImageCardRow on paragraph__image_card_row {
    id
    field_display_title
    relationships {
      field_image_card {
        field_heading
        field_body
        field_link {
          title
          uri_alias
          uri
        }
        relationships {
          field_image {
            ...ImageDetails
          }
        }
      }
    }
  }
`
