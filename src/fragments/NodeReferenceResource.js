import { graphql } from "gatsby"

export const NodeResource = graphql`
  fragment NodeResource on node__resource {
    id
    title
    revised_date: field_last_modified_date(formatString: "MMMM DD, YYYY")
    field_alternate_link {
      uri
    }
    body {
      processed
      summary
    }
    path {
      alias
    }
    relationships {
      node_type {
        name
      }
      field_featured_media {
        ...ImageDetails
      }
    }
  }
`
