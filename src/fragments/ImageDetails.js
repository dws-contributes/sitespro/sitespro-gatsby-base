import { graphql } from "gatsby"

export const ImageDetails = graphql`
  fragment ImageDetails on media__image {
    type: __typename
    field_media_image {
      alt
      width
      height
      title
    }
    relationships {
      field_media_image {
        filemime
        uri {
          url
        }
        image_style_uri {
          scale_width_500
          scale_width_1000
          focal_point_large
          focal_point_medium
          large_2_5_1
          large_3_2
          large_4_3
          large_4_5
          small_2_5_1
          small_3_2
        }
      }
    }
  }
`
