import { graphql } from "gatsby"

export const ParagraphBlogList = graphql`
  fragment ParagraphBlogList on paragraph__blog_listing {
    id
    field_display_title
    field_number_of_items
    field_list_format
    field_link {
      uri_alias
      uri
      title
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
