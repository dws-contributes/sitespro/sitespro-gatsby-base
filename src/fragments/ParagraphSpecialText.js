import { graphql } from "gatsby"

export const ParagraphSpecialText = graphql`
  fragment ParagraphSpecialText on paragraph__special_text {
    id
    field_special_text
  }
`
