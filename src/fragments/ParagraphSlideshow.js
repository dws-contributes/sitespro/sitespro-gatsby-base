import { graphql } from "gatsby"

export const ParagraphSlideshow = graphql`
  fragment ParagraphSlideshow on paragraph__slideshow {
    id
    field_display_title
    relationships {
      field_paragraph_items {
        field_media_caption
        relationships {
          field_image {
            ...ImageDetails
          }
        }
      }
    }
  }
`
