import { graphql } from "gatsby"

export const ParagraphSpacer = graphql`
  fragment ParagraphSpacer on paragraph__spacer {
    id
    field_spacer_divider
    field_spacer_height
  }
`
