import { graphql } from "gatsby"

export const ParagraphStoryList = graphql`
  fragment ParagraphStoryList on paragraph__story_list {
    id
    field_display_title
    field_number_of_items
    field_link {
      uri_alias
      uri
      title
    }
    relationships {
      field_category {
        name
      }
    }
  }
`
