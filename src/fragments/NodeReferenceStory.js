import { graphql } from "gatsby"

export const NodeStory = graphql`
  fragment NodeStory on node__story {
    id
    title
    date: field_date(formatString: "MMMM DD, YYYY")
    field_summary
    path {
      alias
    }
    relationships {
      node_type {
        name
      }
      field_author_reference {
        title
        field_last_name
        path {
          alias
        }
      }
      field_featured_media {
        ...ImageDetails
      }
    }
  }
`
