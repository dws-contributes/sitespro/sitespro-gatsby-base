import { graphql } from "gatsby"

export const ParagraphHeroImageText = graphql`
  fragment ParagraphHeroImageText on paragraph__hero_image_text {
    id
    field_display_title
    field_heading
    field_hero_display_options
    field_link {
      uri_alias
      uri
      title
    }
    field_tagline
    relationships {
      field_image {
        ...ImageDetails
      }
    }
  }
`
