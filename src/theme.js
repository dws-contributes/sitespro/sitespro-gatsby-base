const baseColors = {
  blue: "#012169",
  darkBlue: "#005587",
  shaleBlue: "#0577B1",
  lightBlue: "#dbedf2",
  gray: "#262626",
  darkGray: "#464343",
  mediumGray: "#666666",
  mediumLightGray: "#b5b5b5",
  lightGray: "#E5E5E5", // limestone
  offWhite: "#f3f2f1",
  yellow: "#FFD960",
  orange: "#C84E00",
  lightGreen: "#a1b70d",
  white: "#ffffff",
}

/* To customize the fonts, create fonts.js file in the same folder as theme.js.
   Use the following format to include fonts:

  module.exports = {
    primary: {
      name: "Open Sans",
      face: "Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,400;1,700",
    },  
    secondary: {
      name: "Merriweather",
      face: "Merriweather:ital,wght@0,300;0,700;1,300;1,700",
    },
  }
*/

export const theme = {
  // Header style options: dark, light. Default - dark.
  colorTheme: "dark",

  // Accent style options: solid, pattern. Default - solid.
  accent: "",

  // Header logo options. If the src is not empty, the alt field is required.
  // Logo file which needs to be placed in the 'static' folder. Do not include the path to the image, just the file's name.
  // Default - empty, the logo will be generated based on the site's name and a standard Duke logo.
  headerLogo: {
    src: "",
    alt: "",
    height: "56px",
    width: "auto",
  },

  // Search option. Add Search Engine ID to enable the Google Custom Search. Default - empty (Solr-based search).
  search: "",

  footerBackground: "arch_pattern.svg",

  cssVariables: {
    dark: {
      color: {
        primary: baseColors["blue"], // blue
        secondary: baseColors["shaleBlue"], // shale-blue
        accentOne: baseColors["yellow"],
        accentTwo: baseColors["orange"],
        neutralAccentOne: baseColors["lightGray"],
        text: baseColors["darkGray"], // gray
        link: baseColors["shaleBlue"],
        linkHover: baseColors["shaleBlue"],

        header: {
          primary: baseColors["blue"],
          secondary: baseColors["shaleBlue"],
          divider: baseColors["shaleBlue"],
          logo: baseColors["yellow"],
          search: {
            text: baseColors["white"],
            background: "#344D87",
            border: "#344D87",
            icon: baseColors["yellow"],
          },
        },

        footer: {
          primary: baseColors["blue"],
          logo: baseColors["yellow"],
          text: baseColors["white"],
          divider: baseColors["shaleBlue"],
          social: baseColors["shaleBlue"],
        },

        button: {
          primary: {
            text: baseColors["darkBlue"],
            background: baseColors["white"],
            border: baseColors["shaleBlue"],
            textHover: baseColors["white"],
            backgroundHover: baseColors["shaleBlue"],
            borderHover: baseColors["shaleBlue"],
          },
          cta: {
            text: baseColors["white"],
            background: baseColors["orange"],
            border: baseColors["orange"],
            textHover: baseColors["white"],
            backgroundHover: "#823300", // darker copper (orange)
            borderHover: "#823300", // darker copper (orange)
          },
          footer: {
            text: baseColors["white"],
            background: baseColors["shaleBlue"],
            border: baseColors["shaleBlue"],
            textHover: baseColors["white"],
            backgroundHover: "#04567f", // blue-green
            borderHover: "#04567f", // blue-green
          },
        },

        menu: {
          main: {
            link: baseColors["white"],
            linkHover: baseColors["white"],
            linkActive: baseColors["yellow"],
            linkActiveHover: baseColors["white"],
            caret: baseColors["yellow"],
            backgroundHover: baseColors["shaleBlue"],
            dropdownLink: baseColors["white"],
            linkMobile: baseColors["yellow"],
            dropdownLinkMobile: baseColors["white"],
          },
          utility: {
            link: baseColors["white"],
            linkMobile: baseColors["white"],
          },
          action: {
            link: baseColors["white"],
            background: baseColors["orange"],
            backgroundHover: "#823300",
            linkMobile: baseColors["white"],
            backgroundMobile: baseColors["orange"],
          },          
        },

        // maybe add later if needed
        // hero: {
        //   backgroundOne: baseColors['blue'], // duke blue
        //   backgroundTwo: baseColors['shaleBlue'],
        // },
      },
      font: {
        primary: '"Merriweather", Georgia, serif',
        secondary: '"Open Sans", Arial, Helvetica, sans-serif',
        h1: '"Open Sans", Arial, Helvetica, sans-serif',
        h2: '"Open Sans", Arial, Helvetica, sans-serif',
        h3: '"Open Sans", Arial, Helvetica, sans-serif',
        h4: '"Open Sans", Arial, Helvetica, sans-serif',
        h5: '"Open Sans", Arial, Helvetica, sans-serif',
        h6: '"Open Sans", Arial, Helvetica, sans-serif',
      },
    },
    light: {
      color: {
        primary: baseColors["blue"], // blue
        secondary: baseColors["shaleBlue"], // shale-blue
        accentOne: baseColors["yellow"],
        accentTwo: baseColors["orange"],
        neutralAccentOne: baseColors["lightGray"],
        text: baseColors["darkGray"], // gray
        link: baseColors["shaleBlue"],
        linkHover: baseColors["shaleBlue"],

        header: {
          primary: baseColors["white"],
          secondary: baseColors["shaleBlue"],
          divider: baseColors["shaleBlue"],
          logo: baseColors["blue"],
          search: {
            text: baseColors["darkGray"],
            background: baseColors["white"],
            border: baseColors["mediumLightGray"],
            icon: baseColors["shaleBlue"],
          },
        },

        footer: {
          primary: baseColors["blue"],
          logo: baseColors["yellow"],
          text: baseColors["white"],
          divider: baseColors["shaleBlue"],
          social: baseColors["shaleBlue"],
        },

        button: {
          primary: {
            text: baseColors["darkBlue"],
            background: baseColors["white"],
            border: baseColors["shaleBlue"],
            textHover: baseColors["white"],
            backgroundHover: baseColors["shaleBlue"],
            borderHover: baseColors["shaleBlue"],
          },
          cta: {
            text: baseColors["white"],
            background: baseColors["orange"],
            border: baseColors["orange"],
            textHover: baseColors["white"],
            backgroundHover: "#823300", // darker copper (orange)
            borderHover: "#823300", // darker copper (orange)
          },
          footer: {
            text: baseColors["white"],
            background: baseColors["shaleBlue"],
            border: baseColors["shaleBlue"],
            textHover: baseColors["white"],
            backgroundHover: "#04567f", // blue-green
            borderHover: "#04567f", // blue-green
          },
        },

        menu: {
          main: {
            background: baseColors["blue"],
            link: baseColors["white"],
            linkHover: baseColors["white"],
            linkActive: baseColors["yellow"],
            linkActiveHover: baseColors["white"],
            caret: baseColors["yellow"],
            backgroundHover: baseColors["shaleBlue"],
            dropdownLink: baseColors["white"],
            linkMobile: baseColors["yellow"],
            dropdownLinkMobile: baseColors["white"],
          },
          utility: {
            link: baseColors["shaleBlue"],
            linkMobile: baseColors["white"],
          },
          action: {
            link: baseColors["darkGray"],
            background: baseColors["yellow"],
            backgroundHover: "#CA9D0E",
            linkMobile: baseColors["darkGray"],
            backgroundMobile: baseColors["yellow"],
          }, 
        },

        // maybe add later if needed
        // hero: {
        //   backgroundOne: baseColors['blue'], // duke blue
        //   backgroundTwo: baseColors['shaleBlue'],
        // },
      },
      font: {
        primary: '"Merriweather", Georgia, serif',
        secondary: '"Open Sans", Arial, Helvetica, sans-serif',
        h1: '"Open Sans", Arial, Helvetica, sans-serif',
        h2: '"Open Sans", Arial, Helvetica, sans-serif',
        h3: '"Open Sans", Arial, Helvetica, sans-serif',
        h4: '"Open Sans", Arial, Helvetica, sans-serif',
        h5: '"Open Sans", Arial, Helvetica, sans-serif',
        h6: '"Open Sans", Arial, Helvetica, sans-serif',
      },
    },    
  },
}

export default theme
