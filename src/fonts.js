module.exports = {
  primary: {
    name: "Open Sans",
    face: "Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,400;1,700",
  },  
  secondary: {
    name: "Merriweather",
    face: "Merriweather:ital,wght@0,300;0,700;1,300;1,700",
  },
}
