import React from "react"
import Seo from "components/Atoms/Seo/Seo.js"
import { useSiteInfoData } from "hooks/SiteInfoData"
import { getMetaDetails } from "components/Atoms/Seo/Seo"
import cssVariables from "components/_utils/cssVariables"
import theme from "theme"

export const Head = ({ location, params, data, pageContext }) => {
  const siteInfoData = useSiteInfoData()
  const siteName = siteInfoData?.prefix
    ? siteInfoData?.prefix + ` ` + siteInfoData?.name
    : siteInfoData?.name

  // Function to push data to metatag_normalized
  function pushMetaData(content, property, isHomepage = false) {
    if (!isHomepage) {
      data.fields.metatag_normalized.push({
        tag: "meta",
        attributes: {
          content: content,
          property: property,
        },
      })
    } else {
      pageContext?.metatags.push({
        tag: "meta",
        attributes: {
          content: content,
          property: property,
        },
      })
    }
  }

  /** Page CT and Homepage OG Image **/
  // Check OG Image, if not one, find other options
  // 1st - Meta Tag OG Image, 2nd - Thumbnail Image, 3rd - Hero Image (full width, image + text or 1st carousel image)
  const metaOGContentTypeData =
    data?.fields?.metatag_normalized?.find(
      ogContentType => ogContentType.attributes.property === "og:type"
    ) || null

  const metaOGContentImageData =
    data?.fields?.metatag_normalized?.find(
      ogContentType => ogContentType.attributes.property === "og:image"
    ) ||
    pageContext?.metatags?.find(
      ogContentType => ogContentType.attributes.property === "og:image"
    ) ||
    null

  // If content type is page or homepage and no Meta Tag OG Image, find and set other image options
  if (
    (metaOGContentTypeData?.attributes.content === "page" ||
      pageContext?.isHomepage) &&
    !metaOGContentImageData?.attributes.content
  ) {
    // Set OG Image Options
    let thumbnail = null
    let heroImg = null
    let heroImgCar = null
    let isHomepage = false

    // Page CT OG Image Options
    if (metaOGContentTypeData?.attributes.content === "page") {
      thumbnail =
        data?.fields?.relationships?.field_thumbnail_media?.relationships
          ?.field_media_image?.image_style_uri?.large_3_2
      heroImg =
        data?.fields?.relationships?.hero?.relationships?.field_image
          ?.relationships?.field_media_image?.image_style_uri?.large_3_2
      heroImgCar =
        data?.fields?.relationships?.hero?.relationships
          ?.field_paragraph_items?.[0]?.relationships?.field_image
          ?.relationships?.field_media_image?.image_style_uri?.large_3_2
    }

    // Homepage OG Image Options
    if (pageContext?.isHomepage) {
      isHomepage = true

      thumbnail =
        pageContext?.field_thumbnail_media?.relationships?.field_media_image
          ?.image_style_uri?.large_3_2
      heroImg =
        pageContext?.hero?.relationships?.field_image?.relationships
          ?.field_media_image?.image_style_uri?.large_3_2
      heroImgCar =
        pageContext?.hero?.relationships?.field_paragraph_items?.[0]
          ?.relationships?.field_image?.relationships?.field_media_image
          ?.image_style_uri?.large_3_2

      // Set og:type for homepage
      pushMetaData("homepage", "og:type", isHomepage)
    }

    // Thumbnail Image
    if (thumbnail) {
      pushMetaData(thumbnail, "page:thumbnail_image", isHomepage)
    }
    // Hero Image (Full Width or Image + Text)
    else if (heroImg) {
      pushMetaData(heroImg, "page:hero_image", isHomepage)
    }
    // Hero Carousel (1st carousel item image)
    else if (heroImgCar) {
      pushMetaData(heroImgCar, "page:hero_image", isHomepage)
    }
  }
  /** Page CT and Homepage OG Image **/

  // Event
  if (
    data?.fields?.field_location_text ||
    data?.fields?.date?.start ||
    data?.fields?.field_feed_image_url?.uri
  ) {
    pushMetaData(data?.fields?.field_location_text, "location")
    pushMetaData(data?.fields?.date?.start, "startDate")
    pushMetaData(data?.fields?.field_feed_image_url?.uri, "image")
  }

  let metaTags = null
  if (pageContext?.metatags) {
    metaTags = getMetaDetails(pageContext?.metatags)
  } else if (Object.keys(data).length !== 0) {
    metaTags = getMetaDetails(data?.fields?.metatag_normalized)
  }

  // News
  // Since the Metatag module in Drupal is trimming canonical and original-source field content to 80 characters, we'll replace it directly on the frontend for News
  if (data?.fields?.field_alternate_link?.uri) {
    metaTags.originalSource = data?.fields?.field_alternate_link?.uri
  }

  // Profile
  // Add trimmed bio as description
  if (data?.fields?.field_bio?.processed) {
    metaTags.description = data?.fields?.field_bio?.processed
      .replace(/(<([^>]+)>)/gi, "")
      .substring(0, 180)
  }

  // Story
  // Fix empty summary
  if (data?.fields?.summary) {
    metaTags.description = data?.fields?.summary?.substring(0, 180)
    metaTags.ogDescription = data?.fields?.summary?.substring(0, 180)
  }

  // Profile
  // Add job titles
  if (data?.fields?.relationships?.duke_scholars_profile_positions) {
    metaTags.jobTitle =
      data?.fields?.relationships?.duke_scholars_profile_positions
  }

  // Theme mode selector - defined in theme.js.
  // Can be "dark" or "light".
  const themeMode = theme.colorTheme || "dark"
  const themeCSSVariables = `:root {
    ${cssVariables(theme.cssVariables[themeMode])}
  }`

  // Ensure metaTags is an object before setting its properties
  if (!metaTags || typeof metaTags !== "object") {
    metaTags = {}
  }

  if (pageContext.listPage) {
    metaTags = {
      title: pageContext.title,
      description: `An archive of ${pageContext.title}`,
    }
  }

  const pageTitle = data?.fields?.title || pageContext?.title

  if (!metaTags.title) {
    metaTags.title = pageTitle
  }

  const fullPageTitle = pageTitle ? pageTitle + " | " + siteName : siteName

  return (
    <>
      <meta name="MobileOptimized" content="width" />
      <meta name="HandheldFriendly" content="true" />
      <title>{fullPageTitle}</title>
      <style type="text/css">{`${themeCSSVariables}`}</style>
      <Seo metaTags={metaTags} />
    </>
  )
}
