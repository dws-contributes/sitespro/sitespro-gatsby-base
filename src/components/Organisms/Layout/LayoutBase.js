/**
 * Base layout - includes header, base wrapper, footer
 */
import Footer from "../Footer/Footer.js"
import Header from "../Header/Header.js"
import PropTypes from "prop-types"
import React from "react"
import StatusInfo from "components/Atoms/StatusInfo/StatusInfo"
import SkipLink from "components/Atoms/SkipLink/SkipLink"
import { useSiteInfoData } from "hooks/SiteInfoData"
import "./Layout.scss"

const LayoutBase = ({
  children,
  isListPage = false,
  nid,
  publishedStatus,
  showStatusInfo = true,
  editPath,
}) => {
  const siteData = useSiteInfoData()

  return (
    <> 
      <SkipLink />
      <Header siteData={siteData} />

      {/* Display the notice about unpublished status and a link to edit */}
      {process.env.NODE_ENV === "development" && showStatusInfo && (
        <StatusInfo
          nid={nid}
          publishedStatus={publishedStatus}
          editPath={editPath}
        />
      )}

      {children}
      <Footer siteData={siteData} />
    </>
  )
}

LayoutBase.propTypes = {
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
  showStatusInfo: PropTypes.bool,
}

export default LayoutBase
