import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"

const LayoutHomepage = ({
  children,
  mainBlock = "main-content",
  mainContentModifiers = [],
  title,
  hero,
  nid,
  publishedStatus,
}) => {
  return (
    <LayoutBase
      pageTitle={title}
      nid={nid}
      publishedStatus={publishedStatus}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        {hero && <section>{hero ? hero : ""}</section>}

        <Container>
          <div className={bem(mainBlock, "", mainContentModifiers)}>
            <h1 className="visually-hidden">{title}</h1>
            {children}
          </div>
        </Container>
      </main>
    </LayoutBase>
  )
}

LayoutHomepage.propTypes = {
  title: PropTypes.string,
  hero: PropTypes.object,
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutHomepage
