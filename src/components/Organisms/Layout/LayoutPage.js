import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import SidebarMenu from "components/Molecules/Menu/SidebarMenu/SidebarMenu"

const LayoutPage = ({
  children,
  mainBlock = "main-content",
  mainContentModifiers = [],
  hasSidebar,
  location,
  title,
  hero,
  nid,
  publishedStatus,
}) => {
  return (
    <LayoutBase nid={nid} publishedStatus={publishedStatus}>
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        {hero && <>{hero}</>}
        <PageTitle>{title}</PageTitle>

        <Container>
          <div
            className={`layout-main-wrapper ${
              hasSidebar ? "layout-with-sidebar" : "layout-no-sidebar"
            }`}
          >
            {hasSidebar && (
              <div className="main-sidebar">
                {/* Skip to main content link added for accessibility */}
                <a
                  className="visually-hidden-focusable"
                  href="#main-content-skip"
                >
                  Skip to content
                </a>
                <SidebarMenu />
              </div>
            )}

            <div
              className={bem(mainBlock, "", mainContentModifiers)}
              id="main-content-skip"
            >
              <CustomBreadcrumbs pageTitle={title} location={location} />
              {children}
            </div>
          </div>
        </Container>
      </main>
    </LayoutBase>
  )
}

LayoutPage.propTypes = {
  title: PropTypes.string,
  hero: PropTypes.object,
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutPage
