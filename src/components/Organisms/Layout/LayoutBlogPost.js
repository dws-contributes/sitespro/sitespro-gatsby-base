import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"

import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"

const LayoutBlogPost = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  nid,
  publishedStatus,
}) => {
  return (
    <LayoutBase
      nid={nid}
      publishedStatus={publishedStatus}
    >
      <Container>
        <div className="layout-main-wrapper layout-no-sidebar">
          <main
            className={bem(mainBlock, "", mainContentModifiers)}
            id="reach-skip-nav"
            data-reach-skip-nav-content=""
            tabIndex="-1"
          >
            {children}
          </main>
        </div>
      </Container>
    </LayoutBase>
  )
}

LayoutBlogPost.propTypes = {
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutBlogPost
