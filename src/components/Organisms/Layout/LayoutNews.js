import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "../Layout/LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"
import { CustomBreadcrumbs, GetCustomParents } from "components/Molecules/Breadcrumbs/Breadcrumbs"

const LayoutNews = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = ["layout-no-sidebar"],
  mainContentModifiers = ["news"],
  title,
  metatags,
  nid,
  publishedStatus,
}) => {
  return (
    <LayoutBase
      pageTitle={title}
      metaTags={metatags}
      nid={nid}
      publishedStatus={publishedStatus}
    >
      <main
        className={bem(mainBlock, layoutModifiers, mainContentModifiers)}
        id="reach-skip-nav"
        data-reach-skip-nav-content=""
        tabIndex="-1"
      >
        <Container>
          <CustomBreadcrumbs
            pageTitle={title}
            customParents={GetCustomParents("news")}
          />

          {children}
        </Container>
      </main>
    </LayoutBase>
  )
}

LayoutNews.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutNews
