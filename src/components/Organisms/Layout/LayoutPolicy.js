import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"
import {
  CustomBreadcrumbs,
  GetCustomParents,
} from "components/Molecules/Breadcrumbs/Breadcrumbs"

const LayoutPolicy = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = ["layout-no-sidebar"],
  mainContentModifiers = ["policy"],
  title,
  nid,
  publishedStatus,
}) => {

  return (
    <LayoutBase 
      nid={nid} 
      publishedStatus={publishedStatus}
    >
      <main
        className={bem(mainBlock, "", mainContentModifiers)}
        id="reach-skip-nav"
        data-reach-skip-nav-content=""
        tabIndex="-1"
      >
        <Container>
          <CustomBreadcrumbs
            pageTitle={title}
            customParents={GetCustomParents("policies")}
          />
          {children}
        </Container>
      </main>
    </LayoutBase>
  )
}

LayoutPolicy.propTypes = {
  title: PropTypes.string,
  hero: PropTypes.object,
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutPolicy
