import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import bem from "components/_utils/bem"
import { CustomBreadcrumbs, GetCustomParents } from "components/Molecules/Breadcrumbs/Breadcrumbs"

const LayoutProfile = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  customParents = GetCustomParents("staff"),
  title,
  nid,
  publishedStatus,
  editPath,
}) => {
  return (
    <LayoutBase
      pageTitle={title}
      nid={nid}
      publishedStatus={publishedStatus}
      editPath={editPath}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{title}</PageTitle>

        <Container>
          <CustomBreadcrumbs pageTitle={title} customParents={customParents} />
          <div className="layout-main-wrapper layout-no-sidebar">
            <div className={bem(mainBlock, "", mainContentModifiers)}>
              {children}
            </div>
          </div>
        </Container>
      </main>
    </LayoutBase>
  )
}

LayoutProfile.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutProfile
