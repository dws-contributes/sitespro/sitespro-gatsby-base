import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "../Layout/LayoutBase"
import bem from "components/_utils/bem"

const LayoutStory = ({
  children,
  mainBlock = "",
  mainModifiers = [],
  layoutModifiers = [],
  mainContentModifiers = [],
  title,
  nid,
  publishedStatus,
}) => {
  return (
    <LayoutBase
      pageTitle={title}
      nid={nid}
      publishedStatus={publishedStatus}
    >
      <main
        className={bem(mainBlock, "", mainContentModifiers)}
        id="reach-skip-nav"
        data-reach-skip-nav-content=""
        tabIndex="-1"
      >
        {children}
      </main>
    </LayoutBase>
  )
}

LayoutStory.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutStory
