import React from "react"
import PropTypes from "prop-types"
import LayoutBase from "./LayoutBase"
import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"
import { CustomBreadcrumbs, GetCustomParents } from "components/Molecules/Breadcrumbs/Breadcrumbs"

const LayoutEvent = ({
  children,
  mainBlock = "main-content",
  mainModifiers = [],
  layoutModifiers = ["layout-no-sidebar"],
  mainContentModifiers = ["events"],
  hasSidebar,
  title,
  nid,
  publishedStatus,
  pastEvent
}) => {
  if (hasSidebar) {
    mainModifiers.push("with-sidebar")
    mainContentModifiers.push("with-sidebar")
  }

  const customParents = pastEvent ? "past-events" : "events"

  return (
    <LayoutBase nid={nid} publishedStatus={publishedStatus}>
      <main
        className={bem(mainBlock, layoutModifiers, mainContentModifiers)}
        id="reach-skip-nav"
        data-reach-skip-nav-content=""
        tabIndex="-1"
      >
        <Container>
          <CustomBreadcrumbs
            pageTitle={title}
            customParents={GetCustomParents(customParents)}
          />
          {children}
        </Container>
      </main>
    </LayoutBase>
  )
}

LayoutEvent.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
}

export default LayoutEvent
