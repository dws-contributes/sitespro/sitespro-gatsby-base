import React, { useState, useEffect } from "react"
import "./Search.scss"

const EmbeddedList = ({ numResults, solrQuery, render }) => {
  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)

  const handleSolrResults = results => {
    setData(results)
    setLoading(false)
  }

  useEffect(() => {
    const abortController = new AbortController()
    const fetchData = () => {
      setLoading(true)
      solrQuery
        .search("*")
        .perPage(numResults)
        .page(1)
        .execute({ signal: abortController.signal, success: handleSolrResults })
    }
    fetchData()
    return () => {
      setTimeout(() => abortController.abort(), 5000)
    }
  }, [numResults, solrQuery])

  const { results = [] } = data
  return (
    <div className="embedded-list">
      <div className="embedded-results">
        {loading && (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        )}
        {render(results)}
      </div>
    </div>
  )
}

export default EmbeddedList
