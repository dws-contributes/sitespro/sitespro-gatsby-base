import React, { useState, useEffect } from "react"
import { SearchPagination, SearchForm, SearchResult } from "."
import "./Search.scss"

const defaultRender = results => {
  return results.map(r => <SearchResult key={r.id} result={r} />)
}

const ProfileSearch = ({
  location,
  navigate,
  perPage,
  solrQuery,
  render = defaultRender,
}) => {
  const { search } = location
  const searchParams = new URLSearchParams(search)
  const query = searchParams.get("q")
  const page = searchParams.get("page") || 1

  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)
  const [ready, setReady] = useState(false)

  const handleSolrResults = results => {
    setData(results)
    setLoading(false)
    setReady(true)
  }

  useEffect(() => {
    const abortController = new AbortController()
    const fetchData = () => {
      setLoading(true)
      solrQuery
        .search(query || "*")
        .perPage(perPage)
        .page(page)
        .execute({ signal: abortController.signal, success: handleSolrResults })
    }
    fetchData()
    return () => {
      abortController.abort()
    }
  }, [query, page, perPage, solrQuery])

  // numFound = 0,

  const { numFound = 0, results = [], pagination = {} } = data

  return (
    <div className="search-interface profile-search">
      <div className="top-controls">
        <SearchForm location={location} />
        {loading && (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        )}
      </div>
      <div className="search-results" id="region">
        {!loading && numFound > 0 && render(results)}
        {ready && numFound === 0 && <p>No profiles found.</p>}
      </div>
      <SearchPagination location={location} pagination={pagination} />
    </div>
  )
}

export default ProfileSearch
