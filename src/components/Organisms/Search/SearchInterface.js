import React, { useState, useEffect } from "react"
import { setFromString } from "components/_utils/Set"
import { sortObject } from "components/_utils/Object"
import { reverseSortObject } from "components/_utils/Object"
import NoResults from "components/Atoms/NoResults/NoResults"
import { Collapse, Button } from "react-bootstrap"
import MetaBox from "components/Atoms/MetaBox/MetaBox"
import dayjs from "dayjs"
import { navigate } from "gatsby"
import {
  SearchResult,
  SearchPagination,
  SearchForm,
  SearchSort,
  FacetGroup,
  orDelimiter,
} from "."
import "./Search.scss"

const defaultRender = (results, queryAsTyped) => {
  return results.map(r => (
    <SearchResult key={r.id} result={r} query={queryAsTyped} />
  ))
}

// Define the default nothing found message
const defaultNothingFoundMessage = "No results found."

const SearchInterface = ({
  location,
  // navigate,
  fixedType = false,
  fixedSort = false,
  perPage,
  sortOptions,
  solrQuery,
  facetFields = [],
  facetRanges = [],
  excludeFacetGroups = ["Scholars@Duke VivoTypes"],
  render = defaultRender,
  nothingFoundMessage = defaultNothingFoundMessage,
}) => {
  const { pathname, search } = location
  const searchParams = new URLSearchParams(search)
  const queryAsTyped = searchParams.get("q")
  // add AND (+) delimiter if query contains spaces
  const query = searchParams.get("q")?.replace(/\s/g, "+")
  const page = searchParams.get("page")?.replace("/", "") || 1
  const sort = searchParams.get("sort")
  const typeFilters = searchParams.get("type")
  const termFilters = searchParams.get("term")

  const addTypeFilter = value => {
    const newTypeFilters = setFromString(typeFilters, orDelimiter)
    newTypeFilters.add(value)
    searchParams.set("type", Array.from(newTypeFilters).join(orDelimiter))
    searchParams.set("page", 1)
    navigate(`${pathname}?${searchParams.toString()}`)
  }

  const removeTypeFilter = value => {
    const newTypeFilters = setFromString(typeFilters, orDelimiter)
    newTypeFilters.delete(value)
    searchParams.set("type", Array.from(newTypeFilters).join(orDelimiter))
    searchParams.set("page", 1)
    navigate(`${pathname}?${searchParams.toString()}`)
  }

  const addTermFilter = value => {
    const newTermFilters = setFromString(termFilters, orDelimiter)
    newTermFilters.add(value)
    searchParams.set("term", Array.from(newTermFilters).join(orDelimiter))
    searchParams.set("page", 1)
    navigate(`${pathname}?${searchParams.toString()}`)
  }

  const removeTermFilter = value => {
    const newTermFilters = setFromString(termFilters, orDelimiter)
    newTermFilters.delete(value)
    searchParams.set("term", Array.from(newTermFilters).join(orDelimiter))
    searchParams.set("page", 1)
    navigate(`${pathname}?${searchParams.toString()}`)
  }

  const dateFilters = searchParams.get("date")

  const addDateFilter = value => {
    const newDateFilters = setFromString(dateFilters, orDelimiter)
    newDateFilters.add(value)
    searchParams.set("date", Array.from(newDateFilters).join(orDelimiter))
    searchParams.set("page", 1)
    navigate(`${pathname}?${searchParams.toString()}`)
  }

  const removeDateFilter = value => {
    const newDateFilters = setFromString(dateFilters, orDelimiter)
    newDateFilters.delete(value)
    searchParams.set("date", Array.from(newDateFilters).join(orDelimiter))
    searchParams.set("page", 1)
    navigate(`${pathname}?${searchParams.toString()}`)
  }

  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)
  const [ready, setReady] = useState(false)
  const handleSolrResults = results => {
    setData(results)
    setLoading(false)
    setReady(true)
  }

  useEffect(() => {
    const typeFilterSet = setFromString(typeFilters, orDelimiter)
    const termFilterSet = setFromString(termFilters, orDelimiter)
    const incomingDateFilterSet = setFromString(dateFilters, orDelimiter)
    const dateFilterSet = new Set()
    incomingDateFilterSet.forEach(d => {
      dateFilterSet.add({ from: d, to: `${d}+1MONTH` })
    })

    const abortController = new AbortController()
    const fetchData = () => {
      setLoading(true)
      solrQuery
        .search(query || "*")
        .perPage(perPage)
        .page(page)
      if (!fixedSort) solrQuery.sortBy(sort)
      if (!fixedType)
        solrQuery.filterByField("ss_federated_type", typeFilterSet)
      solrQuery
        .filterByField("sm_federated_terms", termFilterSet)
        .filterByRange("ds_federated_date", dateFilterSet)
        .execute({ signal: abortController.signal, success: handleSolrResults })
    }
    fetchData()
    return () => {
      abortController.abort()
    }
  }, [
    query,
    perPage,
    page,
    sort,
    typeFilters,
    termFilters,
    dateFilters,
    fixedSort,
    fixedType,
    solrQuery,
  ])

  const { numFound = 0, results = [], pagination = {}, facets = {} } = data
  const { fieldFacets = {}, rangeFacets = {} } = facets
  const typeFacets = fieldFacets["ss_federated_type"]
  const termFacets = fieldFacets["sm_federated_terms"]
  const groupedTermFacets = termFacets
    ? Object.keys(termFacets).reduce((acc, key) => {
        const [vocabulary] = key.split(">")
        if (!acc[vocabulary]) acc[vocabulary] = {}
        acc[vocabulary][key] = termFacets[key]
        return acc
      }, {})
    : {}

  const dateFacets = reverseSortObject(rangeFacets["ds_federated_date"])

  const formatDateFacet = date => {
    return dayjs(new Date(date), "YYYY-MM-DD")
      .add(1, "month")
      .format("MMMM YYYY")
  }

  const formatTermFacet = term => {
    return term.split(">")[1]
  }

  const facetGroupComponents = () => {
    const typeFacetGroupComponent = (
      <FacetGroup
        label="Type"
        key="Type"
        field="ss_federated_type"
        facets={typeFacets}
        filters={typeFilters}
        onFacetSelect={addTypeFilter}
        onFacetUnselect={removeTypeFilter}
      />
    )
    const dateFacetGroupComponent = (
      <FacetGroup
        label="Date"
        key="Date"
        formatter={formatDateFacet}
        field="ds_federated_date"
        facets={dateFacets}
        filters={dateFilters}
        onFacetSelect={addDateFilter}
        onFacetUnselect={removeDateFilter}
      />
    )
    const termFacetGroupComponents = Object.entries(groupedTermFacets).reduce(
      (acc, [vocabulary, termFacets]) => {
        acc[vocabulary] = (
          <FacetGroup
            key={vocabulary}
            label={vocabulary}
            formatter={formatTermFacet}
            field="sm_federated_terms"
            facets={termFacets}
            filters={termFilters}
            onFacetSelect={addTermFilter}
            onFacetUnselect={removeTermFilter}
          />
        )
        return acc
      },
      {}
    )
    const sortedFacetGroupComponents = sortObject({
      ...{ Date: dateFacetGroupComponent },
      ...termFacetGroupComponents,
    })
    const allFacetGroupComponents = {
      ...{ Type: typeFacetGroupComponent },
      ...sortedFacetGroupComponents,
    }
    return Object.entries(allFacetGroupComponents)
      .filter(
        ([groupName, component]) => !excludeFacetGroups.includes(groupName)
      )
      .map(([groupName, component]) => component)
  }

  const [open, setOpen] = useState(false)

  // TODO: Should we only render pagination when pagination.pages > 1?
  let hasPagination = pagination.pages > 1 ? true : false
  let resultStart =
    pagination.page * pagination.perPage - pagination.perPage + 1
  let resultsEnd = resultStart + pagination.perPage - 1
  // No Results to Search
  const noResultsType =
    solrQuery.fixedFieldFilters?.ss_federated_type?.[0] ?? ""

  // Check if facets are empty
  const isFacetsEmpty = () => {
    const typeFacetCounts = typeFacets
      ? Object.entries(typeFacets).filter(([value, count]) => count !== 0)
      : []

    const termFacetCounts = termFacets
      ? Object.entries(termFacets).filter(([value, count]) => count !== 0)
      : []

    const dateFacetCounts = dateFacets
      ? Object.entries(dateFacets).filter(([value, count]) => count !== 0)
      : []

    if (
      typeFacetCounts.length === 0 &&
      termFacetCounts.length === 0 &&
      dateFacetCounts.length === 0
    ) {
      return true
    } else {
      return false
    }
  }

  return (
    <div className="search-interface">
      <div className="top-controls">
        <SearchForm location={location} numFound={numFound} />
        <SearchSort location={location} sortOptions={sortOptions} />
        {loading && (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        )}
      </div>
      <div className="row">
        {isFacetsEmpty() ? (
          <div className="search-facets col-md-4">
            {numFound > 0 ? (
              <div className="mb-5 meta-box">
                <NoResults
                  type={noResultsType}
                  customMessage={
                    "There are currently no filters available for this search."
                  }
                />
              </div>
            ) : (
              ""
            )}
          </div>
        ) : (
          <div className="search-facets col-md-4">
            <Button
              className="collapse-facets-btn"
              aria-controls="collapse-facets"
              aria-expanded={open}
              onClick={() => setOpen(!open)}
            >
              <span className="collapse-facets-label">
                {open ? `Hide filters` : `Show filters`}
              </span>
              <span
                className={`plus-minus-toggle ${!open && `collapsed`}`}
              ></span>
            </Button>

            <Collapse in={open}>
              <div id="collapse-facets" className="mb-5">
                <MetaBox>
                  <a href="#search-result-area" className="visually-hidden">
                    Skip to search results
                  </a>
                  <h2 className="visually-hidden">Search facets</h2>
                  {facetGroupComponents()}
                </MetaBox>
              </div>
            </Collapse>
          </div>
        )}

        {/* No Search Results */}
        {ready && numFound === 0 && (
          <div
            className="search-content col-md-8 pl-md-5 main-content"
            aria-atomic="true"
            tabIndex="-1"
            aria-busy={loading}
            aria-live="assertive"
          >
            <h2 className="mt-0">
              {/* If queryAsTyped not null, display, else display searched facets */}
              {!loading &&
                queryAsTyped &&
                `0 search results for "${queryAsTyped}"`}
              {/* Check if queried by facets and list those out as search criteria */}
              {!loading &&
                searchParams &&
                !queryAsTyped &&
                "0 search results for the search criteria"}
            </h2>
          </div>
        )}

        {/* Has Search Results */}
        <div className="search-content col-md-8 pl-md-5 main-content">
          {numFound > 0 && query && (
            <div
              aria-atomic="true"
              tabIndex="-1"
              aria-busy={loading}
              aria-live="assertive"
            >
              <h2>
                {!loading && `${numFound} search results for "${queryAsTyped}`}"
              </h2>
            </div>
          )}
          <div
            id="search-result-area"
            className={`search-results ${
              hasPagination ? "has-pagination" : ""
            }`}
          >
            <p
              aria-live="polite"
              aria-atomic="true"
              className="pagination-showing-info"
            >
              Showing {resultStart}-
              {resultsEnd > numFound ? numFound : resultsEnd} of {numFound}{" "}
              results
            </p>
            {!loading && numFound > 0 && (
              <ul>{render(results, queryAsTyped)}</ul>
            )}
          </div>
          <SearchPagination location={location} pagination={pagination} />
        </div>
      </div>
    </div>
  )
}

export default SearchInterface
