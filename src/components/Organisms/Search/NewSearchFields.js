/*
 * Use this component to add new fields to the Solr search in custom Sitespro sites.
 * For fields that will be added to all sites, add to the solr.js file.
 */
const NewSearchFields = {
  //use this to add new fields to create solr search
  fieldList: [
    //example
    // "ss_newSolrField"
  ],

  //use this to assign the new solr field a name
  normalizeFields: d => {
    return {
      //example
      // newSolrField: d.newSolrField
    }
  },
}

export default NewSearchFields
