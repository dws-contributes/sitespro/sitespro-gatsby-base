import React, { useState } from "react"
import { navigate } from "gatsby"
import { MdSearch } from "react-icons/md"

const SearchForm = ({ location }) => {
  const { pathname, search } = location
  const searchParams = new URLSearchParams(search)
  const query = searchParams.get("q")
  const [value, setValue] = useState(query)

  const submitSearch = e => {
    e.preventDefault()
    const newQuery = e.target.querySelector("[name=q]").value
    const searchParams = new URLSearchParams(search)

    let newSearchParams
    if (newQuery !== query) {
      newSearchParams = new URLSearchParams({})
    } else {
      newSearchParams = searchParams
    }

    newSearchParams.set("q", newQuery)
    newSearchParams.set("page", 1)
    navigate(`${pathname}?${newSearchParams.toString()}`)
  }

  const resetSearch = e => {
    e.preventDefault()
    e.target.querySelector("[name=q]").value = ""
    setValue("")
    navigate(`${pathname}`)
  }
  return (
    <div className="search-form">
      <form action={pathname} onSubmit={submitSearch} onReset={resetSearch}>
        <input
          className="form-control"
          type="search"
          name="q"
          placeholder="Search"
          aria-label="Search keywords"
          value={value}
          onChange={e => setValue(e.target.value)}
        />
        <button type="submit" className="btn btn-primary" aria-label="Search">
          <MdSearch />
        </button>

        {searchParams.size >= 1 && (
          <button type="reset" className="btn button" aria-label="Reset search">
            <span>Reset</span>
          </button>
        )}
      </form>
    </div>
  )
}

export default SearchForm
