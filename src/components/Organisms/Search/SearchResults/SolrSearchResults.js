import React from "react"
import { SearchInterface, createSolrQuery } from "../../Search"

const SolrSearchResults = ({ location, navigate }) => {
  const perPage = 20 // TODO: get from graphql

  const solrQuery = createSolrQuery({
    facetFields: ["ss_federated_type", "sm_federated_terms"],
    facetRanges: [
      {
        field: "ds_federated_date",
        start: "NOW/MONTH-10YEAR",
        end: "NOW/MONTH+10YEAR",
        gap: "+1MONTH",
      },
    ],
  })

  return (
    <SearchInterface
      location={location}
      navigate={navigate}
      perPage={perPage}
      sortOptions={[
        { label: "Sort by Relevance", value: "" },
        { label: "Sort by Date (newest)", value: "ds_federated_date desc" },
      ]}
      excludeFacetGroups={["Event Status", "Scholars@Duke VivoTypes"]}
      solrQuery={solrQuery}
    />
  )
}

export default SolrSearchResults
