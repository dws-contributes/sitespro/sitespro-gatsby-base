import React, { useEffect } from "react"
import "./GoogleSearch.scss"

const GoogleSearchResults = ({ gsearchID }) => {
  useEffect(() => {
    try {
      ;(function () {
        var cx = `${gsearchID}`
        var gcse = document.createElement("script")
        gcse.type = "text/javascript"
        gcse.async = true
        gcse.src = "https://cse.google.com/cse.js?cx=" + cx
        var s = document.getElementsByTagName("script")[0]
        s.parentNode.insertBefore(gcse, s)
      })()
    } catch (e) {
      console.log("GCE Error")
    }
  }, [gsearchID])

  return (
    <section className="google-search-wrapper">
      <div className="gcse-search"></div>
    </section>
  )
}

export default GoogleSearchResults
