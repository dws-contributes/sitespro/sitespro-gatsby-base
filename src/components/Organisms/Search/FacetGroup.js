import React, { useState } from "react"
import { setFromString } from "components/_utils/Set"
import slugify from "components/_utils/slugify"
import { orDelimiter } from "."

const FacetGroup = ({
  field,
  label,
  formatter = i => i,
  facets,
  filters,
  onFacetSelect,
  onFacetUnselect,
  showInitial = 5,
}) => {
  const [showMore, setShowMore] = useState(false)
  if (!facets) return null
  const filterSet = setFromString(filters, orDelimiter)

  const facetCounts = Object.entries(facets).filter(
    ([value, count]) => count !== 0 || filterSet.has(value)
  )

  if (label !== "Date")
    facetCounts.sort(([valueA], [valueB]) => valueA.localeCompare(valueB))

  if (facetCounts.length === 0) return null

  const selectFacetFilter = e => {
    if (e.target.checked) {
      onFacetSelect(e.target.value)
    } else {
      onFacetUnselect(e.target.value)
    }
  }

  const showMoreFacets = e => {
    e.preventDefault()
    setShowMore(true)
  }

  const hideMoreFacets = e => {
    e.preventDefault()
    setShowMore(false)
  }

  const initialFacets = facetCounts.slice(0, showInitial)
  const selectedFacets = facetCounts.filter(([value, count]) => {
    const isInitial = initialFacets.filter(([v, _]) => v === value).length === 1
    return filterSet.has(value) && !isInitial
  })
  const moreFacets = facetCounts.slice(showInitial)
  const hasMore = moreFacets.length > 0

  const facetComponent = ([facetLabel, count]) => {
    let checked = filterSet.has(facetLabel)
    let key = slugify(`facet-${field}-${facetLabel}`)
    return (
      <li className="form-check" key={key}>
        <input
          id={`${key}-checkbox`}
          name={`${field}_filter`}
          type="checkbox"
          checked={checked}
          value={facetLabel}
          onChange={selectFacetFilter}
        />
        <label htmlFor={`${key}-checkbox`} title={facetLabel}>
          <span className="value">{formatter(facetLabel)}</span>
        </label>
      </li>
    )
  }

  const labelOverride = () => {
    if (label === "Project Start Timeframe") {
      return "Start Timeframe"
    }

    if (label === "Project End Timeframe") {
      return "End Timeframe"
    }

    return label
  }

  return (
    <div className="search-facet-group" id={slugify(label) + "-facet-group"}>
      <fieldset>
        <legend className="mb-0">
          <h3>{labelOverride()}</h3>
        </legend>
        <ul>
          {initialFacets.map(facetComponent)}

          {hasMore && !showMore && (
            <>
              {selectedFacets.map(facetComponent)}
              <li>
                <button onClick={showMoreFacets}>
                  {moreFacets.length - selectedFacets.length} more...
                </button>
              </li>
            </>
          )}

          {hasMore && showMore && moreFacets.map(facetComponent)}

          {hasMore && showMore && (
            <li>
              <button onClick={hideMoreFacets}>Show less</button>
            </li>
          )}
        </ul>
      </fieldset>
    </div>
  )
}

export default FacetGroup
