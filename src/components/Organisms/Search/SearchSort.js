import React from "react"
import { navigate } from "gatsby"

const SearchSort = ({ location, sortOptions }) => {
  if (!sortOptions) return null

  const { pathname, search } = location
  const searchParams = new URLSearchParams(search)
  const currentSort = searchParams.get("sort") || ""

  const handleSort = e => {
    const newSort = e.target.value
    const newSearchParams = new URLSearchParams(searchParams)
    if (newSort) {
      newSearchParams.set("sort", newSort)
    } else {
      newSearchParams.delete("sort")
    }
    navigate(`${pathname}?${newSearchParams.toString()}`)
  }

  return (
    <select
      name="sort"
      className="search-sort custom-select"
      value={currentSort}
      onChange={handleSort}
      onBlur={handleSort}
    >
      {sortOptions.map(({ label, value }) => {
        return (
          <option key={`sort-option-${value}`} value={value}>
            {label}
          </option>
        )
      })}
    </select>
  )
}

export default SearchSort
