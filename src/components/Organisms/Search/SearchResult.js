import React, { useContext } from "react"
import Link from "components/Atoms/Link/Link"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import decode from "components/_utils/decode"
import { Accordion, AccordionContext, Card } from "react-bootstrap"
import { useAccordionButton } from "react-bootstrap/AccordionButton"
import { emphasizeString } from "components/_utils/emphasizeString"

const SearchResult = ({ result, query }) => {
  function CustomToggle({ children, eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext)

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    )

    const isCurrentEventKey = activeEventKey === eventKey

    return (
      <button
        type="button"
        className={`accordion-toggle ${isCurrentEventKey ? `current` : ``}`}
        aria-expanded={isCurrentEventKey ? true : false}
        tabIndex="0"
        onClick={decoratedOnClick}
      >
        {children}
      </button>
    )
  }

  return (
    <li className="result" key={result.id}>
      <div className="field-label">{result.type}</div>
      {result.type !== "FAQ" ? (
        <>
          <div className="title">
            <Link to={result.url}>{decode(result.title)}</Link>
          </div>
          <div className="date">
            <DateProcessor startDate={result.date} />
          </div>
          {result.highlight && !result.url.includes("/secure/") && (
            <div
              className="highlight"
              dangerouslySetInnerHTML={{ __html: result.highlight }}
            />
          )}
        </>
      ) : (
        <>
          <div className="faq-card-wrapper">
            <Accordion key={`faq-${result.id}`}>
              <Card>
                <CustomToggle eventKey={`faq-${result.id}`}>
                  <span className="accordion-icon"></span>
                  <span
                    className="mt-0 mb-0"
                    dangerouslySetInnerHTML={{
                      __html: emphasizeString(decode(result.title), query),
                    }}
                  />
                </CustomToggle>
                <Accordion.Collapse eventKey={`faq-${result.id}`}>
                  <Card.Body>
                    <div
                      className="highlight"
                      dangerouslySetInnerHTML={{
                        __html: emphasizeString(decode(result.overview), query),
                      }}
                    />
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </div>
        </>
      )}
    </li>
  )
}

export default SearchResult
