import React from "react"
import Link from "components/Atoms/Link/Link"

const SearchPagination = ({ location, pagination = {}, numLinks = 9 }) => {
  const { pathname, search } = location

  const { page, pages } = pagination
  const currentPage = parseInt(page, 10)
  const numPages = parseInt(pages, 10)
  if (numPages === 1) return null

  let firstLink = 1
  let lastLink = numPages
  if (numPages > numLinks) {
    const delta = Math.floor(numLinks / 2)
    const lowerBound = currentPage - delta
    const upperBound = currentPage + delta
    if (upperBound > numPages) {
      firstLink = numPages - numLinks + 1
      lastLink = numPages
    } else {
      firstLink = lowerBound > 0 ? lowerBound : 1
      lastLink = lowerBound > 0 ? upperBound : numLinks
    }
  }
  let previousLink = currentPage > 1 ? currentPage - 1 : null
  let nextLink = currentPage < lastLink ? currentPage + 1 : null

  let pageList = []
  for (let i = firstLink; i <= lastLink; i++) {
    pageList.push(i)
  }

  const pageUrl = pageNum => {
    const searchParams = new URLSearchParams(search)
    searchParams.set("page", pageNum)
    return `${pathname}?${searchParams.toString()}`
  }

  const pageLink = (pageNum, index, label) => {
    const linkContent = label || pageNum
    return (
      <Link
        className={`pagination-link ${
          pageNum === currentPage ? "active" : "inactive"
        }`}
        key={`page-${pageNum}-${index}`}
        to={pageUrl(pageNum)}
        aria-label={"Page " + pageNum}
      >
        {linkContent}
      </Link>
    )
  }

  const ellipsis = <span>...</span>

  const pageLinks = pageList.map((l, i) => pageLink(l, i, l))

  return (
    <nav className="search-pagination" aria-label="Search Results Page">
      {previousLink && (
        <span className="increment previous">
          {pageLink(previousLink, "prev", "PREVIOUS")}
        </span>
      )}
      <span className="pages">
        {firstLink > 1 && ellipsis}
        {pageLinks}
        {lastLink < numPages && ellipsis}
      </span>
      {nextLink && (
        <span className="increment next">
          {pageLink(nextLink, "next", "NEXT")}
        </span>
      )}
    </nav>
  )
}

export default SearchPagination
