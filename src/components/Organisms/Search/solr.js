import NewsSearchFields from "./NewSearchFields"

const parseCounts = counts => {
  let parsedCounts = {}
  while (counts.length > 0) {
    parsedCounts[counts.shift()] = counts.shift()
  }

  return parsedCounts
}

const normalizeResults = (data, page, perPage) => {
  const { response, facet_counts, highlighting } = data
  const { docs, numFound } = response

  const results = docs.map(d => {
    // Set overview
    let overview = ""
    overview = d.twm_X3b_en_federated_content
      ? d.twm_X3b_en_federated_content.join(" ")
      : d.ss_federated_content
      ? d.ss_federated_content
      : d.twm_X3b_und_federated_content?.[0]

    //Set image default as feed image then overide with feature image if it exists
    let image_alt = d.ss_federated_image_alt
      ? d.ss_federated_image_alt
      : d.ss_federated_feed_image_alt
    let image = d.ss_federated_feed_image

    if (d.ss_federated_image) {
      image = d.ss_federated_image?.split("|")[0]
        ? d.ss_federated_image?.split("|")[0]
        : d.ss_federated_image?.split("|")[1]
    }

    const normalizeFields = {
      id: d.id,
      type: d.ss_federated_type,
      overview: overview,
      title: d.ss_federated_title,
      title_override: d.ss_federated_title_override,
      subtitle: d.ss_federated_subtitle,
      location: d.ss_field_location_text,
      location_url: d.ss_field_location_link,
      date: d.ds_federated_date,
      url: d.ss_additional_url ? d.ss_additional_url : d.ss_urls,
      terms: d.sm_federated_terms,
      contents: d.tm_X3b_en_rendered_item
        ? d.tm_X3b_en_rendered_item
        : d.tm_X3b_und_rendered_item,
      highlight: Object.values(highlighting[d.id]).join("..."),
      image: image,
      image_alt: image_alt,
      author: d.ss_federated_author,
      email: d.ss_federated_email,
      phone: d.ss_federated_phone,
      source: d.ss_federated_source,
    }

    return { ...normalizeFields, ...NewsSearchFields.normalizeFields(d) }
  })

  let pages = 1
  if (perPage > 0) {
    pages = Math.ceil(numFound / perPage)
  }

  const pagination = {
    page,
    perPage,
    pageSize: docs.length,
    pages: pages,
  }

  const { facet_fields, facet_ranges } = facet_counts

  const fieldFacets = Object.entries(facet_fields).reduce((acc, ff) => {
    const [field, counts] = ff
    acc[field] = parseCounts(counts)
    return acc
  }, {})

  const rangeFacets = Object.entries(facet_ranges).reduce((acc, ff) => {
    const [field, { counts }] = ff
    acc[field] = parseCounts(counts)
    return acc
  }, {})

  const facets = {
    fieldFacets,
    rangeFacets,
  }

  return { numFound, pagination, results, facets }
}

class SolrQuery {
  constructor({
    selectUrl,
    defaultSearchField,
    fieldList,
    facetFields,
    facetRanges,
    fixedSort,
    fixedFieldFilters,
    fixedRangeFilters,
    fixedDelimiter,
  }) {
    this.selectUrl = selectUrl
    this.defaultSearchField = defaultSearchField
    this.fieldList = fieldList || []
    this.facetFields = facetFields || []
    this.facetRanges = facetRanges || []
    this.fixedSort = fixedSort
    this.fixedFieldFilters = fixedFieldFilters || {}
    this.fixedRangeFilters = fixedRangeFilters || {}
    this.query = null
    this.rows = -1
    this.start = 0
    this.sort = null
    this.requestedPage = 1
    this.fieldFilters = {}
    this.rangeFilters = {}
    this.fixedDelimiter = fixedDelimiter || " OR "
  }

  search(query) {
    this.query = query
    return this
  }

  perPage(rows) {
    this.rows = rows
    return this
  }

  page(page) {
    this.requestedPage = page
    if (this.rows > 0) {
      this.start = this.rows * (page - 1)
    } else {
      this.start = 0
    }
    return this
  }

  sortBy(sortBy) {
    if (sortBy) {
      this.sort = sortBy
    }
    return this
  }

  filterByField(field, values) {
    this.fieldFilters[field] = values
    return this
  }

  filterByRange(field, values) {
    this.rangeFilters[field] = values
    return this
  }

  execute({ signal, success }) {
    fetch(`${this.selectUrl}?${this._buildQueryString()}`, { signal })
      .then(response => response.json())
      .then(data =>
        success(normalizeResults(data, this.requestedPage, this.rows))
      )
  }

  _quoteValue(val) {
    return `"${val}"`
  }

  _escapeValue(val) {
    const reservedStrings = [
      "\\",
      "+",
      "-",
      "&&",
      "||",
      "!",
      "{",
      "}",
      "[",
      "]",
      "^",
      "~",
      "?",
      "(",
      ")",
    ]
    return reservedStrings.reduce((acc, s) => {
      // Use replaceAll if it's supported by the browser
      // If not, use a regular replace
      if (!String.prototype.replaceAll) acc = acc.replace(`${s}/g`, `\\${s}`)
      else acc = acc.replaceAll(s, `\\${s}`)
      return acc
    }, val)
  }

  _buildQueryString() {
    const params = new URLSearchParams({
      q: this._escapeValue(this.query.replace(/\+/g, " ")),
      qf: this.defaultSearchField,
      bq: "twm_X3b_en_federated_title_sort^2",
      pf: "twm_X3b_en_federated_title_sort^2",
      pf2: "twm_X3b_en_federated_title_sort^4",
      pf3: "twm_X3b_en_federated_title_sort^4",
      facet: "on",
      "facet.limit": -1,
      "facet.sort": "count",
      hl: "on",
      "hl.fl": "tm_X3b_en_rendered_item,tm_X3b_und_rendered_item",
      "hl.usePhraseHighlighter": "true",
      "hl.fragsize": 85,
      "hl.snippets": 3,
      rows: this.rows,
      start: this.start,
      defType: "edismax",
    })

    if (this.fieldList.length > 0) {
      params.append("fl", this.fieldList.join(","))
    }

    const sortValue = this.fixedSort || this.sort
    if (sortValue) {
      params.append("sort", sortValue)
    }

    this.facetFields.forEach(ff =>
      params.append("facet.field", `{!ex=${ff}}${ff}`)
    )

    this.facetRanges.forEach(({ field, start, end, gap }) => {
      params.append("facet.range", `{!ex=${field}}${field}`)
      let prefix = `f.${field}.facet.range`
      params.append(`${prefix}.start`, start)
      params.append(`${prefix}.end`, end)
      params.append(`${prefix}.gap`, gap)
    })

    for (let field in this.fieldFilters) {
      let values = Array.from(this.fieldFilters[field])
      if (values.length > 0) {
        const taxonomies = values.reduce((acc, value) => {
          const taxonomy = value.split(">")[0]
          if (!acc[taxonomy]) {
            acc[taxonomy] = []
          }
          acc[taxonomy].push(value)
          return acc
        }, {})

        const terms = Object.keys(taxonomies)
          .map(
            taxonomy =>
              `(${taxonomies[taxonomy].map(this._quoteValue).join(" OR ")})`
          )
          .join(" AND ")

        params.append("fq", `{!tag=${field}}${field}:(${terms})`)
      }
    }

    for (let field in this.fixedFieldFilters) {
      let values = Array.from(this.fixedFieldFilters[field])
      if (values.length > 0) {
        if (field === "sm_federated_terms") {
          params.append(
            "fq",
            `{!tag=${field}}${field}:(${values
              .map(this._quoteValue)
              .join(this.fixedDelimiter)})`
          )
        } else {
          params.append(
            "fq",
            `{!tag=${field}}${field}:(${values
              .map(this._quoteValue)
              .join(" OR ")})`
          )
        }
      }
    }

    for (let field in this.rangeFilters) {
      let values = Array.from(this.rangeFilters[field])
      if (values.length > 0) {
        params.append(
          "fq",
          `{!tag=${field}}${field}:(${values
            .map(({ from, to }) => `[${from} TO ${to}]`)
            .join(" OR ")})`
        )
      }
    }

    for (let field in this.fixedRangeFilters) {
      let values = Array.from(this.fixedRangeFilters[field])
      if (values.length > 0) {
        params.append(
          "fq",
          `{!tag=${field}}${field}:(${values
            .map(({ from, to }) => `[${from} TO ${to}]`)
            .join(" OR ")})`
        )
      }
    }

    return params.toString().replace(/\+/g, "%20") //toString() replaces spaces with '+', but we need '%20'
  }
}

const selectUrl =
  process.env.NODE_ENV === "development" ? "/solrsearch/search" : "/solrsearch"

const createSolrQuery = ({
  defaultSearchField = "twm_X3b_en_federated_title_sort tm_X3b_en_rendered_item tm_X3b_und_rendered_item",
  facetFields,
  facetRanges,
  fixedSort,
  fixedFieldFilters,
  fixedRangeFilters,
  fixedDelimiter,
}) => {
  const fieldList = [
    "id",
    "ss_federated_type",
    "ss_federated_content",
    "twm_X3b_en_federated_content",
    "twm_X3b_und_federated_content",
    "twm_X3b_en_federated_title_sort",
    "sm_federated_terms",
    "ss_federated_title",
    "ss_federated_title_override",
    "ss_federated_title_sort",
    "ss_federated_subtitle",
    "ss_federated_location",
    "ds_federated_date",
    "ss_urls",
    "ss_field_location_text",
    "ss_additional_url",
    "ss_field_location_link",
    "tm_X3b_en_rendered_item",
    "tm_X3b_und_rendered_item",
    "ss_federated_feed_image",
    "ss_federated_feed_image_alt",
    "ss_federated_image",
    "ss_federated_image_alt",
    "ss_federated_author",
    "ss_federated_email",
    "ss_federated_phone",
    "ss_federated_source",
  ]

  return new SolrQuery({
    selectUrl: selectUrl,
    defaultSearchField: defaultSearchField,
    fieldList: fieldList.concat(NewsSearchFields.fieldList),
    facetFields: facetFields,
    facetRanges: facetRanges,
    fixedSort: fixedSort,
    fixedFieldFilters: fixedFieldFilters,
    fixedRangeFilters: fixedRangeFilters,
    fixedDelimiter: fixedDelimiter,
  })
}

const orDelimiter = "-or-"
export { createSolrQuery, orDelimiter }
