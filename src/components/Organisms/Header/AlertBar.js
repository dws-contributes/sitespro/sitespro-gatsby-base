import React, { useState, useEffect } from "react"
import "./AlertBar.scss"

const AlertBar = () => {
  const [dukeAlertBar, setDukeAlertBar] = useState("")

  useEffect(() => {
    //Grab content from alertbar: when testing use: https://alertbar.oit.duke.edu/test/alert1.html
    fetch("https://alertbar.oit.duke.edu/alert.html")
      .then(response => response.text())
      .then(dukeAlertContent => {
        //Remove document.write from alertbar.
        dukeAlertContent = dukeAlertContent.replace(/\n/g, "")
        dukeAlertContent = dukeAlertContent.split("\\n").join(" ")
        dukeAlertContent = dukeAlertContent.replace("document.write('", "")
        dukeAlertContent = dukeAlertContent.replace("');", "")
        setDukeAlertBar(dukeAlertContent)
      })
      .catch(error => {
        console.error("Error fetching script:", error)
      })
  }, [])

  return (
    <div dangerouslySetInnerHTML={{ __html: dukeAlertBar }} id="alert-bar" />
  )
}

export default AlertBar
