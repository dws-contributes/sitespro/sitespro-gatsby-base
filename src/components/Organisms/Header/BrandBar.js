import React from "react"
import "./BrandBar.scss"

const BrandBar = () => {
  return (
    <div className="centennial centennial-brand-bar centennial-brand-bar--black">
      <a
        href="https://100.duke.edu/"
        className="centennial-brand-bar__link"
        title="Duke Centennial - Celebrating the past, inspiring the present and looking toward the future"
      >
        <img
          className="centennial-brand-bar__logo"
          src="https://assets.styleguide.duke.edu/cdn/logos/centennial/duke-centennial-white.svg"
          alt="Duke 100 Centennial logo"
        />
      </a>
    </div>
  )
}

export default BrandBar
