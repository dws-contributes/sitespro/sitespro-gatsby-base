import React from "react"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import PropTypes from "prop-types"
import Link from "components/Atoms/Link/Link"
import Heading from "components/Atoms/Heading/Heading"
import bem from "components/_utils/bem"
import FooterMenu from "components/Molecules/Menu/FooterMenu/FooterMenu"
import Copyright from "components/Atoms/Copyright/Copyright"
import Button from "components/Atoms/Button/Button"
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaYoutube,
  FaLinkedinIn,
} from "react-icons/fa"
import { MdPlace, MdLocalPhone } from "react-icons/md"
import { HiOutlineAtSymbol } from "react-icons/hi"
import { siteDataShape } from "hooks/SiteInfoData"
// import theme from "theme"
import "./Footer.scss"

const Footer = ({ siteData, block = "footer" }) => {
  const {
    prefix,
    name,
    addressLines,
    email,
    phoneNumber,
    additionalInfo,
    primaryCTA,
    secondaryCTA,
    ctaHeading,
    ctaDescription,
    facebookUri,
    instagramUri,
    linkedinUri,
    twitterUri,
    youtubeUri,
  } = siteData

  // @TODO finish later for the sub-theme
  // const footerBackground = theme.footerBackground ? `url(/${theme.footerBackground}` : null
  return (
    // <footer style={{backgroundImage: footerBackground}}>
    <footer>
      <Container>
        <Row>
          <Col>
            <h3 className="site_name" aria-level={2}>
              <Link to="/">
                {prefix} {name}
              </Link>
            </h3>
          </Col>
        </Row>
        <Row>
          <Col md="4" className="footer-contact">
            {addressLines.length > 0 && (
              <p className={bem(block, "address", [])}>
                <MdPlace />
                {addressLines.map(function (item, index) {
                  return <span key={`address_${index}`}>{item}</span>
                })}
              </p>
            )}

            {email && (
              <p className={bem(block, "email", [])}>
                <HiOutlineAtSymbol />
                <a href={`mailto:${email}`}>{email}</a>
              </p>
            )}

            {phoneNumber && (
              <p className={bem(block, "phone", [])}>
                <MdLocalPhone />
                <a href={`tel:${phoneNumber.replace(/[^\d]/g, "")}`}>
                  {phoneNumber}
                </a>
              </p>
            )}

            {additionalInfo && <p>{additionalInfo}</p>}
          </Col>
          <Col md="4">
            <FooterMenu />
          </Col>
          <Col lg="4" className={bem(block, "right", [])}>
            {/* Primary CTA */}
            {primaryCTA && (
              <Button link={primaryCTA.uri} showIcon={true} modifiers={["cta"]}>
                {primaryCTA.title}
              </Button>
            )}

            {/* Secondary CTA */}
            {ctaHeading && (
              <Heading level={4} className={bem(block, "cta-heading", [])}>
                {ctaHeading}
              </Heading>
            )}

            {ctaDescription && <p className="mb-0">{ctaDescription}</p>}

            {secondaryCTA && (
              <Button
                link={secondaryCTA.uri}
                showIcon={true}
                modifiers={["footer"]}
              >
                {secondaryCTA.title}
              </Button>
            )}

            {/* Social icons */}
            <div className={`${bem(block, "social-icons-wrapper", [])}`}>
              {facebookUri && (
                <a
                  href={facebookUri}
                  aria-label="Facebook link"
                  title="Facebook link"
                >
                  <FaFacebookF />
                </a>
              )}
              {twitterUri && (
                <a
                  href={twitterUri}
                  aria-label="Twitter link"
                  title="Twitter link"
                >
                  <FaTwitter />
                </a>
              )}
              {instagramUri && (
                <a
                  href={instagramUri}
                  aria-label="Instagram link"
                  title="Instagram link"
                >
                  <FaInstagram />
                </a>
              )}
              {youtubeUri && (
                <a
                  href={youtubeUri}
                  aria-label="YouTube link"
                  title="YouTube link"
                >
                  <FaYoutube />
                </a>
              )}
              {linkedinUri && (
                <a
                  href={linkedinUri}
                  aria-label="LinkedIn link"
                  title="LinkedIn link"
                >
                  <FaLinkedinIn />
                </a>
              )}
            </div>
          </Col>
        </Row>
      </Container>
      <hr className="separator" />
      <Container>
        <Row className="align-items-center">
          <Col md="6" className="copyrightWrapper">
            <Copyright />
          </Col>
          <Col md="6">
            <div className="footer-bottom-menu">
              <a href="https://accessibility.duke.edu">Accessibility</a>
              <a href="https://oarc.duke.edu/privacy/duke-university-privacy-statement">
                Privacy Statement
              </a>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  )
}
Footer.propTypes = {
  siteData: PropTypes.exact(siteDataShape),
  block: PropTypes.string,
}

export default Footer
