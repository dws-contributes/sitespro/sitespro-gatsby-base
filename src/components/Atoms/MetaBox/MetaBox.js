import React from "react"
import "./MetaBox.scss"

const MetaBox = ({ as = "div", className = "", children }) => {
  let ContainerTag = `${as}`

  return (
    <ContainerTag className={className + " meta-box"}>{children}</ContainerTag>
  )
}

export default MetaBox
