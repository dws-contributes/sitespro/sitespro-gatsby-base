import React from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Divider from "../Divider/Divider.js"
import { MdKeyboardBackspace } from "react-icons/md"

import "./Pagination.scss"

const Pagination = ({ currentPage, numPages, url }) => {
  const isFirst = currentPage === 1
  const isLast = currentPage === numPages
  const prevPage =
    currentPage - 1 === 1
      ? `/${url}/`
      : `/${url}/${(currentPage - 1).toString()}`
  const nextPage = `/${url}/${(currentPage + 1).toString()}`

  return (
    <>
      <Divider modifiers={["long"]} />

      <nav aria-label="Paginated content" className={"col-12"}>
        <ul className="pagination-list">
          {!isFirst && (
            <Link to={prevPage} rel="prev" className="pagination__previous">
              <MdKeyboardBackspace
                className={bem("pagination", "icon", ["previous"])}
              />{" "}
              Previous Page
            </Link>
          )}
          {Array.from({ length: numPages }, (_, i) => (
            <li key={`pagination-number${i + 1}`}>
              <Link
                to={`/${url}/${i === 0 ? "" : i + 1}`}
                className={`${currentPage === i + 1 ? "active" : ""}`}
              >
                {i + 1}
              </Link>
            </li>
          ))}
          {!isLast && (
            <Link to={nextPage} rel="next" className="pagination__next">
              Next Page{" "}
              <MdKeyboardBackspace
                className={bem("pagination", "icon", ["next"])}
              />
            </Link>
          )}
        </ul>
      </nav>
    </>
  )
}

Pagination.propTypes = {
  prevPage: PropTypes.string,
  nextPage: PropTypes.string,
  currentPage: PropTypes.number,
  numPages: PropTypes.number,
  url: PropTypes.string,
}

export default Pagination
