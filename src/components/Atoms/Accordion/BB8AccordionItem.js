import { Accordion, AccordionContext, Card } from "react-bootstrap"
import { useAccordionButton } from "react-bootstrap/AccordionButton"
import React, { useContext } from "react"
import PropTypes from "prop-types"

const BB8AccordionItem = ({ children, id, heading }) => {
  function CustomToggle({ children, eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext)

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    )

    const isCurrentEventKey = activeEventKey === eventKey

    return (
      <button
        type="button"
        className={`accordion-toggle ${isCurrentEventKey ? `current` : ``}`}
        aria-expanded={isCurrentEventKey ? true : false}
        tabIndex="0"
        onClick={decoratedOnClick}
      >
        {children}
      </button>
    )
  }

  return (
    <Card>
      <CustomToggle eventKey={id}>
        <span className="accordion-icon"></span>
        <span className="mt-0 mb-0">{heading}</span>
      </CustomToggle>

      <Accordion.Collapse eventKey={id}>
        <Card.Body>{children}</Card.Body>
      </Accordion.Collapse>
    </Card>
  )
}

export default BB8AccordionItem

BB8AccordionItem.propTypes = {
  children: PropTypes.any,
  id: PropTypes.string,
  handleClick: PropTypes.func,
  heading: PropTypes.string,
}
