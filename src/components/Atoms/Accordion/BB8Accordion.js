import { Accordion } from "react-bootstrap"
import PropTypes from "prop-types"
import React from "react"

const BB8Accordion = ({ defaultActiveKey, children }) => {
  return (
    <Accordion defaultActiveKey={defaultActiveKey} flush>
      {children}
    </Accordion>
  )
}

export default BB8Accordion

BB8Accordion.propTypes = {
  defaultActiveKey: PropTypes.string,
}
