import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import dayjs from "dayjs"

const Time = ({ startTime, endTime, block = "time", element }) => {
  const startTimeFormatted = dayjs(startTime).format("h:mm a")
  const endTimeFormatted = dayjs(endTime).format("h:mm a")
  const allDay =
    startTimeFormatted === "12:00 am" && endTimeFormatted === "12:00 am"
      ? true
      : false

  return (
    <div className={bem(block, element)}>
      {startTime && (
        <p className={bem(block, "time")}>
          {allDay ? (
            <span>All day</span>
          ) : (
            <>
              <span>{dayjs(startTime).format("h:mm a")}</span>

              {endTime && (
                <span>
                  &nbsp;-&nbsp;
                  {dayjs(endTime).format("h:mm a")}
                </span>
              )}
            </>
          )}
        </p>
      )}
    </div>
  )
}

Time.propTypes = {
  startTime: PropTypes.string,
  endTime: PropTypes.string,
}

export default Time
