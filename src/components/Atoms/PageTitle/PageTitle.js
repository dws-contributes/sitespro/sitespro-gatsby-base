import React from "react"
import PropTypes from "prop-types"
import Header from "../Heading/Heading"
import Container from "react-bootstrap/Container"
import bem from "components/_utils/bem"
import "./PageTitle.scss"

const PageTitle = ({ modifiers, children }) => (
  <div className={bem("page-title", "wrapper", modifiers)}>
    <Container>
      <Header level={1} block="page-title">
        {children}
      </Header>
    </Container>
  </div>
)

PageTitle.propTypes = {
  Modifiers: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.string,
}

export default PageTitle
