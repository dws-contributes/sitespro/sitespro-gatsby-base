import React from "react"
import TestRenderer from "react-test-renderer"
import { render } from "@testing-library/react"
import { useSiteInfoData } from "hooks/SiteInfoData"
import Media from "./Media"
jest.mock("hooks/SiteInfoData")

const mockReactPlayer = jest.fn()
jest.mock("react-player/lazy", () => props => {
  mockReactPlayer(props)
  // Doesn't matter what we return, as long as its valid React. We're just testing that it is called with the right props
  return <div></div>
})

describe("Atoms > Media > Videos", () => {
  beforeAll(() => {
    useSiteInfoData.mockReturnValue({
      allBlockContentSiteInformation: {},
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  // TODO: Test image, document, audio
  it("renders a YouTube video", () => {
    const testNode = {
      type: "media__video",
      field_media_oembed_video: "https://youtu.be/HmZKgaHa3Fg",
    }

    render(Media(testNode))
    expect(mockReactPlayer).toHaveBeenCalledWith(
      expect.objectContaining({
        className: "react-player",
        controls: true,
        height: "100%",
        title: "Video player",
        url: "https://youtu.be/HmZKgaHa3Fg?",
        width: "100%",
      })
    )
  })

  // Ultimately we're testing the url prop here, to make sure the playlist parameter is removed
  it("removes the playlist and renders a YouTube video", () => {
    const testNode = {
      type: "media__video",
      field_media_oembed_video:
        "https://www.youtube.com/watch?v=OTJXAUZY9t0&list=PLLb6i1JpZ81BTy85aiosSuABgmEhmyzQg&index=3",
    }

    render(Media(testNode))
    expect(mockReactPlayer).toHaveBeenCalledWith(
      expect.objectContaining({
        className: "react-player",
        controls: true,
        height: "100%",
        title: "Video player",
        url: "https://www.youtube.com/watch?v=OTJXAUZY9t0&index=3",
        width: "100%",
      })
    )
  })

  it("renders a WarpWire video in an iframe", () => {
    const testNode = {
      type: "media__video",
      field_media_oembed_video: "https://warpwire.duke.edu/test",
    }

    const tree = TestRenderer.create(Media(testNode)).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
