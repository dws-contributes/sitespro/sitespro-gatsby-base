import React, { Fragment } from "react"
import PropTypes from "prop-types"
import Link from "components/Atoms/Link/Link"
import ReactPlayer from "react-player/lazy"
import { BB8Image, imageDetails } from "components/Atoms/Image/BB8Image"
import { useSiteInfoData } from "hooks/SiteInfoData"

const Media = node => {
  const siteInfoData = useSiteInfoData()
  let renderedMedia = null
  switch (node.type) {
    case "media__image":
      renderedMedia =
        node.ckeditor && node.src ? (
          <img
            key={node.src}
            src={node.src}
            alt={node.alt}
            srcSet={node.srcset}
            sizes={node.sizes}
            width={node.width}
            loading="lazy"
          />
        ) : (
          <BB8Image
            imageDetails={imageDetails(node)}
            targetedSizes={["large_3_2", "small_3_2"]}
          />
        )
      break
    case "media__video":
      let url = node.field_media_oembed_video
      const { host, search, origin, pathname } = new URL(url)
      const providerWarpwire = ["warpwire", "warpwire.duke.edu"].includes(host)
      // Remove the "list" parameter from YouTube embeds, otherwise react-player plays the first video in the playlist
      const isYoutube = url.match(
        /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/
      )

      if (isYoutube) {
        const urlSearchObj = new URLSearchParams(search)
        urlSearchObj.delete("list")
        url = origin + pathname + "?" + urlSearchObj.toString()
      }

      if (providerWarpwire) {
        renderedMedia = (
          <div className="ratio ratio-16x9">
            <iframe
              id="wwvideo"
              title="Video player"
              data-ww-id="apiTestFrame"
              src={url}
              frameBorder="0"
              scrolling="0"
              className="embed-responsive-item"
              allowFullScreen
            ></iframe>
          </div>
        )
      } else {
        renderedMedia = (
          <div className="player-wrapper ratio ratio-16x9">
            <ReactPlayer
              url={url}
              controls={true}
              className="react-player"
              title="Video player"
              width="100%"
              height="100%"
            />
          </div>
        )
      }
      break
    case "media__audio":
      renderedMedia = (
        <Fragment key={node.field_media_soundcloud}>
          <iframe
            src={`https://w.soundcloud.com/player/?url=${node.field_media_soundcloud}&auto_play=false&hide_related=false&show_reposts=false&show_teaser=true`}
            allow="autoplay"
            title="Audio player"
            width="100%"
            height="150"
            frameBorder="0"
          ></iframe>
          {node.field_transcript_link?.uri_alias && (
            <p className="caption">
              <Link to={node.field_transcript_link?.uri_alias}>
                Audio transcript
              </Link>
            </p>
          )}
        </Fragment>
      )
      break
    case "media__document":
      let path = node?.relationships?.field_document?.uri?.url
      if (path) {
        let adjustedPath = path.includes(siteInfoData.drupal_url)
          ? path.replace(siteInfoData.drupal_url, siteInfoData.cdn_url)
          : `${siteInfoData.cdn_url}${path}`

        renderedMedia = (
          <a key={path} href={adjustedPath}>
            {node?.name}
          </a>
        )
      } else renderedMedia = ""
      break
    default:
      renderedMedia = ""
      break
  }

  return <div>{renderedMedia}</div>
}

Media.propTypes = {
  node: PropTypes.object,
}

export default Media
