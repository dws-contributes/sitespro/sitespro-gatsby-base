/**
 * Exports a button component.
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Link from "components/Atoms/Link/Link"
import { MdKeyboardBackspace } from "react-icons/md"
import "./Button.scss"

const Button = ({
  link,
  block = "button",
  element = "",
  modifiers,
  children = null,
  showIcon = false,
}) => {
  // If the api returns <nolink> - don't show the icon
  if (link === "route:<nolink>") showIcon = false

  return (
    <Link to={link} className={bem(block, element, modifiers)}>
      <span className={bem(block, "text", modifiers)}>{children}</span>
      {showIcon ? <MdKeyboardBackspace className={bem(block, "icon")} /> : ""}
    </Link>
  )
}

Button.propTypes = {
  link: PropTypes.string,
  showIcon: PropTypes.bool,
  children: PropTypes.node,
  onClick: PropTypes.func,
}

export default Button
