/**
 *  Renders content coming from CKEditor
 */
import React, { useEffect } from "react"
import PropTypes from "prop-types"
import parse, { attributesToProps, domToReact } from "html-react-parser"
import Link from "components/Atoms/Link/Link"
import Media from "components/Atoms/Media/Media"
import "./CKEditor.scss"
import { useSiteInfoData } from "hooks/SiteInfoData"

function _recursiveDomElementParser(object, key, value, res = []) {
  if (object.hasOwnProperty(key) && object[key] === value) {
    res.push(object)
  } else if (object.children) {
    let tags = object.children.filter(obj => obj.type === "tag")
    tags.map(o => _recursiveDomElementParser(o, key, value, res))
  }
  return res
}

function processFileLink(object, siteInfoData) {
  // check if this is a link to file
  if (
    object?.attribs["data-media-source"] === "document" ||
    object?.attribs["type"] === "application/pdf" ||
    object?.attribs["data-entity-type"] === "file" ||
    ((object?.attribs["href"]?.includes(".pdf") ||
      // DOC + DOCX are handled with one includes
      object?.attribs["href"]?.includes(".doc") ||
      // XLS + XLSX are handled with one includes
      object?.attribs["href"]?.includes(".xls") ||
      object?.attribs["href"]?.includes(".xml")) &&
      object?.attribs["href"]?.startsWith("/")) ||
    ((object?.children[0]?.data === "file" ||
      object?.children[0]?.data === "document") &&
      object?.attribs["data-entity-type"] !== "node")
  ) {
    let path = object?.attribs?.href
    let adjustedPath = path.includes(siteInfoData.drupal_url)
      ? path.replace(siteInfoData.drupal_url, siteInfoData.cdn_url)
      : `${siteInfoData.cdn_url}${path}`
    object.attribs.href = adjustedPath
    object.attribs.to = adjustedPath
  }
  return object
}

const CKEditorContent = ({ content = "" }) => {
  let siteInfoData = useSiteInfoData()
  const tags = ["article", "a"]
  // Add image size class to the wrapping <figure>
  useEffect(() => {
    let mediaArticles = document.querySelectorAll("article.media")
    for (const item of mediaArticles) {
      let parentFigure = item.parentElement
      if (parentFigure.tagName.toLowerCase() === "figure") {
        item.classList.forEach(name => {
          if (
            !parentFigure.classList.contains(name) &&
            name.includes("view-mode")
          ) {
            parentFigure.className += " " + name
          }
        })
      }
    }
  }, [])

  const textarea_content_elements = content => {
    const options = {
      replace: node => {
        // Wrap tables
        if (node.type === "tag" && node.name === "table") {
          return (
            <div className="table-wrapper">
              <table>{domToReact(node.children, options)}</table>
            </div>
          )
        }

        // Headings
        if (node.type === "tag" && node.name.match(/^h[1-6]$/)) {
          const HeadingTag = node.name
          const htagClass = node.attribs["class"]

          // Check if the heading wraps a link
          // let hasLink = false
          // if (
          //   node.children &&
          //   ((node.children.type === "tag" && node.children.name === "a") ||
          //     (node.children[0] &&
          //       node.children[0].attribs &&
          //       node.children[0].attribs.href))
          // ) {
          //   hasLink = true
          // }

          return (
            <HeadingTag
              className={`${htagClass ? htagClass + " " : ""}heading`}
              role="heading"
              aria-level="2"
            >
              {domToReact(node.children, options)}
            </HeadingTag>
          )
        }
        // Links
        if (node.type === "tag" && node.name === "a") {
          let atag = _recursiveDomElementParser(node, "name", "a")[0]
          // If the link looks like a file, correct it here.
          atag = processFileLink(atag, siteInfoData)
          // look for anchor tags that actually go places and have content in them
          // example: I encountered <a id="something"></a> in a CKEditor, presumably for an anchor link
          let path = atag.attribs["href"]
          atag.attribs["to"] = path
          let linkNodeProps = attributesToProps(atag.attribs)
          // by converting the node to a React element, we get easy access to its props and children
          return (
            <Link key={path} {...linkNodeProps}>
              {domToReact(node.children, options)}
            </Link>
          )
        }

        let mediaNode = {}

        if (
          node.type === "tag" &&
          tags.indexOf(node.name) > -1 &&
          node.attribs["data-media-source"] !== "image"
        ) {
          // Media - video embed
          if (node.attribs["data-media-source"] === "video") {
            mediaNode = {
              type: "media__video",
              field_media_oembed_video: node.attribs["data-media-source-value"],
            }
            return Media(mediaNode)
          }

          // Media - audio embed
          if (node.attribs["data-media-source"] === "audio") {
            // let fid = node.attribs["data-media-source-value"]
            let transcriptTag = _recursiveDomElementParser(node, "name", "a")[0]
            let transcriptPath = null
            if (transcriptTag) {
              transcriptPath = transcriptTag?.attribs?.href
            }
            mediaNode = {
              type: "media__audio",
              field_media_soundcloud: node.attribs["data-media-source-value"],
              field_transcript_link: {
                uri_alias: transcriptPath,
              },
            }
            return Media(mediaNode)
          }
        }

        // Images
        if (node.type === "tag" && node.name === "img") {
          let origSrc = node.attribs["src"]
          let src = ""

          if (isValidUrl(origSrc)) {
            src = origSrc
          } else if (isValidUrl(siteInfoData.cdn_url + origSrc)) {
            src = siteInfoData.cdn_url + origSrc
          }

          // Split the srcSet so we can prepend it with the cdn_url in place
          let srcSet = node.attribs?.srcset?.split(",")

          srcSet?.forEach((srcImage, index) => {
            if (!srcImage.includes(siteInfoData.cdn_url)) {
              // Reassign the index to the cdn_url (make sure the src is trimmed of any whitespace)
              srcSet[index] = siteInfoData.cdn_url + srcImage.trim()
            }
          })

          mediaNode = {
            type: "media__image",
            ckeditor: true,
            alt: node.attribs.alt,
            src: src,
            width: node.attribs.width,
            srcset: srcSet,
            sizes: node.attribs.sizes,
          }
          return Media(mediaNode)
        }

        return node
      },
    }

    return parse(content, options)
  }

  return <>{content && textarea_content_elements(content)}</>
}

function isValidUrl(string) {
  let url
  try {
    url = new URL(string)
  } catch (_) {
    return false
  }
  return url.protocol === "http:" || url.protocol === "https:"
}

CKEditorContent.propTypes = {
  content: PropTypes.string,
}

export default CKEditorContent
