import React from "react"
import PropTypes from "prop-types"

const NoResults = ({ type, customMessage }) => {
  return (
    <p className="no-results delayed-appearance">
      {customMessage === ""
        ? `There are no ${type} items to show.`
        : customMessage}
    </p>
  )
}

NoResults.propTypes = {
  type: PropTypes.string,
  customMessage: PropTypes.string,
}

NoResults.defaultProps = {
  customMessage: "",
}

export default NoResults
