import React from "react"
import bem from "components/_utils/bem"
import PropTypes from "prop-types"
import { useSiteInfoData } from "hooks/SiteInfoData"
import "./StatusInfo.scss"

const StatusInfo = ({ nid, publishedStatus, modifiers, editPath = "node" }) => {
  const siteData = useSiteInfoData()
  return (
    <div className={bem("status-info", "", modifiers)}>
      {publishedStatus === false && <>This content is not published.</>}
      <a
        href={`${siteData.drupal_url}/${editPath}/${nid}/edit`}
        className="btn hide-icon"
        target="_parent"
      >
        Edit
      </a>
    </div>
  )
}

StatusInfo.propTypes = {
  nid: PropTypes.number,
  publishedStatus: PropTypes.bool,
  modifiers: PropTypes.arrayOf(PropTypes.string),
  editPath: PropTypes.string,
}

export default StatusInfo
