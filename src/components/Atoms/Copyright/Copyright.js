import React from "react"
import bem from "components/_utils/bem"
import PropTypes from "prop-types"
import DukeLogoWhite from "images/DukeLogoWhite.svg"
import "./Copyright.scss"

const Copyright = ({ block = "copyright", modifiers }) => {
  const Copyright = `div`
  return (
    <>
      <a
        className={"hide-icon " + bem(block, "logo-wrapper", [])}
        href="https://duke.edu/"
      >
        <img
          className={bem(block, "logo", [])}
          src={DukeLogoWhite}
          alt="Duke University"
        />
      </a>

      <Copyright className={bem(block, "", modifiers)}>
        Copyright &copy; {new Date().getFullYear()} Duke University
      </Copyright>
    </>
  )
}

Copyright.propTypes = {
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Copyright
