import React from "react"
import PropTypes from "prop-types"
import bem from "../../_utils/bem"
import "./Heading.scss"

const Heading = ({
  block = "heading",
  element = "",
  children,
  modifiers,
  level = "1",
  ariaLevel = "2",
}) => {
  const Tag = `h${level}`

  if (block === "page-title") {
    return <Tag className={bem(block, element, modifiers)}>{children}</Tag>
  }

  return (
    <Tag
      className={bem(block, element, modifiers)}
      role="heading"
      aria-level={ariaLevel}
    >
      {children}
    </Tag>
  )
}

Heading.propTypes = {
  block: PropTypes.string,
  element: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.any,
  level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
  ariaLevel: PropTypes.number,
}

export default Heading
