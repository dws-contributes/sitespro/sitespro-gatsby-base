import React from "react"
import "./SkipLink.scss"

const SkipLink = () => {
  return (
    <a href="#reach-skip-nav" data-reach-skip-nav-link target="_parent">
      Skip to main
    </a>
  )
}

export default SkipLink
