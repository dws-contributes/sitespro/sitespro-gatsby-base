/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useLocation } from "@reach/router"
import { useSiteInfoData } from "hooks/SiteInfoData"
import SchemaOrg from "./SchemaOrg"

const Seo = ({ metaTags }) => {
  const siteInfoData = useSiteInfoData()

  const twitterUsername = siteInfoData?.twitterUri
    ? siteInfoData?.twitterUri?.match(
        /^https?:\/\/(www\.)?twitter\.com\/(#!\/)?([^/]+)(\/\w+)*$/
      )?.[3]
    : ""

  function replaceImgSource(imgPath) {
    return imgPath?.replace(siteInfoData.drupal_url, siteInfoData.cdn_url)
  }

  const siteName = siteInfoData?.prefix
    ? siteInfoData?.prefix + ` ` + siteInfoData?.name
    : siteInfoData?.name
  const { pathname } = useLocation()
  const pageUrl = siteInfoData?.site_url + pathname

  metaTags.image =
    metaTags?.image || `${siteInfoData?.site_url}/duke-logo-blue.png`

  const image = metaTags?.ogImage
    ? replaceImgSource(metaTags?.ogImage)
    : replaceImgSource(metaTags?.image)

  const description = metaTags.description
    ? metaTags.description?.substring(0, 170)
    : metaTags.type === "Person"
    ? metaTags.title +
      "'s profile, publications, research topics, and co-authors"
    : metaTags.title + " - " + siteName

  const title = metaTags.ogTitle ? metaTags.ogTitle : metaTags.title

  return (
    <>
      <link
        rel="canonical"
        href={metaTags.originalSource ? metaTags.originalSource : pageUrl}
      />

      {/* General tags */}
      <meta name="description" content={description} />
      {image && <meta name="image" content={image} />}

      <meta name="publisher" content={siteName} />
      <meta name="author" content={siteName} />
      {metaTags.robots && <meta name="robots" content={metaTags.robots} />}

      {/* OpenGraph tags */}
      <meta property="og:site_name" name="site-name" content={siteName} />
      <meta property="og:url" content={pageUrl} />
      <meta
        property="og:type"
        content={metaTags.type ? metaTags.type : "website"}
      />
      <meta
        property="og:title"
        content={`${title ? title + " |" : ""} ${siteName}`}
      />
      <meta property="og:description" content={description} />
      {image && <meta property="og:image" content={image} />}

      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary" />
      {twitterUsername && (
        <meta name="twitter:site" content={`@${twitterUsername}`} />
      )}
      <meta
        name="twitter:title"
        content={`${title ? title + " |" : ""} ${siteName}`}
      />
      <meta name="twitter:description" content={description} />
      {image && <meta name="twitter:image" content={image} />}
      {/* <meta property="fb:app_id" content="21489041474" /> */}

      <SchemaOrg
        type={metaTags.type}
        author={metaTags.author}
        url={pageUrl}
        datePublished={metaTags.datePublished}
        dateModified={metaTags.dateModified}
        description={metaTags.description}
        image={image}
        title={metaTags.title}
        websiteUrl={siteInfoData?.site_url}
        websiteName={siteName}
        location={metaTags.location}
        startDate={metaTags.startDate}
        jobTitle={metaTags.jobTitle}
      />
    </>
  )
}

export default Seo

export function getMetaDetails(metadata) {
  if (!metadata) {
    return {}
  }
  const filteredMeta = metadata.filter(meta => meta.tag === "meta")
  const metaTitle = filteredMeta.find(
    title => title.attributes.name === "title"
  )
  const metaDescription = filteredMeta.find(
    description => description.attributes.name === "description"
  )
  const metaImage = metadata
    .filter(meta => meta.tag === "link")
    .find(image => image.attributes.rel === "image_src")
  const metaRobots = filteredMeta.find(
    robots => robots.attributes.name === "robots"
  )
  const metaAuthor = filteredMeta.find(
    author => author.attributes.property === "article:author"
  )
  const metaDatePublished = filteredMeta.find(
    datePublished =>
      datePublished.attributes.property === "article:published_time"
  )
  const metaDateModified = filteredMeta.find(
    dateModified => dateModified.attributes.property === "article:modified_time"
  )
  const metaOriginalSource = filteredMeta.find(
    originalSource => originalSource.attributes.name === "original-source"
  )
  const metaOGTitle = filteredMeta.find(
    ogTitle => ogTitle.attributes.property === "og:title"
  )
  const metaOGDescription = filteredMeta.find(
    ogDescription => ogDescription.attributes.property === "og:description"
  )

  const metaLocation = filteredMeta.find(
    location => location.attributes.property === "location"
  )

  const metaStartDate = filteredMeta.find(
    startDate => startDate.attributes.property === "startDate"
  )

  // Determine the Schema.org content type
  const metaOGContentType = filteredMeta.find(
    ogContentType => ogContentType.attributes.property === "og:type"
  )
  let schemaType = "WebPage"
  switch (metaOGContentType?.attributes?.content) {
    case "event":
      schemaType = "Event"
      break
    case "news":
      schemaType = "NewsArticle"
      break
    case "profile":
      schemaType = "Person"
      break
    case "blog-post":
      schemaType = "BlogPosting"
      break
    case "story":
      schemaType = "Article"
      break
    default:
      schemaType = "WebPage"
  }

  // Set OG Image default
  const metaOGImage = filteredMeta.find(
    ogImg => ogImg.attributes.property === "og:image"
  )
  let ogImage = metaOGImage?.attributes.content

  // Function to find image by property
  const findImageByProperty = property =>
    filteredMeta.find(item => item.attributes.property === property)?.attributes
      .content

  // Set OG Image for Page CT and Homepage
  if (
    (metaOGContentType?.attributes?.content === "page" ||
      metaOGContentType?.attributes?.content === "homepage") &&
    !ogImage
  ) {
    // Try finding thumbnail or hero image as fallbacks
    ogImage =
      findImageByProperty("page:thumbnail_image") ||
      findImageByProperty("page:hero_image")
  }

  return {
    title: metaTitle?.attributes.content,
    description: metaDescription?.attributes.content,
    image: metaImage?.attributes.href,
    robots: metaRobots?.attributes.content,
    type: schemaType,
    originalSource: metaOriginalSource?.attributes.content,
    datePublished: metaDatePublished?.attributes.content,
    dateModified: metaDateModified?.attributes.content,
    author: metaAuthor?.attributes.content,
    ogTitle: metaOGTitle?.attributes.content,
    ogDescription: metaOGDescription?.attributes.content,
    ogImage: ogImage,
    location: metaLocation?.attributes.content,
    startDate: metaStartDate?.attributes.content,
  }
}

Seo.defaultProps = {
  metaTags: {},
}

Seo.propTypes = {
  metaTags: PropTypes.object,
}
