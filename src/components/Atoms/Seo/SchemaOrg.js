import React from "react"

export default React.memo(
  ({
    type = "WebPage",
    author,
    url,
    datePublished,
    dateModified,
    description,
    image,
    title,
    websiteUrl,
    websiteName,
    location,
    startDate,
    jobTitle = []
  }) => {
    /**
     * BreadcrumbList schema was moved to Molecules/Breadcrumbs/Breadcrumbs.js where it has immediate access to the custom parents/structure that certain templates use.
     * In the future, we might consider refactoring to empower this component with that data.
     */

    // const baseSchema = [
    //   {
    //     "@context": "http://schema.org",
    //     "@type": "WebSite",
    //     url: websiteUrl,
    //     name: websiteName,
    //   },
    // ]

    const orgSchema = [
      {
        "@context": "http://schema.org",
        "@type": "CollegeOrUniversity",
        "url": "https://www.duke.edu/",
        "name": "Duke University",
        "logo": websiteUrl + "/duke-logo-blue.svg",
        "description": "Duke University is a private research university in Durham, North Carolina.",
      },
    ]
   
    const schema = [
      ...orgSchema,
      {
        "@context": "http://schema.org",
        "@type": type,
        url: url,
        name: title,
        description: description,
        ...(type === "Event" && { location: location }),
        ...(type === "Event" && { startDate: startDate }),
        ...(image && {
          image: {
            "@type": "ImageObject",
            url: image,
          },
        }),
        ...(type !== "Person" && { 
          headline: title,
          publisher: {
            "@type": "CollegeOrUniversity",
            name: websiteName,
            logo: websiteUrl + "/duke-logo-blue.svg",
          },
          datePublished: datePublished,
          dateModified: dateModified,
        }),
        ...(type === "Person" && {
          affiliation: [
          {
            "@type": "Organization",
            name: "Duke University",	
            url: "https://www.duke.edu",
            sameAs: "https://en.wikipedia.org/wiki/Duke_University", 
          },
          {
            "@type": "Organization",
            name: websiteName,	
            url: websiteUrl,
          }]          
        }),  
        ...((type === "Person" && jobTitle?.length > 0) && { 
          jobTitle: jobTitle.map(job => `${job.title}`),          
        }),        
      },
    ]

    return (
      <>
        {/* Schema.org tags */}
        <script type="application/ld+json">{JSON.stringify(schema)}</script>
      </>
    )
  }
)
