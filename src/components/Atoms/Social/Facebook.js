import React, { Fragment } from "react"
import PropTypes from "prop-types"

const Facebook = ({
  type,
  url,
  width,
  height,
  show_text,
  parentClass,
  iframeClass,
}) => {
  const fbUrl = encodeURIComponent(url)
  const iframeBase =
    type === "post"
      ? "https://www.facebook.com/plugins/post.php?href="
      : "https://www.facebook.com/plugins/video.php?href="

  const iframeSrc = `${iframeBase}${fbUrl}&width=500${
    show_text ? "&show_text=true" : ""
  }`

  return (
    <Fragment>
      <div className={`${parentClass}`}>
        <iframe
          className={iframeClass}
          width={width}
          height={height}
          frameborder="0"
          allowtransparency="true"
          allow="clipboard-write; encrypted-media; picture-in-picture; web-share"
          allowfullscreen
          scrolling="no"
          src={iframeSrc}
          title="Facebook embed"
        ></iframe>
      </div>
    </Fragment>
  )
}

Facebook.propTypes = {
  type: PropTypes.string,
  url: PropTypes.string,
  show_text: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
  parentClass: PropTypes.string,
  iframeClass: PropTypes.string,
}

Facebook.defaultProps = {
  type: "post",
  show_text: true,
  url: "",
  width: "640",
  height: "640",
  parentClass: "",
  iframeClass: "",
}

export default Facebook
