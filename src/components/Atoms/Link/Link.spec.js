import React from "react"
import { render, screen } from "@testing-library/react"
import { useSiteInfoData } from "hooks/SiteInfoData"
import { Link as GatsbyLink } from "gatsby" // mocked
import Link from "./Link"
jest.mock("hooks/SiteInfoData")

describe("Atoms > Link", () => {
  beforeAll(() => {
    useSiteInfoData.mockReturnValue({
      site_url: "https://sitespro.duke.edu",
      drupal_url: "https://sitespro-content.oit.duke.edu",
      cdn_url: "https://sitespro-files.oit.duke.edu",
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it("replaces 'internal:' correctly", () => {
    render(<Link to="internal:/students">Test Link</Link>)
    expect(screen.getByText("Test Link")).toHaveAttribute("href", "/students/")
  })

  it("replaces internal absolute links with relative links", () => {
    render(<Link to="https://sitespro.duke.edu/students">Test Link</Link>)
    expect(screen.getByText("Test Link")).toHaveAttribute("href", "/students/")
  })

  it("replaces links to files with the CDN url", () => {
    render(<Link to="https://sitespro.duke.edu/test.pdf">Test Link</Link>)
    expect(screen.getByText("Test Link")).toHaveAttribute(
      "href",
      "https://sitespro-files.oit.duke.edu/test.pdf"
    )
  })

  // it("renders the correct element for route:<nolink>", () => {
  // })

  it("renders Gatsby's Link component for internal, relative URLs", () => {
    render(<Link to="/test-page">Test Link</Link>)
    expect(GatsbyLink).toHaveBeenCalled()
  })

  it("does not render Gatsby's Link component for external URLs", () => {
    render(<Link to="https://google.com">Test Link</Link>)
    expect(GatsbyLink).not.toHaveBeenCalled()
    expect(screen.getByText("Test Link")).toHaveAttribute(
      "href",
      "https://google.com"
    )
  })
})
