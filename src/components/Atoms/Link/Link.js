import React from "react"
import PropTypes from "prop-types"
import { Link as GatsbyLink } from "gatsby"
import { replacePath } from "components/_utils/replacePath"
import { useSiteInfoData } from "hooks/SiteInfoData"
import "./Link.scss"

const Link = ({ to, hideExternalIcon = false, children, ...rest }) => {

  const siteInfoData = useSiteInfoData()
  let websiteUrl = siteInfoData?.site_url

  /**
   * 1. We need to clean up the URL to the best of our ability
   */
  // URL cleaning logic originally from DrupalLink - replace "internal:"
  let cleanUrl = (to && to?.includes("internal:")) ? to.replace("internal:", "") : to

  // Can we pickup any absolute links to the site that should be relatively linked?
  const isInternalAbsolute = cleanUrl?.includes(websiteUrl)
  const isExternalAbsolute = !cleanUrl?.includes(websiteUrl) && cleanUrl?.includes("http")

  // Remove origin from absolute links
  if (isInternalAbsolute) {
    cleanUrl = cleanUrl?.replace(/^.*\/\/[^/]+/, "")
  }

  /**
   * 2. Handle any specific cases coming out of Drupal
   */
  // If the API returns <nolink> for both the link text AND the link URL - let's return null
  if (cleanUrl === "route:<nolink>" && children === "<nolink>") return <></>

  // If the API returns <nolink>, create a span element
  if (cleanUrl === "route:<nolink>") return <span {...rest}>{children}</span>

  // Catch file link and create the direct link to a file.
  // TODO: This logic is also in DrupalCKEditor - can/should we merge the two?
  if (
    !isExternalAbsolute &&
    (cleanUrl?.includes(".pdf") ||
    cleanUrl?.includes(".doc") ||
    cleanUrl?.includes(".docx") ||
    cleanUrl?.includes(".xls"))
  ) {
    cleanUrl = cleanUrl?.includes(siteInfoData.drupal_url)
      ? cleanUrl?.replace(siteInfoData.drupal_url, siteInfoData.cdn_url)
      : `${siteInfoData.cdn_url}${cleanUrl}`
  }

  /**
   * 3. Parse internal, relative links into a GatsbyLink
   */
  // Matches anything that starts with a forward slash (/)
  const isInternal = /^\/(?!\/)/.test(cleanUrl)

  if (isInternal) {
    // Initialize the URL so we can do fun stuff with it
    cleanUrl = new URL(websiteUrl + cleanUrl)

    // Let's put a trailing slash on the path and add the query params back before the hash
    // https://stackoverflow.com/questions/12682952/proper-url-forming-with-a-query-string-and-an-anchor-hashtag
    // These values should be empty strings if they are undefined
    let toWithSlash =
      replacePath(cleanUrl.pathname) + cleanUrl.search + cleanUrl.hash

    return (
      <GatsbyLink to={toWithSlash} {...rest}>
        {children}
      </GatsbyLink>
    )
  }

  /**
   * 4. Lastly, return an anchor link for all other cases (absolute)
   */

  return (
    <a
      href={cleanUrl}
      className={hideExternalIcon ? "hide-icon" : ""}
      {...rest}
    >
      {children}
    </a>
  )
}

Link.propTypes = {
  children: PropTypes.node,
  to: PropTypes.string,
}

export default Link
