import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import dayjs from "dayjs"
import "./DateFormats.scss"

const CardView = ({ sDate, eDate = null }) => {
  return (
    <div className="date-cardview">
      <span className="date-cardview-month">{dayjs(sDate).format("MMM")}</span>
      <span className="date-cardview-day">{dayjs(sDate).format("D")}</span>

      {eDate && (
        <>
          <span>&nbsp;-&nbsp;</span>
          <span className="date-cardview-month">
            {dayjs(eDate).format("MMM")}
          </span>
          <span className="date-cardview-day">{dayjs(eDate).format("D")}</span>
        </>
      )}
    </div>
  )
}

const LineView = ({ sDate, eDate = null }) => {
  return (
    <div className="date-lineview">
      <span className="date-lineview-month">{dayjs(sDate).format("MMMM")}</span>
      <span>&nbsp;</span>
      <span className="date-lineview-day">{dayjs(sDate).format("D")}</span>
      <span>,&nbsp;</span>
      <span className="date-lineview-year">{dayjs(sDate).format("YYYY")}</span>

      {eDate && (
        <>
          <span>&nbsp;-&nbsp;</span>
          <span className="date-lineview-month">
            {dayjs(eDate).format("MMMM")}
          </span>
          <span>&nbsp;</span>
          <span className="date-lineview-day">{dayjs(eDate).format("D")}</span>
          <span>,&nbsp;</span>
          <span className="date-lineview-year">
            {dayjs(eDate).format("YYYY")}
          </span>
        </>
      )}
    </div>
  )
}

const DateProcessor = ({
  startDate,
  endDate = null,
  block = "date",
  element,
  cardView,
  modifier = ["full-width"],
}) => {
  return (
    <div className={bem(block, element, modifier)}>
      {cardView ? (
        <CardView sDate={startDate} eDate={endDate} />
      ) : (
        <LineView sDate={startDate} eDate={endDate} />
      )}
    </div>
  )
}

DateProcessor.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  cardView: PropTypes.bool,
  modifier: PropTypes.array,
}

export default DateProcessor
