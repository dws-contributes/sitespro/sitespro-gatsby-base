/**
 * Iframe component (responsive)
 * Use: <Iframe src={} title={} width={} height={} />
 * @TODO: need to add logic/script for resizing the iFrame based on its content
 * @TODO: need to add responsiveness for fixed-width iFrames (100% width on smaller screens, fixed width on larger screens)
 */
import React, { useState, useEffect, useRef } from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import "./Iframe.scss"

const Iframe = ({
  block = "iframe",
  element,
  modifiers,
  src,
  width,
  height,
  title,
}) => {
  const [loadIframe, setLoadIframe] = useState(false)
  const iframeRef = useRef(null) // Reference to the wrapper div

  useEffect(() => {
    const observer = new IntersectionObserver(
      entries => {
        // Whenever the wrapper div enters the viewport, set loadIframe to true
        if (entries[0].isIntersecting) {
          setLoadIframe(true)
          observer.disconnect() // Disconnect the observer once the iframe is loaded
        }
      },
      { threshold: 0.1 } // Trigger when at least 10% of the element is visible
    )

    if (iframeRef.current) {
      observer.observe(iframeRef.current)
    }

    return () => {
      observer.disconnect() // Cleanup the observer on component unmount
    }
  }, [])

  return (
    <div className={bem(block, element, modifiers)} ref={iframeRef}>
      {loadIframe && (
        <iframe
          id={title}
          title={title}
          width={width ? width : "100%"}
          height={height ? height : "400"}
          src={src}
          allow="fullscreen"
          loading="lazy"
          style={{ border: 0 }}
          className="embed-responsive-item"
        ></iframe>
      )}
    </div>
  )
}

Iframe.propTypes = {
  src: PropTypes.string.isRequired,
  width: PropTypes.string,
  height: PropTypes.string,
  title: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Iframe
