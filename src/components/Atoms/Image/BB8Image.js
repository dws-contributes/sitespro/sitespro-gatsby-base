import React from "react"
import PropTypes from "prop-types"
import "./Image.scss"
import { useSiteInfoData } from "hooks/SiteInfoData"
import ContentLoader from "react-content-loader"
import { LazyLoadImage } from "react-lazy-load-image-component"
import "react-lazy-load-image-component/src/effects/blur.css"

export const BB8ImageLoader = () => (
  // <ContentLoader>
  //   <rect x="0" y="0" width="100%" height="100%" />
  // </ContentLoader>
  <ContentLoader
    speed={1}
    width={400}
    height={475}
    viewBox="0 0 700 475"
    backgroundColor="rgb(0,0,0)"
    foregroundColor="rgb(0,0,0)"
    backgroundOpacity={0.06}
    foregroundOpacity={0.12}
    animate={true}
  >
    <rect x="15" y="15" rx="4" ry="4" width="350" height="25" />
    <rect x="15" y="50" rx="2" ry="2" width="350" height="150" />
    <rect x="15" y="230" rx="2" ry="2" width="170" height="20" />
    <rect x="60" y="230" rx="2" ry="2" width="170" height="20" />
  </ContentLoader>
)

export const BB8Image = ({
  imageDetails,
  targetedSizes = ["scale_width_1220", "scale_width_500"],
  placeholderHeight,
  visibleByDefault,
}) => {
  let { derivatives, fallback } = BB8ImageProcessVariants(
    imageDetails,
    targetedSizes
  )

  return (
    <>
      {fallback && (
        <LazyLoadImage
          height={placeholderHeight}
          effect="blur"
          offset={100}
          src={fallback}
          alt={imageDetails.alt}
          srcSet={derivatives.join(", ")}
          placeholder={<div>Loading...</div>}
          visibleByDefault={visibleByDefault}
        />
      )}
    </>
  )
}

export function BB8ImageProcessVariants(
  imageDetails,
  targetedSizes,
  forceSize = true
) {
  const siteInfoData = useSiteInfoData()

  let derivatives = []
  let fallback = ""

  // Hack to strip out the styles part from the path for solr svg images
  if (
    imageDetails?.uri?.includes(".svg") &&
    imageDetails?.uri?.includes("files/styles")
  ) {
    imageDetails.uri = imageDetails.uri.replace(/styles.*public\//, "")
  }

  // Graceful handling of svg images - rendering the original instead of the style derivatives
  if (
    imageDetails?.fileMime === "image/svg+xml" ||
    imageDetails?.uri?.includes(".svg")
  ) {
    return {
      derivatives: [],
      fallback: imageDetails.uri.includes("http")
        ? replaceSource(imageDetails.uri)
        : siteInfoData.cdn_url + replaceSource(imageDetails.uri),
    }
  }

  function replaceSource(imgPath) {
    imgPath = imgPath.replace(siteInfoData.drupal_url, siteInfoData.cdn_url)
    imgPath = imgPath.replace("http://default", siteInfoData.cdn_url)
    return imgPath
  }

  const derivativesNormalized = Object.entries(
    imageDetails?.variants || {}
  ).filter(
    ([derivative, variantPath]) =>
      variantPath !== null && targetedSizes.indexOf(derivative) > -1
  )

  derivativesNormalized.forEach(([derivative, value]) => {
    let screenWidth = "480w"
    switch (derivative) {
      case "scale_width_1220":
        screenWidth = "1220w"
        break
      case "scale_width_500":
        screenWidth = "500w"
        break
      case "focal_point_large":
      case "large_2_5_1":
        screenWidth = "480w"
        break
      case "focal_point_medium":
        screenWidth = "220w"
        break
      case "large_3_2":
        screenWidth = "1200w"
        break
      case "large_4_5":
      case "large_4_3":
        screenWidth = "750w"
        break
      case "small_2_5_1":
        screenWidth = "400w"
        break
      case "small_3_2":
        screenWidth = "600w"
        break
      default:
        screenWidth = "600w"
    }
    let src = replaceSource(value)
    if (forceSize) derivatives.push(`${src} ${screenWidth}`)
    else derivatives.push(`${src}`)
    fallback = fallback ? fallback : src
  })

  return {
    derivatives,
    fallback,
  }
}

export function imageDetails(data) {
  return {
    variants: data?.relationships?.field_media_image?.image_style_uri,
    alt: data?.field_media_image?.alt,
    uri: data?.relationships?.field_media_image?.uri?.url,
    fileMime: data?.relationships?.field_media_image?.filemime,
  }
}

const BB8ImageVariantsDataShape = {
  scale_width_1220: PropTypes.string,
  scale_width_500: PropTypes.string,
  large_2_5_1: PropTypes.string,
  large_3_2: PropTypes.string,
  large_4_3: PropTypes.string,
  large_4_5: PropTypes.string,
  small_2_5_1: PropTypes.string,
  small_3_2: PropTypes.string,
}

export const imageDetailsShape = {
  variants: PropTypes.shape(BB8ImageVariantsDataShape),
  alt: PropTypes.string,
  uri: PropTypes.string,
  fileMime: PropTypes.string,
}

BB8Image.propTypes = {
  imageDetails: PropTypes.shape(imageDetailsShape),
  targetedSizes: PropTypes.array,
  placeholderHeight: PropTypes.number,
  visibleByDefault: PropTypes.bool,
}
