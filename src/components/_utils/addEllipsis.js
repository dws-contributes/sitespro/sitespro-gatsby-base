// Adds an ellipsis to a string if it does not ends with a punctuation mark (.?!)
const addEllipsis = string => {
  if (!string) return null
  string = string.trim()
  if (!/[.!?]$/.test(string)) {
    string += "..."
  }
  return string
}

export default addEllipsis
