const uniqueById = (value, index, self) => {
  return index === self.findIndex(obj => obj.id === value.id)
}

export default uniqueById
