// A simple means of checking whether or not a path has a trailing slash before adding one.
// Pulled into _utils for use throughout the project. This is currently an easier means than replacing pathes in Drupal
const replacePath = function (_path) {
  if (_path === `/`) {
    return _path
  } else {
    return _path.replace(/\/$|$/, `/`)
  }
}

module.exports.replacePath = replacePath
