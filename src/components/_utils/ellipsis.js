const ellipsis = (string, length) => {
  if (!string) return null
  if (string.length < length) return string
  const truncated = string.substr(0, length)
  return `${truncated.substr(0, truncated.lastIndexOf(" "))} ...`
}

export default ellipsis
