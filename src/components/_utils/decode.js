import { parseEntities } from "parse-entities"

// handling undefined until parse-entities can support it
const decode = value => {
  if (value == null) {
    return
  }

  return parseEntities(value)
}

export default decode
