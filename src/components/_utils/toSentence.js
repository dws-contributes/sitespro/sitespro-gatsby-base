const toSentence = arr => {
  if (!arr) return ""
  if (arr.length === 0) return ""
  if (arr.length === 1) return arr[0]
  // Handle the last case where there is no serial comma
  if (arr.length === 2) return `${arr[0]}, ${arr[1]}`
  // const tail = arr[arr.length - 1]
  // const rest = arr.slice(0, arr.length - 1)
  return arr.join(", ")
}

export default toSentence
