const convertCamelCaseToDashes = (str, prefix = "") => {
  let s = [...str]
  s.forEach((l, i) => {
    if (l === l.toUpperCase() && isNaN(l)) {
      s[i] = "-" + l.toLowerCase()
    }
  })
  return `${prefix ? `${prefix}-` : ""}${s.join("")}`
}

const buildRootCSSVariables = (obj, prefix = "") => {
  const cssVariables = {}

  for (const key in obj) {
    const customPropertyName = convertCamelCaseToDashes(key, prefix)
    if (typeof obj[key] === "object") {
      const flattenedValues = buildRootCSSVariables(
        obj[key],
        customPropertyName
      )
      for (const customKey in flattenedValues) {
        cssVariables[`${customKey}`] = flattenedValues[customKey]
      }
    } else {
      cssVariables[`--sp-${customPropertyName}`] = obj[key]
    }
  }
  return cssVariables
}

const cssVariables = themeObject => {
  let cssVariablesString = ""
  const cssVariablesObject = buildRootCSSVariables(themeObject)
  for (const [key, value] of Object.entries(cssVariablesObject)) {
    cssVariablesString += `${key}: ${value};`
  }
  return cssVariablesString
}

export default cssVariables
