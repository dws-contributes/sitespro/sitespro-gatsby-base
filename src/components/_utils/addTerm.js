const addTerm = term => {
  // Add category filter -- the latter part was added because in transistion to becoming an array some of the fields were cached and not updating
  // This is a temporary fix until the cache is cleared
  const termFilter = []
  if (term) {
    if (Array.isArray(term)) {
      if (term.length > 0) {
        term.forEach(e => {
          if (e?.name) {
            termFilter.push(`Category>${e?.name}`)
          }
        })
      }
    } else if (term.name) {
      termFilter.push(`Category>${term.name}`)
    }
  }

  return termFilter
}

export default addTerm
