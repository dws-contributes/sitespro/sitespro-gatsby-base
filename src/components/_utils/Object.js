const sortObject = obj => {
  if (!obj) return null
  return Object.keys(obj)
    .sort()
    .reduce((acc, key) => {
      acc[key] = obj[key]
      return acc
    }, {})
}

const reverseSortObject = obj => {
  if (!obj) return null
  return Object.keys(obj)
    .sort()
    .reverse()
    .reduce((acc, key) => {
      acc[key] = obj[key]
      return acc
    }, {})
}

export { sortObject, reverseSortObject }
