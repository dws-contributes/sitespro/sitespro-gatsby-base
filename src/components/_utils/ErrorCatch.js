import React from "react"

export function ErrorCatch(error) {
  console.error(error)
  if (process.env.NODE_ENV === "development") {
    return (
      <p className="p-3 mb-2 bg-danger text-white">
        Something went wrong. See console log for details.
      </p>
    )
  } else return ""
}
