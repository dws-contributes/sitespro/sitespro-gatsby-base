const setFromString = (str, sep) => {
  if (!str) return new Set()
  return new Set(str.split(sep))
}

export { setFromString }
