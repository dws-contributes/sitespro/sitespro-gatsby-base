const parseInternalUri = uri => (uri ? `${uri}`.replace("internal:", "") : null)

export default parseInternalUri
