import React, { useState, useEffect } from "react"

const SiteContext = React.createContext([{}, () => {}])

const SiteContextProvider = props => {
  let local =
    typeof localStorage !== "undefined"
      ? JSON.parse(localStorage.getItem("siteContext"))
      : null

  const [state, setState] = useState(
    local && local !== "undefined" ? local : { showAnnouncement: true }
  )

  useEffect(() => {
    localStorage.setItem("siteContext", JSON.stringify(state))
  }, [state])

  return (
    <SiteContext.Provider value={[state, setState]}>
      {props.children}
    </SiteContext.Provider>
  )
}

export default SiteContext

export { SiteContextProvider }
