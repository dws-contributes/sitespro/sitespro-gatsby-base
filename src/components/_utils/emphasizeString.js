// Function to emphasize a short string by adding <em></em> tags within larger text content
// Note: Created to emphasize query search term in search results for FAQ content but could have other uses down the road
export const emphasizeString = (fullText, shortString) => {
  if (!fullText || !shortString) {
    return fullText // Return the original text if either is null or undefined
  }

  // Escape special characters in shortString for use in a regular expression
  const escapeRegExp = str => str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")
  const escapedShortString = escapeRegExp(shortString)

  const regex = new RegExp(`(${escapedShortString})`, "gi")
  return fullText.replace(regex, "<em>$1</em>")
}
