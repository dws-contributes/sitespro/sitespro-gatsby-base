import React, { Fragment } from "react"
import Link from "components/Atoms/Link/Link"
import parse from "html-react-parser"

// authorsArray is array of react objects with:
// - path to the author url or email
// - title - name of author

// TODO: Can parse be replaced with a lighter function?

export const FormattedAuthors = ({ authorsArray }) => {
  if (authorsArray.length < 1) return null
  else {
    const FormattedAuthorsString = authorsArray.map((author, index) => (
      <Fragment key={index}>
        {author?.path ? (
          <Link to={author?.path}>{author?.title}</Link>
        ) : (
          <>{author?.title}</>
        )}
        {index < authorsArray?.length - 1
          ? index === authorsArray?.length - 2 && authorsArray?.length !== 2
            ? ", & "
            : authorsArray?.length === 2
            ? " & "
            : ", "
          : " "}
      </Fragment>
    ))
    return FormattedAuthorsString
  }
}

export function AuthorsStringToObject(authors) {
  const authorsObjectArray = []
  authors?.split(",").forEach((e, i) => {
    authorsObjectArray[i] = {
      path: parse(e.trim())?.props?.href,
      title: parse(e.trim())?.props?.children,
    }
  })
  return authorsObjectArray
}
