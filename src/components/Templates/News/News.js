import React, { Suspense } from "react"
import PropTypes from "prop-types"
// import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import Share from "components/Atoms/Share/Share"
import "./News.scss"
import { BB8Image, imageDetailsShape } from "components/Atoms/Image/BB8Image"
import { FormattedAuthors } from "components/_utils/FormattedAuthors"
import MetaBox from "components/Atoms/MetaBox/MetaBox"
const CKEditorContent = React.lazy(() => import("components/Atoms/CKEditor/DrupalCKEditor"));

const News = ({
  title,
  subtitle,
  imageDetails,
  date,
  authors,
  body,
  summary,
  feedImageUrl,
  feedImageAltText,
  slideshow,
}) => {
  let featuredImageComponent = null
  if (imageDetails) {
    featuredImageComponent = (
      <BB8Image
        imageDetails={imageDetails}
        targetedSizes={["large_2_5_1", "small_2_5_1"]}
      />
    )
  } else if (feedImageUrl) {
    featuredImageComponent = <img src={feedImageUrl} alt={feedImageAltText} />
  }

  // TODO: This "content-type-tag" is an "eyebrow" in other places? Should it be here?
  return (
    <>
      <div className="news-title">
        <div className="content-type-tag">News</div>
        <h1>{title}</h1>
        {subtitle && <h2>{subtitle}</h2>}
      </div>
      {featuredImageComponent && (
        <div className="featured-image news">{featuredImageComponent}</div>
      )}
      <div className="row">
        <div className="col-lg-3 col-md-5 col-sm-12">
          <MetaBox as={"aside"} className="news-meta">
            {date && (
              <>
                <div className="field-label">Date</div>
                <DateProcessor startDate={date} />
              </>
            )}
            {authors.length > 0 && (
              <>
                <div className="field-label">By</div>
                <p>
                  <FormattedAuthors authorsArray={authors} />
                </p>
              </>
            )}
            <Share
              shareTitle="Share this story"
              pageTitle={title}
              pageSummary={summary}
            />
          </MetaBox>
        </div>
        {body && (
          <div className="body col-lg-9 col-md-7 col-sm-12">
            <Suspense fallback={<div>Loading</div>}>
              <CKEditorContent content={body} />
              {slideshow && <div className="slideshow">{slideshow}</div>}
            </Suspense>
          </div>
        )}
      </div>
    </>
  )
}

News.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  imageDetails: PropTypes.shape(imageDetailsShape),
  date: PropTypes.string,
  authors: PropTypes.any,
  body: PropTypes.string,
  summary: PropTypes.string,
  feedImageUrl: PropTypes.string,
  feedImageAltText: PropTypes.string,
  slideshow: PropTypes.element,
}

export default News
