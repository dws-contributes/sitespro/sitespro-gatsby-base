/**
 * News teaser display template
 */
import React, { Fragment } from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import Link from "components/Atoms/Link/Link"
import { Row, Col } from "react-bootstrap"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { BsDot } from "react-icons/bs"
import decode from "components/_utils/decode"
import addEllipsis from "components/_utils/addEllipsis"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import "./NewsTeaser.scss"

const NewsTeaser = ({
  date,
  link,
  title,
  summary,
  source,
  image,
  imageAlt,
  block = "node-teaser",
  modifiers = "news",
}) => {
  let imageWidth = 4
  let titleWidth = image ? 8 : 12

  let imageNormalized = null
  if (image) {
    imageNormalized = {
      imageDetails: {
        variants: { small_2_5_1: image },
        alt: imageAlt,
        uri: image,
      },
      targetedSizes: ["small_2_5_1"],
    }
  }

  let featuredImageComponent = null
  if (imageNormalized) {
    featuredImageComponent = (
      <div className="news-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }
  return (
    <article className={`node-teaser ${bem(block, modifiers)}`}>
      <Row>
        {featuredImageComponent && (
          <Col md={imageWidth}>{featuredImageComponent}</Col>
        )}
        <Col md={titleWidth}>
          <Heading level={3} modifiers={["teaser"]}>
            <Link to={link}>{decode(title)}</Link>
          </Heading>

          {summary && !link.includes("/secure/") && (
            <div className="highlight">
              <CKEditorContent content={decode(addEllipsis(summary))} />
            </div>
          )}

          <div className={bem(block, "meta", ["date"]) + " news-teaser"}>
            <DateProcessor startDate={date} />
            {source && (
              <Fragment>
                <span>
                  <BsDot />
                </span>
                {decode(source)}
              </Fragment>
            )}
          </div>
        </Col>
      </Row>
    </article>
  )
}

NewsTeaser.propTypes = {
  date: PropTypes.string,
  summary: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  source: PropTypes.string,
}

export default NewsTeaser
