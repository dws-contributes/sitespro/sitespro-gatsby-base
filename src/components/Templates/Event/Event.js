import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import TimeProcessor from "components/Atoms/TimeProcessor/TimeProcessor"
import Divider from "components/Atoms/Divider/Divider"
import Button from "components/Atoms/Button/Button"
import Heading from "components/Atoms/Heading/Heading"
import Share from "components/Atoms/Share/Share"
import { MdPlace } from "react-icons/md"
import { AiOutlineClockCircle } from "react-icons/ai"
import { BsCalendar } from "react-icons/bs"
import toSentence from "components/_utils/toSentence"
import "./Event.scss"
import { BB8Image, imageDetailsShape } from "components/Atoms/Image/BB8Image"
import MetaBox from "components/Atoms/MetaBox/MetaBox"

const Event = ({
  title,
  imageDetails,
  startAt,
  endAt,
  locationText,
  locationUrl,
  presenter,
  cost,
  summary,
  body,
  additionalLinks,
  additionalLocationInfo,
  additionalInfo,
  contactName,
  contactEmail,
  contactPhone,
  moreEventInfoUrl,
  categories,
  categories_site_specific,
  series,
  sponsor,
  cosponsors,
  webcastUrl,
  feedImageUrl,
  feedImageAltText,
  status,
}) => {
  let featuredImageComponent = null

  // Combined categories and site specific categories which removes duplicates
  const combinedCategoriesSet = new Set([
    ...categories,
    ...categories_site_specific,
  ])
  // Convert the set back into an array
  const combinedCategories = Array.from(combinedCategoriesSet).sort()

  // TODO: imageDetails.variants is probably correct
  if (imageDetails.variants || imageDetails.uri) {
    featuredImageComponent = (
      <div className="events-featured-image">
        <BB8Image
          imageDetails={imageDetails}
          targetedSizes={["large_3_2", "small_3_2"]}
        />
      </div>
    )
  } else if (feedImageUrl) {
    featuredImageComponent = (
      <div className="events-featured-image">
        <img src={feedImageUrl} alt={feedImageAltText} />
      </div>
    )
  }

  // Strip time from start and end dates for comparison
  const startAtDateOnly = startAt ? startAt.split("T")[0] : ""
  const endAtDateOnly = endAt ? endAt.split("T")[0] : ""

  return (
    <>
      <Heading level={1}>
        {status && status !== "CONFIRMED" && (
          <span className="event-status">
            <i>{status}</i>
            <br />
          </span>
        )}
        {title}
      </Heading>
      <div className="row">
        <aside className="col-lg-4 col-md-5 col-sm-12">
          <div className="aside-top">
            <MetaBox className="event-meta">
              <div className="field-container">
                <BsCalendar />
                <DateProcessor
                  startDate={startAt}
                  endDate={startAtDateOnly !== endAtDateOnly ? endAt : null}
                />
              </div>
              <div className="field-container">
                <AiOutlineClockCircle />
                <TimeProcessor startTime={startAt} endTime={endAt} />
              </div>
              {locationUrl && locationText && locationText !== "None" ? (
                <div className="field-container">
                  <MdPlace />
                  <a href={locationUrl}>{locationText}</a>
                </div>
              ) : (
                <>
                  {locationText && (
                    <div className="field-container">
                      <MdPlace />
                      <p>{locationText}</p>
                    </div>
                  )}
                </>
              )}
              {cost && (
                <>
                  <div className="field-label">Cost</div>
                  <p>{cost}</p>
                </>
              )}
              {webcastUrl && (
                <>
                  <div className="field-label">Webcast</div>
                  <p>
                    <a href={webcastUrl}>Link</a>
                  </p>
                </>
              )}
              <Share
                shareTitle="Share this event"
                pageTitle={title}
                pageSummary={summary}
              />
            </MetaBox>
          </div>
          <div className="aside-bottom">
            {contactName && contactName !== "None" && (
              <>
                <div className="field-label">Contact</div>
                <div>
                  <a href={contactEmail && "mailto:" + contactEmail}>
                    {contactName}
                  </a>
                </div>
                {contactPhone && <div>{contactPhone}</div>}
              </>
            )}
            {sponsor && (
              <>
                <div className="field-label">Event Sponsored By</div>
                <p>
                  <span className="event-sponsor">
                    {toSentence([sponsor, ...cosponsors])}
                  </span>
                </p>
              </>
            )}
            {series.length > 0 && (
              <>
                <div className="field-label">Series</div>
                <ul className="series-list">
                  {series.map((s, i) => (
                    <li key={"series-" + i + 1}>{s}</li>
                  ))}
                </ul>
              </>
            )}
          </div>
        </aside>
        <div className="event-content col-lg-8 col-md-7 col-sm-12">
          {featuredImageComponent}
          {presenter && (
            <>
              <div className="field-label">Speaker</div>
              <p className="speaker">{presenter}</p>
            </>
          )}
          {body && (
            <div className="body">
              <CKEditorContent content={body} />
            </div>
          )}
          {additionalInfo && (
            <>
              <CKEditorContent content={additionalInfo} />
            </>
          )}
          <div className="more-event-info">
            {moreEventInfoUrl && (
              <p>
                <Button link={moreEventInfoUrl} showIcon={true}>
                  More Event Information
                </Button>
              </p>
            )}
            {additionalLinks.length > 0 && (
              <>
                {additionalLinks.map((link, i) => (
                  <p>
                    <Button
                      link={link.uri}
                      key={"addl-link-" + i + 1}
                      showIcon={true}
                    >
                      {link.title}
                    </Button>
                  </p>
                ))}
              </>
            )}
            {additionalLocationInfo && (
              <>
                <p className="field-label">Additional Location Information</p>
                <CKEditorContent content={additionalLocationInfo} />
              </>
            )}
          </div>
          <Divider />
          {combinedCategories.length > 0 && (
            <>
              <div className="field-label">Categories</div>
              <p>
                <span className="category">
                  {toSentence(combinedCategories)}
                </span>
              </p>
            </>
          )}
        </div>
      </div>
    </>
  )
}

Event.propTypes = {
  title: PropTypes.string.isRequired,
  alternate_link: PropTypes.string,
  alternate_link_title: PropTypes.string,
  imageDetails: PropTypes.shape(imageDetailsShape),
  locationUrl: PropTypes.string,
  locationText: PropTypes.string,
  presenter: PropTypes.string,
  cost: PropTypes.string,
  summary: PropTypes.string,
  body: PropTypes.string,
  additionalLinks: PropTypes.array,
  contactName: PropTypes.string,
  contactEmail: PropTypes.string,
  contactPhone: PropTypes.string,
  moreEventInfoUrl: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.string),
  categories_site_specific: PropTypes.arrayOf(PropTypes.string),
  series: PropTypes.arrayOf(PropTypes.string),
  sponsor: PropTypes.string,
  cosponsors: PropTypes.arrayOf(PropTypes.string),
  webcastUrl: PropTypes.string,
  feed_image_url: PropTypes.object,
  feed_image_alt: PropTypes.string,
  status: PropTypes.string,
}

export default Event
