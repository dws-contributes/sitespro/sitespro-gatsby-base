import React from "react"
import PropTypes from "prop-types"
import Heading from "components/Atoms/Heading/Heading"
import Meta from "components/Molecules/Meta/Meta"
import Divider from "components/Atoms/Divider/Divider"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import Share from "components/Atoms/Share/Share"
import Authors from "components/Molecules/Authors/Authors"
import Container from "react-bootstrap/Container"
import {
  BB8Image,
  imageDetails,
  imageDetailsShape,
} from "components/Atoms/Image/BB8Image"
import { getParagraph } from "components/Molecules/Paragraphs/ParagraphsHelper"
import "./Story.scss"
import { CustomBreadcrumbs, GetCustomParents } from "components/Molecules/Breadcrumbs/Breadcrumbs"

// TODO: Possible inconsistencies in the "Share" component/section across various CTs
// TODO: Authors component is only used here - might consider localizing it? Or can we use it elsewhere?
const Story = ({
  title,
  subTitle,
  imageDetails,
  paragraphs,
  authors,
  date,
  summary,
}) => {
  return (
    <>
      {imageDetails && (
        <div className="story-featured-image">
          <BB8Image
            imageDetails={imageDetails}
            targetedSizes={["large_2_5_1", "small_2_5_1"]}
          />
        </div>
      )}

      <Container>
        <div className="story-wrapper">
          <div className="story-meta-wrapper">
            <CustomBreadcrumbs
              pageTitle={title}
              customParents={GetCustomParents("stories")}
            />
            <Eyebrow text="Story" />
            <Heading level={1}>{title}</Heading>
            <Heading level={2}>{subTitle}</Heading>
            <Meta authors={authors} date={date} />
            <Divider />
            <Share
              shareTitle="Share this story"
              pageTitle={title}
              pageSummary={summary}
            />
          </div>

          <div className="story-paragraph-wrapper">{paragraphs}</div>

          <div className="story-footer-wrapper">
            <Share
              shareTitle="Share this story"
              pageTitle={title}
              pageSummary={summary}
            />

            <Divider />
            {authors && authors.length > 0 && <Authors authors={authors} />}
          </div>
        </div>
      </Container>
    </>
  )
}

Story.propTypes = {
  title: PropTypes.string,
  summary: PropTypes.string,
  subTitle: PropTypes.string,
  imageDetails: PropTypes.shape(imageDetailsShape),
  paragraphs: PropTypes.array,
  authors: PropTypes.array,
  date: PropTypes.string,
}

export default Story

export const storyDataNormalizer = storyData => {
  let authors = storyData?.relationships?.field_author_reference.map(
    author => ({
      title: author?.title,
      path: author?.path?.alias,
      imageDetails: imageDetails(author?.relationships?.field_image),
      summary: author?.bio?.summary,
      description: author?.bio?.processed,
    })
  )

  return {
    title: storyData.title,
    subTitle: storyData.subtitle,
    imageDetails: imageDetails(storyData?.relationships?.field_featured_media),
    summary: storyData.summary,
    paragraphs: storyData?.relationships?.paragraphs.map(getParagraph),
    authors: authors,
    date: storyData.date,
    metatags: storyData.metatag_normalized,
  }
}
