/**
 * Story teaser display template
 */
import React, { Fragment } from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import { Row, Col } from "react-bootstrap"
import bem from "components/_utils/bem"
import {
  FormattedAuthors,
  AuthorsStringToObject,
} from "components/_utils/FormattedAuthors"
import Heading from "components/Atoms/Heading/Heading"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { BsDot } from "react-icons/bs"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import decode from "components/_utils/decode"
import "./Story.scss"

const StoryTeaser = ({
  title,
  date,
  link,
  summary,
  image,
  imageAlt,
  authors,
  block = "node-teaser",
  modifiers = "story",
}) => {
  let imageWidth = 4
  let titleWidth = image ? 8 : 12
  let featuredImageComponent = null
  if (image) {
    let imageNormalized = {
      imageDetails: {
        variants: { small_2_5_1: image },
        alt: imageAlt,
        uri: image,
      },
      targetedSizes: ["small_2_5_1"],
    }
    featuredImageComponent = (
      <div className="news-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  return (
    <article className={`${bem(block, modifiers)}`}>
      <Row>
        {featuredImageComponent && (
          <Col lg={imageWidth}>{featuredImageComponent}</Col>
        )}
        <Col lg={titleWidth}>
          <Heading level={3} modifiers={["teaser"]}>
            <Link to={link}>{decode(title)}</Link>
          </Heading>

          {summary && !link.includes('/secure/') && (
            <div className="highlight">
              <CKEditorContent content={summary} />
            </div>
          )}

          <div className={bem(block, "meta", ["date"])}>
            <DateProcessor startDate={date} />
            {authors && (
              <Fragment>
                <span className={bem(block, "meta", ["dot"])}>
                  <BsDot />
                </span>
                <span className={bem(block, "meta", ["authors"])}>
                  <FormattedAuthors
                    authorsArray={AuthorsStringToObject(authors)}
                  />
                </span>
              </Fragment>
            )}
          </div>
        </Col>
      </Row>
    </article>
  )
}

StoryTeaser.propTypes = {
  date: PropTypes.string,
  summary: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  authors: PropTypes.string,
}

export default StoryTeaser
