import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Link from "components/Atoms/Link/Link"
import Heading from "components/Atoms/Heading/Heading"
import decode from "components/_utils/decode"
import addEllipsis from "components/_utils/addEllipsis"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import "./Project.scss"

const RelatedProjects = ({ relatedProjects }) => {
  return (
    <>
      <div className="related-projects-container project-related">
        <div className="related-projects-list-line"></div>
        <Heading level={3}>Related</Heading>

        {relatedProjects.nodes.length === 0 ? (
          <p>There are no related items at the moment</p>
        ) : (
          <ul>
            {relatedProjects.nodes.map(project => {
              // Related Project image
              let image =
                project?.relationships?.field_featured_media?.relationships
                  ?.field_media_image?.image_style_uri?.small_2_5_1
              let imageAlt =
                project.relationships?.field_featured_media?.field_media_image
                  ?.alt
              let featuredImageComponent = null
              if (image) {
                let imageNormalized = {
                  imageDetails: {
                    variants: { small_2_5_1: image },
                    alt: imageAlt,
                    uri: image,
                  },
                  targetedSizes: ["small_2_5_1"],
                }

                featuredImageComponent = (
                  <BB8Image
                    imageDetails={imageNormalized?.imageDetails}
                    targetedSizes={imageNormalized?.targetedSizes}
                  />
                )
              }

              return (
                <li key={project.id}>
                  {featuredImageComponent && (
                    <div className="column">{featuredImageComponent}</div>
                  )}
                  <div className="column">
                    {project.relationships?.field_project_start_timeframe
                      ?.name && (
                      <div
                        key={project.id + "-timeframe"}
                        className="related-project-timeframe"
                      >
                        {
                          project.relationships?.field_project_start_timeframe
                            ?.name
                        }
                        {project.relationships?.field_project_end_timeframe
                          ?.name && (
                          <>
                            {" "}
                            -{" "}
                            {
                              project.relationships?.field_project_end_timeframe
                                ?.name
                            }
                          </>
                        )}
                      </div>
                    )}
                    <Link
                      to={project.path?.alias}
                      className="related-project-title"
                    >
                      <div key={project.id + "-title"}>
                        {decode(project.title)}
                      </div>
                    </Link>
                    <div
                      key={project.id + "-summary"}
                      className="related-project-summary"
                    >
                      <CKEditorContent
                        content={addEllipsis(decode(project.body?.summary))}
                      />
                    </div>
                  </div>
                </li>
              )
            })}
          </ul>
        )}
      </div>
    </>
  )
}

RelatedProjects.propTypes = {
  numItems: PropTypes.number,
  relatedProjects: PropTypes.object,
}

export default RelatedProjects
