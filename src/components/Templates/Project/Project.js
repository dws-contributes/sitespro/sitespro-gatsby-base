import React from "react"
import PropTypes from "prop-types"
import Heading from "components/Atoms/Heading/Heading"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Link from "components/Atoms/Link/Link"
import Divider from "components/Atoms/Divider/Divider"
import Button from "components/Atoms/Button/Button"
import toSentence from "components/_utils/toSentence"
import RelatedProjects from "components/Templates/Project/RelatedProjects.js"
import "./Project.scss"
import { BB8Image, imageDetailsShape } from "components/Atoms/Image/BB8Image"
import { Container } from "react-bootstrap"

const Project = ({
  title,
  body,
  imageDetails,
  additionalLinks,
  actionLink,
  contact,
  members,
  leaders,
  sponsors,
  relatedCourses,
  timeframeDetails,
  categories,
  timeframeStart,
  timeframeEnd,
  status,
  relatedProjects,
}) => {
  let featuredImageComponent = null

  if (imageDetails.variants || imageDetails.uri) {
    featuredImageComponent = (
      <div className="project-header-image">
        <BB8Image
          imageDetails={imageDetails}
          targetedSizes={["large_3_2", "small_3_2"]}
        />
      </div>
    )
  }
  const hasImage = featuredImageComponent ? "has-image" : ""

  return (
    <Container>
      <div className={`project-header row ${hasImage}`}>
        {featuredImageComponent && (
          <div className="col-lg-5 col-sm-12">{featuredImageComponent}</div>
        )}
        <div className="project-header-text col-lg-7 col-sm-12 flex-grow-1">
          <Heading level={1}>{title}</Heading>
          {actionLink?.uri_alias && (
            <Button link={actionLink?.uri_alias} showIcon={true}>
              {actionLink?.title}
            </Button>
          )}
        </div>
      </div>
      <div className="project-content col">
        {(timeframeStart || timeframeDetails || status) && (
          <>
            <Heading level={2}>Timeline</Heading>
            <p className="project-timeline">
              {timeframeStart}
              {timeframeEnd && <> - {timeframeEnd}</>}
            </p>
            {status && <p className="project-status">Status: {status}</p>}
            <div className="project-timeline-details">
              <CKEditorContent content={timeframeDetails} />
            </div>
          </>
        )}

        {body && (
          <>
            <Heading level={2}>Description</Heading>
            <CKEditorContent content={body} />
          </>
        )}

        {(members || leaders || sponsors || contact) && (
          <Heading level={2}>Team</Heading>
        )}

        {members && (
          <>
            <Heading level={3}>Members</Heading>
            <CKEditorContent content={members} />
          </>
        )}

        {leaders && (
          <>
            <Heading level={3}>Leaders</Heading>
            <CKEditorContent content={leaders} />
          </>
        )}

        {sponsors && (
          <>
            <Heading level={3}>Sponsors</Heading>
            <CKEditorContent content={sponsors} />
          </>
        )}

        {contact && (
          <>
            <Heading level={3}>Contact</Heading>
            <CKEditorContent content={contact} />
          </>
        )}

        {(additionalLinks.length > 0 || relatedCourses) && (
          <Heading level={2}>Related Content</Heading>
        )}

        {additionalLinks.length > 0 && (
          <>
            <Heading level={3}>Related Links</Heading>
            <ul>
              {additionalLinks.map((l, i) => (
                <li>
                  <Link
                    to={l.uri_alias ? l.uri_alias : l.uri}
                    key={"additional-link-" + i + 1}
                  >
                    {l.title}
                  </Link>
                </li>
              ))}
            </ul>
          </>
        )}

        {relatedCourses && (
          <>
            <Heading level={3}>Related Courses</Heading>
            <CKEditorContent content={relatedCourses} />
          </>
        )}

        {relatedProjects?.nodes?.length > 0 && (
          <RelatedProjects relatedProjects={relatedProjects} />
        )}
      </div>
      <Divider modifiers={["long"]} />
      {categories.length > 0 && (
        <>
          <div className="field-label">Categories</div>
          <p>
            <span className="category">{toSentence(categories)}</span>
          </p>
        </>
      )}
    </Container>
  )
}

Project.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string,
  imageDetails: PropTypes.shape(imageDetailsShape),
  additionalLinks: PropTypes.array,
  actionLink: PropTypes.object,
  contact: PropTypes.string,
  members: PropTypes.string,
  leaders: PropTypes.string,
  sponsors: PropTypes.string,
  relatedCourses: PropTypes.string,
  timeframeDetails: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.string),
  timeframeStart: PropTypes.string,
  timeframeEnd: PropTypes.string,
  status: PropTypes.string,
}

export default Project
