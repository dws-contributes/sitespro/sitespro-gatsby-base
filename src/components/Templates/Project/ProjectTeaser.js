/**
 * Project teaser display template
 */
import React from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import { Row, Col } from "react-bootstrap"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import decode from "components/_utils/decode"
import addEllipsis from "components/_utils/addEllipsis"
import "./Project.scss"

const ProjectTeaser = ({
  title,
  timeframe,
  link,
  summary,
  image,
  imageAlt,
  displayInGrid,
  block = "node-teaser",
  modifiers = "project",
  ariaLevel = 2,
}) => {
  let imageWidth = 4
  let titleWidth = image ? 8 : 12
  let featuredImageComponent = null
  if (image) {
    let imageNormalized = {
      imageDetails: {
        variants: { small_2_5_1: image },
        alt: imageAlt,
        uri: image,
      },
      targetedSizes: ["small_2_5_1"],
    }

    featuredImageComponent = (
      <div className="project-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  return (
    <article
      className={`${bem(block, modifiers)} ${
        displayInGrid === "grid" ? "display-in-grid" : ""
      }`}
    >
      <Row>
        {featuredImageComponent && (
          <Col lg={imageWidth}>{featuredImageComponent}</Col>
        )}
        <Col lg={titleWidth}>
          <Heading level={3} ariaLevel={ariaLevel} modifiers={["teaser"]}>
            <Link to={link}>{decode(title)}</Link>
          </Heading>

          {timeframe && timeframe.trim() !== "-" && (
            <div className={bem(block, "date", [])}>
              {timeframe.trim().replace(/^-|-$/, "")}
            </div>
          )}

          {summary && (
            <div className="highlight">
              <CKEditorContent content={addEllipsis(decode(summary))} />
            </div>
          )}
        </Col>
      </Row>
    </article>
  )
}

ProjectTeaser.propTypes = {
  date: PropTypes.string,
  summary: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default ProjectTeaser
