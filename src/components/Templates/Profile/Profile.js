import { Col, Row } from "react-bootstrap"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Heading from "components/Atoms/Heading/Heading"
import ProfileRelatedContentTeaser from "./ProfileRelatedContentTeaser"
import PropTypes from "prop-types"
import React from "react"
import bem from "components/_utils/bem"
import { MdLocalPhone, MdPlace } from "react-icons/md"
import { HiOutlineAtSymbol } from "react-icons/hi"
import MetaBox from "components/Atoms/MetaBox/MetaBox"
import "./Profile.scss"

export const Profile = ({
  roles,
  bio,
  email_address,
  phone_number,
  address_1,
  address_2,
  address_3,
  website_url,
  website_title,
  imageDetails,
  authoredContent,
  relatedContent,
  block = "profile",
}) => {
  let authoredItems = authoredContent?.map((item, index) => (
    <ProfileRelatedContentTeaser key={`authored-content-${index}`} {...item} />
  ))

  let relatedItems = relatedContent?.map((item, index) => (
    <ProfileRelatedContentTeaser key={`related-content-${index}`} {...item} />
  ))

  return (
    <section className={bem(block, "")}>
      <Row className="justify-content-between">
        <Col md={5} lg={4}>
          {imageDetails && (
            <div className={bem(block, "image")}>
              <BB8Image
                imageDetails={imageDetails}
                targetedSizes={["focal_point_large", "focal_point_medium"]}
              />
            </div>
          )}
          {(email_address || phone_number || address_1 || website_url) && (
            <MetaBox as={"aside"} className="aside-top profile-meta">
              <Heading level={4}>Contact Info</Heading>
              {email_address && (
                <div className="field-container">
                  <HiOutlineAtSymbol />
                  <p>
                    <a href={`mailto:${email_address}`}>{email_address}</a>
                  </p>
                </div>
              )}
              {phone_number && (
                <div className="field-container">
                  <MdLocalPhone />
                  <p>
                    <a href={`tel:${phone_number.replace(/[^\d]/g, "")}`}>
                      {phone_number}
                    </a>
                  </p>
                </div>
              )}

              {address_1 && (
                <div className="field-container">
                  <MdPlace />
                  <p className={bem(block, "address")}>
                    {address_1 && <span>{address_1}</span>}
                    {address_2 && <span>{address_2}</span>}
                    {address_3 && <span>{address_3}</span>}
                  </p>
                </div>
              )}

              {website_url && (
                <>
                  <div className="field-label">Personal Link</div>
                  <a href={website_url} className={bem(block, "personal-link")}>
                    {website_title ? website_title : website_url}
                  </a>
                </>
              )}
            </MetaBox>
          )}
        </Col>

        <Col md={6} lg={7}>
          {roles && (
            <Heading level={2}>
              {roles.map((role, index) => (
                <span key={index}>
                  {role}
                  <br />
                </span>
              ))}
            </Heading>
          )}

          {bio && (
            <>
              <CKEditorContent content={bio} />
            </>
          )}

          {authoredItems && authoredItems.length > 0 && (
            <section className="related-content">
              <Heading level={3}>Authored Posts</Heading>
              {authoredItems}
            </section>
          )}

          {relatedItems && relatedItems.length > 0 && (
            <section className="related-content">
              <Heading level={3}>Related Content</Heading>
              {relatedItems}
            </section>
          )}
        </Col>
      </Row>
    </section>
  )
}

Profile.propTypes = {
  roles: PropTypes.array,
  bio: PropTypes.string,
  email_address: PropTypes.string,
  phone_number: PropTypes.string,
  address_1: PropTypes.string,
  address_2: PropTypes.string,
  address_3: PropTypes.string,
  website_url: PropTypes.string,
  imageDetails: PropTypes.string,
  authoredContent: PropTypes.array,
  relatedContent: PropTypes.array,
  block: PropTypes.string,
}

export default Profile
