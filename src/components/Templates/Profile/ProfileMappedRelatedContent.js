export function MapRelatedItems(items) {
  const mappedItems =
    items &&
    items.map(function (item, i) {
      let url = item?.field_alternate_link?.uri
        ? item?.field_alternate_link?.uri
        : item?.path?.alias

      return {
        title: item.title,
        url: url,
        date: item?.date ? item?.date : item?.field_event_date?.date,
        type: item?.relationships?.node_type?.name,
      }
    })
  return mappedItems
}
