/**
 * Profile teaser display template
 */
import React from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import ProfilePlaceholder from "images/ProfilePlaceholder.jpg"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import decode from "components/_utils/decode"
import HTMLEllipsis from "react-lines-ellipsis/lib/html"
import "../Profile/Profile.scss"

export const ProfileTeaser = ({
  link,
  title,
  subtitle,
  body,
  summary,
  type,
  displayBio = true,
  displayScholarsSummary = true,
  image,
  imageAlt,
  displayPosition = true,
  block = "profile-teaser",
  modifiers,
}) => {
  let imageNormalized = null
  if (image) {
    imageNormalized = {
      imageDetails: {
        variants: { focal_point_large: image },
        alt: title,
        uri: image,
      },
      targetedSizes: ["focal_point_large"],
    }
  }

  return (
    <div className={`${bem(block, modifiers)}`}>
      <div className={bem(block, "image")}>
        {imageNormalized ? (
          <BB8Image
            imageDetails={imageNormalized?.imageDetails}
            targetedSizes={imageNormalized?.targetedSizes}
          />
        ) : (
          <img src={ProfilePlaceholder} alt={title} />
        )}
      </div>

      {/* Name */}
      {title && (
        <div className={`${bem(block, "name")}`}>
          <Link to={link}>{decode(title)}</Link>
          {subtitle && (
            <p className={`${bem(block, "role")}`}>{decode(subtitle)}</p>
          )}
        </div>
      )}

      {/* Summary */}
      {summary && displayScholarsSummary && type === "Scholars@Duke Profile" ? (
        <div className="person-card-summary">{summary}</div>
      ) : (
        ""
      )}

      {body && displayBio ? (
        <div className="person-card-body">
          <HTMLEllipsis
            unsafeHTML={body}
            maxLine="3"
            ellipsis="..."
            basedOn="words"
          />
        </div>
      ) : (
        ""
      )}
    </div>
  )
}

ProfileTeaser.propTypes = {
  link: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  modifiers: PropTypes.array,
  displayPosition: PropTypes.bool,
  body: PropTypes.string,
}

export default ProfileTeaser
