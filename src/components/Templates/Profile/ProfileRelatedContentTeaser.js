import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import Link from "components/Atoms/Link/Link"
import dayjs from "dayjs"

const ProfileRelatedContentTeaser = ({
  title,
  date,
  url,
  type,
  block = "profile",
}) => {
  return (
    <div className={bem(block, "related-content-item")}>
      <Eyebrow text={type} />
      <p>
        <Link to={url}>{title}</Link>
      </p>
      <span className="date">{dayjs(date).format("MMMM D, YYYY")}</span>
    </div>
  )
}

ProfileRelatedContentTeaser.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  url: PropTypes.string,
  type: PropTypes.string,
  block: PropTypes.string,
}

export default ProfileRelatedContentTeaser
