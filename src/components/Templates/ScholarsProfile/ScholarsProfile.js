import "../Profile/Profile.scss"
import "@dws-contributes/duke-scholars-widgets-react/dist/index.css"

import {
  Awards,
  Courses,
  Educations,
  Grants,
  Newsfeeds,
  Overview,
  Publications,
} from "@dws-contributes/duke-scholars-widgets-react"
import { Col, Row } from "react-bootstrap"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import BB8Accordion from "components/Atoms/Accordion/BB8Accordion"
import BB8AccordionItem from "components/Atoms/Accordion/BB8AccordionItem"
import Heading from "components/Atoms/Heading/Heading"
import MetaBox from "components/Atoms/MetaBox/MetaBox"
import ProfileRelatedContentTeaser from "../Profile/ProfileRelatedContentTeaser"
import PropTypes from "prop-types"
import React from "react"
import bem from "components/_utils/bem"
import { MdLocalPhone, MdPlace } from "react-icons/md"
import { HiOutlineAtSymbol } from "react-icons/hi"

function _handleClick(event) {
  // event.target.closest(".accordion-toggle").classList.toggle("expanded")
}

const _scholarsAccordion = ({ children, displayLabel }) => {
  let noSpaces = displayLabel.replace(/\s/g, "")

  return (
    <BB8AccordionItem
      children={children}
      id={noSpaces}
      heading={displayLabel}
      key={noSpaces}
      handleClick={_handleClick}
    />
  )
}

const ScholarsProfile = ({
  preferred_title,
  additional_information,
  roles,
  email_address,
  phone_number,
  address_1,
  address_2,
  address_3,
  website_url,
  thumbnail_photo,
  portrait_photo,
  alt_text,
  scholarsUri,
  related_content,
  block = "profile",
  element = "profile",
}) => {
  const accordionItems = {
    overview: (
      <Overview
        displayLabel={false}
        uri={scholarsUri}
        wrapper={_scholarsAccordion}
        key="overview"
      />
    ),
    news: (
      <Newsfeeds
        displayLabel={false}
        uri={scholarsUri}
        wrapper={_scholarsAccordion}
        numberToDisplay="5"
        key="newsfeeds"
      />
    ),
    awards: (
      <Awards
        displayLabel={false}
        uri={scholarsUri}
        wrapper={_scholarsAccordion}
        numberToDisplay="5"
        key="awards"
      />
    ),
    publications: (
      <Publications
        displayLabel={false}
        uri={scholarsUri}
        wrapper={_scholarsAccordion}
        numberToDisplay={5}
        key="publications"
      />
    ),
    grants: (
      <Grants
        displayLabel={false}
        uri={scholarsUri}
        wrapper={_scholarsAccordion}
        numberToDisplay="5"
        key="grants"
      />
    ),
    courses: (
      <Courses
        displayLabel={false}
        uri={scholarsUri}
        wrapper={_scholarsAccordion}
        numberToDisplay="5"
        key="courses"
      />
    ),
  }

  let relatedItems = related_content?.map((item, index) => (
    <ProfileRelatedContentTeaser key={`related-content-${index}`} {...item} />
  ))

  return (
    <section className={bem(block, "")}>
      <Row className="justify-content-between">
        <Col md={5} lg={4}>
          {portrait_photo && (
            <div className={bem(block, "image")}>
              <img src={portrait_photo} alt={alt_text} />
            </div>
          )}

          <MetaBox as={"aside"} className="aside-top profile-meta">
            <Heading level={4}>Contact Info</Heading>
            {email_address && (
              <>
                <div className="field-container">
                  <HiOutlineAtSymbol />
                  <p>
                    <a href={`mailto:${email_address}`}>{email_address}</a>
                  </p>
                </div>
              </>
            )}
            {phone_number && (
              <>
                <div className="field-container">
                  <MdLocalPhone />
                  <p>
                    <a href={`tel:${phone_number.replace(/[^\d]/g, "")}`}>
                      {phone_number}
                    </a>
                  </p>
                </div>
              </>
            )}

            {address_1 && (
              <div className="field-container">
                <MdPlace />
                <p className={bem(block, "address")}>
                  {/* Physical address only */}
                  {address_1 && <span>{address_1}</span>}
                  {/* {address_2 && <span>{address_2}</span>}
                  {address_3 && <span>{address_3}</span>} */}
                </p>
              </div>
            )}

            {website_url && (
              <>
                <div className="field-label">Personal Link</div>
                <a href={website_url} className={bem(block, "personal-link")}>
                  {website_url}
                </a>
              </>
            )}
          </MetaBox>
        </Col>
        <Col md={6} lg={7}>
          {preferred_title && <Heading level={2}>{preferred_title}</Heading>}
          {roles && (
            <div className={bem(block, "position")}>
              {roles.map((role, index) => (
                <span key={index}>
                  {role}
                  <br />
                </span>
              ))}
            </div>
          )}

          <Educations displayLabel={true} uri={scholarsUri} />

          {additional_information && (
            <div className="body">
              <CKEditorContent content={additional_information} />
            </div>
          )}

          {/* begin accordion */}
          <BB8Accordion defaultActiveKey="Overview" className="bb8accordion">
            {Object.keys(accordionItems).map(function (key, index) {
              return accordionItems[key]
            })}
          </BB8Accordion>
          {relatedItems && relatedItems.length > 0 && (
            <section className="related-content">
              <Heading level={3}>Related Content</Heading>
              {relatedItems}
            </section>
          )}
        </Col>
      </Row>
    </section>
  )
}

ScholarsProfile.propTypes = {
  preferred_title: PropTypes.string,
  additional_information: PropTypes.string,
  roles: PropTypes.array,
  email_address: PropTypes.string,
  phone_number: PropTypes.string,
  address_1: PropTypes.string,
  address_2: PropTypes.string,
  address_3: PropTypes.string,
  website_url: PropTypes.string,
  thumbnail_photo: PropTypes.string,
  portrait_photo: PropTypes.string,
  alt_text: PropTypes.string,
  scholarsUri: PropTypes.string.isRequired,
  related_content: PropTypes.array,
  block: PropTypes.string,
  element: PropTypes.string,
}

export default ScholarsProfile
