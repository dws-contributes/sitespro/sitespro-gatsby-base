/**
 * ScholarsProfile teaser display template
 */
import React from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import ProfilePlaceholder from "images/ProfilePlaceholder.jpg"
import "../Profile/Profile.scss"
import decode from "components/_utils/decode"

export const ScholarsProfileTeaser = ({
  link,
  title,
  subtitle,
  image,
  summary,
  type,
  displayScholarsSummary = true,
  imageAlt,
  displayPosition = true,
  block = "profile-teaser",
  modifiers,
}) => {
  return (
    <div className={`${bem(block, modifiers)}`}>
      <div className={bem(block, "image")}>
        {image ? (
          <img src={image} alt={title} />
        ) : (
          <img src={ProfilePlaceholder} alt={title} />
        )}
      </div>

      {/* Name */}
      {title && (
        <h5 className={`${bem(block, "name")}`}>
          <Link to={link}>{decode(title)}</Link>
          {subtitle && (
            <p className={`${bem(block, "role")}`}>{decode(subtitle)}</p>
          )}
        </h5>
      )}

      {/* Summary */}
      {summary && displayScholarsSummary ? (
        <div className="person-card-summary">{summary}</div>
      ) : (
        ""
      )}
    </div>
  )
}

ScholarsProfileTeaser.propTypes = {
  link: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  modifiers: PropTypes.array,
  displayPosition: PropTypes.bool,
}

export default ScholarsProfileTeaser
