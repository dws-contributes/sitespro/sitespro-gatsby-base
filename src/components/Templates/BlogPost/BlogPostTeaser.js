/**
 * BlogPost teaser display template
 */
import React, { Fragment } from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import { Row, Col } from "react-bootstrap"
import bem from "components/_utils/bem"
import {
  FormattedAuthors,
  AuthorsStringToObject,
} from "components/_utils/FormattedAuthors"
import Heading from "components/Atoms/Heading/Heading"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import { BsDot } from "react-icons/bs"
import decode from "components/_utils/decode"
import addEllipsis from "components/_utils/addEllipsis"
import "./BlogPost.scss"

// TODO: BsDot is used here, but character codes elsewhere. I do like the <span> wrapped around it for styling.
// TODO: news-featured-image on the image component?
// TODO: Can this be combined with blog teaser?

const BlogPostTeaser = ({
  title,
  date,
  link,
  summary,
  image,
  imageAlt,
  authors,
  block = "node-teaser",
  modifiers = "blog",
}) => {
  let imageWidth = 4
  let titleWidth = image ? 8 : 12

  let featuredImageComponent = null
  if (image) {
    let imageNormalized = {
      imageDetails: {
        variants: { small_2_5_1: image },
        alt: imageAlt,
        uri: image,
      },
      targetedSizes: ["small_2_5_1"],
    }
    featuredImageComponent = (
      <div className="news-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  return (
    <article className={`${bem(block, modifiers)}`}>
      <Row>
        {featuredImageComponent && (
          <Col md={imageWidth}>{featuredImageComponent}</Col>
        )}
        <Col md={titleWidth}>
          <Heading level={3} modifiers={["teaser"]}>
            <Link to={link}>{decode(title)}</Link>
          </Heading>

          {summary && (
            <div className="summary">
              <CKEditorContent content={decode(addEllipsis(summary))} />
            </div>
          )}

          <div className={bem(block, "meta", ["date"])}>
            <DateProcessor startDate={date} />

            {authors && (
              <Fragment>
                <span className={bem(block, "meta", ["dot"])}>
                  <BsDot />
                </span>
                <span className={bem(block, "meta", ["authors"])}>
                  <FormattedAuthors
                    authorsArray={AuthorsStringToObject(authors)}
                  />
                </span>
              </Fragment>
            )}
          </div>
        </Col>
      </Row>
    </article>
  )
}

BlogPostTeaser.propTypes = {
  date: PropTypes.string,
  summary: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  authors: PropTypes.string,
}

export default BlogPostTeaser
