import React from "react"
import PropTypes from "prop-types"
import Heading from "components/Atoms/Heading/Heading"
import Meta from "components/Molecules/Meta/Meta"
import Share from "components/Atoms/Share/Share"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Divider from "components/Atoms/Divider/Divider"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import Categories from "components/Molecules/Categories/Categories"
import bem from "components/_utils/bem"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import { Row, Col } from "react-bootstrap"
import "./BlogPost.scss"
import {
  CustomBreadcrumbs,
  GetCustomParents,
} from "components/Molecules/Breadcrumbs/Breadcrumbs"

const BlogPost = ({
  title,
  description,
  summary,
  date,
  imageDetails,
  categories,
  authors,
  slideShow,
  block = "blog-post",
  modifiers = imageDetails?.variants ? "withimage" : "noimage",
}) => {
  return (
    <>
      {imageDetails?.variants ? (
        <div className="blog-featured-image">
          <BB8Image
            imageDetails={imageDetails}
            targetedSizes={["large_2_5_1", "small_2_5_1"]}
          />
        </div>
      ) : (
        ""
      )}
      <div className={bem(block, "content", [modifiers])}>
        <CustomBreadcrumbs
          pageTitle={title}
          customParents={GetCustomParents("blog")}
        />

        <Eyebrow text="Blog" />
        <Heading level={1}>{title}</Heading>

        <Meta authors={authors} date={date} />

        <Divider />
        <Share
          shareTitle="Share this post"
          pageTitle={title}
          pageSummary={summary}
        />
        {description && (
          <div className="body">
            <CKEditorContent content={description} />
          </div>
        )}

        {slideShow && (
          <Row className={bem(block, "slideShow", [])}>
            <Col md={12}>{slideShow}</Col>
          </Row>
        )}

        <Divider modifiers={["long"]} />

        {categories && (
          <Row className={bem(block, "categories", [])}>
            <Col md={12}>
              <Categories categories={categories} linkto={"blog"} />
            </Col>
          </Row>
        )}
      </div>
    </>
  )
}

BlogPost.propTypes = {
  summary: PropTypes.string,
  featured_media: PropTypes.object,
}

export default BlogPost
