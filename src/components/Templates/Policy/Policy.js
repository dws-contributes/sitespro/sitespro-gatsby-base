import React from "react"
import PropTypes from "prop-types"
import Heading from "components/Atoms/Heading/Heading"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Link from "components/Atoms/Link/Link"
import Divider from "components/Atoms/Divider/Divider"
import Media from "components/Atoms/Media/Media"
import toSentence from "components/_utils/toSentence"
import "./Policy.scss"

const Policy = ({
  title,
  body,
  additionalLinks,
  owners,
  date,
  lastModified,
  lastReviewed,
  documents,
  reviewFrequency,
  categories,
  types,
}) => {
  const showSidebar =
    owners || date || lastModified || lastReviewed || reviewFrequency
  return (
    <>
      <div className={`row`}>
        <div className="col-lg-8 col-sm-12 flex-grow-1">
          <Heading level={1}>{title}</Heading>
          {body && <CKEditorContent content={body} />}
        </div>
        <div className="col-lg-4 col-sm-12">
          {showSidebar && (
            <div className="policy-sidebar">
              {owners && (
                <>
                  <Heading level={3}>Owner</Heading>
                  <CKEditorContent content={owners} />
                </>
              )}

              {date && (
                <>
                  <Heading level={3}>Issued Date</Heading>
                  {date}
                </>
              )}

              {lastModified && (
                <>
                  <Heading level={3}>Last Revised</Heading>
                  {lastModified}
                </>
              )}

              {lastReviewed && (
                <>
                  <Heading level={3}>Last Reviewed</Heading>
                  {lastReviewed}
                </>
              )}

              {reviewFrequency && (
                <>
                  <Heading level={3}>Review Frequency</Heading>
                  <CKEditorContent content={reviewFrequency} />
                </>
              )}
            </div>
          )}
          {documents.length > 0 && (
            <>
              <Heading level={5}>Related Documents</Heading>
              <ul>
                {documents.map((m, i) => (
                  <li key={"related-documents-" + i + 1}>{Media(m)}</li>
                ))}
              </ul>
            </>
          )}
          {additionalLinks.length > 0 && (
            <>
              <Heading level={5}>Related Links</Heading>
              <ul>
                {additionalLinks.map((l, i) => (
                  <li key={"additional-link-" + i + 1}>
                    <Link to={l.uri_alias}>{l.title}</Link>
                  </li>
                ))}
              </ul>
            </>
          )}
        </div>
      </div>

      <Divider modifiers={["long"]} />

      {types.length > 0 && (
        <>
          <div className="field-label">Policy types</div>
          <p>
            <span className="category">{toSentence(types)}</span>
          </p>
        </>
      )}

      {categories.length > 0 && (
        <>
          <div className="field-label">Categories</div>
          <p>
            <span className="category">{toSentence(categories)}</span>
          </p>
        </>
      )}
    </>
  )
}

Policy.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string,
  additionalLinks: PropTypes.array,
  owners: PropTypes.string,
  date: PropTypes.string,
  lastModified: PropTypes.string,
  lastReviewed: PropTypes.string,
  documents: PropTypes.array,
  reviewFrequency: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.string),
  types: PropTypes.arrayOf(PropTypes.string),
}

export default Policy
