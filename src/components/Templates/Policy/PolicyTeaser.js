/**
 * Policy teaser display template
 */
import React from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import { Row, Col } from "react-bootstrap"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import decode from "components/_utils/decode"
import addEllipsis from "components/_utils/addEllipsis"
import "./Policy.scss"

const PolicyTeaser = ({
  title,
  link,
  summary,
  block = "node-teaser",
  modifiers = "policy",
  ariaLevel = 2,
}) => {
  return (
    <article className={`${bem(block, modifiers)}`}>
      <Row>
        <Col>
          <Heading level={3} ariaLevel={ariaLevel} modifiers={["teaser"]}>
            <Link to={link}>{decode(title)}</Link>
          </Heading>

          {summary && (
            <div className="highlight">
              <CKEditorContent content={addEllipsis(decode(summary))} />
            </div>
          )}
        </Col>
      </Row>
    </article>
  )
}

PolicyTeaser.propTypes = {
  summary: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default PolicyTeaser
