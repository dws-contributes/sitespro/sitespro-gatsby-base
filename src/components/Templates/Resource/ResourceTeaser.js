import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Heading from "components/Atoms/Heading/Heading"
import Link from "components/Atoms/Link/Link"
import bem from "components/_utils/bem"
import addEllipsis from "components/_utils/addEllipsis"
import decode from "components/_utils/decode"
import { BB8Image } from "components/Atoms/Image/BB8Image"

import "./Resource.scss"

const ResourceTeaser = ({
  title,
  date,
  summary,
  link,
  image,
  imageAlt,
  displayInGrid,
  block = "resource",
  modifiers = "",
  ariaLevel = 2,
}) => {
  let imageNormalized = null
  if (image) {
    imageNormalized = {
      imageDetails: {
        variants: {
          [displayInGrid === "grid" ? "small_3_2" : "focal_point_large"]:
            displayInGrid === "grid"
              ? image.replace("focal_point_large", "small_3_2")
              : image,
        },
        alt: imageAlt,
        uri: image,
      },
      targetedSizes: [
        displayInGrid === "grid" ? "small_3_2" : "focal_point_large",
      ],
    }
  }

  let featuredImageComponent = null
  if (imageNormalized) {
    featuredImageComponent = (
      <div className="resource-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  return (
    <article
      className={`node-teaser ${bem(block, modifiers)} ${
        displayInGrid === "grid" ? "display-in-grid" : ""
      }`}
    >
      {featuredImageComponent && (
        <div className="image-wrapper flex-shrink-0">
          {featuredImageComponent}
        </div>
      )}

      <div className="text-wrapper">
        <Heading level={3} ariaLevel={ariaLevel} modifiers={["teaser"]}>
          <Link to={link}>{decode(title)}</Link>
        </Heading>

        {summary && !link.includes("/secure/") && (
          <div className="summary">
            <CKEditorContent content={addEllipsis(decode(summary))} />
          </div>
        )}
      </div>
    </article>
  )
}

ResourceTeaser.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  link: PropTypes.string,
  summary: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  date: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default ResourceTeaser
