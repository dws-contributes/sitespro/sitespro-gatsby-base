import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import Heading from "components/Atoms/Heading/Heading"
import Divider from "components/Atoms/Divider/Divider"
import "./Resource.scss"

const Resource = ({
  title,
  description,
  categories,
  lastModified,
  block = "resource",
}) => {
  return (
    <div className="resource-wrapper">
      <Heading level={1}>{title}</Heading>

      {lastModified && (
        <div className="resource-date">
          Revised <DateProcessor startDate={lastModified} />
        </div>
      )}

      {description && <CKEditorContent content={description} />}

      {categories.length > 0 && (
        <>
          <Divider />
          <div className="field-label">Categories</div>
          <p>
            <span className="category">{categories.join(", ")}</span>
          </p>
        </>
      )}
    </div>
  )
}

Resource.propTypes = {
  title: PropTypes.string,
  lastModified: PropTypes.string,
  owners: PropTypes.string,
  description: PropTypes.string,
  summary: PropTypes.string,
}

export default Resource
