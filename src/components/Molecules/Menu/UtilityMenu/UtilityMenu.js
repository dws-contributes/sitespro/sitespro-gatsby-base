import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "components/_utils/bem"
import "./Utility-menu.scss"
import { CountLinksInMenu } from "hooks/MenuData"

const UtilityMenu = ({ block = "utilitymenu", element = "", modifiers }) => {
  const utilityMenuLinkCount = CountLinksInMenu("utility")

  if (utilityMenuLinkCount > 0) {
    return (
      <nav
        className={`nav ${bem(block, element, modifiers)}`}
        aria-labelledby="utilitymenulabel"
      >
        <h2 id="utilitymenulabel" className="visually-hidden">
          Utility menu
        </h2>
        <Menu menuName={`utility`} startLevel={0} ignoreChildren={true} />
      </nav>
    )
  } else {
    return ""
  }
}

UtilityMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default UtilityMenu
