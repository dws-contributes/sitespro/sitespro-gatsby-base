import React, { useState } from "react"
import PropTypes from "prop-types"
import { Nav, Navbar } from "react-bootstrap"
import Menu from "../MenuWalker"
import bem from "components/_utils/bem"
import "./Sidebar-menu.scss"

const SidebarMenu = ({ block = "sidebar-menu", element = "", modifiers }) => {
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false)
  const mobileMenuToggle = () => setIsMobileMenuOpen(!isMobileMenuOpen)

  return (
    <Navbar
      expand="lg"
      aria-label="sub navigation"
      className={`${bem(block, element, modifiers)}`}
    >
      <Navbar.Toggle
        aria-controls="navbar-nav"
        aria-haspopup="true"
        onClick={mobileMenuToggle}
        aria-expanded={isMobileMenuOpen}
      >
        <span className="navbar-toggle-word">IN THIS SECTION</span>
      </Navbar.Toggle>
      <Navbar.Collapse id="sidebar-nav">
        <Nav className="sidebar-nav-inner">
          <Menu menuName={`main`} startLevel={0} menuLocation={`sidebar`} />
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

SidebarMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default SidebarMenu
