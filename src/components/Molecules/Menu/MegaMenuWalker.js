import React, { useState, useEffect } from "react"
import { Nav, Dropdown, NavLink, NavItem } from "react-bootstrap"
import PropTypes from "prop-types"
import { globalHistory } from "@gatsbyjs/reach-router"
import { useMenuData } from "hooks/MenuData"
import { ErrorCatch } from "components/_utils/ErrorCatch"
import { replacePath } from "components/_utils/replacePath"
import MenuWaiting from "./MenuWaiting"
import Link from "components/Atoms/Link/Link"

function buildDropdown(menuArray) {
  if (!menuArray) {
    return
  }
  let menu = []

  try {
    for (let item in menuArray) {
      let link = menuArray[item].link.uri_alias
        ? menuArray[item].link.uri_alias
        : menuArray[item].link.uri.replace("internal:", "")

      // Don't show link in menu if no link exists
      if (link !== "route:<nolink>") {
        menu.push(
          <Link
            key={menuArray[item].drupal_id}
            title={menuArray[item].description}
            className={"dropdown-item nav-link"}
            to={link}
          >
            {menuArray[item].title}
          </Link>
        )
      }
    }
    return menu
  } catch (error) {
    ErrorCatch(error)
  }
}

const MegaMenuDropdownItem = ({ menuItem, topParentID, menuChildren }) => {
  const [isOpen, setOpen] = React.useState(false)

  return (
    <Dropdown
      as={NavItem}
      id={menuItem.drupal_id}
      bsPrefix={""}
      navbar={true}
      focusFirstItemOnShow={true}
      onToggle={() => {
        setOpen(!isOpen)
      }}
    >
      <Dropdown.Toggle
        as={NavLink}
        id={`dropdownToggle-${menuItem.drupal_id}`}
        key={menuItem.drupal_id}
        className={`${menuItem.drupal_id === topParentID ? `active` : ``}`}
        aria-controls={`dropdown-${menuItem.drupal_id}`}
        aria-expanded={isOpen}
        title={menuItem.description}
      >
        {menuItem.title}
        <span className="dropdown-caret"></span>
      </Dropdown.Toggle>

      <Dropdown.Menu renderOnMount={true}>
        <div className="container">
          <div className="dropdown-links">{buildDropdown(menuChildren)}</div>
        </div>
      </Dropdown.Menu>
    </Dropdown>
  )
}

const MegaMenu = ({ storyItems, menuName, startLevel }) => {
  const menuDataDrupal = useMenuData()
  const menuData = storyItems ? storyItems : menuDataDrupal
  const [hasMounted, setHasMounted] = useState(false)

  useEffect(() => {
    setHasMounted(true)
  }, [])

  // if (!hasMounted) {
  //   return null
  // }

  const { location } = globalHistory

  function createMenuHierarchy(menuData, menuName) {
    let tree = [],
      mappedArr = {},
      arrElem,
      mappedElem

    try {
      // First map the nodes of the array to an object -> create a hash table.
      for (let i = 0, len = menuData.length; i < len; i++) {
        arrElem = menuData[i].node
        if (arrElem.menu_name === menuName && arrElem.enabled === true) {
          mappedArr[arrElem.drupal_id] = arrElem
          if (
            arrElem.drupal_parent_menu_item != null &&
            arrElem.drupal_parent_menu_item.includes(arrElem.bundle)
          ) {
            let stripped_drupal_id = arrElem.drupal_parent_menu_item.replace(
              arrElem.bundle + ":",
              ""
            )
            mappedArr[arrElem.drupal_id].drupal_parent_menu_item =
              stripped_drupal_id
          }
          mappedArr[arrElem.drupal_id]["children"] = []
        }
      }

      for (let id in mappedArr) {
        if (mappedArr.hasOwnProperty(id)) {
          mappedElem = mappedArr[id]
          // If the element is not at the root level, add it to its parent array of children.
          if (
            mappedElem.drupal_parent_menu_item &&
            mappedArr[mappedElem.drupal_parent_menu_item]
          ) {
            mappedArr[mappedElem.drupal_parent_menu_item]["children"].push(
              mappedElem
            )
          }
          // If the element is at the root level, add it to first level elements array.
          else {
            tree.push(mappedElem)
          }
        }
      }
      return tree
    } catch (error) {
      ErrorCatch(error)
    }
  }

  function buildLink(link, classes = "") {
    try {
      if (!link.external && link.link.uri_alias) {
        return (
          <Link
            to={link.link.uri_alias}
            className={`nav-link ${classes}`}
            title={link.description}
          >
            {link.title}
          </Link>
        )
      } else if (!link.external && link.link.uri.includes("internal:")) {
        // TODO: Where does this happen? Should it be removed?
        return (
          <Link
            to={link.link.uri.replace("internal:", "")}
            className={`nav-link hide-icon ${classes}`}
            title={link.description}
          >
            {link.title}
          </Link>
        )
      } else {
        return (
          <Nav.Link
            href={link.link.uri_alias}
            className={`external nav-link ${classes}`}
            title={link.description}
          >
            {link.title}
          </Nav.Link>
        )
      }
    } catch (error) {
      ErrorCatch(error)
    }
  }

  function buildMenu(menuArray, topParentID) {
    if (!menuArray) {
      return
    }
    let menu = []

    try {
      for (let item in menuArray) {
        let menuItem = [menuArray[item]].concat(menuArray[item].children)
        if (menuArray[item].children.length !== 0) {
          menu.push(
            <MegaMenuDropdownItem
              key={item}
              menuItem={menuArray[item]}
              topParentID={topParentID}
              menuChildren={menuItem}
            />
          )
        } else {
          menu.push(
            <Nav.Item key={menuArray[item].drupal_id} className="nav-item">
              {buildLink(menuArray[item])}
            </Nav.Item>
          )
        }
      }
      return menu
    } catch (error) {
      ErrorCatch(error)
    }
  }

  function generateMenu(pageSlug, menuLinks, menuName, startLevel) {
    let menu
    let currentPage = {
      path: pageSlug,
      drupal_id: 0,
      parent_id: 0,
    }
    let menuLinksArray = menuLinks

    try {
      // homepage
      if (currentPage.path === "/") {
        currentPage.path = null
      }

      for (let item of menuLinksArray) {
        let uriAliasWithSlash = replacePath(item.node.link.uri_alias)

        if (uriAliasWithSlash === currentPage.path) {
          currentPage.drupal_id = item.node.drupal_id
          currentPage.parent_id = item.node.drupal_parent_menu_item
          break
        }
      }

      function getParent(childID, nodeArray) {
        let parentID = 0

        for (let item of nodeArray) {
          if (
            item.node.drupal_id === childID &&
            item.node.drupal_parent_menu_item
          ) {
            if (
              item.node.drupal_parent_menu_item.includes("menu_link_content:")
            ) {
              item.node.drupal_parent_menu_item =
                item.node.drupal_parent_menu_item.replace(
                  "menu_link_content:",
                  ""
                )
            }
            parentID = item.node.drupal_parent_menu_item
            break
          }
        }
        return parentID
      }

      // Find the top level parent
      function getTopParent(childID, nodeArray) {
        let topParentID = 0
        let nextParentID = getParent(childID, nodeArray)
        if (nextParentID === 0) {
          topParentID = childID
        } else {
          while (nextParentID !== 0) {
            topParentID = nextParentID
            nextParentID = getParent(nextParentID, nodeArray)
          }
        }
        return topParentID
      }

      let topParentID = getTopParent(currentPage.drupal_id, menuLinksArray)

      menu = createMenuHierarchy(menuLinks, menuName)

      if (startLevel === 1 && currentPage.path !== null) {
        let newMenu = menu
        menu.forEach(function (item, index) {
          if (item.drupal_id === topParentID) {
            newMenu = item.children
          }
        })
        menu = newMenu
      }

      menu = buildMenu(menu, topParentID)
      return menu
    } catch (error) {
      ErrorCatch(error)
    }
  }

  return (
    <>
      {!hasMounted && <MenuWaiting />}
      {hasMounted &&
        generateMenu(location.pathname, menuData, menuName, startLevel)}
    </>
  )
}

MegaMenu.propTypes = {
  menuData: PropTypes.array,
  menuName: PropTypes.string,
  startLevel: PropTypes.number,
  storyItems: PropTypes.array,
}

MegaMenu.defaultProps = {
  menuName: `main`,
  startLevel: 0,
}

export default MegaMenu
