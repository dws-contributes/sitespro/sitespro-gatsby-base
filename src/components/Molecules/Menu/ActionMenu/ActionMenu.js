import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "components/_utils/bem"
import "./Action-menu.scss"
import { CountLinksInMenu } from "hooks/MenuData"

const ActionMenu = ({ block = "actionmenu", element = "", modifiers }) => {
  const actionMenuLinkCount = CountLinksInMenu("action")

  // Don't return the action menu nav if there are no enabled action menu links
  if (actionMenuLinkCount > 0) {
    return (
      <nav
        className={`nav ${bem(block, element, modifiers)}`}
        aria-labelledby="actionmenulabel"
      >
        <h2 id="actionmenulabel" className="visually-hidden">
          Action menu
        </h2>
        <Menu menuName={`action`} startLevel={0} />
      </nav>
    )
  } else {
    return ""
  }
}

ActionMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ActionMenu
