import React from "react"
import PropTypes from "prop-types"
import MegaMenu from "../MegaMenuWalker"
import bem from "../../../_utils/bem"
import "./main-menu.scss"
import { Nav } from "react-bootstrap"

const MainMenu = ({ block = "mainmenu", element = "", modifiers }) => {
  return (
    <nav
      className={`navbar-nav ${bem(block, element, modifiers)}`}
      aria-labelledby="primary-navigation"
    >
      <h2 id="primary-navigation" className="visually-hidden">
        Primary navigation
      </h2>
      <Nav>
          <MegaMenu menuName={`main`} startLevel={0} />
      </Nav>
    </nav>
  )
}

MainMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default MainMenu
