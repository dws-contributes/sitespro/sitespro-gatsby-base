import React from "react"
import PropTypes from "prop-types"
import Menu from "../MenuWalker"
import bem from "components/_utils/bem"
import { CountLinksInMenu } from "hooks/MenuData"
import "./Footer-menu.scss"

const FooterMenu = ({ block = "footer-menu", element = "", modifiers }) => {
  const footerMenuLinkCount = CountLinksInMenu("footer")

  if (footerMenuLinkCount > 0) {
    return (
      <nav
        className={`nav ${bem(block, element, modifiers)}`}
        aria-label="footer quick links"
      >
        <Menu menuName={`footer`} startLevel={0} />
      </nav>
    )
  } else {
    return ""
  }
}

FooterMenu.propTypes = {
  block: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default FooterMenu
