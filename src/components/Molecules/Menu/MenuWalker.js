import React, { useState, useEffect } from "react"
import Link from "components/Atoms/Link/Link"
import PropTypes from "prop-types"
import { globalHistory } from "@reach/router"
import { useMenuData } from "hooks/MenuData"
import { ErrorCatch } from "components/_utils/ErrorCatch"
import { replacePath } from "components/_utils/replacePath"

function createMenuHierarchy(menuData, menuName) {
  let tree = [],
    mappedArr = {},
    arrElem,
    mappedElem

  try {
    // First map the nodes of the array to an object -> create a hash table.
    for (let i = 0, len = menuData.length; i < len; i++) {
      arrElem = menuData[i].node
      if (arrElem.menu_name === menuName && arrElem.enabled === true) {
        mappedArr[arrElem.drupal_id] = arrElem
        if (
          arrElem.drupal_parent_menu_item != null &&
          arrElem.drupal_parent_menu_item.includes(arrElem.bundle)
        ) {
          let stripped_drupal_id = arrElem.drupal_parent_menu_item.replace(
            arrElem.bundle + ":",
            ""
          )
          mappedArr[arrElem.drupal_id].drupal_parent_menu_item =
            stripped_drupal_id
        }
        mappedArr[arrElem.drupal_id]["children"] = []
      }
    }

    for (let id in mappedArr) {
      if (mappedArr.hasOwnProperty(id)) {
        mappedElem = mappedArr[id]
        // If the element is not at the root level, add it to its parent array of children.
        if (
          mappedElem.drupal_parent_menu_item &&
          mappedArr[mappedElem.drupal_parent_menu_item]
        ) {
          mappedArr[mappedElem.drupal_parent_menu_item]["children"].push(
            mappedElem
          )
        }
        // If the element is at the root level, add it to first level elements array.
        else {
          tree.push(mappedElem)
        }
      }
    }
    return tree
  } catch (error) {
    ErrorCatch(error)
  }
}

function buildLink(link, classes = "") {
  try {
    if (
      !link.external &&
      link.link.uri_alias &&
      !link.link.uri_alias.includes("http")
    ) {
      return (
        <Link
          className={`nav-link ${classes}`}
          to={link.link.uri_alias}
          hideExternalIcon={true}
        >
          {link.title}
        </Link>
      )
    } else if (!link.external && link.link.uri.includes("internal:")) {
      return (
        <Link
          className={`nav-link ${classes}`}
          to={`${link.link.uri.replace("internal:", "")}`}
          hideExternalIcon={true}
        >
          {link.title}
        </Link>
      )
    } else {
      return (
        <a href={link.link.uri} className={`external nav-link ${classes}`}>
          {link.title}
        </a>
      )
    }
  } catch (error) {
    ErrorCatch(error)
  }
}

function buildMenu(menuArray, currPage, ignoreChildren) {
  if (!menuArray) {
    return
  }
  let menu = []

  // checks which menu group should be active
  var checkActiveGroup = function (menuItem) {
    if (menuItem.drupal_id === currPage) {
      return true
    }
    for (var i in menuItem.children) {
      var foundActive = checkActiveGroup(menuItem.children[i])
      if (foundActive) {
        return foundActive
      }
    }
    return false
  }

  try {
    for (let item in menuArray) {
      if (menuArray[item].children.length !== 0 && !ignoreChildren) {
        if (checkActiveGroup(menuArray[item])) {
          // If the item is top-level
          if (!menuArray[item].drupal_parent_menu_item) {
            menu.push(
              <li
                key={menuArray[item].drupal_id}
                className="nav-item top-level"
              >
                {buildLink(menuArray[item])}
              </li>,
              buildMenu(menuArray[item].children, currPage, false)
            )
          } else if (menuArray[item].drupal_id === currPage) {
            menu.push(
              <li
                key={menuArray[item].drupal_id}
                className="nav-item has-dropdown active-section"
              >
                {buildLink(menuArray[item], "dropdown-toggle")}
                <ul className="dropdown">
                  {buildMenu(menuArray[item].children, currPage, false)}
                </ul>
              </li>
            )
          } else {
            menu.push(
              <li
                key={menuArray[item].drupal_id}
                className="nav-item has-dropdown"
              >
                {buildLink(menuArray[item], "dropdown-toggle")}
                <ul className="dropdown">
                  {buildMenu(menuArray[item].children, currPage, false)}
                </ul>
              </li>
            )
          }
        } else {
          // Add dropdown icon
          menu.push(
            <li
              key={menuArray[item].drupal_id}
              className="nav-item has-dropdown"
            >
              {buildLink(menuArray[item])}
            </li>
          )
        }
      } else {
        menu.push(
          <li key={menuArray[item].drupal_id} className="nav-item">
            {buildLink(menuArray[item])}
          </li>
        )
      }
    }

    return menu
  } catch (error) {
    ErrorCatch(error)
  }
}

function generateMenu(
  pageSlug,
  menuLinks,
  menuName,
  menuLocation,
  startLevel,
  ignoreChildren
) {
  let menu
  let currentPage = {
    path: pageSlug,
    drupal_id: 0,
    parent_id: 0,
  }
  let menuLinksArray = menuLinks

  try {
    // homepage
    if (currentPage.path === "/") {
      currentPage.path = null
    }

    for (let item of menuLinksArray) {
      let uriAliasWithSlash = replacePath(item.node.link.uri_alias)

      if (
        uriAliasWithSlash === currentPage.path &&
        // When the currentPage exists in two menus, we need to get the right drupal_id to associate it correctly
        menuName === item.node.menu_name
      ) {
        currentPage.drupal_id = item.node.drupal_id
        currentPage.parent_id = item.node.drupal_parent_menu_item
        break
      }
    }

    function getParent(childID, nodeArray) {
      let parentID = 0

      for (let item of nodeArray) {
        if (
          item.node.drupal_id === childID &&
          item.node.drupal_parent_menu_item
        ) {
          if (
            item.node.drupal_parent_menu_item.includes("menu_link_content:")
          ) {
            item.node.drupal_parent_menu_item =
              item.node.drupal_parent_menu_item.replace(
                "menu_link_content:",
                ""
              )
          }
          parentID = item.node.drupal_parent_menu_item
          break
        }
      }
      return parentID
    }

    // Find the top level parent
    function getTopParent(childID, nodeArray) {
      let topParentID = 0
      let nextParentID = getParent(childID, nodeArray)
      if (nextParentID === 0) {
        topParentID = childID
      } else {
        while (nextParentID !== 0) {
          topParentID = nextParentID
          nextParentID = getParent(nextParentID, nodeArray)
        }
      }
      return topParentID
    }

    let topParentID = getTopParent(currentPage.drupal_id, menuLinksArray)
    menu = createMenuHierarchy(menuLinks, menuName)

    if (currentPage.path !== null) {
      // startLevel = 0 is not enough here, we have to be specific and target the sidebar otherwise the other menus are affected
      if (startLevel === 0 && menuLocation === "sidebar") {
        let newMenu = []
        menu.forEach(function (item) {
          // If the item is a topParent and startLevel = 0 - add the topParent to the menu. Especially useful in the Sidebar
          if (item.drupal_id === topParentID) {
            newMenu.push(item)
          }
        })
        menu = newMenu
      } else if (startLevel === 1) {
        let newMenu = menu
        menu.forEach(function (item) {
          if (item.drupal_id === topParentID) {
            newMenu = item.children
          }
        })
        menu = newMenu
      }
    }

    menu = buildMenu(menu, currentPage.drupal_id, ignoreChildren)
    return menu
  } catch (error) {
    ErrorCatch(error)
  }
}

const Menu = ({
  storyItems,
  menuName,
  menuLocation,
  startLevel,
  ignoreChildren,
}) => {
  const menuDataDrupal = useMenuData()
  const menuData = storyItems ? storyItems : menuDataDrupal
  const [hasMounted, setHasMounted] = useState(false)

  useEffect(() => {
    setHasMounted(true)
  }, [])

  if (!hasMounted) {
    return null
  }

  const { location } = globalHistory

  return (
    <>
      <ul>
        {generateMenu(
          location.pathname,
          menuData,
          menuName,
          menuLocation,
          startLevel,
          ignoreChildren
        )}
      </ul>
    </>
  )
}

Menu.propTypes = {
  menuData: PropTypes.array,
  menuName: PropTypes.string,
  startLevel: PropTypes.number,
  storyItems: PropTypes.array,
  ignoreChildren: PropTypes.bool,
}

Menu.defaultProps = {
  menuName: `main`,
  startLevel: 0,
  ignoreChildren: false,
}

export default Menu
