import React from "react"
import ContentLoader from "react-content-loader"

const MenuWaiting = () => (
  <ContentLoader 
    speed={1}
    width={700}
    height={56}
    viewBox="0 0 700 56"
    backgroundColor="rgb(0,0,0)"
    foregroundColor="rgb(0,0,0)"
    backgroundOpacity={0.06}
    foregroundOpacity={0.12}
    animate={true}
  >
    <rect x="13" y="13" rx="3" ry="3" width="151" height="29" /> 
    <rect x="340" y="13" rx="3" ry="3" width="151" height="29" /> 
    <rect x="176" y="13" rx="3" ry="3" width="151" height="29" />
  </ContentLoader>
)
export default MenuWaiting