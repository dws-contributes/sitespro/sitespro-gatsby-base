/**
 *  Renders the meta information for News article, Story, and Blog article:
 *  - By
 *  - Authors (both internal and external)
 *  - //
 *  - Date (Month d, yyyy)
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import { FormattedAuthors } from "components/_utils/FormattedAuthors"
import "./Meta.scss"

const Meta = ({ authors, date, modifiers }) => {
  return (
    <div className={bem("meta", "wrapper", modifiers)}>
      {authors?.length ? (
        <span className={bem("meta", "authors")}>
          By <FormattedAuthors authorsArray={authors} />
          <span className={bem("meta", "separator", modifiers)}>
            &#47;&#47;
          </span>
        </span>
      ) : (
        ""
      )}
      &nbsp;{date}
    </div>
  )
}

export const authorDataShape = {
  title: PropTypes.string,
  path: PropTypes.string,
}

Meta.propTypes = {
  authors: PropTypes.arrayOf(PropTypes.shape(authorDataShape)),
  date: PropTypes.string,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Meta
