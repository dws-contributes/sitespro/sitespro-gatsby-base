import React from "react"
import PageCard from "components/Molecules/PageCard/PageCard"

const PageNodeReference = ({ node }) => {

  return (
    <PageCard
      title={node.title}
      summary={node.field_summary}
      link={node.path?.alias}
    />
  )
}

export default PageNodeReference
