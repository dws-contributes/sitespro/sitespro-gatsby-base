import React from "react"
import ellipsis from "components/_utils/ellipsis"
import decode from "components/_utils/decode"
import ResourceTeaser from "components/Templates/Resource/ResourceTeaser"

const ResourceNodeReference = ({ node }) => {
  return (
    <ResourceTeaser
      title={node.title}
      date={node.revised_date}
      summary={
        node.body?.summary || ellipsis(decode(node.body?.processed), 255)
      }
      link={node.field_alternate_link?.uri || node.path?.alias}
      image={
        node.relationships?.field_featured_media?.relationships
          ?.field_media_image?.image_style_uri?.focal_point_large
      }
      imageAlt={
        node.relationships?.field_featured_media?.field_media_image?.alt
      }
    />
  )
}

export default ResourceNodeReference
