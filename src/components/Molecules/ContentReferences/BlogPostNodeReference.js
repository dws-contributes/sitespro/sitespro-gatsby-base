import React, { Fragment } from "react"
import Link from "components/Atoms/Link/Link"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import { BsDot } from "react-icons/bs"
import { BB8Image, imageDetails } from "components/Atoms/Image/BB8Image"
import { FormattedAuthors } from "components/_utils/FormattedAuthors"
import { MdKeyboardBackspace } from "react-icons/md"
import "./ContentReference.scss"

const BlogPostNodeReference = ({
  node,
  block = "reference-card",
  modifiers = "blog",
}) => {
  let { imageDetails, date, authors, title, path } =
    BlogPostNodeReferenceDataNormalizer(node)

  // TODO: Why does this not use the Featured Card component?

  return (
    <section className={`${block} ${bem(block, modifiers)}`}>
      {imageDetails && (
        <BB8Image
          imageDetails={imageDetails}
          targetedSizes={["large_3_2", "small_3_2"]}
        />
      )}

      <div className={bem(block, "content", [modifiers])}>
        <div className={bem(block, "meta")}>
          {/* Date and Authors should Eyebrow? */}

          {/* Date */}
          {date && <span>{date}</span>}

          {/* Authors, separated by a comma */}
          {authors.length > 0 && (
            <Fragment>
              <span>
                <BsDot />
              </span>
              <FormattedAuthors authorsArray={authors} />
            </Fragment>
          )}
        </div>

        {/* Title */}
        {title && (
          <Heading level={4}>
            <Link to={path}>{title}</Link>
            <MdKeyboardBackspace className={bem(block, "icon")} />
          </Heading>
        )}
      </div>
    </section>
  )
}

const BlogPostNodeReferenceDataNormalizer = data => {
  return {
    imageDetails: imageDetails(data?.relationships?.field_featured_media),
    date: data?.date,
    authors: data?.relationships?.field_author_reference.map(author => ({
      title: author.title,
    })),
    title: data?.title,
    path: data?.path?.alias,
  }
}

export default BlogPostNodeReference
