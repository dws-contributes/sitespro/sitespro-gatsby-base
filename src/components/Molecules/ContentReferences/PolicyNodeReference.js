import React from "react"
import PolicyTeaser from "components/Templates/Policy/PolicyTeaser"

const PolicyNodeReference = ({ node }) => {

  return (
    <PolicyTeaser
      title={node.title}
      summary={node.summary?.summary}
      link={node.path?.alias}
    />
  )
}

export default PolicyNodeReference
