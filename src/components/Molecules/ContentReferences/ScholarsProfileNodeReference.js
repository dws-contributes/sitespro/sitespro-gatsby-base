import React from "react"
import decode from "components/_utils/decode"
import PersonCard from "components/Molecules/PersonCard/PersonCard"

const ScholarsProfileNodeReference = ({ node }) => {
  return (
    <div className="scholars-card-wrapper">
      <PersonCard
        name={node?.title}
        link={node?.path?.alias}
        position={node?.duke_scholars_profile_preferred_title}
        imageUrl={node?.duke_scholars_profile_thumbnail?.thumbnail}
        email={node?.duke_scholars_profile_email}
        phone={node?.duke_scholars_profile_phone}
        body={decode(node?.duke_scholars_profile_overview?.processed)}
        summary={node?.field_summary}
        type={"Scholars@Duke Profile"}
      />
    </div>
  )
}

export default ScholarsProfileNodeReference
