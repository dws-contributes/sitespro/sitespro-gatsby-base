import React, { Fragment } from "react"
import Link from "components/Atoms/Link/Link"
import bem from "components/_utils/bem"
import { BB8Image, imageDetails } from "components/Atoms/Image/BB8Image"
import Heading from "components/Atoms/Heading/Heading"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { BsDot } from "react-icons/bs"
import { FormattedAuthors } from "components/_utils/FormattedAuthors"
import { MdKeyboardBackspace } from "react-icons/md"
import "./ContentReference.scss"

const StoryNodeReference = ({
  node,
  block = "reference-card",
  modifiers = "story",
}) => {
  let { imageDetails, date, authors, title, path, summary } =
    StoryNodeReferenceDataNormalizer(node)

  // TODO: Why does this not use the StoryCard component?

  return (
    <section className={`${block} ${bem(block, modifiers)}`}>
      {imageDetails && (
        <BB8Image
          imageDetails={imageDetails}
          targetedSizes={["large_3_2", "small_3_2"]}
        />
      )}

      <div className={bem(block, "content", [modifiers])}>
        <div className={bem(block, "meta")}>
          {/* Date and Authors should Eyebrow? */}

          {/* Date */}
          {date && <span>{date}</span>}

          {/* Authors, separated by a comma */}
          {authors.length > 0 && (
            <>
              <span>
                <BsDot />
              </span>
              <FormattedAuthors authorsArray={authors} />
            </>
          )}
        </div>

        {title && (
          <Heading level={4}>
            <Link to={path}>{title}</Link>
            <MdKeyboardBackspace className={bem(block, "icon")} />
          </Heading>
        )}

        {summary && !path?.includes("/secure/") && (
          <CKEditorContent content={summary} />
        )}
      </div>
    </section>
  )
}

const StoryNodeReferenceDataNormalizer = data => {
  return {
    imageDetails: imageDetails(data?.relationships?.field_featured_media),
    date: data?.date,
    authors: data?.relationships?.field_author_reference.map(author => ({
      title: author.title,
    })),
    title: data?.title,
    path: data?.path?.alias,
    summary: data?.field_summary,
  }
}

export default StoryNodeReference
