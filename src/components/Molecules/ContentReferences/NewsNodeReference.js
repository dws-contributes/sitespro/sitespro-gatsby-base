import React from "react"
import Link from "components/Atoms/Link/Link"
import bem from "components/_utils/bem"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Heading from "components/Atoms/Heading/Heading"
import decode from "components/_utils/decode"
import { BsDot } from "react-icons/bs"
import "./ContentReference.scss"
import { BB8Image, imageDetails } from "components/Atoms/Image/BB8Image"
import { MdKeyboardBackspace } from "react-icons/md"
import "./ContentReference.scss"

const NewsNodeReference = ({
  node,
  block = "reference-card",
  modifiers = "news",
}) => {
  let {
    featured,
    feedImageUrl,
    feedImageAltText,
    date,
    title,
    titleOverride,
    summary,
    source,
    path,
  } = newsNodeReferenceDataNormalizer(node)

  // TODO: Why does this not use the Featured Card component?
  return (
    <section className={`${block} ${bem(block, modifiers)}`}>
      {(feedImageUrl || featured.imageDetails?.variants) && (
        <div className="news-featured-image">
          {feedImageUrl && !featured.imageDetails?.variants && (
            <img src={feedImageUrl} alt={feedImageAltText} />
          )}
          {featured.imageDetails && (
            <BB8Image
              imageDetails={featured.imageDetails}
              targetedSizes={["focal_point_large", "focal_point_medium"]}
            />
          )}
        </div>
      )}

      <div className={bem(block, "content")}>
        <div className={bem(block, "meta")}>
          {date && <span>{date}</span>}

          {/* Source */}
          {source && (
            <span>
              <span>
                <BsDot />
              </span>
              {source}
            </span>
          )}
        </div>

        {title && (
          <Heading level={4}>
            <Link to={path}>{titleOverride ? titleOverride : title}</Link>
            <MdKeyboardBackspace className={bem(block, "icon")} />
          </Heading>
        )}

        {summary && !path.includes("/secure/") && (
          <CKEditorContent content={decode(summary)} />
        )}
      </div>
    </section>
  )
}

const newsNodeReferenceDataNormalizer = data => {
  let featured = {
    imageDetails: imageDetails(data?.relationships?.field_featured_media),
  }

  return {
    featured: featured,
    feedImageUrl: data?.field_feed_image_url?.uri,
    feedImageAltText: data?.field_feed_image_alt_text,
    date: data?.date,
    source: data?.field_news_source,
    title: data?.title,
    titleOverride: data?.field_title_override,
    summary: data?.summary?.summary,
    path: data?.field_alternate_link?.uri
      ? data?.field_alternate_link?.uri
      : data?.path?.alias,
  }
}

export default NewsNodeReference
