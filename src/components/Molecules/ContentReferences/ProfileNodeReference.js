import React from "react"
import {
  BB8ImageProcessVariants,
  imageDetails,
} from "components/Atoms/Image/BB8Image"
import PersonCard from "components/Molecules/PersonCard/PersonCard"

const ProfileNodeReference = ({ node }) => {
  let { imageDetails, title, path, roles, email, phone, summary, description } =
    ProfileNodeReferenceDataNormalizer(node)

  let imageVariants = BB8ImageProcessVariants(imageDetails, "focal_point_large")

  return (
    <div className="profile-card-wrapper">
      <PersonCard
        name={title}
        link={path}
        roles={roles}
        imageUrl={imageVariants?.fallback}
        email={email}
        phone={phone}
        body={summary ? summary : description}
        type={"Profile"}
      />
    </div>
  )
}

export default ProfileNodeReference

const ProfileNodeReferenceDataNormalizer = data => {
  return {
    imageDetails: imageDetails(data?.relationships?.field_image),
    title: data?.title,
    path: data?.path?.alias,
    roles: data?.field_role,
    email: data?.field_email_address,
    phone: data?.field_phone_number,
    summary: data?.field_bio?.summary,
    description: data?.field_bio?.processed,
  }
}
