import React from "react"
import ProfileNodeReference from "../ContentReferences/ProfileNodeReference"
import NewsNodeReference from "../ContentReferences/NewsNodeReference"
import BlogPostNodeReference from "../ContentReferences/BlogPostNodeReference"
import StoryNodeReference from "../ContentReferences/StoryNodeReference"
import EventNodeReference from "../ContentReferences/EventNodeReference"
import FAQNodeReference from "../ContentReferences/FAQNodeReference"
import ResourceNodeReference from "../ContentReferences/ResourceNodeReference"
import PageNodeReference from "../ContentReferences/PageNodeReference"
import PolicyNodeReference from "../ContentReferences/PolicyNodeReference"
import ProjectNodeReference from "../ContentReferences/ProjectNodeReference"

const components = {
  node__profile: ProfileNodeReference,
  node__news: NewsNodeReference,
  node__blog_post: BlogPostNodeReference,
  node__event: EventNodeReference,
  node__story: StoryNodeReference,
  node__faq: FAQNodeReference,
  node__resource: ResourceNodeReference,
  node__page: PageNodeReference,
  node__policy: PolicyNodeReference,
  node__project: ProjectNodeReference,
}

export const getNodeReference = node => {
  if (components.hasOwnProperty(node.type)) {
    const NodeComponent = components[node.type]
    return <NodeComponent key={node.id} node={node} />
  }
  return <p key={node.id}>Unknown type node reference {node.__typename}</p>
}
