import React, { useContext } from "react"
import bem from "components/_utils/bem"
import { Accordion, AccordionContext, Card } from "react-bootstrap"
import { useAccordionButton } from "react-bootstrap/AccordionButton"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import decode from "components/_utils/decode"

const FAQNodeReference = ({
  node,
  block = "reference-card",
  modifiers = "faq",
}) => {
  let AccordionSectionID = node.id

  function CustomToggle({ children, eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext)

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    )

    const isCurrentEventKey = activeEventKey === eventKey

    return (
      <button
        type="button"
        className={`accordion-toggle ${isCurrentEventKey ? `current` : ``}`}
        aria-expanded={isCurrentEventKey ? true : false}
        tabIndex="0"
        onClick={decoratedOnClick}
      >
        {children}
      </button>
    )
  }

  return (
    <section className={`${block} ${bem(block, modifiers)}`}>
      <Accordion key={AccordionSectionID} defaultActiveKey="0">
        <Card key={AccordionSectionID + "--card"}>
          <CustomToggle eventKey={AccordionSectionID + "--accordion"}>
            <span className="accordion-icon"></span>
            {decode(node.title)}
          </CustomToggle>
          <Accordion.Collapse eventKey={AccordionSectionID + "--accordion"}>
            <Card.Body>
              {node.field_answer && (
                <CKEditorContent content={node.field_answer} />
              )}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </section>
  )
}

export default FAQNodeReference
