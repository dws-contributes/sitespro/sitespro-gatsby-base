import React from "react"
import EventCard from "../EventCard/EventCard"

const EventNodeReference = ({ node }) => {
  return (
    <EventCard
      title={node.title}
      status={
        node.relationships?.field_status
          ? node.relationships?.field_status?.name
          : ""
      }
      link={node.path?.alias}
      date={node.field_event_date ? node.field_event_date?.date : ""}
      locationText={node.field_location_text ? node.field_location_text : ""}
    />
  )
}

export default EventNodeReference
