import React from "react"
import ProjectTeaser from "components/Templates/Project/ProjectTeaser"

const ProjectNodeReference = ({ node }) => {
  let timeframe = node.relationships?.field_project_start_timeframe?.name
    ? node.relationships?.field_project_start_timeframe?.name + 
      (node.relationships?.field_project_end_timeframe?.name ? ' - ' + node.relationships?.field_project_end_timeframe?.name : '')
    : ''

  return (
    <ProjectTeaser
      title={node.title}
      timeframe={timeframe}
      summary={node.summary?.summary}
      link={node.path?.alias}
      image={node?.relationships?.field_featured_media?.relationships?.field_media_image?.image_style_uri?.small_2_5_1}
      imageAlt={
        node.relationships?.field_featured_media?.field_media_image?.alt
      }
    />
  )
}

export default ProjectNodeReference
