import decode from "components/_utils/decode"
import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Heading from "components/Atoms/Heading/Heading"
import Link from "components/Atoms/Link/Link"
import bem from "components/_utils/bem"
import "./PageCard.scss"
import { BB8Image } from "components/Atoms/Image/BB8Image"

const PageCard = ({
  title,
  summary,
  link,
  image,
  imageAlt,
  element,
  modifiers,
  displayInGrid,
  block = "page-card",
  ariaLevel = 2,
}) => {
  let imageNormalized = null
  if (image) {
    imageNormalized = {
      imageDetails: {
        variants: {
          [displayInGrid === "grid" ? "small_3_2" : "focal_point_large"]:
            displayInGrid === "grid"
              ? image.replace("focal_point_large", "small_3_2")
              : image,
        },
        alt: imageAlt,
        uri: image,
      },
      targetedSizes: [
        displayInGrid === "grid" ? "small_3_2" : "focal_point_large",
      ],
    }
  }

  let thumbnailImageComponent = null
  if (imageNormalized) {
    thumbnailImageComponent = (
      <div className="page-thumbnail-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  return (
    <div
      className={`${bem(block, element, modifiers)} ${
        displayInGrid === "grid" ? "display-in-grid" : ""
      }`}
    >
      {thumbnailImageComponent && (
        <div className="image-wrapper flex-shrink-0">
          {thumbnailImageComponent}
        </div>
      )}
      <div className="text-wrapper">
        <Heading level={4} ariaLevel={ariaLevel}>
          <Link to={link}>{decode(title)}</Link>
        </Heading>
        {summary && !link.includes("/secure/") && (
          <CKEditorContent content={decode(summary)} />
        )}
      </div>
    </div>
  )
}

PageCard.propTypes = {
  title: PropTypes.string,
  summary: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default PageCard
