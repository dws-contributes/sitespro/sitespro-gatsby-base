import React, { useState, useEffect } from "react"
import Pagination from "./Pagination"
import PublicationPlaceholder from "./PublicationPlaceholder"
import NoResults from "components/Atoms/NoResults/NoResults"

const endpoint = "https://sites-pro-scholars.api.oit.duke.edu/graphiql/"

const ScholarsPublications = ({
  itemsPerPage,
  orgArray,
  orgField,
  yearFilter,
}) => {
  const perPage = itemsPerPage > 0 ? itemsPerPage : 2000
  const showPagination = itemsPerPage > 0 ? true : false

  let totalPages = 0
  const pageNumberLimit = 6
  const [pubData, setPubData] = useState([{}])
  const pubPlaceholders = Array(perPage)
    .fill()
    .map((_, idx) => 1 + idx)
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const [currentPage, setCurrentPage] = useState(1)
  const [maxPageLimit, setMaxPageLimit] = useState(6)
  const [minPageLimit, setMinPageLimit] = useState(0)

  const PUBLICATIONS_QUERY = `
  { 
    publications(
      filter: [
        ${yearFilter}
        {field: ${orgField}, value: [${orgArray}]},
      ], 
      pageSize: ${perPage},
      startPage: ${currentPage},
      sort: {field: PUBLICATION_DATE, direction: DESC}
    ) {
      totalCount: count
      results {
        id
        citations {
          html
          style
        }
      }
    }
  }
  `

  useEffect(() => {
    let abortController = new AbortController()

    const fetchData = async () => {
      try {
        setLoading(true)
        const response = await fetch(endpoint, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ query: PUBLICATIONS_QUERY }),
          signal: abortController.signal,
        })
        const newData = await response.json()

        setPubData(newData?.data)
        setLoading(false)
      } catch (error) {
        if (error.name === "AbortError") {
          setError(error)
          setLoading(false)
        }
      }
    }

    fetchData()
    return () => {
      abortController.abort()
    }
  }, [PUBLICATIONS_QUERY, perPage, currentPage])

  let formatted_publications = []
  let totalResultsCount = 0
  if (!loading) {
    totalResultsCount = pubData.publications?.totalCount
    totalPages = Math.ceil(totalResultsCount / perPage)
  }

  pubData.publications?.results?.forEach((pub, index) => {
    let citation = pub.citations?.filter(
      cite => cite.style === "chicago-fullnote-bibliography"
    )
    formatted_publications[index] = {
      id: pub.id,
      body: citation[0]?.html,
    }
  })

  const onPageChange = pageNumber => {
    const element = document.getElementById("pubListStart")
    element.scrollIntoView({ behavior: "smooth", block: "start" })
    setCurrentPage(pageNumber)
    const delta = Math.floor(pageNumberLimit / 2)
    setMaxPageLimit(pageNumber + delta)
    setMinPageLimit(pageNumber - delta - 1)
  }

  const onPrevClick = () => {
    if ((currentPage - 1) % pageNumberLimit === 0) {
      setMaxPageLimit(maxPageLimit - pageNumberLimit)
      setMinPageLimit(minPageLimit - pageNumberLimit)
    }
    setCurrentPage(prev => prev - 1)
  }

  const onNextClick = () => {
    if (currentPage + 1 > maxPageLimit) {
      setMaxPageLimit(maxPageLimit + pageNumberLimit)
      setMinPageLimit(minPageLimit + pageNumberLimit)
    }
    setCurrentPage(prev => prev + 1)
  }

  const paginationAttributes = {
    currentPage,
    maxPageLimit,
    minPageLimit,
    totalPages,
  }

  return (
    <div aria-live="polite" aria-atomic="true" aria-busy={loading}>
      {totalResultsCount === 0 && <NoResults type={"Publication"} />}

      {loading && (
        <>
          {/* <FadeIn> */}
          {pubPlaceholders.map(pub => (
            <div key={`pub--${pub}`}>
              <PublicationPlaceholder />
            </div>
          ))}
          {/* </FadeIn> */}
        </>
      )}
      {!loading && !error ? (
        <>
          {showPagination && totalResultsCount !== 0 && (
            <p className="pagination-showing-info">
              Showing {perPage * currentPage - perPage + 1} -{" "}
              {perPage * currentPage} of {totalResultsCount} results
            </p>
          )}
          <ul>
            {formatted_publications.map(pub => {
              return (
                <li key={pub.id}>
                  <div dangerouslySetInnerHTML={{ __html: pub.body }} />
                </li>
              )
            })}
          </ul>

          {showPagination && totalResultsCount !== 0 && (
            <Pagination
              {...paginationAttributes}
              onPrevClick={onPrevClick}
              onNextClick={onNextClick}
              onPageChange={onPageChange}
            />
          )}
        </>
      ) : (
        <div></div>
      )}
    </div>
  )
}

export default ScholarsPublications
