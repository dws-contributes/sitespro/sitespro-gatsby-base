import React from "react"
import ContentLoader from "react-content-loader"

const PublicationPlaceholder = () => (
  <ContentLoader viewBox="0 0 778 100" width={778} height={100}>
    <rect x="15" y="10" rx="0" ry="0" width="800" height="25" />
    <rect x="15" y="40" rx="0" ry="0" width="400" height="25" />
    <rect x="434" y="80" rx="0" ry="0" width="0" height="0" />
    <rect x="15" y="100" rx="0" ry="0" width="749" height="25" />
  </ContentLoader>
)
export default PublicationPlaceholder
