import React from "react"
import "./../../Organisms/Search/Search.scss"

const Pagination = props => {
  // init
  const { currentPage, maxPageLimit, minPageLimit, totalPages } = props
  // const totalPages = props.response.totalPages - 1

  // build page numbers list based on total number of pages
  const pages = []
  for (let i = 1; i <= totalPages; i++) {
    pages.push(i)
  }

  const handlePrevClick = () => {
    props.onPrevClick()
  }

  const handleNextClick = () => {
    props.onNextClick()
  }

  const handlePageClick = e => {
    props.onPageChange(Number(e.target.id))
    
  }

  const pageNumbers = pages.map(page => {
    if (page <= maxPageLimit && page > minPageLimit) {
      return (
          <a 
            href="#pubListStart"
            className={`pagination-link ${currentPage === page ? "active" : "inactive"}`} 
            aria-current={currentPage === page ? true : false}
            tabIndex="0" 
            aria-label={`${currentPage === page ? "Current page" : "Go to page"} ${page}`}
            key={page}
            id={page}
            onClick={handlePageClick}
            >
            {page}
          </a>
      )
    } else {
      return null
    }
  })

  // page ellipses
  let pageIncrementEllipses = null
  if (pages.length > maxPageLimit) {
    pageIncrementEllipses = <span>&hellip;</span>
  }
  let pageDecremenEllipses = null
  if (minPageLimit >= 1) {
    pageDecremenEllipses = <span>&hellip;</span>
  }

  return (
    <nav aria-label="Pagination navigation" className="search-pagination" role="navigation">
      <span className="pages">
        {currentPage !== pages[0] && (
          <span className="increment previous">
            <a 
              href="#pubListStart"
              className="pagination-link" 
              onClick={handlePrevClick}
              aria-label="Previous page">
              PREVIOUS
            </a>
          </span>
        )}

        {pageDecremenEllipses}
        {pageNumbers}
        {pageIncrementEllipses}
        
        {currentPage < pages[pages.length - 1] && (
          <span className="increment next">
            <a
              className="pagination-link" 
              onClick={handleNextClick}
              href="#pubListStart"
              aria-label="Next page"
            >
              NEXT
            </a>
          </span>
        )}
      </span>
    </nav>
  )
}

export default Pagination
