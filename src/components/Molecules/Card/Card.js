import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import "components/Molecules/Card/Card.scss"
import { BB8Image, imageDetailsShape } from "components/Atoms/Image/BB8Image"

const Card = ({
  imageDetails,
  heading,
  description,
  linkText,
  linkUrl,
  block = "imagecard",
  element,
  modifiers,
}) => {
  imageDetails.uri || imageDetails.variants
    ? modifiers.push("has-image")
    : modifiers.push("no-image")

  return (
    <div className={bem(block, element, modifiers)}>
      {imageDetails && (
        <div className={bem(block, "image")}>
          <BB8Image
            imageDetails={imageDetails}
            targetedSizes={["large_3_2", "small_3_2"]}
          />
        </div>
      )}

      <div className={bem(block, "content")}>
        {/* Heading */}
        {heading && (
          <Heading level={4} block={block} element="heading">
            {heading}
          </Heading>
        )}
        {/* Body */}
        {description && <CKEditorContent content={description} />}
        {/* Button */}
        {linkUrl && (
          <Button link={linkUrl} showIcon={true} modifiers={["plain", "white"]}>
            {linkText}
          </Button>
        )}
      </div>
    </div>
  )
}

Card.propTypes = {
  imageDetails: PropTypes.shape(imageDetailsShape),
  imageCrop: PropTypes.string,
  heading: PropTypes.string,
  children: PropTypes.string,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
}

export default Card
