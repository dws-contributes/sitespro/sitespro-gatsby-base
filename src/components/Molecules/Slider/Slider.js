import React, { useState } from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Carousel from "react-bootstrap/Carousel"
import "./Slider.scss"
import { BB8Image } from "components/Atoms/Image/BB8Image"
// import { forceCheck, forceVisible } from "react-lazy-load"

const Slider = ({ slides, block = "slider", element, modifiers }) => {
  const [active, setActive] = useState(1)

  function handleSlide(selectedIndex) {
    setActive(selectedIndex + 1)
  }

  // useEffect(() => {
  //   forceVisible()
  // }, [active])

  return (
    <div className={bem(block, element, modifiers)} aria-live="polite">
      <Carousel
        interval={null}
        onSelect={handleSlide}
        wrap={false}
        // onSlide={forceCheck()}
      >
        {slides?.map((slide, index) => (
          <Carousel.Item key={index}>
            <BB8Image
              imageDetails={slide?.imageDetails}
              targetedSizes={["large_3_2", "small_3_2"]}
              placeholderHeight={600}
              visibleByDefault={true}
            />
            <Carousel.Caption>
              <CKEditorContent content={slide?.caption} />
            </Carousel.Caption>
            <div className="custom-indicators">
              <p>{active + " of " + slides?.length}</p>
            </div>
          </Carousel.Item>
        ))}
      </Carousel>

      <br className="clearfix" />
    </div>
  )
}

export const SliderDataShape = {
  imageDetails: PropTypes.object,
  caption: PropTypes.string,
}

Slider.propTypes = {
  slides: PropTypes.arrayOf(PropTypes.shape(SliderDataShape)),
}

export default Slider
