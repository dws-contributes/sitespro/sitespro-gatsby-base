import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import TimeProcessor from "components/Atoms/TimeProcessor/TimeProcessor"
import Link from "components/Atoms/Link/Link"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import { MdPlace } from "react-icons/md"
import { AiOutlineClockCircle } from "react-icons/ai"
import decode from "components/_utils/decode"
import "./EventCard.scss"

const EventCard = ({
  date,
  link,
  title,
  status,
  locationUrl,
  locationText,
  block = "event-card",
  element,
  modifiers,
  ariaLevel = 2,
}) => {
  // Remove the UTC format that comes from Solr
  // TODO: What does this do for timezones? Does the Z tell Day.js what timezone to convert to?
  var dateLocal = date?.includes("Z") ? date?.slice(0, -1) : date

  return (
    <div className={bem(block, element, modifiers)}>
      <div className="cardHeaderWrapper">
        <DateProcessor startDate={dateLocal} cardView />
        <div className={bem(block, "cardInfoWrapper")}>
          {status && status !== "CONFIRMED" && (
            <Eyebrow text={status} modifiers={["warning"]} />
          )}
          <Heading
            level={4}
            ariaLevel={ariaLevel}
            className={bem(block, "title")}
          >
            <Link to={link}>{decode(title)}</Link>
          </Heading>

          <div className="field-container">
            <AiOutlineClockCircle />
            <TimeProcessor startTime={dateLocal} />
          </div>
          {locationUrl ? (
            <div className="field-container">
              <MdPlace />
              <Link to={locationUrl}>{locationText || locationUrl}</Link>
            </div>
          ) : (
            <>
              {locationText && (
                <div className="field-container">
                  <MdPlace />
                  <p>{locationText}</p>
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  )
}

EventCard.propTypes = {
  date: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  status: PropTypes.string,
  locationUrl: PropTypes.string,
  locationText: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default EventCard
