import React from "react"
import PropTypes from "prop-types"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import Link from "components/Atoms/Link/Link"
import Heading from "components/Atoms/Heading/Heading"
import decode from "components/_utils/decode"
import addEllipsis from "components/_utils/addEllipsis"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import "./RelatedContent.scss"

const RelatedContent = ({ relatedContent, heading }) => {
  return (
    <div className="related-content-container content-related">
      {relatedContent.length > 0 && (
        <>
          <div className="related-list-heading">
            <Heading level={3}>{heading}</Heading>
          </div>
          <ul>
            {relatedContent.map(item => {
              return (
                <li key={item.id}>
                  {item.imageDetails?.variants ? (
                    <div className="column">
                      <BB8Image
                        imageDetails={item.imageDetails}
                        targetedSizes={["small_2_5_1"]}
                      />
                    </div>
                  ) : item.feedImageUrl ? (
                    <div className="column">
                      <img
                        src={item.feedImageUrl}
                        alt={item.feedImageAltText}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="column">
                    {item.date !== null && (
                      <div key={item.id + "-date"}>
                        <DateProcessor startDate={item.date} />
                      </div>
                    )}
                    <p className="related-content-item-title">
                      <Link to={item.path} key={item.id + "-title"}>
                        {decode(item.title)}
                      </Link>
                    </p>
                    {item.summary && !item.path?.includes("/secure/") && (
                      <div key={item.id + "-summary"}>
                        <CKEditorContent
                          content={addEllipsis(decode(item.summary))}
                        />
                      </div>
                    )}
                  </div>
                </li>
              )
            })}
          </ul>
        </>
      )}
    </div>
  )
}

RelatedContent.propTypes = {
  numItems: PropTypes.number,
  relatedContent: PropTypes.array,
}

export default RelatedContent
