import React from "react"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import ScholarsPublications from "../Publications/ScholarsPublications"
import "./PublicationsListParagraph.scss"

const PublicationListParagraph = ({ node, modifiers }) => {
  let { heading, itemsPerPage, orgID, onlyPrimary, timeframe, idType } =
    PublicationListParagraphDataNormalizer(node)

  const today = new Date()
  const currentYear = today.getFullYear()
  let pubYears = []

  switch (timeframe) {
    case "1":
      pubYears.push(`"${currentYear}"`)
      break
    case "2":
      pubYears.push(`"${currentYear}"`, `"${currentYear - 1}"`)
      break
    case "3":
      pubYears.push(
        `"${currentYear}"`,
        `"${currentYear - 1}"`,
        `"${currentYear - 2}"`
      )
      break
    case "5":
      pubYears.push(
        `"${currentYear}"`,
        `"${currentYear - 1}"`,
        `"${currentYear - 2}"`,
        `"${currentYear - 3}"`,
        `"${currentYear - 4}"`
      )
      break
    case "all":
      pubYears.push("0")
      break
    default:
      break
  }

  const yearFilter =
    pubYears[0] === "0"
      ? ""
      : `{field: PUBLICATION_YEAR, value: [${pubYears}]},`
  const orgArrayQuoted = orgID.map(org => `"${org}"`)
  const orgField =
    idType === "duid"
      ? "DUID"
      : onlyPrimary === "1"
      ? "PRIMARY_ORG_ID"
      : "ANY_ORG_ID"

  return (
    <section
      className={bem("paragraph", "publicationlist", modifiers)}
      id="pubListStart"
    >
      <div className="list-heading">
        <Heading level={2}>{heading}</Heading>
      </div>
      <ScholarsPublications
        itemsPerPage={itemsPerPage}
        orgArray={orgArrayQuoted}
        orgField={orgField}
        yearFilter={yearFilter}
      />
    </section>
  )
}

const PublicationListParagraphDataNormalizer = data => {
  return {
    heading: data?.field_pubs_display_title,
    itemsPerPage: data?.field_pubs_items_per_page,
    orgID: data?.field_pubs_org_id,
    onlyPrimary: data?.field_pubs_primary_only,
    timeframe: data?.field_pubs_timeframe,
    idType: data?.field_pubs_id_type,
  }
}

export default PublicationListParagraph
