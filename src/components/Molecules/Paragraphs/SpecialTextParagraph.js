import React from "react"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import bem from "components/_utils/bem"
import "./SpecialText.scss"

const SpecialTextParagraph = ({
  node,
  block = "paragraph",
  element = "specialtext",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <CKEditorContent
        content={node.field_special_text ? node.field_special_text : ""}
      />
    </section>
  )
}

export default SpecialTextParagraph
