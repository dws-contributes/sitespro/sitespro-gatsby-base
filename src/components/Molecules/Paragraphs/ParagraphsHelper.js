import React from "react"
import AccordionSectionParagraph from "./AccordionSectionParagraph"
import BlogListParagraph from "./BlogListParagraph"
import ButtonParagraph from "./ButtonParagraph"
import CalloutParagraph from "./CalloutParagraph"
import CarouselParagraph from "./CarouselParagraph"
import ContentParagraph from "./ContentParagraph"
import ContentReferenceParagraph from "./ContentReferenceParagraph"
import CourseListParagraph from "./CourseListParagraph"
import EventListParagraph from "./EventListParagraph"
import FaqsListParagraph from "./FaqsListParagraph"
import HeroImageParagraph from "./HeroImageParagraph"
import HeroImageTextParagraph from "./HeroImageTextParagraph"
import HeroVideoTextParagraph from "./HeroVideoTextParagraph"
import HeroFullWidthVideoParagraph from "./HeroFullWidthVideoParagraph"
import IframeParagraph from "./IframeParagraph"
import IconRowParagraph from "./IconRowParagraph"
import ImageCardRowParagraph from "./ImageCardRowParagraph"
import ImageTextParagraph from "./ImageTextParagraph"
import MediaParagraph from "./MediaParagraph"
import NewsListParagraph from "./NewsListParagraph"
import PageListParagraph from "./PageListParagraph"
import PersonListParagraph from "./PersonListParagraph"
import PolicyListParagraph from "./PolicyListParagraph"
import ProjectListParagraph from "./ProjectListParagraph"
import PublicationListParagraph from "./PublicationListParagraph"
import PullQuoteParagraph from "./PullQuoteParagraph"
import ResourceListParagraph from "./ResourceListParagraph"
import ScholarsListParagraph from "./ScholarsListParagraph"
import SlideshowParagraph from "./SlideshowParagraph"
import SpacerParagraph from "./SpacerParagraph"
import SocialMediaParagraph from "./SocialMediaParagraph"
import SpecialTextParagraph from "./SpecialTextParagraph"
import StoryListParagraph from "./StoryListParagraph"
import TwoColumnParagraph from "./TwoColumnParagraph"
import ThreeColumnParagraph from "./ThreeColumnParagraph"

const components = {
  paragraph__accordion_section: AccordionSectionParagraph,
  paragraph__blog_listing: BlogListParagraph,
  paragraph__button: ButtonParagraph,
  paragraph__callout: CalloutParagraph,
  paragraph__carousel: CarouselParagraph,
  paragraph__content: ContentParagraph,
  paragraph__content_reference: ContentReferenceParagraph,
  paragraph__course_list: CourseListParagraph,
  paragraph__event_listing: EventListParagraph,
  paragraph__faqs_list: FaqsListParagraph,
  paragraph__hero_image: HeroImageParagraph,
  paragraph__hero_image_text: HeroImageTextParagraph,
  paragraph__hero_video_text: HeroVideoTextParagraph,
  paragraph__hero_full_width_video: HeroFullWidthVideoParagraph,
  paragraph__iframe: IframeParagraph,
  paragraph__icon_row: IconRowParagraph,
  paragraph__image_card_row: ImageCardRowParagraph,
  paragraph__image_text: ImageTextParagraph,
  paragraph__media: MediaParagraph,
  paragraph__news_list: NewsListParagraph,
  paragraph__page_list: PageListParagraph,
  paragraph__person_list: PersonListParagraph,
  paragraph__policy_list: PolicyListParagraph,
  paragraph__project_list: ProjectListParagraph,
  paragraph__publication_list: PublicationListParagraph,
  paragraph__pull_quote: PullQuoteParagraph,
  paragraph__resource_list: ResourceListParagraph,
  paragraph__scholars_profile_list: ScholarsListParagraph,
  paragraph__slideshow: SlideshowParagraph,
  paragraph__social_media: SocialMediaParagraph,
  paragraph__spacer: SpacerParagraph,
  paragraph__special_text: SpecialTextParagraph,
  paragraph__story_list: StoryListParagraph,
  paragraph__two_column: TwoColumnParagraph,
  paragraph__three_column: ThreeColumnParagraph,
}

export const getParagraph = node => {
  if (!node) return null
  if (components.hasOwnProperty(node.type)) {
    const ParagraphComponent = components[node.type]

    return (
      <div id={node.type + "_" + node.id} key={node.type + "_" + node.id}>
        <ParagraphComponent key={node.id} node={node} />
      </div>
    )
  }
  return <p key={node.id}>Unknown type paragraph {node.__typename}</p>
}
