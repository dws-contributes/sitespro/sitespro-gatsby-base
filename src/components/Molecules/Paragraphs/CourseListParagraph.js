import React from "react"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Courses from "../Courses/Courses"

const CourseListParagraph = ({
  node,
  block = "paragraph",
  element = "couserlist",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <Heading>{node.field_display_title}</Heading>
      <Courses subject={node.field_course_list_subject[0]} />
    </section>
  )
}

export default CourseListParagraph
