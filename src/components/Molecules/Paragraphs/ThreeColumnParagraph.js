import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import { getParagraph } from "./ParagraphsHelper.js"

const ThreeColumnParagraph = ({
  node,
  block = "paragraph",
  element = "threecolumn",
  modifiers,
}) => {
  const left_column_content = node.relationships?.field_left_column?.length
    ? node?.relationships?.field_left_column?.map(getParagraph)
    : ""

  const middle_column_content = node.relationships?.field_middle_column?.length
    ? node?.relationships?.field_middle_column?.map(getParagraph)
    : ""

  const right_column_content = node.relationships?.field_right_column?.length
    ? node?.relationships?.field_right_column?.map(getParagraph)
    : ""

  return (
    <section className={bem(block, element, modifiers)}>
      {node.field_display_title && (
        <h2 aria-level={2}>{node.field_display_title}</h2>
      )}
      <Row>
        {left_column_content && <Col md={4}>{left_column_content}</Col>}
        {middle_column_content && <Col md={4}>{middle_column_content}</Col>}
        {right_column_content && <Col md={4}>{right_column_content}</Col>}
      </Row>
    </section>
  )
}

export const ThreeColumnParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_display_title: PropTypes.string,
  relationships: PropTypes.shape({
    field_left_column: PropTypes.arrayOf(PropTypes.any),
    field_middle_column: PropTypes.arrayOf(PropTypes.any),
    field_right_column: PropTypes.arrayOf(PropTypes.any),
  }),
  type: PropTypes.string,
})

ThreeColumnParagraph.propTypes = {
  node: ThreeColumnParagraphTypeProps,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ThreeColumnParagraph
