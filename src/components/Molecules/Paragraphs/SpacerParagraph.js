import React from "react"
import styled from "styled-components"
import bem from "components/_utils/bem"
import "./Spacer.scss"

const Spacer = styled.div`
  height: ${props => props.height}px;

  @media (max-width: 768px) {
    height: 20px;
  }
`

const SpacerParagraph = ({
  node,
  block = "paragraph",
  element = "spacer",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <Spacer height={node.field_spacer_height}>
        {node.field_spacer_divider && <hr />}
      </Spacer>
    </section>
  )
}

export default SpacerParagraph
