import React from "react"
import bem from "components/_utils/bem"
import PropTypes from "prop-types"
import { imageDetails } from "components/Atoms/Image/BB8Image"
import Callout from "components/Molecules/Callout/Callout"
import "./CalloutParagraph.scss"

const CalloutParagraph = ({
  node,
  block = "paragraph",
  element = "callout",
  modifiers,
}) => {
  let { title, text, image, link } = normalizeCalloutData(node)

  return (
    <section className={bem(block, element, modifiers)}>
      <Callout
        imageDetails={image?.imageDetails}
        imageTargetedSizes={image?.targetedSizes}
        heading={title}
        linkText={link?.title}
        linkUrl={link?.path}
        description={text}
      />
    </section>
  )
}

const normalizeCalloutData = node => {
  let image = {
    imageDetails: imageDetails(node?.relationships?.field_image),
    targetedSizes: ["focal_point_large", "focal_point_medium"],
  }

  let linkUri = node?.field_link?.uri_alias
    ? node.field_link?.uri_alias
    : node.field_link?.uri
  let link = linkUri
    ? {
        path: linkUri,
        title: node?.field_link?.title,
      }
    : false

  return {
    title: node?.field_display_title,
    text: node?.field_body,
    link,
    image,
  }
}

export const CalloutParagraphTypeProps = PropTypes.shape({
  field_display_title: PropTypes.string,
  field_body: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_image: PropTypes.object,
  }),
})

CalloutParagraph.propTypes = {
  node: CalloutParagraphTypeProps,
  image: PropTypes.object,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default CalloutParagraph
