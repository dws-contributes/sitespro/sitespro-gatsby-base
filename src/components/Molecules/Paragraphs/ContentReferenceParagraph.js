import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import { getNodeReference } from "components/Molecules/ContentReferences/ContentReferenceHelper"
import ScholarsProfileNodeReference from "components/Molecules/ContentReferences/ScholarsProfileNodeReference"
import "./ContentReferenceParagraph.scss"
const ContentReferenceParagraph = ({
  node,
  block = "paragraph",
  element = "content-reference",
  modifiers,
}) => {
  const nodeReferences =
    node.relationships.nodeReferences?.map(getNodeReference)

  const scholarsProfiles = node.relationships.scholarsReferences?.map(
    (scholarsProfile, index) => {
      return <ScholarsProfileNodeReference node={scholarsProfile} key={index} />
    }
  )

  return (
    <section className={bem(block, element, modifiers)}>
      {node.field_display_title && (
        <div className="list-heading">
          <Heading level={2}>{node.field_display_title}</Heading>
        </div>
      )}

      {nodeReferences}

      {scholarsProfiles}
    </section>
  )
}

ContentReferenceParagraph.propTypes = {
  node: PropTypes.shape({
    field_display_title: PropTypes.string,
    relationships: PropTypes.object,
  }),
}

export default ContentReferenceParagraph
