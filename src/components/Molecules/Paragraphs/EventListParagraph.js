import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import NoResults from "components/Atoms/NoResults/NoResults"
import EventCard from "components/Molecules/EventCard/EventCard"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"
import "./EventListParagraph.scss"

const buildEventCard = federatedEvent => {
  const statusTerm = federatedEvent.terms?.filter(t =>
    t.startsWith("Event Status")
  )[0]
  const status = statusTerm ? statusTerm.split(">")[1] : null

  return {
    id: federatedEvent.id,
    title: federatedEvent.title_override || federatedEvent.title,
    date: federatedEvent.date,
    link: federatedEvent.url,
    locationUrl: federatedEvent.location_url,
    locationText: federatedEvent.location,
    status: status,
  }
}

const EventListParagraph = ({
  node,
  block = "paragraph",
  element = "eventlist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Event"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ds_federated_date asc",
    fixedRangeFilters: {
      ds_federated_date: [{ from: "NOW/DAY", to: "*" }],
    },
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4

  // Set wrapper styling defaults
  let wrapperStyling = { justifyContent: "flex-start" }

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const eventCardsProperties = results.map(e => buildEventCard(e))
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"Event"} />}
              <div className="event-card-wrapper" style={wrapperStyling}>
                {eventCardsProperties.map((n, i) => (
                  <EventCard
                    key={"event-card-" + i}
                    ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                    {...n}
                  />
                ))}
              </div>
              {node.field_link && (
                <Button
                  link={
                    node.field_link?.uri_alias
                      ? node.field_link?.uri_alias
                      : node.field_link?.uri
                  }
                  children={node.field_link?.title}
                  showIcon={true}
                />
              )}
            </>
          )
        }}
      />
    </section>
  )
}

export default EventListParagraph
