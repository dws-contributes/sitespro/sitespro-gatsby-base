import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import PersonCard from "components/Molecules/PersonCard/PersonCard"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"

const buildScholarsCard = facultyNode => {
  const body = facultyNode.overview ? facultyNode.overview : ""

  return {
    id: facultyNode.id,
    name: facultyNode.title,
    position: facultyNode.subtitle,
    content: facultyNode.content,
    link: facultyNode.url,
    imageUrl: facultyNode.image,
    email: facultyNode.email,
    phone: facultyNode.phone,
    body: body,
    type: "Scholars@Duke Profile",
  }
}

const ScholarsListParagraph = ({
  node,
  block = "paragraph",
  element = "scholarslist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Scholars@Duke Profile"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ds_federated_title asc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const scholarsCardsProperties = results.map(e => buildScholarsCard(e))
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2}>{heading}</Heading>
                </div>
              )}

              <div className="scholars-card-wrapper">
                {scholarsCardsProperties.map((n, i) => (
                  <PersonCard key={"scholars-card-" + i} {...n} />
                ))}
              </div>
            </>
          )
        }}
      />
    </section>
  )
}

export default ScholarsListParagraph
