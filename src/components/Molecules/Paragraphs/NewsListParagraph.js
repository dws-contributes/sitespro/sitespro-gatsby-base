import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import NoResults from "components/Atoms/NoResults/NoResults"
import FeaturedCard from "components/Molecules/FeaturedCard/FeaturedCard"
import TeaserCard from "components/Molecules/TeaserCard/TeaserCard"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"
import "./NewsListParagraph.scss"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const buildFeaturedNewsCard = newsNode => {
  return {
    title: newsNode.title,
    date: newsNode.date,
    link: newsNode.url,
    summary: newsNode.overview,
    source: newsNode.source,
    image: newsNode.image,
    imageAlt: newsNode.image_alt,
  }
}

const buildNewsCard = newsNode => {
  return {
    title: newsNode.title,
    date: newsNode.date,
    link: newsNode.url,
    source: newsNode.source,
  }
}

const NewsListParagraph = ({
  node,
  block = "paragraph",
  element = "newslist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["News"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ds_federated_date desc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4
  const displayInGrid = node.field_list_format || "list"

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const featuredNewsCardProperties =
            results.length > 0 ? buildFeaturedNewsCard(results[0]) : null
          const newsCardsProperties = results
            .slice(1, results.length)
            .map(n => buildNewsCard(n))

          // TODO: This heading is an h2, but most paragraphs have an h3 for their title (same for other lists) - should it be consistent?
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"News"} />}
              <div className={bem(element, "wrapper")}>
                {featuredNewsCardProperties && (
                  <div className={bem(element, "col")}>
                    <FeaturedCard
                      type="news"
                      ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                      {...featuredNewsCardProperties}
                    />
                  </div>
                )}
                <div className={bem(element, "col")}>
                  {displayInGrid === "grid" ? (
                    <Row className="display-in-grid">
                      {newsCardsProperties.map((n, i) => (
                        <Col md={4} key={"news-card-" + i}>
                          <TeaserCard
                            type="news"
                            key={"news-card-" + i}
                            ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2*/
                            {...n}
                          />
                        </Col>
                      ))}
                    </Row>
                  ) : (
                    newsCardsProperties.map((n, i) => (
                      <TeaserCard
                        displayInGrid={displayInGrid}
                        type="news"
                        key={"news-card-" + i}
                        ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                        {...n}
                      />
                    ))
                  )}
                  {node.field_link && (
                    <Button
                      link={
                        node.field_link?.uri_alias
                          ? node.field_link?.uri_alias
                          : node.field_link?.uri
                      }
                      children={node.field_link?.title}
                      showIcon={true}
                    />
                  )}
                </div>
              </div>
            </>
          )
        }}
      />
    </section>
  )
}

export default NewsListParagraph
