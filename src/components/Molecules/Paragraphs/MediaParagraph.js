import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Media from "components/Atoms/Media/Media"
import "./MediaParagraph.scss"

const MediaParagraph = ({
  node,
  block = "paragraph",
  element = "media",
  modifiers,
}) => {
  if (node.relationships.field_media_item !== null) {
    let mediaItem = node.relationships.field_media_item
    let renderedMediaItem = null
    renderedMediaItem = Media(mediaItem)
    // switch (mediaItem.type) {
    //   case "media__image":
    //     renderedMediaItem = (
    //       <BB8Image
    //         imageDetails={imageDetails(mediaItem)}
    //         targetedSizes={["large_3_2", "small_3_2"]}
    //       />
    //     )
    //     break
    //   case "media__video":
    //     const { host } = new URL(mediaItem.field_media_oembed_video)
    //     const providerWarpwire = ["warpwire", "warpwire.duke.edu"].includes(
    //       host
    //     )
    //     if (providerWarpwire) {
    //       renderedMediaItem = (
    //         <div className="ratio ratio-16x9">
    //           <iframe
    //             id="wwvideo"
    //             title="Video player"
    //             data-ww-id="apiTestFrame"
    //             src={mediaItem.field_media_oembed_video}
    //             frameBorder="0"
    //             scrolling="0"
    //             className="embed-responsive-item"
    //             allowFullScreen
    //           ></iframe>
    //         </div>
    //       )
    //     } else {
    //       renderedMediaItem = (
    //         <div className='player-wrapper'>
    //           <ReactPlayer
    //             url={mediaItem.field_media_oembed_video}
    //             controls={true}
    //             className='react-player'
    //             title="Video player"
    //             width='100%'
    //             height='100%'
    //           />
    //         </div>
    //       )
    //     }
    //     break
    //   case "media__audio":
    //     renderedMediaItem = (
    //       <Fragment key={mediaItem.field_media_soundcloud}>
    //         <iframe src={`https://w.soundcloud.com/player/?url=${mediaItem.field_media_soundcloud}&auto_play=false&hide_related=false&show_reposts=false&show_teaser=true`}
    //           allow="autoplay"
    //           title="Audio player"
    //           width="100%"
    //           height="150"
    //           frameBorder="0">
    //         </iframe>
    //         {mediaItem.field_transcript_link?.uri_alias &&
    //           <p className="caption">
    //             <Link to={mediaItem.field_transcript_link?.uri_alias}>Audio transcript</Link>
    //           </p>
    //         }
    //       </Fragment>
    //     )
    //     break
    //   case "media__document":
    //     let path = mediaItem?.relationships?.field_document?.uri?.url
    //     if (path) {
    //       let adjustedPath = path.includes(siteInfoData.drupal_url) ? path.replace(siteInfoData.drupal_url, siteInfoData.cdn_url) : `${siteInfoData.cdn_url}${path}`
    //       renderedMediaItem = (
    //         <a key={path} href={adjustedPath}>{mediaItem?.name}</a>
    //       )
    //     }
    //     else renderedMediaItem = ""
    //     break
    //   default:
    //     renderedMediaItem = ""
    //     break
    // }

    return (
      <section className={bem(block, element, modifiers)}>
        <figure role="figure">
          {node.field_tagline ? <Eyebrow text={node.field_tagline} /> : ""}
          {node.field_display_title ? (
            <Heading level={2}>{node.field_display_title}</Heading>
          ) : (
            ""
          )}
          {renderedMediaItem}
          {node.field_media_caption ? (
            <figcaption>
              <CKEditorContent content={node.field_media_caption} />
            </figcaption>
          ) : (
            ""
          )}
        </figure>
      </section>
    )
  } else {
    return ""
  }
}

export const MediaParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_display_title: PropTypes.string,
  field_tagline: PropTypes.string,
  field_media_caption: PropTypes.string,
  field_transcript_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

MediaParagraph.propTypes = {
  node: MediaParagraphTypeProps,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default MediaParagraph
