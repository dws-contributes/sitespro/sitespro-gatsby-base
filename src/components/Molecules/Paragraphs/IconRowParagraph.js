import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import IconCard from "../IconCard/IconCard"
import { imageDetails } from "components/Atoms/Image/BB8Image"
import "./IconRowParagraph.scss"

const IconRowParagraph = ({
  node,
  block = "paragraph",
  element = "iconrow",
  modifiers,
}) => {
  let { heading, items } = IconRowParagraphDataNormalizer(node)

  return (
    <section className={bem(block, element, modifiers)}>
      {heading && <Heading level={3}>{heading}</Heading>}
      <Row>
        {items.map((item, index) => (
          <Col key={index} md={6} lg={4}>
            <IconCard
              imageDetails={item.imageDetails}
              heading={item.heading}
              linkText={item.linkText}
              linkUrl={item.linkUrl}
              description={item.description}
            />
          </Col>
        ))}
      </Row>
    </section>
  )
}

export const IconRowParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_textarea: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_paragraph_items: PropTypes.array,
  }),
  type: PropTypes.string,
})

IconRowParagraph.propTypes = {
  node: IconRowParagraphTypeProps,
}

const IconRowParagraphDataNormalizer = data => {
  let items = data?.relationships?.field_paragraph_items?.map(item => ({
    imageDetails: imageDetails(item?.relationships?.field_image),
    heading: item?.field_heading,
    linkText: item?.field_link?.title,
    linkUrl: item?.field_link?.uri_alias
      ? item?.field_link?.uri_alias
      : item?.field_link?.uri,
    description: item?.field_body,
  }))

  return {
    heading: data?.field_display_title,
    items: items,
  }
}

export default IconRowParagraph
