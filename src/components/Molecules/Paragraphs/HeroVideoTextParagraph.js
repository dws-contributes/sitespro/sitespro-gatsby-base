import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import ReactPlayer from "react-player/lazy"
import { Container } from "react-bootstrap"
import "./HeroVideoTextParagraph.scss"

const HeroVideoTextParagraph = ({
  node,
  block = "paragraph",
  element = "hero-video-text",
  modifiers,
}) => {
  let { linkTitle, linkUrl, heading, tagline, title } =
    HeroVideoTextParagraphDataNormalizer(node)

  return (
    <section className={bem(block, element, modifiers)}>
      <Container>
        <div className="row">
          <div className="col-lg-4 col-sm-12">
            <div className="hero-text">
              {/* Tagline */}
              {tagline && <Eyebrow text={tagline} />}

              {/* Title */}
              {title && <Heading level={2}>{title}</Heading>}

              {/* Subtitle */}
              {heading && <Heading level={3}>{heading}</Heading>}

              {linkUrl && (
                <Button showIcon={true} link={linkUrl}>
                  {linkTitle}
                </Button>
              )}
            </div>
          </div>
          <div className="col-lg-8 col-sm-12">
            <div className="ratio ratio-16x9">
              <ReactPlayer
                url={
                  node.relationships?.field_media_item?.field_media_oembed_video
                }
                controls={true}
                loop={false}
                volume={30}
                playing={false}
                config={{
                  youtube: {
                    playerVars: { showinfo: 1, modestbranding: 1, rel: 0 },
                  },
                }}
                max-width="inherit"
                height="100%"
                width="100%"
              />
            </div>
          </div>
        </div>
      </Container>
    </section>
  )
}

const HeroVideoTextParagraphDataNormalizer = data => {
  return {
    linkUrl: data?.field_link?.uri_alias
      ? data?.field_link?.uri_alias
      : data?.field_link?.uri,
    linkTitle: data?.field_link?.title,
    tagline: data?.field_tagline,
    title: data?.field_display_title,
    heading: data?.field_heading,
  }
}

export const HeroVideoTextParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_display_title: PropTypes.string,
  field_tagline: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
})

HeroVideoTextParagraph.propTypes = {
  node: HeroVideoTextParagraphTypeProps,
}

export default HeroVideoTextParagraph
