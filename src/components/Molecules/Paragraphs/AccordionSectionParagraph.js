import React, { useContext } from "react"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import { Accordion, AccordionContext, Card } from "react-bootstrap"
import { useAccordionButton } from "react-bootstrap/AccordionButton"
import "./AccordionSectionParagraph.scss"

const AccordionSectionParagraph = ({
  node,
  block = "paragraph",
  element = "accordion",
  modifiers,
}) => {
  let AccordionSectionID = node.id

  function CustomToggle({ children, eventKey, callback }) {
    const { activeEventKey } = useContext(AccordionContext)

    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey)
    )

    const isCurrentEventKey = activeEventKey === eventKey

    return (
      <button
        type="button"
        className={`accordion-toggle ${isCurrentEventKey ? `current` : ``}`}
        aria-expanded={isCurrentEventKey ? true : false}
        tabIndex="0"
        onClick={decoratedOnClick}
      >
        {children}
      </button>
    )
  }

  // function CustomToggle({ children, eventKey }) {
  //   return (
  //     <button
  //       type="button"
  //       eventKey={eventKey}
  //       className="accordion-toggle"
  //       tabIndex="0"
  //       aria-haspopup="true"
  //       aria-expanded={isCurrentEventKey ? true : false }
  //       onClick={decoratedOnClick}
  //     >
  //       {children}
  //     </button>
  //   );
  // }

  return (
    <section className={bem(block, element, modifiers)}>
      {node.field_display_title && (
        <Heading level={3}>{node.field_display_title}</Heading>
      )}
      {node.relationships &&
        node.relationships.field_accordion_item.map((item, i) => (
          <Accordion
            key={AccordionSectionID + (i + 1)}
            defaultActiveKey="0"
            flush
          >
            <Card key={AccordionSectionID + "--card-" + i + 1}>
              <CustomToggle
                eventKey={AccordionSectionID + "--accordion-" + i + 1}
              >
                <span className="accordion-icon"></span>
                {item.field_heading}
              </CustomToggle>
              <Accordion.Collapse
                eventKey={AccordionSectionID + "--accordion-" + i + 1}
              >
                <Card.Body>
                  {item.field_content && (
                    <CKEditorContent content={item.field_content} />
                  )}
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        ))}
    </section>
  )
}

export default AccordionSectionParagraph
