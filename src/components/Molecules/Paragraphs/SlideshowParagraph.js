import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Slider from "../Slider/Slider"
import { imageDetails } from "components/Atoms/Image/BB8Image"
import "./SlideshowParagraph.scss"

const SlideshowParagraph = ({
  node,
  block = "paragraph",
  element = "slideshow",
  modifiers,
}) => {
  let { slides, title } = SlideshowDataNormalizer(node)

  return (
    <section className={bem(block, element, modifiers)}>
      {title && <Heading level={3}>{title}</Heading>}
      {slides && <Slider slides={slides} />}
    </section>
  )
}

SlideshowParagraph.propTypes = {
  title: PropTypes.string,
  slides: PropTypes.arrayOf(PropTypes.object),
}

const SlideshowDataNormalizer = data => {
  let slides = data?.relationships?.field_paragraph_items?.map(item => ({
    imageDetails: imageDetails(item?.relationships?.field_image),
    caption: item?.field_media_caption,
  }))

  return {
    slides: slides,
    title: data?.field_display_title,
  }
}

export default SlideshowParagraph
