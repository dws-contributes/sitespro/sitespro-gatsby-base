import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import NoResults from "components/Atoms/NoResults/NoResults"
import { createSolrQuery, EmbeddedList } from "components/Organisms/Search"
import PolicyTeaser from "components/Templates/Policy/PolicyTeaser"
import Button from "components/Atoms/Button/Button"
import "./PolicyListParagraph.scss"

const buildPolicyCard = PolicyNode => {
  return {
    title: PolicyNode.title,
    link: PolicyNode.url,
    summary: PolicyNode.overview,
  }
}

const PolicyListParagraph = ({
  node,
  block = "paragraph",
  element = "policylist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Policy"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ss_federated_title_sort_string asc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const PolicyCardProperties = results.map(n => buildPolicyCard(n))

          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"Policy"} />}
              {PolicyCardProperties.map((n, i) => (
                <PolicyTeaser
                  key={"Policy-card-" + i}
                  ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                  {...n}
                />
              ))}

              {node.field_link && (
                <Button
                  link={
                    node.field_link?.uri_alias
                      ? node.field_link?.uri_alias
                      : node.field_link?.uri
                  }
                  children={node.field_link?.title}
                  showIcon={true}
                />
              )}
            </>
          )
        }}
      />
    </section>
  )
}

export default PolicyListParagraph
