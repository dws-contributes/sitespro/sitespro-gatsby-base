import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import "./ImageCardRow.scss"
import Card from "../Card/Card"
import { imageDetails } from "components/Atoms/Image/BB8Image"

const ImageCardRowParagraph = ({
  node,
  block = "paragraph",
  element = "imagecardrow",
  modifiers,
}) => {
  let { title, items } = ImageCardRowParagraphDataNormalizer(node)

  return (
    <section className={bem(block, element, modifiers)}>
      {title && <Heading level={3}>{title}</Heading>}
      <Row>
        {items.map((card, index) => (
          <Col key={index}>
            <Card
              imageDetails={card.imageDetails}
              heading={card.heading}
              description={card.description}
              modifiers={card.modifiers}
              linkText={card.linkText}
              linkUrl={card.linkUrl}
            />
          </Col>
        ))}
      </Row>
    </section>
  )
}

export const ImageCardRowParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_textarea: PropTypes.string,
  field_link: PropTypes.object,
  field_image_alignment: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

ImageCardRowParagraph.propTypes = {
  node: ImageCardRowParagraphTypeProps,
}

const ImageCardRowParagraphDataNormalizer = data => {
  let items = data?.relationships?.field_image_card?.map((item, index) => ({
    imageDetails: imageDetails(item?.relationships?.field_image),
    heading: item?.field_heading,
    linkText: item?.field_link?.title,
    linkUrl: item?.field_link?.uri_alias
      ? item?.field_link?.uri_alias
      : item?.field_link?.uri,
    modifiers: ["image-card", index % 2 ? "even" : ""],
    description: item?.field_body,
  }))

  return {
    items: items,
    title: data.field_display_title,
  }
}

export default ImageCardRowParagraph
