import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import { Container } from "react-bootstrap"
import {
  BB8ImageProcessVariants,
  imageDetails,
} from "components/Atoms/Image/BB8Image"
// import { forceCheck } from "react-lazy-load"
import "./HeroImageParagraph.scss"

const HeroImageParagraph = ({
  node,
  block = "paragraph",
  element = "hero-image",
  modifiers,
}) => {
  let { heroImageSource, heading, linkUrl, linkTitle, imageAltText } =
    HeroImageParagraphDataNormalizer(node)

  let heroImageStyle = { backgroundImage: `url(${heroImageSource})` }
  if (heading)
    heroImageStyle = {
      backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${heroImageSource})`,
    }

  // useEffect(() => {
  //   forceCheck()
  // }, [])

  return (
    <section className={bem(block, element, modifiers)}>
      <div
        className="hero-image"
        style={heroImageStyle}
        role="img"
        aria-label={imageAltText}
      >
        <Container>
          {heading && (
            <Heading level={2} modifiers={["hero"]}>
              {heading}
            </Heading>
          )}

          {linkUrl ? (
            <Button showIcon={true} link={linkUrl}>
              {linkTitle}
            </Button>
          ) : (
            ""
          )}
        </Container>
      </div>
    </section>
  )
}

const HeroImageParagraphDataNormalizer = data => {
  let imageVariants = BB8ImageProcessVariants(
    imageDetails(data?.relationships?.field_image),
    ["large_2_5_1"]
  )

  return {
    heroImageSource: imageVariants?.fallback,
    heading: data?.field_heading,
    linkUrl: data?.field_link?.uri_alias
      ? data?.field_link?.uri_alias
      : data?.field_link?.uri,
    linkTitle: data?.field_link?.title,
    imageAltText: data?.relationships?.field_image?.field_media_image?.alt,
  }
}

export const HeroImageParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_image: PropTypes.object,
  }),
})

HeroImageParagraph.propTypes = {
  node: HeroImageParagraphTypeProps,
}

export default HeroImageParagraph
