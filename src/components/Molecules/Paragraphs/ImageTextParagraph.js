import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Callout from "../Callout/Callout"
import { imageDetails } from "components/Atoms/Image/BB8Image"
import "./ImageTextParagraph.scss"

const ImageTextParagraph = ({ node, modifiers }) => {
  let {
    imageDetails,
    imageAlignment,
    heading,
    linkText,
    linkUrl,
    description,
  } = ImageTextParagraphDataNormalizer(node)

  return (
    <section className={bem("paragraph", "imagetext", modifiers)}>
      <Callout
        imageDetails={imageDetails}
        imageTargetedSizes={["large_3_2", "small_3_2"]}
        imageAlignment={imageAlignment}
        heading={heading}
        linkText={linkText}
        linkUrl={linkUrl}
        description={description}
        modifiers={["image-text"]}
      />
    </section>
  )
}

export const ImageTextParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  field_heading: PropTypes.string,
  field_textarea: PropTypes.string,
  field_link: PropTypes.object,
  field_image_alignment: PropTypes.string,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
  type: PropTypes.string,
})

ImageTextParagraph.propTypes = {
  node: ImageTextParagraphTypeProps,
  image: PropTypes.object,
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

const ImageTextParagraphDataNormalizer = data => {
  return {
    imageDetails: imageDetails(data?.relationships?.field_image),
    imageAlignment: data?.field_image_alignment,
    heading: data?.field_heading,
    linkText: data?.field_link?.title,
    linkUrl: data?.field_link?.uri_alias
      ? data?.field_link?.uri_alias
      : data?.field_link?.uri,
    description: data?.field_textarea,
  }
}

export default ImageTextParagraph
