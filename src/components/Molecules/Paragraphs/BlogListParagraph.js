import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import NoResults from "components/Atoms/NoResults/NoResults"
import FeaturedCard from "components/Molecules/FeaturedCard/FeaturedCard"
import TeaserCard from "components/Molecules/TeaserCard/TeaserCard"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"
import {
  FormattedAuthors,
  AuthorsStringToObject,
} from "components/_utils/FormattedAuthors"
import "./BlogListParagraph.scss"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const buildFeaturedBlogListCard = federatedBlogList => {
  const authors = federatedBlogList.author
    ? AuthorsStringToObject(federatedBlogList.author)
    : ""
  return {
    title: federatedBlogList.title,
    date: federatedBlogList.date,
    link: federatedBlogList.url,
    image: federatedBlogList.image,
    imageAlt: federatedBlogList.image_alt,
    source: authors ? <FormattedAuthors authorsArray={authors} /> : "",
  }
}

const buildBlogListCard = federatedBlogList => {
  const authors = federatedBlogList.author
    ? AuthorsStringToObject(federatedBlogList.author)
    : ""
  return {
    title: federatedBlogList.title,
    date: federatedBlogList.date,
    link: federatedBlogList.url,
    source: authors ? <FormattedAuthors authorsArray={authors} /> : "",
  }
}

const BlogListListParagraph = ({
  node,
  block = "paragraph",
  element = "bloglist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Blog Post"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ds_federated_date desc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4
  const displayInGrid = node.field_list_format || "list"

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const featuredBlogListCardProperties =
            results.length > 0 ? buildFeaturedBlogListCard(results[0]) : null
          const blogCardsProperties = results
            .slice(1, results.length)
            .map(n => buildBlogListCard(n))
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"Blog"} />}
              <div className={bem(element, "wrapper")}>
                {featuredBlogListCardProperties && (
                  <div className={bem(element, "col")}>
                    <FeaturedCard
                      type="blog"
                      ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                      {...featuredBlogListCardProperties}
                    />
                  </div>
                )}
                <div className={bem(element, "col")}>
                  {displayInGrid === "grid" ? (
                    <Row className="display-in-grid">
                      {blogCardsProperties.map((n, i) => (
                        <Col md={4} key={"blog-card-" + i}>
                          <TeaserCard
                            ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2*/
                            type="blog"
                            {...n}
                          />
                        </Col>
                      ))}
                    </Row>
                  ) : (
                    blogCardsProperties.map((n, i) => (
                      <TeaserCard
                        displayInGrid={displayInGrid}
                        ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2*/
                        type="blog"
                        key={"blog-card-" + i}
                        {...n}
                      />
                    ))
                  )}
                  {node.field_link && (
                    <Button
                      link={
                        node.field_link?.uri_alias
                          ? node.field_link?.uri_alias
                          : node.field_link?.uri
                      }
                      children={node.field_link?.title}
                      showIcon={true}
                    />
                  )}
                </div>
              </div>
            </>
          )
        }}
      />
    </section>
  )
}

export default BlogListListParagraph
