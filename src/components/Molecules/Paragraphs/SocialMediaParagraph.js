import React, { useEffect } from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
// import InstagramEmbed from "react-instagram-embed"
import Twitter from "components/Atoms/Social/Twitter"
import Facebook from "components/Atoms/Social/Facebook"
import Heading from "components/Atoms/Heading/Heading"
import "./SocialMediaParagraph.scss"

const SocialMediaParagraph = ({
  node,
  block = "paragraph",
  element = "social-media",
  modifiers,
}) => {
  // Force title attribute for the Instagram embeds
  useEffect(() => {
    let iframesInstagram = document.getElementsByClassName("instagram-media")
    for (const item of iframesInstagram) {
      item.setAttribute("title", "Instagram embed")
    }
  }, [])

  if (node.field_url !== null) {
    let renderedSocialMediaItem = ""
    const url = node.field_url.uri
    const { host } = new URL(url)

    // const providerInstagram = [
    //   "instagr.am",
    //   "www.instagr.am",
    //   "instagram.com",
    //   "www.instagram.com",
    // ].includes(host)

    const providerFacebook = [
      "facebook",
      "www.facebook",
      "facebook.com",
      "www.facebook.com",
    ].includes(host)

    const providerTwitter = [
      "twitter",
      "twitter.com",
      "www.twitter.com",
    ].includes(host)

    // if (providerInstagram)
    //   renderedSocialMediaItem = (
    //     <InstagramEmbed
    //       url={url}
    //       clientAccessToken="131113315549032|9196866f04fef870606396d5059bc5cd"
    //       maxWidth={1220}
    //       hideCaption={false}
    //       containerTagName="div"
    //       protocol=""
    //       injectScript
    //       onLoading={() => {}}
    //       onSuccess={() => {}}
    //       onAfterRender={() => {}}
    //       onFailure={() => {}}
    //     />
    //   )
    if (providerFacebook) renderedSocialMediaItem = <Facebook url={url} />
    if (providerTwitter) renderedSocialMediaItem = <Twitter url={url} />

    return (
      <section className={bem(block, element, modifiers)}>
        {node.field_display_title && (
          <Heading level={2}>{node.field_display_title}</Heading>
        )}

        {renderedSocialMediaItem}
      </section>
    )
  } else {
    return ""
  }
}

SocialMediaParagraph.propTypes = {
  node: PropTypes.shape({
    field_display_title: PropTypes.string,
    field_url: PropTypes.shape({
      uri: PropTypes.string,
    }),
  }),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default SocialMediaParagraph
