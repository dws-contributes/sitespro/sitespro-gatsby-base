import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import NoResults from "components/Atoms/NoResults/NoResults"
import { createSolrQuery, EmbeddedList } from "components/Organisms/Search"
import { globalHistory } from "@reach/router"
import PageCard from "../PageCard/PageCard"
import Button from "components/Atoms/Button/Button"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const buildPageCard = pageNode => {
  return {
    title: pageNode.title,
    summary: pageNode.overview,
    link: pageNode.url,
    image: pageNode.image,
    imageAlt: pageNode.image_alt,
  }
}

const PageListParagraph = ({
  node,
  block = "paragraph",
  element = "pagelist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Page"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ss_federated_title_sort_string asc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4
  const { location } = globalHistory
  const displayInGrid = node.field_list_format || "list"

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const filteredResults = results.filter(
            page => page.url !== location?.pathname.replace(/\/$/, "")
          )
          const pageCardProperties = filteredResults.map(n => buildPageCard(n))

          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"Page"} />}

              {displayInGrid === "grid" ? (
                <Row>
                  {pageCardProperties.map((n, i) => (
                    <Col key={"page-card-" + i} md={4}>
                      <PageCard
                        displayInGrid={displayInGrid}
                        ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                        {...n}
                      />
                    </Col>
                  ))}
                </Row>
              ) : (
                pageCardProperties.map((n, i) => (
                  <PageCard
                    key={"page-card-" + i}
                    ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                    {...n}
                  />
                ))
              )}

              {node.field_link && (
                <Button
                  link={
                    node.field_link?.uri_alias
                      ? node.field_link?.uri_alias
                      : node.field_link?.uri
                  }
                  children={node.field_link?.title}
                  showIcon={true}
                />
              )}
            </>
          )
        }}
      />
    </section>
  )
}

export default PageListParagraph
