import React, { useContext, useState, useEffect } from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Heading from "components/Atoms/Heading/Heading"
import ButtonAtom from "components/Atoms/Button/Button"
import NoResults from "components/Atoms/NoResults/NoResults"
import { Accordion, AccordionContext, Card } from "react-bootstrap"
import { useAccordionButton } from "react-bootstrap/AccordionButton"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"
import decode from "components/_utils/decode"
import "./FaqsListParagraph.scss"

const buildFaqCard = federatedFaq => {
  return {
    id: federatedFaq.id,
    question: federatedFaq.title,
    answer: federatedFaq.overview,
    urlAnchor: federatedFaq.url?.replace(/\//g, "-").substring(1),
  }
}

const FaqsListParagraph = ({
  node,
  block = "paragraph",
  element = "faqlist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["FAQ"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ss_federated_title_sort_string asc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4
  const expandedList = node.field_expanded_faq_list || false

  function CustomToggle({ children, eventKey, callback, expandedList }) {
    const { activeEventKey } = useContext(AccordionContext)
    const [isExpanded, setIsExpanded] = useState(expandedList)
    const [isComponentMounted, setIsComponentMounted] = useState(true)
    const isCurrentEventKey = activeEventKey === eventKey

    useEffect(() => {
      // Set the component mounted flag to true after the initial render
      setIsComponentMounted(true)
    }, [])

    const decoratedOnClick = useAccordionButton(eventKey, () => {
      callback && callback(eventKey)
      setIsExpanded(!isExpanded) // Toggle the expansion state on click
    })

    useEffect(() => {
      setIsExpanded(expandedList) // Set the expansion state based on the value of expandedList
    }, [expandedList])

    return (
      <button
        type="button"
        className={`accordion-toggle ${isCurrentEventKey ? `current` : ``}`}
        aria-expanded={isComponentMounted ? isExpanded : expandedList}
        tabIndex="0"
        onClick={decoratedOnClick}
      >
        {children}
      </button>
    )
  }

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const faqCardsProperties = results.map(e => buildFaqCard(e))
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2}>{heading}</Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"FAQ"} />}
              <div className="faq-card-wrapper">
                {faqCardsProperties.map(
                  ({ id, question, answer, urlAnchor }) => (
                    <Accordion
                      defaultActiveKey={expandedList ? `faq-${id}` : undefined}
                      key={`faq-${id}`}
                      id={urlAnchor}
                    >
                      <Card>
                        <CustomToggle
                          eventKey={`faq-${id}`}
                          expandedList={expandedList}
                        >
                          <span className="accordion-icon"></span>
                          <span className="mt-0 mb-0">{decode(question)}</span>
                        </CustomToggle>
                        <Accordion.Collapse eventKey={`faq-${id}`}>
                          <Card.Body>
                            <CKEditorContent content={decode(answer)} />
                          </Card.Body>
                        </Accordion.Collapse>
                      </Card>
                    </Accordion>
                  )
                )}
              </div>
              {node.field_link && (
                <ButtonAtom
                  link={
                    node.field_link?.uri_alias
                      ? node.field_link?.uri_alias
                      : node.field_link?.uri
                  }
                  children={node.field_link?.title}
                />
              )}
            </>
          )
        }}
      />
    </section>
  )
}

export default FaqsListParagraph
