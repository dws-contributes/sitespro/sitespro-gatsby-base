import React, { useState } from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import Button from "components/Atoms/Button/Button"
import Link from "components/Atoms/Link/Link"
import { BB8Image, imageDetails } from "components/Atoms/Image/BB8Image"
import {
  Container,
  Nav,
  NavLink,
  NavItem,
  TabContainer,
  TabPane,
  TabContent,
} from "react-bootstrap"
// import { forceCheck, forceVisible } from "react-lazy-load"
import "./CarouselParagraph.scss"
import { MdKeyboardBackspace } from "react-icons/md"

const CarouselParagraph = ({
  node,
  block = "paragraph__hero-carousel",
  element = "",
  modifiers,
}) => {
  let { slides } = CarouselParagraphDataNormalizer(node)

  const slideTitles = []
  const [activeSlide, setActiveSlide] = useState(0)

  slides && slides.forEach(slide => slideTitles.push(slide))

  // useEffect(() => {
  //   forceVisible()
  // }, [activeSlide])

  function nextBtnClick() {
    setActiveSlide((activeSlide + 1) % slides.length)
  }

  function prevBtnClick() {
    setActiveSlide(
      (((activeSlide - 1) % slides.length) + slides.length) % slides.length
    )
  }

  // TODO: Does this even select anything?
  function handleTitleClick() {
    Array.prototype.forEach.call(
      document.getElementsByClassName(
        "paragraph__hero-carousel--title-list-item"
      ),
      function (item) {
        if (item.getAttribute("index") === activeSlide) {
          if (!item.classList.contains("active")) {
            item.classList.add("active")
          }
        } else {
          item.classList.remove("active")
        }
      }
    )
  }

  return (
    <section className={bem(block)}>
      <Container fluid="lg" className="position-relative">
        <div className={bem(block, "wrapper")}>
          <TabContainer
            activeKey={activeSlide}
            id={node.id}
            mountOnEnter={false}
          >
            <div className={`${bem(block, "title-list")}`}>
              <Nav className="flex-column">
                {slideTitles
                  .slice(0, slideTitles.length)
                  .map((slide, subIndex) => (
                    <NavItem
                      key={subIndex + 1}
                      className={subIndex === activeSlide && "active"}
                      role="presentation"
                      onClick={() => {
                        setActiveSlide(subIndex)
                        handleTitleClick()
                        // forceCheck()
                      }}
                      onKeyDown={e => {
                        if (e.key === "Enter" || e.key === " ") {
                          setActiveSlide(subIndex)
                          handleTitleClick()
                          // forceCheck()
                        } else if (e.key === "ArrowDown") {
                          e.preventDefault()
                          const nextIndex = (subIndex + 1) % slideTitles.length
                          document
                            .querySelector(`[index="${nextIndex}"]`)
                            .focus()
                        } else if (e.key === "ArrowUp") {
                          e.preventDefault()
                          const prevIndex =
                            (subIndex - 1 + slideTitles.length) %
                            slideTitles.length
                          document
                            .querySelector(`[index="${prevIndex}"]`)
                            .focus()
                        }
                      }}
                    >
                      <NavLink index={subIndex} eventKey={subIndex}>
                        {slide?.heading}
                      </NavLink>
                    </NavItem>
                  ))}
              </Nav>
            </div>
            <div className={bem(block, "image-wrapper tab-content")}>
              {slides?.map((slide, index) => (
                <div
                  className={
                    "fade tab-pane" +
                    (activeSlide === index ? " active show" : "")
                  }
                  key={index}
                >
                  <div className={bem(block, "image")}>
                    <BB8Image
                      imageDetails={slide.imageDetails}
                      targetedSizes={["large_3_2", "small_3_2"]}
                    />
                  </div>
                </div>
              ))}
            </div>
            <TabContent className={bem(block, "info-wrapper")}>
              {slides?.map((slide, index) => (
                <TabPane
                  eventKey={index}
                  key={index}
                  className={bem(block, "info-container")}
                >
                  <div className={bem(block, "info")}>
                    <button
                      className="control-prev"
                      onClick={prevBtnClick}
                      onKeyDown={prevBtnClick}
                    >
                      <span
                        aria-hidden="true"
                        className="control-prev-icon"
                      ></span>
                      <span className="visually-hidden">Previous</span>
                    </button>
                    <div className={bem(block, "content")}>
                      {slide.tagline && <Eyebrow text={slide.tagline} />}

                      {slide.heading && slide.linkUrl && (
                        <Heading level={3} ariaLevel={2}>
                          {<Link to={slide.linkUrl}>{slide.heading}</Link>}
                          <MdKeyboardBackspace className={bem(block, "icon")} />
                        </Heading>
                      )}
                      {slide.heading && !slide.linkUrl && (
                        <Heading level={3} ariaLevel={2}>
                          {slide.heading}
                        </Heading>
                      )}

                      {slide.linkUrl && (
                        <Button
                          link={slide.linkUrl}
                          modifiers={["uppercase"]}
                          showIcon={true}
                        >
                          {slide.linkTitle}
                        </Button>
                      )}
                    </div>
                    <button
                      className="control-next"
                      onClick={nextBtnClick}
                      onKeyDown={nextBtnClick}
                    >
                      <span
                        aria-hidden="true"
                        className="control-next-icon"
                      ></span>
                      <span className="visually-hidden">Next</span>
                    </button>
                  </div>
                </TabPane>
              ))}
            </TabContent>
          </TabContainer>
        </div>
        <div className={`${bem(block, "bottom-placeholder")}`}></div>
      </Container>
    </section>
  )
}

export const CarouselParagraphTypeProps = PropTypes.shape({
  id: PropTypes.string,
  relationships: PropTypes.shape({
    field_paragraph_items: PropTypes.array,
  }),
})

CarouselParagraph.propTypes = {
  node: CarouselParagraphTypeProps,
}

const CarouselParagraphDataNormalizer = data => {
  let slides = data.relationships?.field_paragraph_items.map(item => ({
    tagline: item?.field_tagline,
    heading: item?.field_heading,
    imageDetails: imageDetails(item?.relationships?.field_image),
    linkTitle: item?.field_url?.title,
    linkUrl: item?.field_url?.uri_alias
      ? item?.field_url?.uri_alias
      : item?.field_url?.uri,
  }))

  return {
    slides: slides,
  }
}

export default CarouselParagraph
