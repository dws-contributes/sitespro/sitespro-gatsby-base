import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import NoResults from "components/Atoms/NoResults/NoResults"
import PersonCard from "components/Molecules/PersonCard/PersonCard"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"

const buildPersonCard = personNode => {
  let body = personNode.overview ? personNode.overview : ""
  if (personNode.type === "Scholars@Duke Profile") {
    body = ""
  }
  let summary = personNode.title_override ? personNode.title_override : ""
  if (personNode.type === "Profile") {
    summary = ""
  }

  return {
    id: personNode.id,
    name: personNode.title,
    position: personNode.subtitle,
    content: personNode.content,
    link: personNode.url,
    imageUrl: personNode.image,
    email: personNode.email,
    phone: personNode.phone,
    body: body,
    summary: summary,
    type: personNode.type,
    categories: personNode.terms,
  }
}

const PersonListParagraph = ({
  node,
  block = "paragraph",
  element = "personlist",
  modifiers,
}) => {
  let personType = null
  if (node.field_profile_type === "both")
    personType = ["Scholars@Duke Profile", "Profile"]
  else if (node.field_profile_type === "profiles") personType = ["Profile"]
  else if (node.field_profile_type === "scholars-profiles")
    personType = ["Scholars@Duke Profile"]

  const fixedFieldFilters = {
    ss_federated_type: personType,
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_categories
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const profileGroups = node.relationships?.field_profile_groups
  if (profileGroups.length) {
    profileGroups.forEach(e => {
      if (e?.name) {
        fixedFieldFilters["sm_federated_terms"].push(`Profile Group>${e?.name}`)
      }
    })
  }

  const profileTypes = node.relationships?.field_profile_types
  if (profileTypes.length) {
    profileTypes.forEach(e => {
      if (e?.name) {
        fixedFieldFilters["sm_federated_terms"].push(`Profile Type>${e?.name}`)
      }
    })
  }

  const organizationalUnit = node.relationships?.field_organizational_unit
  if (organizationalUnit.length) {
    organizationalUnit.forEach(e => {
      if (e?.name) {
        fixedFieldFilters["sm_federated_terms"].push(
          `Organizational Unit>${e?.name}`
        )
      }
    })
  }

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ss_federated_title_sort_string asc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const layoutFormat = node.field_profile_list_format || "grid"

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={200}
        render={results => {
          const PersonCardsProperties = results.map(e => buildPersonCard(e))
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"People"} />}
              <div className={bem(element, "wrapper", [`${layoutFormat}`])}>
                {PersonCardsProperties.map((n, i) => (
                  <PersonCard
                    displayBio={true}
                    displayScholarsSummary={true}
                    displayContacts={false}
                    key={"Person-card-" + i}
                    ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                    {...n}
                  />
                ))}
              </div>
            </>
          )
        }}
      />
    </section>
  )
}

export default PersonListParagraph
