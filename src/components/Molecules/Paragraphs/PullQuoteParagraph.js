import React from "react"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import bem from "components/_utils/bem"
import "./PullQuote.scss"

const PullQuoteParagraph = ({
  node,
  block = "paragraph",
  element = "pullquote",
  modifiers,
}) => {
  return (
    <section className={bem(block, element, modifiers)}>
      <CKEditorContent content={node.field_quote} />
      <p className={bem(element, 'attribution')}>
        {node.field_attribution !== null && node.field_attribution}
      </p>
    </section>
  )
}

export default PullQuoteParagraph
