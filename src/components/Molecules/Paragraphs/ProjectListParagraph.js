import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import NoResults from "components/Atoms/NoResults/NoResults"
import { createSolrQuery, EmbeddedList } from "components/Organisms/Search"
import ProjectTeaser from "components/Templates/Project/ProjectTeaser"
import Button from "components/Atoms/Button/Button"
import "./ProjectListParagraph.scss"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

const buildProjectCard = ProjectNode => {
  let initialValue = ""
  const categories = ProjectNode.terms?.reduce(
    (previousValue, currentValue) =>
      (previousValue ? previousValue + ", " : "") + currentValue.split(">")[1],
    initialValue
  )
  return {
    title: ProjectNode.title,
    timeframe: ProjectNode.subtitle,
    link: ProjectNode.url,
    image: ProjectNode.image,
    imageAlt: ProjectNode.image_alt,
    summary: ProjectNode.overview,
    categories: categories,
  }
}

const ProjectListParagraph = ({
  node,
  block = "paragraph",
  element = "projectlist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Project"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ss_federated_title_sort_string asc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4
  const displayInGrid = node.field_list_format || "list"

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const ProjectCardProperties = results.map(n => buildProjectCard(n))

          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"Project"} />}

              {displayInGrid === "grid" ? (
                <Row>
                  {ProjectCardProperties.map((n, i) => (
                    <Col key={"project-card-" + i} md={4}>
                      <ProjectTeaser
                        displayInGrid={displayInGrid}
                        ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                        {...n}
                      />
                    </Col>
                  ))}
                </Row>
              ) : (
                ProjectCardProperties.map((n, i) => (
                  <ProjectTeaser
                    key={"project-card-" + i}
                    ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                    {...n}
                  />
                ))
              )}

              {node.field_link && (
                <Button
                  link={
                    node.field_link?.uri_alias
                      ? node.field_link?.uri_alias
                      : node.field_link?.uri
                  }
                  children={node.field_link?.title}
                  showIcon={true}
                />
              )}
            </>
          )
        }}
      />
    </section>
  )
}

export default ProjectListParagraph
