/**
 *  Queries and displays content from the Button Paragraph Bundle
 */
import React from "react"
import bem from "components/_utils/bem"
import PropTypes from "prop-types"
import Button from "components/Atoms/Button/Button"

const ButtonParagraph = ({
  node,
  block = "paragraph",
  element = "button",
  modifiers,
  showIcon = true,
}) => {
  const linkUri = node.field_link?.uri_alias
    ? node.field_link?.uri_alias
    : node.field_link?.uri
  return (
    <section
      className={`${bem(block, element, modifiers)} text-align-${
        node.field_alignment
      }`}
    >
      {node.field_link && (
        <Button link={linkUri} showIcon={showIcon}>
          {node.field_link?.title}
        </Button>
      )}
    </section>
  )
}

ButtonParagraph.propTypes = {
  node: PropTypes.shape({
    field_alignment: PropTypes.string,
    field_link: PropTypes.object,
  }),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default ButtonParagraph
