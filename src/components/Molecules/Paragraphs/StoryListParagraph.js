import React from "react"
import addTerm from "components/_utils/addTerm"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import NoResults from "components/Atoms/NoResults/NoResults"
import Button from "components/Atoms/Button/Button"
import StoryCard from "components/Molecules/StoryCard/StoryCard"
import { EmbeddedList, createSolrQuery } from "components/Organisms/Search"

const buildFeaturedStoryCard = federatedStory => {
  return {
    title: federatedStory.title,
    date: federatedStory.date,
    link: federatedStory.url,
    summary: federatedStory.overview,
    image: federatedStory.image,
    imageAlt: federatedStory.image_alt,
    featured: "featured",
  }
}

const buildStoryCard = federatedStory => {
  return {
    title: federatedStory.title,
    date: federatedStory.date,
    link: federatedStory.url,
    image: federatedStory.image,
    imageAlt: federatedStory.image_alt,
    featured: "nonFeaturedCard",
  }
}

const StoryListParagraph = ({
  node,
  block = "paragraph",
  element = "storylist",
  modifiers,
}) => {
  const fixedFieldFilters = {
    ss_federated_type: ["Story"],
    sm_federated_terms: [],
  }

  const category = node.relationships?.field_category
  fixedFieldFilters["sm_federated_terms"] = [...addTerm(category)]

  const solrQuery = createSolrQuery({
    fixedFieldFilters: fixedFieldFilters,
    fixedSort: "ds_federated_date desc",
    fixedDelimiter: " AND ",
  })

  const heading = node.field_display_title
  const numItems = node.field_number_of_items || 4

  return (
    <section className={bem(block, element, modifiers)}>
      <EmbeddedList
        solrQuery={solrQuery}
        numResults={numItems}
        render={results => {
          const featuredStoryCardProperties =
            results.length > 0 ? buildFeaturedStoryCard(results[0]) : null
          const storyCardsProperties = results
            .slice(1, results.length)
            .map(n => buildStoryCard(n))
          return (
            <>
              {heading && (
                <div className="list-heading">
                  <Heading level={2} ariaLevel={2}>
                    {heading}
                  </Heading>
                </div>
              )}
              {results.length === 0 && <NoResults type={"Story"} />}
              <div className="featuredWrapper">
                {featuredStoryCardProperties && (
                  <StoryCard
                    ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                    {...featuredStoryCardProperties}
                  />
                )}
              </div>
              <div className="nonFeaturedWrapper">
                {storyCardsProperties.map((n, i) => (
                  <StoryCard
                    key={"story-card-" + i}
                    ariaLevel={heading ? 3 : 2} // if heading, pass aria level as 3, otherwise, make aria level 2
                    {...n}
                  />
                ))}
              </div>
              {node.field_link && (
                <Button
                  link={
                    node.field_link?.uri_alias
                      ? node.field_link?.uri_alias
                      : node.field_link?.uri
                  }
                  children={node.field_link?.title}
                  showIcon={true}
                />
              )}
            </>
          )
        }}
      />
    </section>
  )
}

export default StoryListParagraph
