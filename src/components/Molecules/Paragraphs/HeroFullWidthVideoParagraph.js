import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import ReactPlayer from "react-player/lazy"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import { Container } from "react-bootstrap"
import "./HeroFullWidthVideoParagraph.scss"
import { FaPlay, FaPause } from "react-icons/fa"

const HeroFullWidthVideoParagraph = ({
  node,
  block = "paragraph",
  element = "hero-full-width-video",
  modifiers,
}) => {
  const [isPlaying, setPlaying] = useState(true)

  useEffect(() => {
    const prefersReducedMotion = window.matchMedia(
      "(prefers-reduced-motion: reduce)"
    ).matches
    if (prefersReducedMotion) {
      setPlaying(false)
    }
  }, [])

  const youtubeAutoPlay = isPlaying ? 1 : 0
  let { heading, linkUrl, linkTitle } =
    HeroFullWidthVideoParagraphDataNormalizer(node)

  return (
    <section className={bem(block, element, modifiers)}>
      <div className="row">
        <div className="col-12 video-player-wrapper">
          <div className="ratio ratio-16x9">
            <ReactPlayer
              url={
                node.relationships?.field_media_item?.field_media_oembed_video
              }
              controls={false}
              loop={true}
              volume={0.3}
              muted={true}
              playing={isPlaying}
              config={{
                youtube: {
                  playerVars: {
                    showinfo: 0,
                    modestbranding: 1,
                    rel: 0,
                    autoplay: { youtubeAutoPlay },
                    loop: 1,
                    mute: 1,
                  },
                },
              }}
              max-width="inherit"
              height="100%"
              width="100%"
            />
          </div>

          {(heading || linkUrl) && (
            <>
              <div className="video-dark-overlay"></div>
              <Container fluid className="heading-link-wrapper">
                {heading && (
                  <Heading level={2} modifiers={["hero"]}>
                    {heading}
                  </Heading>
                )}
                {linkUrl ? (
                  <Button showIcon={true} link={linkUrl}>
                    {linkTitle}
                  </Button>
                ) : (
                  ""
                )}
              </Container>
            </>
          )}

          <div className="video-play-pause-wrapper">
            {!isPlaying ? (
              <button onClick={() => setPlaying(true)} className="play-btn">
                Play Video <FaPlay className="play-icon"></FaPlay>
              </button>
            ) : (
              <button onClick={() => setPlaying(false)} className="pause-btn">
                Pause Video <FaPause className="pause-icon"></FaPause>
              </button>
            )}
          </div>
        </div>
      </div>
    </section>
  )
}

const HeroFullWidthVideoParagraphDataNormalizer = data => {
  return {
    heading: data?.field_heading,
    linkUrl: data?.field_link?.uri_alias
      ? data?.field_link?.uri_alias
      : data?.field_link?.uri,
    linkTitle: data?.field_link?.title,
  }
}

export const HeroFullWidthVideoParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_link: PropTypes.object,
  relationships: PropTypes.shape({
    field_media_item: PropTypes.object,
  }),
})

HeroFullWidthVideoParagraph.propTypes = {
  node: HeroFullWidthVideoParagraphTypeProps,
}

export default HeroFullWidthVideoParagraph
