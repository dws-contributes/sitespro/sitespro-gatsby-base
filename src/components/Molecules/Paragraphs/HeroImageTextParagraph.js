import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import { Container } from "react-bootstrap"
import "./HeroImageTextParagraph.scss"
import { BB8Image, imageDetails } from "components/Atoms/Image/BB8Image"

// TODO: At some point it might be worth refactoring "orientation" to be more generic and match "option 1" and "option 2"

const HeroImageTextParagraph = ({
  node,
  block = "paragraph",
  element = "hero-image-text",
  modifiers,
}) => {
  let {
    imageDetails,
    linkTitle,
    linkUrl,
    heading,
    tagline,
    title,
    orientation,
  } = HeroImageTextParagraphDataNormalizer(node)

  let left_col, right_col
  if (orientation === "landscape") {
    left_col = "col-md-7 col-sm-12"
    right_col = "col-md-5 col-sm-12"
  } else {
    left_col = "col-md-5 col-sm-12"
    right_col = "col-md-7 col-sm-12"
  }

  return (
    <section className={bem(block, element, modifiers) + " " + orientation}>
      <Container>
        <div className="row">
          <div className={left_col}>
            <div className="hero-image">
              {orientation === "landscape" ? (
                <BB8Image
                  imageDetails={imageDetails}
                  targetedSizes={["large_3_2", "small_3_2"]}
                />
              ) : (
                <BB8Image
                  imageDetails={imageDetails}
                  targetedSizes={["large_4_5", "small_4_5"]}
                />
              )}
            </div>
          </div>
          <div className={right_col}>
            <div className="hero-text">
              {/* Tagline */}
              {tagline && <Eyebrow text={tagline} />}

              {/* Title */}
              {title && <Heading level={2}>{title}</Heading>}

              {/* Subtitle */}
              {heading && <Heading level={3}>{node.field_heading}</Heading>}

              {linkUrl && (
                <Button showIcon={true} link={linkUrl}>
                  {linkTitle}
                </Button>
              )}
            </div>
          </div>
        </div>
      </Container>
    </section>
  )
}

const HeroImageTextParagraphDataNormalizer = data => {
  return {
    imageDetails: imageDetails(data?.relationships?.field_image),
    linkUrl: data?.field_link?.uri_alias
      ? data?.field_link?.uri_alias
      : data?.field_link?.uri,
    linkTitle: data?.field_link?.title,
    tagline: data?.field_tagline,
    title: data?.field_display_title,
    heading: data?.field_heading,
    orientation: data?.field_hero_display_options,
  }
}

export const HeroImageTextParagraphTypeProps = PropTypes.shape({
  field_heading: PropTypes.string,
  field_display_title: PropTypes.string,
  field_tagline: PropTypes.string,
  field_link: PropTypes.object,
  field_hero_display_options: PropTypes.string,
  relationships: PropTypes.shape({
    field_image: PropTypes.object,
  }),
})

HeroImageTextParagraph.propTypes = {
  node: HeroImageTextParagraphTypeProps,
}

export default HeroImageTextParagraph
