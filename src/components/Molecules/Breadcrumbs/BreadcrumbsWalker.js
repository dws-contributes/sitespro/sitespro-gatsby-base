import { useMenuData } from "hooks/MenuData"
import { ErrorCatch } from "components/_utils/ErrorCatch"
import { replacePath } from "components/_utils/replacePath"

function getParent(childID, nodeArray) {
  let parentID = 0
  try {
    for (let item of nodeArray) {
      if (
        item.node.drupal_id === childID &&
        item.node.drupal_parent_menu_item
      ) {
        if (item.node.drupal_parent_menu_item.includes("menu_link_content:")) {
          item.node.drupal_parent_menu_item =
            item.node.drupal_parent_menu_item.replace("menu_link_content:", "")
        }
        parentID = item.node.drupal_parent_menu_item
        break
      }
    }
    return parentID
  } catch (error) {
    return ErrorCatch(error)
  }
}

// Creating the array menu itmes with drupal_ids as keyes
function createMappedMenu(menuData, menuName) {
  let mappedArr = {},
    arrElem

  try {
    // First map the nodes of the array to an object -> create a hash table.
    for (let i = 0, len = menuData.length; i < len; i++) {
      arrElem = menuData[i].node
      if (arrElem.menu_name === menuName && arrElem.enabled === true) {
        mappedArr[arrElem.drupal_id] = arrElem
        if (
          arrElem.drupal_parent_menu_item != null &&
          arrElem.drupal_parent_menu_item.includes(arrElem.bundle)
        ) {
          let stripped_drupal_id = arrElem.drupal_parent_menu_item.replace(
            arrElem.bundle + ":",
            ""
          )
          mappedArr[arrElem.drupal_id].drupal_parent_menu_item =
            stripped_drupal_id
        }
      }
    }
    return mappedArr
  } catch (error) {
    ErrorCatch(error)
  }
}

function generateBC(pageSlug, pageTitle, menuLinksArray) {
  let currentPage = {
    path: pageSlug,
    title: pageTitle,
    uri: "",
    drupal_id: 0,
    parent_id: 0,
    menu_name: "",
  }

  // create the breacrumbs trail array, add the first element - homepage
  let trail = []

  // homepage
  if (currentPage.path === "/") {
    currentPage.path = null
  }

  try {
    // populate information about current page
    for (let item of menuLinksArray) {
      let uriAliasWithSlash = replacePath(item.node.link.uri_alias)

      if (uriAliasWithSlash === currentPage.path) {
        currentPage.drupal_id = item.node.drupal_id
        currentPage.title = item.node.title
        currentPage.uri = replacePath(item.node.link.uri_alias)
        currentPage.parent_id = item.node.drupal_parent_menu_item
        currentPage.menu_name = item.node.menu_name
        break
      }
    }

    // if page is not in the menu, append the uri/title at the end of trail
    if (currentPage.menu_name === "") {
      currentPage.uri = currentPage.path
    }

    const mappedMenu = createMappedMenu(menuLinksArray, currentPage.menu_name)
    let nextParentID = getParent(currentPage.drupal_id, menuLinksArray)

    // while hasn't reached the top of the tree, keep adding the parents to the trail
    while (nextParentID !== 0 && mappedMenu[nextParentID]) {
      trail?.unshift({
        title: `${mappedMenu[nextParentID]?.title}`,
        uri: `${mappedMenu[nextParentID]?.link.uri_alias}`,
      })
      nextParentID = getParent(nextParentID, menuLinksArray)
    }

    // inseart the last trail item - current page
    trail?.push({
      title: `${currentPage.title}`,
      uri: `${currentPage.uri}`,
    })

    // insert the homepage item at the beginning of trail
    trail?.unshift({
      title: "Home",
      uri: "/",
    })

    return trail
  } catch (error) {
    ErrorCatch(error)
  }
}

export const BreadcrumbsTrail = (customParents, pageTitle, location) => {
  const menuData = useMenuData()

  // if customParent is defined, reconstruct the trail using them
  // otherwise, use the menuData
  if (customParents && customParents.length) {
    let trail = [
      {
        title: "Home",
        uri: "/",
      },
    ]

    // merge the homepage item and the parents array
    trail = trail?.concat(customParents)

    // add the current page to the end of trail
    trail?.push({
      title: pageTitle,
      uri: "",
    })
    return trail
  } else {
    return generateBC(location?.pathname, pageTitle, menuData)
  }
}
