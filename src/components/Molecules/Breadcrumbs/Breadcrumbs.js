/**
 *  Renders breadcrumbs trail
 *  The separator is styled in CSS
 */
import React from "react"
import PropTypes from "prop-types"
import Link from "components/Atoms/Link/Link"
import { FaHome } from "react-icons/fa"
import bem from "components/_utils/bem"
import { BreadcrumbsTrail } from "./BreadcrumbsWalker"
import { useSiteInfoData } from "../../../hooks/SiteInfoData"
import { Helmet } from "react-helmet"
import { ErrorCatch } from "../../_utils/ErrorCatch"

import "./Breadcrumbs.scss"

const blockName = "breadcrumb"

const buildBreadcrumbSchema = (trail = [], websiteUrl) => {
  let itemListElement = []

  trail?.forEach((breadcrumb, index) => {
    itemListElement.push({
      "@type": "ListItem",
      position: index + 1,
      item: {
        ...(breadcrumb.uri && { "@id": websiteUrl + breadcrumb.uri }),
        name: breadcrumb.title,
      },
    })
  })

  return {
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    itemListElement,
  }
}

// Get default breadcrumb parents given the SiteInfo data
export const GetCustomParents = pageSlug => {
  const siteInfoData = useSiteInfoData()

  return [
    {
      title: siteInfoData.listPages[pageSlug].title,
      uri: "/" + siteInfoData.listPages[pageSlug].url,
    },
  ]
}

export const CustomBreadcrumbs = ({ customParents, pageTitle, location }) => {
  const siteInfoData = useSiteInfoData()
  let websiteUrl = siteInfoData?.site_url
  let trail = BreadcrumbsTrail(customParents, pageTitle, location)
  let breadcrumbSchema = buildBreadcrumbSchema(trail, websiteUrl)
  let breadcrumbItems = []

  try {
    if (!trail) return null

    // get the index of the last item to add markup for the current item
    const trailLastIndex = trail?.length - 1

    breadcrumbItems = trail?.map((item, index) => {
      switch (index) {
        case 0:
          return (
            <li key={`breadcrumb_item${index}`} className="breadcrumb-item">
              <Link to={item.uri} title="Home">
                <FaHome />
              </Link>
            </li>
          )
        case trailLastIndex:
          return (
            <li
              key={`breadcrumb_item${index}`}
              className="breadcrumb-item active"
              aria-current="page"
            >
              {pageTitle}
            </li>
          )
        default:
          return (
            <li key={`breadcrumb_item${index}`} className="breadcrumb-item">
              <Link to={item.uri}>{item.title}</Link>
            </li>
          )
      }
    })
  } catch (error) {
    return ErrorCatch(error)
  }

  return (
    <>
      <Helmet>
        <script type="application/ld+json">
          {JSON.stringify(breadcrumbSchema)}
        </script>
      </Helmet>
      <nav
        aria-label="breadcrumb"
        className={bem(blockName, "wrapper")}
        role="navigation"
      >
        {breadcrumbItems && (
          <ol className="breadcrumb breadcrumb-enterprise">
            {breadcrumbItems}
          </ol>
        )}
      </nav>
    </>
  )
}

CustomBreadcrumbs.propTypes = {
  customParents: PropTypes.array,
  pageTitle: PropTypes.string,
}
