import React, { useState, useEffect } from "react"
import { MdLoop } from "react-icons/md"
import "./Courses.scss"

const SearchResult = ({ result }) => {
  return (
    <tr>
      <td>
        <strong>{result.id}</strong>
        <br />
        {result.term}
      </td>
      <td>{result.title}</td>
      <td>
        {result.description ? result.description : "Not offered this semester."}
      </td>
      <td>{result.instructor}</td>
    </tr>
  )
}

const Courses = ({ subject }) => {
  const streamer_url = "https://streamer.oit.duke.edu"
  const endpoint = "curriculum/courses/subject"
  const access_token = "a89900c0688c7c3e1de7025e0e0e04a2"

  const baseURL =
    [streamer_url, endpoint, subject].join("/") +
    "?access_token=" +
    access_token

  // let courseDetailsURL = ''
  const term = "1790%20-%202022%20Spring%20Term"

  const [postData, setPostData] = useState([])
  const [ready, setReady] = useState(false)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const abortController = new AbortController()
    const fetchCoursesData = () => {
      // try {
      setLoading(true)
      fetch(baseURL)
        .then(response => response.json())
        .then(data => {
          return data
        })
        .then(async data => {
          const courseSummaryData =
            data.ssr_get_courses_resp.course_search_result.subjects.subject
              .course_summaries.course_summary
          await Promise.all(
            courseSummaryData.map((e, index, array) => {
              let courseDetailsURL =
                [
                  streamer_url,
                  "/curriculum/classes/strm/",
                  term,
                  "crse_id",
                  e.crse_id,
                ].join("/") +
                "?crse_offer_nbr=" +
                e.crse_offer_nbr +
                "&access_token=" +
                access_token
              return fetch(courseDetailsURL)
                .then(response => response.json())
                .then(coursesData => {
                  array[index] = {
                    id: e.crse_id,
                    title: e.course_title_long,
                    description:
                      coursesData?.ssr_get_classes_resp?.search_result?.subjects
                        ?.subject?.classes_summary?.class_summary[0]
                        ?.ssr_descrlong,
                    term: coursesData?.ssr_get_classes_resp?.search_result
                      ?.subjects?.subject?.classes_summary?.class_summary[0]
                      ?.strm_lov_descr,
                    instructor:
                      coursesData?.ssr_get_classes_resp?.search_result?.subjects
                        ?.subject?.classes_summary?.class_summary[0]
                        ?.classes_meeting_patterns?.class_meeting_pattern
                        ?.ssr_instr_long,
                  }
                })
            })
          )
          let resultsCourseData =
            data.ssr_get_courses_resp.course_search_result.subjects.subject
              .course_summaries.course_summary
          setPostData(resultsCourseData)
          setReady(true)
          setLoading(false)
        })
    }
    fetchCoursesData()
    return () => {
      abortController.abort()
    }
  }, [baseURL, subject])

  return (
    <div>
      <h3 aria-level={2}>Courses for {subject}</h3>
      {loading && (
        <div className="spinner-border" role="status">
          <span>Loading...</span>
          <MdLoop className="loading-icon" />
        </div>
      )}

      {postData && ready && (
        <table>
          <thead>
            <tr>
              <th>CourseID / Term</th>
              <th>Name</th>
              <th>Description</th>
              <th>Instructor</th>
            </tr>
          </thead>
          <tbody>
            {postData.map(r => (
              <SearchResult result={r} />
            ))}
          </tbody>
        </table>
      )}
    </div>
  )
}

export default Courses
