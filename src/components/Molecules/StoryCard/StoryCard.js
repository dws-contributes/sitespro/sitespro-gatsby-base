import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Link from "components/Atoms/Link/Link"
import Heading from "components/Atoms/Heading/Heading"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import decode from "components/_utils/decode"
import { MdKeyboardBackspace } from "react-icons/md"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import "./StoryCard.scss"

const StoryCard = ({
  date,
  link,
  title,
  summary,
  image,
  imageAlt,
  block = "story-card",
  element,
  modifiers,
  featured,
  ariaLevel = 2,
}) => {
  let featuredImageComponent = null
  if (image) {
    const imageLarge = image.replace("styles/small_2_5_1", "styles/large_3_2")
    let imageNormalized = {
      imageDetails: {
        variants: { small_3_2: imageLarge },
        alt: imageAlt,
        uri: imageLarge,
      },
      targetedSizes: ["small_3_2"],
    }

    featuredImageComponent = (
      <div className="story-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  return (
    <div className={`${bem(block, element, modifiers)} ` + featured}>
      {featuredImageComponent}
      <div className="storyContentWrapper">
        <div className={bem(block, "meta")}>
          <DateProcessor startDate={date} />
        </div>
        <Heading level={4} ariaLevel={ariaLevel}>
          <Link to={link}>{decode(title)}</Link>
          <MdKeyboardBackspace className={bem(block, "icon")} />
        </Heading>

        {summary && !link.includes("/secure/") && (
          <div className="summary">
            <CKEditorContent content={decode(summary)} />
          </div>
        )}
      </div>
    </div>
  )
}

StoryCard.propTypes = {
  date: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  body: PropTypes.string,
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  featured: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default StoryCard
