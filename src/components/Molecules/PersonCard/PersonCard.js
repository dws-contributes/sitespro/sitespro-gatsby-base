import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Link from "components/Atoms/Link/Link"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import ProfilePlaceholder from "images/ProfilePlaceholder.jpg"
import HTMLEllipsis from "react-lines-ellipsis/lib/html"
import { MdLocalPhone } from "react-icons/md"
import { HiOutlineAtSymbol } from "react-icons/hi"
import decode from "components/_utils/decode"
// import Eyebrow from "components/Atoms/Eyebrow/Eyebrow"
import "./PersonCard.scss"

const PersonCard = ({
  email,
  position,
  roles,
  name,
  link,
  phone,
  imageUrl,
  body,
  summary,
  displayBio = true,
  displayContacts = true,
  type,
  categories = [],
  block = "person-card",
  element,
  modifiers,
  ariaLevel = 2,
}) => {
  let personImageComponent = <img src={ProfilePlaceholder} alt="Unavailable" />

  // Image for regular Profile (always local file)
  if (imageUrl && type === "Profile") {
    let imageNormalized = {
      imageDetails: {
        variants: { focal_point_large: imageUrl },
        alt: name,
        uri: imageUrl,
      },
      targetedSizes: ["focal_point_large"],
    }

    personImageComponent = (
      <BB8Image
        imageDetails={imageNormalized?.imageDetails}
        targetedSizes={imageNormalized?.targetedSizes}
      />
    )
  }

  // Image for Scholars Profile (always external)
  if (imageUrl && type === "Scholars@Duke Profile") {
    personImageComponent = <img src={imageUrl} alt={name} />
  }

  let imageCheck = personImageComponent ? ["with-image"] : ["without-image"]

  return (
    <>
      <div className={`${bem(block, element, imageCheck)}`}>
        <div className="person-card-image">{personImageComponent}</div>
        <div className={`${bem(block, "content")}`}>
          {/* <Eyebrow text={type} modifiers={['warning']} />

        {categories.length>0 && categories.map((item, i) =>
          <Eyebrow text={item} modifiers={['warning']} />
        )} */}

          {link ? (
            <Heading level={3} ariaLevel={ariaLevel}>
              <Link to={link}>{decode(name)}</Link>
            </Heading>
          ) : (
            <Heading level={3} ariaLevel={ariaLevel}>
              {decode(name)}
            </Heading>
          )}

          {position && (
            <p className="person-card-position">{decode(position)}</p>
          )}

          {roles &&
            roles?.map((role, index) => (
              <p className="person-card-position" key={index}>
                {role}
              </p>
            ))}

          {email && displayContacts ? (
            <div className="person-card-email">
              <HiOutlineAtSymbol />
              <a href={`mailto:${email}`}>{email}</a>
            </div>
          ) : (
            ""
          )}

          {phone && displayContacts ? (
            <div className="person-card-phone">
              <MdLocalPhone />
              <a href={`tel:${phone.replace(/[^\d]/g, "")}`}>{phone}</a>
            </div>
          ) : (
            ""
          )}

          {summary && type === "Scholars@Duke Profile" ? (
            <div className="person-card-summary">{summary}</div>
          ) : (
            body &&
            displayBio && (
              <div className="person-card-body">
                <HTMLEllipsis
                  unsafeHTML={body}
                  maxLine="3"
                  ellipsis="..."
                  basedOn="words"
                />
              </div>
            )
          )}
        </div>
      </div>
    </>
  )
}

PersonCard.propTypes = {
  email: PropTypes.string,
  position: PropTypes.string,
  name: PropTypes.string,
  link: PropTypes.string,
  phone: PropTypes.string,
  imageUrl: PropTypes.string,
  body: PropTypes.string,
  summary: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default PersonCard
