/**
 *  Renders the authors information block for Story (full format):
 *  - Written by
 *  - Author(s) name, photo, short bio, link to profile
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Link from "components/Atoms/Link/Link"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

import HTMLEllipsis from "react-lines-ellipsis/lib/html"
import "./Authors.scss"
import { BB8Image } from "components/Atoms/Image/BB8Image"

const Authors = ({ authors, block = "authors", modifiers }) => {
  return (
    <div className={bem(block, "wrapper", modifiers)}>
      <Heading level={3} modifiers={["label"]}>
        Written by
      </Heading>
      {authors?.length
        ? authors?.map((author, index) => (
            <Row className={bem("author-card")} key={index}>
              {author.imageDetails.variants && (
                <Col md={2}>
                  <BB8Image imageDetails={author.imageDetails} />
                </Col>
              )}

              <Col md={10}>
                <p className={bem("author-card", "name")}>
                  {author.path && <Link to={author.path}>{author.title}</Link>}
                </p>
                <div className={bem("author-card", "bio")}>
                  {author.summary ? (
                    <CKEditorContent content={author.summary} />
                  ) : (
                    <HTMLEllipsis
                      unsafeHTML={author.description}
                      maxLine="3"
                      ellipsis="..."
                      basedOn="words"
                    />
                  )}
                </div>
              </Col>
            </Row>
          ))
        : ""}
    </div>
  )
}

Authors.propTypes = {
  authors: PropTypes.arrayOf(PropTypes.object),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Authors
