import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { Container, Row, Col } from "react-bootstrap"
import { announcementShape } from "hooks/SiteInfoData"
import "./Announcement.scss"

const Announcement = ({ announcement, block = "announcement" }) => {
  const { body, priority } = announcement

  return (
    <div className={bem(block, "wrapper", [priority])}>
      {announcement && (
        <Container>
          <Row className="justify-content-md-center">
            <p className={bem(block, "priority")}>{priority}</p>
            <Col className={bem(block, "content")}>
              <span className={bem(block, "icon")}></span>
              {body && <CKEditorContent content={body} />}
            </Col>
          </Row>
        </Container>
      )}
    </div>
  )
}

Announcement.propTypes = {
  announcement: PropTypes.exact(announcementShape),
  block: PropTypes.string,
}

export default Announcement
