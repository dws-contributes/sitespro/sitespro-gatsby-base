import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import "./IconCard.scss"
import { BB8Image, imageDetailsShape } from "components/Atoms/Image/BB8Image"

const IconCard = ({
  imageDetails,
  heading,
  linkText,
  linkUrl,
  description,
  block = "iconcard",
  element,
  modifiers,
}) => {
  return (
    <div className={bem(block, element, modifiers)}>
      {imageDetails && (
        <BB8Image
          imageDetails={imageDetails}
          targetedSizes={["focal_point_large", "focal_point_medium"]}
        />
      )}

      <div className={bem(block, "content")}>
        {/* Heading */}
        {heading && (
          <Heading level={4} block={block} element="heading">
            {heading}
          </Heading>
        )}
        {/* Body */}
        {description && <CKEditorContent content={description} />}
        {/* Button */}
        {linkUrl && (
          <Button link={linkUrl} showIcon={true} modifiers={["plain", "blue"]}>
            {linkText}
          </Button>
        )}
      </div>
    </div>
  )
}

IconCard.propTypes = {
  imageDetails: PropTypes.shape(imageDetailsShape),
  alt: PropTypes.string,
  heading: PropTypes.string,
  description: PropTypes.string,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
}

export default IconCard
