import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import Link from "components/Atoms/Link/Link"
import Heading from "components/Atoms/Heading/Heading"
import decode from "components/_utils/decode"
import "./TeaserCard.scss"
// TODO: Should bsDot be used here for more control over dot size?
// The char code is hard to size up without looking wonky vs increasing an svg size

const TeaserCard = ({
  title,
  date,
  link,
  block = "teasercard",
  element,
  source,
  modifiers,
  type,
  ariaLevel = 2,
}) => {
  let sourceComponent = type === "blog" ? source : decode(source)
  return (
    <div className={bem(block, element, modifiers)}>
      {date && (
        <div className={bem(block, "meta")}>
          <DateProcessor startDate={date} />
          {source && (
            <p className={bem(block, "meta", ["source"])}>
              <span>&nbsp;&#183;&nbsp;</span>
              {sourceComponent}
            </p>
          )}
        </div>
      )}
      <Heading level={4} ariaLevel={ariaLevel} modifiers={["teasercard"]}>
        <Link to={link}>{decode(title)}</Link>
      </Heading>
    </div>
  )
}

TeaserCard.propTypes = {
  title: PropTypes.string,
  date: PropTypes.string,
  link: PropTypes.string,
  source: PropTypes.string || PropTypes.element || PropTypes.object,
  ariaLevel: PropTypes.number,
}

export default TeaserCard
