import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import Button from "components/Atoms/Button/Button"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import { BB8Image, imageDetailsShape } from "components/Atoms/Image/BB8Image"

const Callout = ({
  imageDetails,
  imageTargetedSizes,
  imageAlignment = "left",
  heading,
  description,
  linkText,
  linkUrl,
  block = "callout",
  element,
  modifiers,
  additionalClasses,
}) => {
  let flexAlignment = imageAlignment === "right" ? "image-right" : ""

  return (
    <div
      className={`${bem(
        block,
        element,
        modifiers,
        additionalClasses
      )} ${flexAlignment}`}
    >
      {(imageDetails?.uri || imageDetails?.variants) && (
        <div className={bem(block, "image")}>
          <BB8Image
            imageDetails={imageDetails}
            targetedSizes={imageTargetedSizes}
          />
        </div>
      )}

      <div className="content-wrapper">
        <div className={bem(block, "content")}>
          {/* Heading */}
          {heading && (
            <Heading level={3} block={block} element="heading">
              {heading}
            </Heading>
          )}
          {/* Body */}
          {description && <CKEditorContent content={description} />}
          {/* Button */}
          {linkUrl && (
            <Button link={linkUrl} showIcon={true}>
              {linkText}
            </Button>
          )}
        </div>
      </div>
    </div>
  )
}

Callout.propTypes = {
  imageDetails: PropTypes.shape(imageDetailsShape),
  imageAlignment: PropTypes.string,
  heading: PropTypes.string,
  linkText: PropTypes.string,
  linkUrl: PropTypes.string,
  description: PropTypes.string,
}

export default Callout
