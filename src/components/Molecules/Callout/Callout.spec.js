import React from "react"
import { render, screen, waitFor } from "@testing-library/react"
import { useSiteInfoData } from "hooks/SiteInfoData"
import Callout from "./Callout"
jest.mock("hooks/SiteInfoData")

const imageDetails = {
  variants: {
    scale_width_500:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/scale_width_500/public/teaching-student-looks-on.jpg?itok=mCeQITgw",
    scale_width_1000: null,
    focal_point_large:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/focal_point_large/public/teaching-student-looks-on.jpg?h=05c678d6&itok=6vl7c6VE",
    focal_point_medium: null,
    large_2_5_1:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/large_2_5_1/public/teaching-student-looks-on.jpg?h=05c678d6&itok=zJ_nvSl8",
    large_3_2:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/large_3_2/public/teaching-student-looks-on.jpg?h=05c678d6&itok=TDeofGQk",
    large_4_3:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/large_4_3/public/teaching-student-looks-on.jpg?h=05c678d6&itok=C67jkYdf",
    large_4_5:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/large_4_5/public/teaching-student-looks-on.jpg?h=05c678d6&itok=Eg7XUf3w",
    small_2_5_1:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/small_2_5_1/public/teaching-student-looks-on.jpg?h=05c678d6&itok=CD-nkQnr",
    small_3_2:
      "https://gradschool-content.oit.duke.edu/sites/default/files/styles/small_3_2/public/teaching-student-looks-on.jpg?h=05c678d6&itok=12Q-6-4J",
  },
  alt: "Student looks on from the end of a long conference table",
  uri: "/sites/default/files/teaching-student-looks-on.jpg",
  fileMime: "image/jpeg",
}

describe("Molecules > Callout", () => {
  // TODO: Can this be globally mocked?
  beforeAll(() => {
    useSiteInfoData.mockReturnValue({
      allBlockContentSiteInformation: {},
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  // Let's figure out the image test later

  it("renders a heading", () => {
    render(<Callout heading="Test Heading" />)
    expect(screen.getByText("Test Heading")).toBeInTheDocument()
  })

  it("renders a description", () => {
    render(<Callout description="Test Description" />)
    expect(screen.getByText("Test Description")).toBeInTheDocument()
  })

  it("renders a button", () => {
    render(<Callout linkText="Test Link" linkUrl="https://google.com" />)
    expect(screen.getByText("Test Link")).toBeInTheDocument()
    expect(screen.getByText("Test Link").closest("a")).toHaveAttribute(
      "href",
      "https://google.com"
    )
  })
})
