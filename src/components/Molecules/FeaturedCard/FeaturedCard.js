import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import DateProcessor from "components/Atoms/DateProcessor/DateProcessor"
import { BB8Image } from "components/Atoms/Image/BB8Image"
import CKEditorContent from "components/Atoms/CKEditor/DrupalCKEditor"
import Link from "components/Atoms/Link/Link"
import decode from "components/_utils/decode"
import "./FeaturedCard.scss"
import { MdKeyboardBackspace } from "react-icons/md"

const FeaturedCard = ({
  image,
  imageAlt,
  title,
  date,
  link,
  summary,
  block = "featuredcard",
  element,
  source,
  type = "news",
  ariaLevel = 2,
}) => {
  let featuredImageComponent = null

  if (image) {
    const imageLarge = image.replace("styles/small_2_5_1", "styles/large_3_2")
    let imageNormalized = {
      imageDetails: {
        variants: { large_3_2: imageLarge },
        alt: imageAlt,
        uri: imageLarge,
      },
      targetedSizes: ["large_3_2"],
    }

    featuredImageComponent = (
      <div className="news-featured-image">
        <BB8Image
          imageDetails={imageNormalized?.imageDetails}
          targetedSizes={imageNormalized?.targetedSizes}
        />
      </div>
    )
  }

  let imageCheck = featuredImageComponent ? ["with-image"] : ["without-image"]
  let sourceComponent =
    type === "news" ? <CKEditorContent content={source} /> : source

  return (
    <>
      <div className={`${bem(block, element, imageCheck)}`}>
        {featuredImageComponent}
        <div className={`${bem(block, "content")}`}>
          <div className={bem(block, "meta")}>
            <DateProcessor startDate={date} />
            {source && (
              <span>
                &nbsp;&#183;&nbsp;
                {sourceComponent}
              </span>
            )}
          </div>
          {link && (
            <Heading
              level={4}
              ariaLevel={ariaLevel}
              modifiers={["featuredcard"]}
            >
              <Link to={link}>{decode(title)}</Link>
              <MdKeyboardBackspace className={bem(block, "icon")} />
            </Heading>
          )}

          {summary && !link.includes("/secure/") && (
            <div className="contentWrapper">
              {<CKEditorContent content={decode(summary)} />}
            </div>
          )}
        </div>
      </div>
    </>
  )
}

FeaturedCard.propTypes = {
  image: PropTypes.string,
  imageAlt: PropTypes.string,
  id: PropTypes.string,
  title: PropTypes.string,
  date: PropTypes.string,
  link: PropTypes.string,
  summary: PropTypes.string,
  source: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  feedImageUrl: PropTypes.string,
  feedImageAltText: PropTypes.string,
  ariaLevel: PropTypes.number,
}

export default FeaturedCard
