/**
 *  Renders the categories information block for Story (full format):
 *  - Written by
 *  - Author(s) name, photo, short bio, link to profile
 */
import React from "react"
import PropTypes from "prop-types"
import bem from "components/_utils/bem"
import Heading from "components/Atoms/Heading/Heading"
import "./Categories.scss"
import { ErrorCatch } from "components/_utils/ErrorCatch"
import Link from "components/Atoms/Link/Link"
import { useSiteInfoData } from "../../../hooks/SiteInfoData"

const Categories = ({
  categories = [],
  linkto = null,
  block = "categories",
  modifiers,
}) => {

  const siteInfoData = useSiteInfoData()
  const listPageURL = linkto ? siteInfoData?.listPages[linkto]?.url : null

  let formattedCategories = []

  try {
    formattedCategories = categories.map((category, index) => (
      <span key={index} className={bem(block, "name", modifiers)}>
        {listPageURL ? (
          <Link to={`/${listPageURL}?term=Category>${category?.title}`}>
            {category?.title}
          </Link>
        ) : (
          category?.title
        )}

        {index < categories.length - 1
          ? index === categories.length - 2 && categories.length !== 2
            ? ", and "
            : ", "
          : " "}
      </span>
    ))
  } catch (error) {
    ErrorCatch(error)
  }
  return categories.length ? (
    <div className={bem(block, "wrapper", modifiers)}>
      <Heading level={3} modifiers={["label"]}>
        Categories
      </Heading>
      {formattedCategories}
    </div>
  ) : (
    ""
  )
}

Categories.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.object),
  modifiers: PropTypes.arrayOf(PropTypes.string),
}

export default Categories
