import React from "react"
import LayoutBase from "components/Organisms/Layout/LayoutBase"
import Container from "react-bootstrap/Container"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import theme from "theme"
import GoogleSearchResults from "components/Organisms/Search/SearchResults/GoogleSearchResults"
import SolrSearchResults from "components/Organisms/Search/SearchResults/SolrSearchResults"

const SearchPage = ({ location, navigate }) => {
  const pageTitle = "Search"

  return (
    <LayoutBase
      pageTitle={pageTitle}
      metaTags={{
        title: "Search page",
        description: "Search results page",
      }}
      showStatusInfo={false}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          {theme.search ? (
            // Google search
            <GoogleSearchResults gsearchID={theme.search} />
          ) : (
            // Solr search
            <SolrSearchResults location={location} navigate={navigate} />
          )}
        </Container>
      </main>
    </LayoutBase>
  )
}

export default SearchPage
export { Head } from "components/Organisms/Layout/Head"
