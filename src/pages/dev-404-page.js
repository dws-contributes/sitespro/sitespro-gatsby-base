/**
 * Dev 404 page, also displays in Drupal Preview
 */
import React from "react"
import LayoutBase from "../components/Organisms/Layout/LayoutBase"

import { CustomBreadcrumbs } from "../components/Molecules/Breadcrumbs/Breadcrumbs"
import PageTitle from "../components/Atoms/PageTitle/PageTitle"
import Heading from "../components/Atoms/Heading/Heading"
import { Container } from "react-bootstrap"

const Dev404Page = () => {
  const pageTitle = "Page not found"

  return (
    <LayoutBase
      pageTitle={pageTitle}
      metaTags={{
        title: pageTitle,
        description: pageTitle,
      }}
    >
      <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
        <PageTitle>{pageTitle}</PageTitle>
        <Container>
          <CustomBreadcrumbs pageTitle={pageTitle} />

          <Heading level={2}>Don't worry! Nothing is broken!</Heading>

          <p>
            We're busy building your content, and it will display here once it
            is ready. Thank you for your patience.
          </p>
        </Container>
      </main>
    </LayoutBase>
  )
}

export default Dev404Page
export { Head } from "components/Organisms/Layout/Head"
