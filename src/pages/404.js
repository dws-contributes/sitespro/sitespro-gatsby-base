/**
 * Production 404 page
 */
import React from "react"
import { navigate } from "@reach/router"
import LayoutBase from "components/Organisms/Layout/LayoutBase"

import { CustomBreadcrumbs } from "components/Molecules/Breadcrumbs/Breadcrumbs"
import PageTitle from "components/Atoms/PageTitle/PageTitle"
import { FaSearch } from "react-icons/fa"
import { Container } from "react-bootstrap"

const browser = typeof window !== "undefined" && window

const NotFoundPage = () => {
  const pageTitle = "Page not found"

  const submitSearch = e => {
    e.preventDefault()
    const newQueryField = e.target.querySelector("[name=q]")
    const queryString = new URLSearchParams({
      q: newQueryField.value,
    }).toString()
    navigate(`/search?${queryString}`)
    newQueryField.value = null
  }

  return (
    browser && (
      <LayoutBase
        pageTitle={pageTitle}
        metaTags={{
          title: pageTitle,
          description: pageTitle,
        }}
      >
        <main id="reach-skip-nav" data-reach-skip-nav-content="" tabIndex="-1">
          <PageTitle>{pageTitle}</PageTitle>
          <Container>
            <CustomBreadcrumbs pageTitle={pageTitle} />

            <p>
              The page you requested was not found. It may have moved or may no
              longer exist, or you may have typed the URL incorrectly. You can
              try searching for it:
            </p>

            <div className="not-found-search mb-3">
              <form action="/search" onSubmit={submitSearch} className="row">
                <div className="col-9">
                  <input
                    className="form-control"
                    type="search"
                    name="q"
                    placeholder="Search"
                  />
                </div>
                <button type="submit" className="btn btn-primary col-1">
                  <FaSearch alt="Go" />
                </button>
              </form>
            </div>

            <p>
              You might try double-checking the URL, or you can return to the
              homepage. If you're still unable to find what you're looking for,
              please contact the site owner.
            </p>
          </Container>
        </main>
      </LayoutBase>
    )
  )
}

export default NotFoundPage
export { Head } from "components/Organisms/Layout/Head"
