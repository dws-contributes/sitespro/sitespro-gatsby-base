/**
 *  Renders Page content type template
 */
import { graphql, useStaticQuery } from "gatsby"
import LayoutHomepage from "components/Organisms/Layout/LayoutHomepage"
import React from "react"
import { getParagraph } from "components/Molecules/Paragraphs/ParagraphsHelper.js"
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true })
    console.log("all is bad")
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>
    }
    return this.props.children
  }
}

const HomepageTemplate = ({ pageContext }) => {
  const homepageQuery = useStaticQuery(graphql`
    {
      site: allBlockContentSiteInformation {
        nodes {
          relationships {
            fields: field_site_homepage {
              id
              drupal_internal__nid
              title
              status
              relationships {
                field_thumbnail_media {
                  ...ImageDetails
                }
                hero: field_hero {
                  type: __typename
                  ...ParagraphCarousel
                  ...ParagraphHeroImage
                  ...ParagraphHeroImageText
                  ...ParagraphHeroVideoText
                  ...ParagraphHeroFullWidthVideo
                }
                paragraphs: field_content_blocks {
                  type: __typename
                  ...ParagraphAccordionSection
                  ...ParagraphBlogList
                  ...ParagraphButton
                  ...ParagraphCallout
                  ...ParagraphContent
                  ...ParagraphContentReference
                  ...ParagraphEventList
                  ...ParagraphFaqsList
                  ...ParagraphIconRow
                  ...ParagraphIframe
                  ...ParagraphImageCardRow
                  ...ParagraphImageText
                  ...ParagraphMedia
                  ...ParagraphNewsList
                  ...ParagraphPageList
                  ...ParagraphPersonList
                  ...ParagraphPolicyList
                  ...ParagraphProjectList
                  ...ParagraphPublicationList
                  ...ParagraphPullQuote
                  ...ParagraphResourceList
                  ...ParagraphScholarsList
                  ...ParagraphSlideshow
                  ...ParagraphSocialMedia
                  ...ParagraphSpacer
                  ...ParagraphSpecialText
                  ...ParagraphStoryList
                  ...ParagraphTwoColumn
                  ...ParagraphThreeColumn
                }
              }
              metatag_normalized {
                tag
                attributes {
                  content
                  property
                  name
                }
              }
            }
          }
        }
      }
    }
  `)

  const homepage = homepageQuery?.site?.nodes
    ? homepageQuery.site.nodes[0]?.relationships?.fields
    : ""

  const hero = homepage?.relationships?.hero
    ? getParagraph(homepage?.relationships?.hero)
    : null
  const paragraphs = homepage?.relationships?.paragraphs?.map(getParagraph)

  // Add homepage metatags to the pageContext, otherwise it's empty
  pageContext.metatags = homepage?.metatag_normalized
  pageContext.title = homepage?.title

  // Add hero and thumbnail for use in meta tag og image
  pageContext.hero = homepage?.relationships?.hero
  pageContext.field_thumbnail_media =
    homepage?.relationships?.field_thumbnail_media
  pageContext.isHomepage = true

  return (
    <ErrorBoundary>
      <LayoutHomepage
        hasSidebar={false}
        title={homepage?.title}
        hero={hero ? hero : null}
        nid={homepage?.drupal_internal__nid}
        publishedStatus={homepage?.status}
      >
        {paragraphs}
      </LayoutHomepage>
    </ErrorBoundary>
  )
}

export default HomepageTemplate
export { Head } from "components/Organisms/Layout/Head"
