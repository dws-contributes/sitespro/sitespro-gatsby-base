const { replacePath } = require("./src/components/_utils/replacePath.js")
const path = require("path")

exports.onPostBuild = ({ reporter }) => {
  reporter.info(`the site has been built`)
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage, createRedirect } = actions

  function snakeToPascal(str) {
    str += ""
    str = str.split("_")
    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].slice(0, 1).toUpperCase() + str[i].slice(1, str[i].length)
    }
    return str.join("")
  }

  const publishedFilter =
    process.env.NODE_ENV === "development"
      ? ``
      : `(filter: {status: {eq: true}})`

  const queryNodeTypes = `{
    allNodeTypeNodeType(filter: {drupal_internal__type: {nin:["announcement", "faq"]}}) {
      nodes {
        name
        drupal_internal__type
      }
    }
  }`

  const queryRedirects = `{
    allRedirectRedirect {
      edges {
        node {
          status_code
          redirect_source {
            path
          }
          redirect_redirect {
            uri
            uri_alias
          }
        }
      }
    }
  }`

  const queryNodeFaqs = `{
    allNodeFaq(filter: {status: {eq: true}}) {
      nodes {
        path {
          alias
        }
      }  
    }
  }`

  const queryScholars = `{
    allDukeScholarsProfileDukeScholarsProfile${publishedFilter} {
      nodes {
        id
        drupal_internal__id
        status
        path {
          alias
        }
      }
    }
  }`

  const queryPageUrls = `{
    allBlockContentSiteInformation(
      limit: 1
      sort: { drupal_id: ASC }
    ) {
      nodes {
        field_site_news_page_url
        field_site_blog_page_url
        field_site_events_page_url
        field_site_past_events_page_url
        field_site_policies_page_url
        field_site_projects_page_url
        field_site_stories_page_url
        field_site_faculty_page_url
        field_site_staff_page_url
        field_site_faqs_page_url
        field_site_students_page_url
        field_site_news_page_title
        field_site_blog_page_title
        field_site_events_page_title
        field_site_pastevents_page_title
        field_site_policies_page_title
        field_site_projects_page_title
        field_site_stories_page_title
        field_site_faculty_page_title
        field_site_staff_page_title
        field_site_faqs_page_title
        field_site_students_page_title  
        field_site_activate_news
        field_site_activate_blog
        field_site_activate_events
        field_site_activate_past_events
        field_site_activate_policies
        field_site_activate_projects
        field_site_activate_resources 
        field_site_activate_stories
        field_site_activate_faculty
        field_site_activate_staff
        field_site_activate_faqs
        field_site_activate_students
        site_resources_page_url
        site_resources_page_title
      }
    }  
  }`

  const redirects = await graphql(queryRedirects)
  const nodesScholars = await graphql(queryScholars)
  const nodeTypes = await graphql(queryNodeTypes)
  const nodeFaqs = await graphql(queryNodeFaqs)
  const nodePageUrls = await graphql(queryPageUrls)

  await Promise.all(
    nodeTypes?.data?.allNodeTypeNodeType?.nodes.map(async node => {
      const nodeName = snakeToPascal(node.drupal_internal__type)
      const nodeType = node.drupal_internal__type
      let relatedNodesData

      if (nodeName === "News") {
        relatedNodesData = `relationships {
          field_categories {
            name
          }
          field_categories_site_specific {
            name
          }   
        }`
      } else if (nodeName === "Story" || nodeName === "Project") {
        relatedNodesData = `relationships {
          field_categories {
            name
          }
        }`
      } else {
        relatedNodesData = ``
      }

      const query = `{
        allNode${nodeName}${publishedFilter} {
          nodes {
            id
            drupal_internal__nid
            status
            path {
              alias
            }
            ${relatedNodesData}
          }
        }
      }`

      const homeUrlQuery = `{
        allBlockContentSiteInformation(
          limit: 1
        ) {
          nodes {
            relationships {
              field_site_homepage {
                path {
                  alias
                }
              }
            }
          }
        }  
      }`

      const nodes = await graphql(query)
      const homeUrlData = await graphql(homeUrlQuery)

      nodes.data[`allNode${nodeName}`].nodes.forEach(node => {
        let nodePath = node.path.alias
          ? node.path.alias
          : `/${nodeType}/${node.drupal_internal__nid}`
        let fixedPath = replacePath(nodePath)

        /**
         * Don't build the page at /homepage (or whatever is set in Drupal) in production
         * I don't know if this page ever needs built period, but it might require extra path mapping in the Drupal config
         * to fix the Drupal home page link + the preview server iframe.
         */
        const homeUrlPath = replacePath(
          homeUrlData?.data?.allBlockContentSiteInformation?.nodes[0]
            ?.relationships?.field_site_homepage.path.alias
        )
        if (
          process.env.NODE_ENV === "production" &&
          nodeName === "Page" &&
          fixedPath === homeUrlPath
        ) {
          // Let's create a redirect in case someone accidentally links to it in Drupal
          createRedirect({
            fromPath: fixedPath,
            toPath: "/",
            statusCode: 301,
            force: true,
            redirectInBrowser: true,
            isPermanent: true,
          })

          // Exit & skip the page creation - meaning it won't be built nor will it appear in the sitemap
          return
        }

        // manage the page context higher up so that we can alter it independently if necessary
        let pageContext = {
          ID: node.id,
          NID: node.drupal_internal__nid,
          uri: nodePath,
          publishedStatus: node.status,
        }

        // Add imported and site specific categories to the page context of the news nodes that will be built as pages.
        // This allows us to make use of page queries to get related news.
        if (nodeName === "News") {
          const categoriesImported = node.relationships?.field_categories ?? []
          const categoriesSiteSpecific =
            node.relationships?.field_categories_site_specific ?? []

          pageContext = {
            ...pageContext,
            categories:
              [...categoriesImported, ...categoriesSiteSpecific].flatMap(
                category => category.name
              ) || [],
          }
        }

        // Add categories to the page context of the projects or stories nodes that will be built as pages.
        // This allows us to make use of page queries to get related projects or stories.
        if (nodeName === "Story" || nodeName === "Project") {
          pageContext = {
            ...pageContext,
            categories:
              node.relationships?.field_categories?.flatMap(
                category => category.name
              ) || [],
          }
        }

        createPage({
          path: fixedPath,
          component: path.resolve(
            `${__dirname}/src/templates/${nodeName}Template.js`
          ),
          context: pageContext,
        })
      })
    }),

    nodesScholars?.data?.allDukeScholarsProfileDukeScholarsProfile?.nodes.forEach(
      node => {
        let nodePath = node.path.alias
          ? node.path.alias
          : `/duke_scholars_profile/${node.drupal_internal__id}`
        let fixedPath = replacePath(nodePath)

        createPage({
          path: fixedPath,
          component: path.resolve(
            `${__dirname}/src/templates/ScholarsTemplate.js`
          ),
          context: {
            ID: node.id,
            NID: node.drupal_internal__id,
            uri: nodePath,
            publishedStatus: node.status,
          },
        })
      }
    ),

    redirects?.data?.allRedirectRedirect?.edges.map(redirect => {
      createRedirect({
        fromPath: replacePath(`/${redirect?.node?.redirect_source?.path}`),
        toPath: redirect?.node?.redirect_redirect?.uri_alias,
        statusCode: redirect?.node?.status_code,
        force: true,
        redirectInBrowser: true,
        isPermanent: true,
      })
    }),

    // create redirects for the FAQ pages
    nodeFaqs?.data?.allNodeFaq?.nodes?.map(node => {
      createRedirect({
        fromPath: `${node?.path?.alias}/`,
        toPath: `/faqs/?f=${node?.path?.alias}` || "/index.js",
        statusCode: 301,
        force: true,
        redirectInBrowser: true,
        isPermanent: true,
      })
    }),

    // create custom list pages
    nodePageUrls?.data?.allBlockContentSiteInformation?.nodes?.map(node => {
      let listPages = {
        news: {
          url: node.field_site_news_page_url || "news",
          title: node.field_site_news_page_title || "News",
          activate: node.field_site_activate_news,
        },
        stories: {
          url: node.field_site_stories_page_url || "stories",
          title: node.field_site_stories_page_title || "Stories",
          activate: node.field_site_activate_stories,
        },
        blog: {
          url: node.field_site_blog_page_url || "blog",
          title: node.field_site_blog_page_title || "Blog",
          activate: node.field_site_activate_blog,
        },
        events: {
          url: node.field_site_events_page_url || "events",
          title: node.field_site_events_page_title || "Events",
          activate: node.field_site_activate_events,
        },
        "past-events": {
          url: node.field_site_past_events_page_url || "past-events",
          title: node.field_site_pastevents_page_title || "Past Events",
          activate: node.field_site_activate_past_events,
        },
        policies: {
          url: node.field_site_policies_page_url || "policies",
          title: node.field_site_policies_page_title || "Policies",
          activate: node.field_site_activate_policies,
        },
        projects: {
          url: node.field_site_projects_page_url || "projects",
          title: node.field_site_projects_page_title || "Projects",
          activate: node.field_site_activate_projects,
        },
        faculty: {
          url: node.field_site_faculty_page_url || "faculty",
          title: node.field_site_faculty_page_title || "Faculty",
          activate: node.field_site_activate_faculty,
        },
        resources: {
          url: node.site_resources_page_url || "resources",
          title: node.site_resources_page_title || "Resources",
          activate: node.field_site_activate_resources,
        },
        staff: {
          url: node.field_site_staff_page_url || "staff",
          title: node.field_site_staff_page_title || "Staff",
          activate: node.field_site_activate_staff,
        },
        faqs: {
          url: node.field_site_faqs_page_url || "faqs",
          title: node.field_site_faqs_page_title || "FAQs",
          activate: node.field_site_activate_faqs,
        },
        students: {
          url: node.field_site_students_page_url || "students",
          title: node.field_site_students_page_title || "Students",
          activate: node.field_site_activate_students,
        },
      }

      Object.keys(listPages).map(pageSlug => {
        let page = listPages[pageSlug]
        const pagePath = `/${page.url}`
        if (page.activate !== false) {
          createPage({
            path: pagePath,
            component: path.resolve(
              `${__dirname}/src/templates/list-pages/${pageSlug}.js`
            ),
            context: {
              uri: pagePath,
              publishedStatus: "published",
              title: page.title,
              listPage: true,
            },
          })
        }
      })
    })
  )
}

exports.onCreateWebpackConfig = ({ actions, getConfig }) => {
  const config = getConfig()
  const miniCssExtractPlugin = config.plugins.find(
    plugin => plugin.constructor.name === "MiniCssExtractPlugin"
  )
  if (miniCssExtractPlugin) {
    miniCssExtractPlugin.options.ignoreOrder = true
  }
  actions.replaceWebpackConfig(config)
  actions.setWebpackConfig({
    devtool: "cheap-module-source-map",
  })
}
