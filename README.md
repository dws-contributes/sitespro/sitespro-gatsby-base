### Changelog:
- 1/14/2025 - Getsy (12.0.0)
- 9/25/2024 - Ryder 3 (11.2.0)
- 8/7/2024 - Ryder 2 (11.1.1)
- 7/2/2024 - Ryder 1 accessibility hotfix release (11.0.0)
- 4/3/2024 - Wally 2 release (10.1.0)
- 2/16/2024 - Wally 1 release (10.0.4)
- 1/10/2024 - Charlie urgent request for brand bar release (9.0.6)
- 12/21/2023 - Charlie hotfix release (9.0.5)
- 12/19/2023 - Charlie hotfix release (9.0.3)
- 12/11/2023 - Charlie release fix (9.0.1)
- 12/5/2023 - Charlie release (9.0.0)
- 10/4/23 - Taco release 2 (8.1.0) node packages updates
- 8/24/23 - Taco hotfix (8.0.1)
- 8/23/23 - Taco release (8.0.0) 
- 5/4/23 - Guillermo release (7.0.0)
- 3/24/23 - Hotfix for the carousel images
- 3/17/23 - Breaking change: add Policy CT, Enzo release #2 (6.0)
- 12/16/22 - Breaking change: Remove Document CT, add Project CT. Buddy Release (5.0).
- 7/1/2022 - Breaking change: updated handling of image styles.
- 6/14/2022 - Add Events XML feed.
- 6/8/2022 - Change version, add Piper and Patch changes.
- 3/23/2022 - Change version, GREG release changes.
- 6/11/2021 - Breaking change: added field enhancers to the textarea fields.
- 4/20/2021 - Testing Beta Release

### Extending theme.js (example)
```
import deepmerge from 'deepmerge'
import baseTheme from "@dws-contributes/sitespro-gatsby-base/src/theme"

const customTheme = {}

const theme = deepmerge(baseTheme, customTheme)
export default theme
```

### How to create a new release version (change of the first number)
Use the following format for the commit. Pay a special attention to the empty lines! Believe me, it's important. You don't want to spend another full hour trying to figure out how to create a darn release (like some). For more information about how to format a semantic commit, see https://github.com/angular/angular/blob/main/CONTRIBUTING.md#commit-message-footer
````
<-- Start of commit message -->
Lots of awesome changes for this release!

BREAKING CHANGE: describe your release changes. FTW!

<-- End of commit message -->
````
