/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import "components/_utils/global.scss"
import React from "react"
import { SiteContextProvider } from "components/_utils/SiteContext"

export const wrapRootElement = ({ element }) => (
  <SiteContextProvider>{element}</SiteContextProvider>
)

export const onRouteUpdate = ({ location, prevLocation }) => {
  if (prevLocation !== null) {
    // Skip focus when a link goes to an anchor
    if (location.hash.length) {
      return
    }

    // Select the skip link element
    const skipLink = document.querySelector("#reach-skip-nav")
    if (skipLink) {
      setTimeout(() => {
        skipLink.focus()
        // Fix: https://stackoverflow.com/a/56996986
        window.scrollTo(0, 0)
      }, 100)
    }
  }
}
