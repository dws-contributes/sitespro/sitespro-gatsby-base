const { createProxyMiddleware } = require("http-proxy-middleware")
const path = require("path")
const Fs = require("fs")

// import the fonts or custom fonts
const srcPath = Fs.existsSync(
  path.resolve(
    __dirname,
    "./../../../src/@dws-contributes/sitespro-gatsby-base/fonts.js"
  )
)
  ? path.resolve(
      __dirname,
      "./../../../src/@dws-contributes/sitespro-gatsby-base"
    )
  : path.join(__dirname, "/src")
const fonts = require(path.join(srcPath, "/fonts"))

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const globalConfig = {
  trailingSlash: "always",
  developMiddleware: app => {
    app.use(
      "/solrsearch",
      createProxyMiddleware({
        target: process.env.SOLR_SERVER_URL,
        followRedirects: true,
        toProxy: true,
        changeOrigin: true,
        pathRewrite: {
          "^/solrsearch/search": process.env.SOLR_REWRITTEN_PATH,
        },
      })
    )
  },
  flags: {
    PRESERVE_FILE_DOWNLOAD_CACHE:
      process.env.PRESERVE_FILE_DOWNLOAD_CACHE || false,
    FAST_DEV: process.env.FAST_DEV || true,
  },
  siteMetadata: {
    siteUrl: process.env.SITE_URL,
    site_url: process.env.SITE_URL,
    cdnUrl: process.env.CDN_BASE_URL,
    drupalBaseUrl: process.env.DRUPAL_BASE_URL,
  },
  plugins: [
    // Uncomment as needed for performance debugging
    // `gatsby-plugin-perf-budgets`,
    // `gatsby-plugin-webpack-bundle-analyser-v2`,
    `gatsby-plugin-no-sourcemaps`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-meta-redirect`,
    {
      resolve: "gatsby-plugin-root-import",
      options: {
        resolveModules: [
          path.join(__dirname, "src"),
          path.join(__dirname, "src/@dws-contributes/sitespro-gatsby-base"),
        ],
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: "sitemap",
        excludes: [`/allcontent`, `/secure/**`],
      },
    },
    {
      resolve: `gatsby-plugin-gatsby-cloud`,
      options: {
        headers: {}, // option to add more headers. `Link` headers are transformed by the below criteria
        allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
        mergeSecurityHeaders: true, // boolean to turn off the default security headers
        mergeLinkHeaders: true, // boolean to turn off the default gatsby js headers
        mergeCachingHeaders: true, // boolean to turn off the default caching headers
        generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
      },
    },
    {
      resolve: "gatsby-plugin-sass",
      options: {
        additionalData: `@import "${__dirname}/src/config";`,
        sassOptions: {
          includePaths: [
            `${__dirname}/src/`,
            "node_modules/@dws-contributes/sitespro-gatsby-base/src/",
          ],
          sourceMap: true,
          sourceMapIncludeSources: true,
        },
        useResolveUrlLoader: {
          options: {
            sourceMap: true,
          },
        },
        cssLoaderOptions: {
          esModule: true,
          modules: {
            namedExport: true,
          },
          loader: "css-loader",
          options: {
            sourceMap: true,
            modules: {
              exportOnlyLocals: true,
            },
          },
        },
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`,
      },
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: process.env.SITE_URL,
        sitemap: process.env.SITE_URL + "/sitemap/sitemap-0.xml",
        output: "/robots.txt",
        policy: [
          {
            userAgent: "*",
            allow: "/",
            disallow: ["/404", "/dev-404-page", "/secure/*"],
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        lang: `en`,
        short_name: `starter`,
        start_url: `/`,
        // TODO: It would be cool if we could use a primary color variable, somehow...
        background_color: `#012169`,
        theme_color: `#012169`,
        display: `minimal-ui`,
        icon: `${__dirname}/src/images/duke-icon.svg`,
        icons: [
          {
            src: "favicons/icon-144x144.png",
            sizes: "144x144",
            type: "image/png",
          }, // Add or remove icon sizes as desired
        ],
      },
    },
    {
      resolve: "gatsby-omni-font-loader",
      options: {
        /* Font loading mode */
        mode: "async",
        /* Enable font loading listener to handle FOUT */
        enableListener: true,
        /* Preconnect URL-s. This example is for Google Fonts */
        preconnect: ["https://fonts.gstatic.com"],
        /* Self-hosted fonts config. Add font files and font CSS files to "static" folder */
        custom: [],
        /* Web fonts. File link should point to font CSS file. */
        web: [
          {
            /* Exact name of the font as defied in @font-face CSS rule */
            name: `${fonts.primary.name}`,
            /* URL to the font CSS file with @font-face definition */
            file: `https://fonts.googleapis.com/css2?family=${fonts.primary.face}`,
          },
          {
            /* Exact name of the font as defied in @font-face CSS rule */
            name: `${fonts.secondary.name}`,
            /* URL to the font CSS file with @font-face definition */
            file: `https://fonts.googleapis.com/css2?family=${fonts.secondary.face}`,
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-drupal`,
      options: {
        baseUrl: process.env.DRUPAL_BASE_URL,
        apiBase: process.env.API_BASE || `jsonapi`, // optional, defaults to `jsonapi`
        concurrentFileRequests: process.env.CONCURRENT_FILE_REQUESTS || `40`,
        concurrentAPIRequests: process.env.CONCURRENT_FILE_REQUESTS || `40`,
        skipFileDownloads: true,
        imageCDN: false,
        placeholderStyleName: `default`,
        secret: process.env.DRUPAL_SECRET_KEY,
        basicAuth: {
          username: process.env.BASIC_AUTH_USERNAME,
          password: process.env.BASIC_AUTH_PASSWORD,
        },
        fastBuilds: process.env.FAST_BUILDS || false,
        filters: {
          "file--file": "filter[status][value]=1",
        },
        disallowedLinkTypes: [
          `self`,
          `describedby`,
          `action--action`,
          `editor--editor`,
          `block--block`,
          `crop--focal_point`,
          `entity_browser--entity_browser`,
          `entity_embed_fake_entity--entity_embed_fake_entity`,
          `entity_form_display--entity_form_display`,
          `entity_form_mode--entity_form_mode`,
          `entity_view_display--entity_view_display`,
          `entity_view_mode--entity_view_mode`,
          `features_bundle--features_bundle`,
          `field_config--field_config`,
          `field_storage_config--field_storage_config`,
          `filter_format--filter_format`,
          `jsonapi_resource_config--jsonapi_resource_config`,
          `linkit_profile--linkit_profile`,
          `pathauto_pattern--pathauto_pattern`,
          `user_role--user_role`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-schema-snapshot`,
      options: {
        path: `${__dirname}/src/schema/schema.gql`,
        exclude: {
          plugins: [],
        },
        update: false,
      },
    },
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
              }
            }
          }
        `,
        feeds: [
          // News Feed
          {
            serialize: ({ query: { site, allNodeNews } }) => {
              var $description = ""
              var $url = ""
              var $categories = ""

              return allNodeNews.edges.map(edge => {
                $url =
                  edge.node.field_alternate_link &&
                  edge.node.field_alternate_link.uri
                    ? edge.node.field_alternate_link.uri
                    : site.siteMetadata.siteUrl + edge.node.path.alias

                if (edge.node.body)
                  $description = edge.node.body.summary
                    ? edge.node.body.summary
                    : edge.node.body.value.substring(0, 300)

                if (edge.node.relationships.field_categories) {
                  const $category_names =
                    edge.node.relationships.field_categories?.map(category => {
                      return category.name
                    })
                  $categories = $category_names ? $category_names.join("|") : ""
                }

                return Object.assign(
                  {},
                  {
                    title: edge.node.title,
                    description: $description,
                    date: edge.node.field_date,
                    author: edge.node.relationships.field_author_reference
                      ? edge.node.relationships.field_author_reference.title +
                        edge.node.relationships.field_author_reference
                          .field_last_name
                      : "",
                    url: $url,
                    guid: edge.node.drupal_id,
                    custom_elements: [{ category: $categories }],
                  }
                )
              })
            },
            query: `
              {
                allNodeNews(
                  sort: { field_date: DESC },
                  filter: {status: {eq: true}, field_shibboleth_access: {ne: true}},
                ) {
                  edges {
                    node {
                      title
                      field_date
                      drupal_id
                      path {
                        alias
                      }
                      field_alternate_link {
                        uri
                      }
                      body {
                        summary
                        value
                      }
                      relationships {
                        field_author_reference {
                          title
                          field_last_name
                        }
                        field_categories {
                          name
                        }                        
                      }
                    }
                  }
                }
              }
            `,
            output: "/news/rss.xml",
            title: "News RSS Feed",
            link: `${process.env.SITE_URL}/news/rss.xml`,
          },
          // Event Feed
          {
            serialize: ({ query: { site, allNodeEvent } }) => {
              var $description = ""
              var $url = ""
              var $categories = ""
              var $series = ""
              var $image = ""
              const today = new Date().toISOString()

              return allNodeEvent.edges
                .filter(
                  edgeEvent => edgeEvent.node?.field_event_date?.value >= today
                )
                .map(edge => {
                  $url =
                    edge.node.field_alternate_link &&
                    edge.node.field_alternate_link.uri
                      ? edge.node.field_alternate_link.uri
                      : site.siteMetadata.siteUrl + edge.node.path.alias

                  if (edge.node.body)
                    $description = edge.node.body.summary
                      ? edge.node.body.summary
                      : edge.node.body.value.substring(0, 300)

                  if (edge.node.relationships.field_categories) {
                    const $category_names =
                      edge.node.relationships.field_categories?.map(
                        category => {
                          return category.name
                        }
                      )
                    $categories = $category_names
                      ? $category_names.join("|")
                      : ""
                  }

                  if (edge.node.relationships.field_series) {
                    const $series_names =
                      edge.node.relationships.field_series?.map(series => {
                        return series.name
                      })
                    $series = $series_names ? $series_names.join("|") : ""
                  }

                  const featuredImagePath =
                    edge.node.relationships.field_featured_media?.relationships
                      ?.field_media_image?.image_style_uri?.focal_point_large ||
                    null
                  if (featuredImagePath) {
                    $image = featuredImagePath.replace(
                      `${process.env.DRUPAL_BASE_URL}`,
                      `${process.env.CDN_BASE_URL}`
                    )
                  } else if (edge.node.field_feed_image_url?.uri) {
                    $image = edge.node.field_feed_image_url?.uri
                  }

                  return Object.assign(
                    {},
                    {
                      title: edge.node.title,
                      description: $description,
                      url: $url,
                      guid: edge.node.drupal_id,
                      custom_elements: [
                        { category: $categories },
                        {
                          "start-timestamp":
                            edge.node.field_event_date?.value || "",
                        },
                        {
                          "end-timestamp":
                            edge.node.field_event_date?.end_value || "",
                        },
                        {
                          location: [
                            { address: edge.node.field_location_text || "" },
                            { link: edge.node.field_location_link?.uri || "" },
                          ],
                        },
                        {
                          contact: [
                            { name: edge.node.field_contact_name || "" },
                            { email: edge.node.field_contact_email || "" },
                            { phone: edge.node.field_contact_phone || "" },
                          ],
                        },
                        {
                          sponsors:
                            edge.node.relationships?.field_sponsors?.name || "",
                        },
                        { series: $series },
                        { image: encodeURI($image) },
                        { speaker: edge.node.field_presenter_speaker || "" },
                        {
                          status:
                            edge.node.relationships?.field_status?.name || "",
                        },
                      ],
                    }
                  )
                })
            },
            query: `
              {
                allNodeEvent(
                  sort: { field_event_date: {value: ASC} },
                  filter: {status: {eq: true}},
                ) {
                  edges {
                    node {
                      title
                      field_event_date {
                        value
                        end_value
                      }
                      field_location_text
                      field_location_link {
                        uri
                      }
                      field_presenter_speaker
                      field_contact_email
                      field_contact_name
                      field_contact_phone
                      drupal_id
                      path {
                        alias
                      }
                      field_alternate_link {
                        uri
                      }
                      body {
                        summary
                        value
                      }
                      field_feed_image_url {
                        uri
                      }                      
                      relationships {
                        field_categories {
                          name
                        }
                        field_status {
                          name
                        }
                        field_series {
                          name
                        }
                        field_sponsors {
                          name
                        }                                                       
                        field_featured_media {
                          relationships {
                            field_media_image {
                              image_style_uri {
                                focal_point_large
                              }
                            }
                          }
                        }                                 
                      }
                    }
                  }
                }
              }
            `,
            output: "/events/index.xml",
            title: "Events XML Feed",
            link: `${process.env.SITE_URL}/events/index.xml`,
          },
          // Blog Feed
          {
            serialize: ({ query: { site, allNodeBlogPost } }) => {
              let $description = ""
              let $url = ""
              let $categories = ""
              let $image = ""
              let $authors = ""

              return allNodeBlogPost.edges.map(edge => {
                $url = site.siteMetadata.siteUrl + edge.node.path.alias

                if (edge.node.body) {
                  $description = edge.node.body.summary
                    ? edge.node.body.summary
                    : edge.node.body.processed?.substring(0, 300)
                }

                if (edge.node.relationships.field_categories) {
                  const $category_names =
                    edge.node.relationships.field_categories?.map(category => {
                      return category.name
                    })
                  $categories = $category_names ? $category_names.join("|") : ""
                }

                const featuredImagePath =
                  edge.node.relationships.field_featured_media?.relationships
                    ?.field_media_image?.image_style_uri?.focal_point_large ||
                  null
                if (featuredImagePath) {
                  $image = featuredImagePath.replace(
                    `${process.env.DRUPAL_BASE_URL}`,
                    `${process.env.CDN_BASE_URL}`
                  )
                }

                if (edge.node.relationships.field_author_reference) {
                  const $author_names =
                    edge.node.relationships.field_author_reference?.map(
                      author => {
                        return author.title
                      }
                    )
                  $authors = $author_names ? $author_names.join("|") : ""
                }

                return Object.assign(
                  {},
                  {
                    title: edge.node.title,
                    description: $description,
                    date: edge.node.field_date,
                    url: $url,
                    guid: edge.node.drupal_id,
                    custom_elements: [
                      { category: $categories },
                      { image: encodeURI($image) },
                      { author: $authors },
                    ],
                  }
                )
              })
            },
            query: `
              {
                allNodeBlogPost(
                  sort: { field_date: DESC },
                  filter: {status: {eq: true}, field_shibboleth_access: {ne: true}},
                ) {
                  edges {
                    node {
                      title
                      body {
                        summary
                        processed
                      }
                      field_date
                      drupal_id
                      path {
                        alias
                      }
                      relationships {
                        field_author_reference {
                          title
                          field_last_name
                        }
                        field_categories {
                          name
                        }             
                        field_featured_media {
                          relationships {
                            field_media_image {
                              image_style_uri {
                                focal_point_large
                              }
                            }
                          }
                        }   
                      }
                    }
                  }
                }
              }
            `,
            output: "/blog/rss.xml",
            title: "Blog RSS Feed",
            link: `${process.env.SITE_URL}/blog/rss.xml`,
          },
        ],
      },
    },
    {
      resolve: "gatsby-plugin-htaccess",
      options: {
        https: true,
        www: false,
        SymLinksIfOwnerMatch: true,
        ErrorDocument: `
          ErrorDocument 401 /404.html
          ErrorDocument 404 /404.html
          ErrorDocument 404 /404.html
          ErrorDocument 500 /404.html
        `,
        // This has been disabled, the search engine denial is now handled in the apache config
        // If SITE_URL contains .static - lets hide it from search engines. This is updated in Vault when the site launches (to reflect a prod URL)
        // custom: `${
        //   process.env.SITE_URL?.includes("-static.oit.duke.edu")
        //     ? `Header set X-Robots-Tag "noindex, nofollow"`
        //     : ``
        // }`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}

globalConfig.plugins = [
  ...globalConfig.plugins,
  ...(process.env.SITE_GTM_ID
    ? [
        {
          resolve: "gatsby-plugin-google-tagmanager",
          options: {
            id: process.env.SITE_GTM_ID,
            includeInDevelopment: false,
            // Defaults to null
            defaultDataLayer: { platform: "gatsby" },
          },
        },
      ]
    : []),
  ...(process.env.SITE_GA_ID
    ? [
        {
          resolve: `gatsby-plugin-google-gtag`,
          options: {
            // You can add multiple tracking ids and a pageview event will be fired for all of them.
            trackingIds: process.env.SITE_GA_ID.split(","), // Google Analytics / GA
            // This object is used for configuration specific to this plugin
            pluginConfig: {
              // Puts tracking script in the head instead of the body
              head: false,
              // Setting this parameter is also optional
              respectDNT: true,
              // Avoids sending pageview hits from custom paths
              exclude: ["/preview/**", "/do-not-track/me/too/"],
            },
          },
        },
      ]
    : []),
]

module.exports = globalConfig
