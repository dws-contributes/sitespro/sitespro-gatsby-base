/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import React from "react"
import { SiteContextProvider } from "components/_utils/SiteContext"
import theme from "theme"
import SSRProvider from "react-bootstrap/SSRProvider"

export const wrapRootElement = ({ element }) => (
  <SSRProvider>
    <SiteContextProvider>{element}</SiteContextProvider>
  </SSRProvider>
)

export const onPreRenderHTML = ({
  getHeadComponents,
  replaceHeadComponents,
}) => {
  const headComponents = getHeadComponents()
  headComponents.sort((x, y) => {
    if (x.key === "googleFonts") {
      return -1
    } else if (y.key === "googleFonts") {
      return 1
    }
    return 0
  })
  replaceHeadComponents(headComponents)
}

const BodyAttributes = {
  "color-theme": theme.colorTheme,
}

export const onRenderBody = ({ setHtmlAttributes, setBodyAttributes }) => {
  setHtmlAttributes({ lang: "en", prefix: "og: https://ogp.me/ns#" })
  setBodyAttributes(BodyAttributes)
}
